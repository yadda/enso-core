<meta property="og:type" content="website" />

@php
  $meta = EnsoMeta::get();
@endphp

@if (isset($meta) && !is_null($meta))
  <title>{{ $meta->getMetaTitle() }}</title>
  @if (!empty($meta->getMetaKeywords()))
    <meta name="keywords" content="{{ $meta->getMetaKeywords() }}">
  @endif
  @if (!empty($meta->getMetaDescription()))
    <meta name="description" content="{{ $meta->getMetaDescription() }}">
  @endif

  {{-- Facebook / OG --}}
  @if (!empty($meta->getFacebookTitle()))
    <meta name="og:title" property="og:title" content="{{ $meta->getFacebookTitle() }}">
  @endif
  @if (!empty($meta->getFacebookDescription()))
    <meta property="og:description" content="{{ $meta->getFacebookDescription() }}">
  @endif

  @if ($meta->hasFacebookImage(true))
    <meta property="og:image" content="{{ $meta->getFacebookImageUrl() }}">
    <meta property="og:image:width" content="{{ $meta->getFacebookImageWidth() }}">
    <meta property="og:image:height" content="{{ $meta->getFacebookImageHeight() }}">
  @endif
  <meta property="og:url" content="{{ $meta->getCanonicalUrl() }}">
  <meta property="og:locale" content="{{ config('app.locale') }}">
  <meta property="og:site_name" content="{{ config('app.name') }}">

  {{-- Twitter --}}
  <meta name="twitter:card" content="{{ $meta->getTwitterCardType() }}">
  @if (!empty($meta->getTwitterSite()))
    <meta name="twitter:site" content="{{ $meta->getTwitterSite() }}">
  @endif
  @if (!empty($meta->getTwitterCreator()))
    <meta name="twitter:creator" content="{{ $meta->getTwitterCreator() }}">
  @endif
  @if (!empty($meta->getTwitterTitle()))
    <meta name="twitter:title" content="{{ $meta->getTwitterTitle() }}">
  @endif
  @if (!empty($meta->getTwitterDescription()))
    <meta name="twitter:description" content="{{ $meta->getTwitterDescription() }}">
  @endif
  @if ($meta->hasTwitterImage(true))
    <meta name="twitter:image" content="{{ $meta->getTwitterImageUrl() }}">
  @endif
@else
  {{-- No Meta Available --}}
  <title>{{ config('app.name') }}</title>
  <meta property="og:title" content="{{ config('app.name') }}">
@endif
