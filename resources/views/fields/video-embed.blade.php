@if (!empty($video))
  @if (View::exists('crud.video.' . $video['type']))
    @include('crud.video.' . $video['type'])
  @elseif (View::exists('enso-fields::video.' . $video['type']))
    @include('enso-fields::video.' . $video['type'])
  @else
    <p style="color:red;">No template exists for video type {{ $video['type'] }}</p>
  @endif
@endif
