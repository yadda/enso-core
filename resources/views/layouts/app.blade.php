<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>{{ $page_title ?? 'Admin' }}</title>
        <script src="https://use.fontawesome.com/c230120eaa.js"></script>
        <link href="https://cdn.quilljs.com/1.2.6/quill.snow.css" rel="stylesheet">
        <link href="https://cdn.quilljs.com/1.2.6/quill.bubble.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.1/leaflet.css" />
        <link href="https://fonts.googleapis.com/css?family=Miriam+Libre:400,700" rel="stylesheet">
        <link rel="stylesheet" href="{{ mix('css/enso-admin.css', config('enso.settings.manifest_directory', 'enso')) }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body class="{{ isset($body_classes) ? $body_classes->implode(' ') : '' }}">
        <div id="app" class="enso-app-wrapper is-flex-tablet">
            @include('enso::layouts.partials.navbar')
            <div class="enso-main-col">
                <section class="section">
                    @if (config('enso.admin.show_not_production_warning', true))
                        @if (config('app.env') !== 'production')
                            <article class="message is-warning">
                                <div class="message-header">
                                    <p>Warning!</p>
                                </div>
                                <div class="message-body">
                                    <p>
                                        This website is not in production and is intended for development and testing purposes only.
                                    </p>
                                    <p>
                                        Changes made here may not be reflected on the live site!
                                    </p>
                                </div>
                            </article>
                        @endif
                        @if (config('app.env') === 'production' && config('app.debug'))
                            <article class="message is-danger">
                                <div class="message-header">
                                    <p>Danger!</p>
                                </div>
                                <div class="message-body">
                                    <p>
                                        This website is in production and debug mode is enabled! Please <a href="mailto:development@maya.agency?subject=Debug+mode+in+production!">let a developer know</a> as soon as possible.
                                    </p>
                                </div>
                            </article>
                        @endif
                    @endif
                    @yield('content')
                </section>
            </div>
            <portal-target name="modals"></portal-target>
        </div>
        <script type="text/javascript">
            var enso = {!! json_encode(Enso::getJSData()) !!};
        </script>
        @yield('footer-scripts')
        <script type="text/javascript" src="{{ mix('js/enso-admin.js', config('enso.settings.manifest_directory', 'enso')) }}"></script>
    </body>
</html>
