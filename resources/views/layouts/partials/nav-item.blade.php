<li>
    <a href="{{ $item->getUrl() }}#1" class="{{ $item->isActive() ? 'is-active' : '' }}">
        <span class="icon is-small">
            @if ($item->getIcon())
                <i class="{{ $item->getIcon() }}"></i>
            @endif
        </span>
        {{ $item->getLabel() }}
    </a>
    @if ($item->hasItems())
        <ul class="menu-list depth-{{ $item->getDepth() }}">
            @foreach($item->getItems() as $child_item)
                @include('enso::layouts.partials.nav-item', ['item' => $child_item])
            @endforeach
        </ul>
    @endif
</li>
