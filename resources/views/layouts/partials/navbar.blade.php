<aside class="menu enso-menu">
    <a href="{{ url('admin') }}" class="enso-menu-logo">
        <img class ="enso-admin-logo-main" src="{{ asset(config('enso.admin.menu.logo')) }}" alt="">
    </a>
    <ul class="menu-list">
        @if(Auth::check())
          <li>
              <a href="{{ route('admin.users.edit', Auth::id()) }}">
                  <span class="icon is-small">
                      <i class="fa fa-user"></i>
                  </span>
                  {{ Auth::user()->getUsername() }}
              </a>
          </li>
        @endif
        <li>
            <a class="{{ Request::is(substr(parse_url(url('admin'), PHP_URL_PATH), 1)) ? 'is-active' : '' }}" href="{{ url('/admin') }}">
                <span class="icon is-small">
                    <i class="fa fa-home"></i>
                </span>
                Home
            </a>
        </li>
        @foreach ($menu_items as $item)
            @include('enso::layouts.partials.nav-item', ['item' => $item])
        @endforeach
        <li>
            <a
              href="{{ url('/logout') }}"
              onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
            >
                <span class="icon is-small">
                    <i class="fa fa-sign-out"></i>
                </span>
                Log out
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
</aside>
