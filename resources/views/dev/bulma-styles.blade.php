@extends('layouts.app')

@section('content')
  <section class="hero is-primary">
    <div class="hero-body">
      <div class="container">
        <h1 class="title">
          Bulma Styles Test Page
        </h1>
        <h2 class="subtitle">
          This is a hero
        </h2>
      </div>
    </div>
  </section>

  <div class="container">
    <section class="section">
      <aside class="menu">
        <p class="menu-label">Quick Menu</p>
        <ul class="menu-list">
          <li><a href="#wysiwyg-content">WYSIWYG Content</a></li>
          <li><a href="#buttons">Buttons</a></li>
          <li><a href="#notifications">Notifications</a></li>
          <li><a href="#titles">Titles</a></li>
          <li><a href="#grid">Grid</a></li>
          <li><a href="#forms">Forms</a></li>
        </ul>
      </aside>
    </section>

    <hr>

    <section id="wysiwyg-content" class="section">
      <h1 class="title is-1">Wysiwyg content</h1>

      <div class="content">
        <h1>This is an H1</h1>
        <h2>This is an H2</h2>
        <h3>This is an H3</h3>
        <h4>This is an H4</h4>
        <h5>This is an H5</h5>
        <h6>This is an H6</h6>
        <p>
          Here is a paragraph of regular text.
          Here is a paragraph of regular text.
          <em>This is bit is in bold.</em>
          Here is a paragraph of regular text.
          Here is a paragraph of regular text.
          Here is a paragraph of regular text.
          <strong>This bit is strong.</strong>
          Here is a paragraph of regular text.
          Here is a paragraph of regular text.
          Here is a paragraph of regular text.
          Here is a paragraph of regular text.
        </p>
        <ul>
          <li>Here is an unordered list.</li>
          <li>Here is an unordered list.</li>
          <li>Here is an unordered list.</li>
          <li>Here is an unordered list.</li>
        </ul>
        <ol>
          <li>Here is an ordered list.</li>
          <li>Here is an ordered list.</li>
          <li>Here is an ordered list.</li>
          <li>Here is an ordered list.</li>
          <li>Here is an ordered list.</li>
        </ol>
        <dl>
          <dt>This is a definition list</dt>
          <dd>And here is a definition</dd>
          <dt>This is a definition list</dt>
          <dd>And here is a definition</dd>
          <dt>This is a definition list</dt>
          <dd>And here is a definition</dd>
          <dt>This is a definition list</dt>
          <dd>And here is a definition</dd>
        </dl>
        <blockquote>
          Here is a block quote. Here is a block quote. Here is a block quote.
        </blockquote>
        <pre>&lt;!DOCTYPE html&gt;
          &lt;html&gt;
            &lt;head&gt;
              &lt;title&gt;Hello World&lt;/title&gt;
            &lt;/head&gt;
            &lt;body&gt;
              &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec viverra nec nulla vitae mollis.&lt;/p&gt;
            &lt;/body&gt;
          &lt;/html&gt;
        </pre>
        <table>
          <thead>
            <tr>
              <th>Here is a</th>
              <th>table</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>it</td>
              <td>has</td>
            </tr>
            <tr>
              <td>stuff</td>
              <td>in it</td>
            </tr>
          </tbody>
        </table>
      </div>
    </section>

    <h1 id="buttons" class="title is-1">Buttons</h1>
    <section class="section">
      <h2 class="title is-3">Grayscale</h2>
      <a class="button is-white">White</a>
      <a class="button is-light">Light</a>
      <a class="button is-dark">Dark</a>
      <a class="button is-black">Black</a>
      <a class="button is-text">Text</a>
    </section>
    <section class="section">
      <h2 class="title is-3">Coloured</h2>
      <a class="button is-primary">Primary</a>
      <a class="button is-link">Link</a>
      <a class="button is-info">Info</a>
      <a class="button is-success">Success</a>
      <a class="button is-warning">Warning</a>
      <a class="button is-danger">Danger</a>
    </section>
    <section class="section">
      <h2 class="title is-3">Sizes</h2>
      <a class="button is-small">Small</a>
      <a class="button">Normal</a>
      <a class="button is-medium">Medium</a>
      <a class="button is-large">Large</a>
    </section>
    <section class="section">
      <h2 class="title is-3">Outlined</h2>
      <a class="button is-outlined">Outlined</a>
      <a class="button is-primary is-outlined">Outlined</a>
      <a class="button is-link is-outlined">Outlined</a>
      <a class="button is-info is-outlined">Outlined</a>
      <a class="button is-success is-outlined">Outlined</a>
      <a class="button is-danger is-outlined">Outlined</a>
    </section>

    <hr>

    <section id="notifications" class="section">
        <h1 class="title is-1">Notifications</h1>

        <div class="notification is-primary">
          <button class="delete"></button>
          Primar lorem ipsum dolor sit amet, consectetur
          adipiscing elit lorem ipsum dolor. <strong>Pellentesque risus mi</strong>. Nullam gravida purus diam, et dictum <a>felis venenatis</a> efficitur. Sit amet,
          consectetur adipiscing elit
        </div>

        <div class="notification is-link">
          <button class="delete"></button>
          Primar lorem ipsum dolor sit amet, consectetur
          adipiscing elit lorem ipsum dolor. <strong>Pellentesque risus mi</strong>. Nullam gravida purus diam, et dictum <a>felis venenatis</a> efficitur. Sit amet,
          consectetur adipiscing elit
        </div>

        <div class="notification is-info">
          <button class="delete"></button>
          Primar lorem ipsum dolor sit amet, consectetur
          adipiscing elit lorem ipsum dolor. <strong>Pellentesque risus mi</strong>. Nullam gravida purus diam, et dictum <a>felis venenatis</a> efficitur. Sit amet,
          consectetur adipiscing elit
        </div>

        <div class="notification is-success">
          <button class="delete"></button>
          Primar lorem ipsum dolor sit amet, consectetur
          adipiscing elit lorem ipsum dolor. <strong>Pellentesque risus mi</strong>. Nullam gravida purus diam, et dictum <a>felis venenatis</a> efficitur. Sit amet,
          consectetur adipiscing elit
        </div>

        <div class="notification is-warning">
          <button class="delete"></button>
          Primar lorem ipsum dolor sit amet, consectetur
          adipiscing elit lorem ipsum dolor. <strong>Pellentesque risus mi</strong>. Nullam gravida purus diam, et dictum <a>felis venenatis</a> efficitur. Sit amet,
          consectetur adipiscing elit
        </div>

        <div class="notification is-danger">
          <button class="delete"></button>
          Primar lorem ipsum dolor sit amet, consectetur
          adipiscing elit lorem ipsum dolor. <strong>Pellentesque risus mi</strong>. Nullam gravida purus diam, et dictum <a>felis venenatis</a> efficitur. Sit amet,
          consectetur adipiscing elit
        </div>
    </section>

    <hr>

    <section id="titles" class="section">
      <h1 class="title is-1">Titles</h1>
      <p>
        <h1 class="title is-1">Title 1</h1>
        <h2 class="title is-2">Title 2</h2>
        <h3 class="title is-3">Title 3</h3>
        <h4 class="title is-4">Title 4</h4>
        <h5 class="title is-5">Title 5</h5>
        <h6 class="title is-6">Title 6</h6>
      </p>
      <p>
        <h1 class="subtitle is-1">Subtitle 1</h1>
        <h2 class="subtitle is-2">Subtitle 2</h2>
        <h3 class="subtitle is-3">Subtitle 3</h3>
        <h4 class="subtitle is-4">Subtitle 4</h4>
        <h5 class="subtitle is-5">Subtitle 5</h5>
        <h6 class="subtitle is-6">Subtitle 6</h6>
      </p>
      <p>
        <p class="title is-1">Title 1</p>
        <p class="subtitle is-3">Subtitle 3</p>

        <p class="title is-2">Title 2</p>
        <p class="subtitle is-4">Subtitle 4</p>

        <p class="title is-3">Title 3</p>
        <p class="subtitle is-5">Subtitle 5</p>
      </p>
    </section>

    <section class="section" id="grid">
      <div class="columns is-multiline">
        <div class="column is-half"><div class="notification is-primary">Half</div></div>
        <div class="column is-half"><div class="notification is-primary">Half</div></div>
        <div class="column is-one-quarter"><div class="notification is-info">Quarter</div></div>
        <div class="column is-one-quarter"><div class="notification is-info">Quarter</div></div>
        <div class="column is-one-quarter"><div class="notification is-info">Quarter</div></div>
        <div class="column is-one-quarter"><div class="notification is-info">Quarter</div></div>
        <div class="column is-one-third"><div class="notification is-success">Third</div></div>
        <div class="column is-one-third"><div class="notification is-success">Third</div></div>
        <div class="column is-one-third"><div class="notification is-success">Third</div></div>
        <div class="column is-2"><div class="notification is-warning">Sixth</div></div>
        <div class="column is-2"><div class="notification is-warning">Sixth</div></div>
        <div class="column is-2"><div class="notification is-warning">Sixth</div></div>
        <div class="column is-2"><div class="notification is-warning">Sixth</div></div>
        <div class="column is-2"><div class="notification is-warning">Sixth</div></div>
        <div class="column is-2"><div class="notification is-warning">Sixth</div></div>
        <div class="column is-1"><div class="notification is-danger">1/12</div></div>
        <div class="column is-1"><div class="notification is-danger">1/12</div></div>
        <div class="column is-1"><div class="notification is-danger">1/12</div></div>
        <div class="column is-1"><div class="notification is-danger">1/12</div></div>
        <div class="column is-1"><div class="notification is-danger">1/12</div></div>
        <div class="column is-1"><div class="notification is-danger">1/12</div></div>
        <div class="column is-1"><div class="notification is-danger">1/12</div></div>
        <div class="column is-1"><div class="notification is-danger">1/12</div></div>
        <div class="column is-1"><div class="notification is-danger">1/12</div></div>
        <div class="column is-1"><div class="notification is-danger">1/12</div></div>
        <div class="column is-1"><div class="notification is-danger">1/12</div></div>
        <div class="column is-1"><div class="notification is-danger">1/12</div></div>
      </div>
    </section>

    <section class="section" id="forms">
      <h1 class="title is-1">Forms</h1>
      <div class="field">
        <label class="label">Name</label>
        <div class="control">
          <input class="input" type="text" placeholder="Text input">
        </div>
      </div>

      <div class="field">
        <label class="label">Username</label>
        <div class="control">
          <input class="input is-success" type="text" placeholder="Text input" value="bulma">
        </div>
        <p class="help is-success">This username is available</p>
      </div>

      <div class="field">
        <label class="label">Email</label>
        <div class="control">
          <input class="input is-danger" type="email" placeholder="Email input" value="hello@">
        </div>
        <p class="help is-danger">This email is invalid</p>
      </div>

      <div class="field">
        <label class="label">Subject</label>
        <div class="control">
          <div class="select">
            <select>
              <option>Select dropdown</option>
              <option>With options</option>
            </select>
          </div>
        </div>
      </div>

      <div class="field">
        <label class="label">Message</label>
        <div class="control">
          <textarea class="textarea" placeholder="Textarea"></textarea>
        </div>
      </div>

      <div class="field">
        <div class="control">
          <label class="checkbox">
            <input type="checkbox">
            I agree to the <a href="#">terms and conditions</a>
          </label>
        </div>
      </div>

      <div class="field">
        <div class="control">
          <label class="radio">
            <input type="radio" name="question">
            Yes
          </label>
          <label class="radio">
            <input type="radio" name="question">
            No
          </label>
        </div>
      </div>

      <div class="field is-grouped">
        <div class="control">
          <button class="button is-link">Submit</button>
        </div>
        <div class="control">
          <button class="button is-text">Cancel</button>
        </div>
      </div>
    </section>
  </div>
@endsection