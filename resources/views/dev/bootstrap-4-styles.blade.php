@extends('layouts.app')

@section('content')
  <div class="jumbotron">
    <div class="container">
      <h1 class="display-4">Bootstrap Styles Test Page</h1>
      <p class="lead">This is a hero</p>
    </div>
  </div>

  <div class="container">
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link" href="#wysiwyg-content">WYSIWYG Content</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#buttons">Buttons</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#alerts">Alerts</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#titles">Titles</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#grid">Grid</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#buttons">Forms</a>
      </li>
    </ul>


    <section id="wysiwyg-content">
      <h1>Wysiwyg content</h1>

      <h1>This is an H1</h1>
      <h2>This is an H2</h2>
      <h3>This is an H3</h3>
      <h4>This is an H4</h4>
      <h5>This is an H5</h5>
      <h6>This is an H6</h6>
      <p>
        Here is a paragraph of regular text.
        Here is a paragraph of regular text.
        <em>This is bit is in bold.</em>
        Here is a paragraph of regular text.
        Here is a paragraph of regular text.
        Here is a paragraph of regular text.
        <strong>This bit is strong.</strong>
        Here is a paragraph of regular text.
        Here is a paragraph of regular text.
        Here is a paragraph of regular text.
        Here is a paragraph of regular text.
      </p>
      <ul>
        <li>Here is an unordered list.</li>
        <li>Here is an unordered list.</li>
        <li>Here is an unordered list.</li>
        <li>Here is an unordered list.</li>
      </ul>
      <ol>
        <li>Here is an ordered list.</li>
        <li>Here is an ordered list.</li>
        <li>Here is an ordered list.</li>
        <li>Here is an ordered list.</li>
        <li>Here is an ordered list.</li>
      </ol>
      <dl>
        <dt>This is a definition list</dt>
        <dd>And here is a definition</dd>
        <dt>This is a definition list</dt>
        <dd>And here is a definition</dd>
        <dt>This is a definition list</dt>
        <dd>And here is a definition</dd>
        <dt>This is a definition list</dt>
        <dd>And here is a definition</dd>
      </dl>
      <blockquote>
        Here is a block quote. Here is a block quote. Here is a block quote.
      </blockquote>
      <pre>&lt;!DOCTYPE html&gt;
        &lt;html&gt;
          &lt;head&gt;
            &lt;title&gt;Hello World&lt;/title&gt;
          &lt;/head&gt;
          &lt;body&gt;
            &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec viverra nec nulla vitae mollis.&lt;/p&gt;
          &lt;/body&gt;
        &lt;/html&gt;
      </pre>
    </section>

    <hr>

    <section id="buttons">
      <h1>Buttons</h1>
      <h2>Colours</h2>
      <p>
        <button type="button" class="btn btn-primary">Primary</button>
        <button type="button" class="btn btn-secondary">Secondary</button>
        <button type="button" class="btn btn-success">Success</button>
        <button type="button" class="btn btn-danger">Danger</button>
        <button type="button" class="btn btn-warning">Warning</button>
        <button type="button" class="btn btn-info">Info</button>
        <button type="button" class="btn btn-light">Light</button>
        <button type="button" class="btn btn-dark">Dark</button>

        <button type="button" class="btn btn-link">Link</button>
      </p>
      <h2>Outlined</h2>
      <p>
        <button type="button" class="btn btn-outline-primary">Primary</button>
        <button type="button" class="btn btn-outline-secondary">Secondary</button>
        <button type="button" class="btn btn-outline-success">Success</button>
        <button type="button" class="btn btn-outline-danger">Danger</button>
        <button type="button" class="btn btn-outline-warning">Warning</button>
        <button type="button" class="btn btn-outline-info">Info</button>
        <button type="button" class="btn btn-outline-light">Light</button>
        <button type="button" class="btn btn-outline-dark">Dark</button>
      </p>
      <h2>Sizes</h2>
      <p>
        <button type="button" class="btn btn-primary btn-lg">Large button</button>
        <button type="button" class="btn btn-secondary btn-lg">Large button</button>
        <button type="button" class="btn btn-primary btn-sm">Small button</button>
        <button type="button" class="btn btn-secondary btn-sm">Small button</button>
      </p>
    </section>

    <hr>

    <section id="alerts">
      <h1>Alerts</h1>
      <div class="alert alert-primary" role="alert">
        A simple primary alert—check it out!
      </div>
      <div class="alert alert-secondary" role="alert">
        A simple secondary alert—check it out!
      </div>
      <div class="alert alert-success" role="alert">
        A simple success alert—check it out!
      </div>
      <div class="alert alert-danger" role="alert">
        A simple danger alert—check it out!
      </div>
      <div class="alert alert-warning" role="alert">
        A simple warning alert—check it out!
      </div>
      <div class="alert alert-info" role="alert">
        A simple info alert—check it out!
      </div>
      <div class="alert alert-light" role="alert">
        A simple light alert—check it out!
      </div>
      <div class="alert alert-dark" role="alert">
        A simple dark alert—check it out!
      </div>
    </section>

    <hr>

    <section id="titles">
      <h1>Titles By Element name</h1>
      <h1>Title 1</h1>
      <h2>Title 2</h2>
      <h3>Title 3</h3>
      <h4>Title 4</h4>
      <h5>Title 5</h5>
      <h6>Title 6</h6>
      <h1>Titles By Class</h1>
      <p class="h1">Title 1</p>
      <p class="h2">Title 2</p>
      <p class="h3">Title 3</p>
      <p class="h4">Title 4</p>
      <p class="h5">Title 5</p>
      <p class="h6">Title 6</p>
    </section>

    <hr>

    <section id="grid">
      <div class="row">
        <div class="col-12 col-sm-6"><div class="alert alert-primary">Half</div></div>
        <div class="col-12 col-sm-6"><div class="alert alert-primary">Half</div></div>
        <div class="col-12 col-sm-3"><div class="alert alert-info">Quarter</div></div>
        <div class="col-12 col-sm-3"><div class="alert alert-info">Quarter</div></div>
        <div class="col-12 col-sm-3"><div class="alert alert-info">Quarter</div></div>
        <div class="col-12 col-sm-3"><div class="alert alert-info">Quarter</div></div>
        <div class="col-12 col-sm-4"><div class="alert alert-success">Third</div></div>
        <div class="col-12 col-sm-4"><div class="alert alert-success">Third</div></div>
        <div class="col-12 col-sm-4"><div class="alert alert-success">Third</div></div>
        <div class="col-12 col-sm-2"><div class="alert alert-warning">Sixth</div></div>
        <div class="col-12 col-sm-2"><div class="alert alert-warning">Sixth</div></div>
        <div class="col-12 col-sm-2"><div class="alert alert-warning">Sixth</div></div>
        <div class="col-12 col-sm-2"><div class="alert alert-warning">Sixth</div></div>
        <div class="col-12 col-sm-2"><div class="alert alert-warning">Sixth</div></div>
        <div class="col-12 col-sm-2"><div class="alert alert-warning">Sixth</div></div>
        <div class="col-12 col-sm-1"><div class="alert alert-danger">1/12</div></div>
        <div class="col-12 col-sm-1"><div class="alert alert-danger">1/12</div></div>
        <div class="col-12 col-sm-1"><div class="alert alert-danger">1/12</div></div>
        <div class="col-12 col-sm-1"><div class="alert alert-danger">1/12</div></div>
        <div class="col-12 col-sm-1"><div class="alert alert-danger">1/12</div></div>
        <div class="col-12 col-sm-1"><div class="alert alert-danger">1/12</div></div>
        <div class="col-12 col-sm-1"><div class="alert alert-danger">1/12</div></div>
        <div class="col-12 col-sm-1"><div class="alert alert-danger">1/12</div></div>
        <div class="col-12 col-sm-1"><div class="alert alert-danger">1/12</div></div>
        <div class="col-12 col-sm-1"><div class="alert alert-danger">1/12</div></div>
        <div class="col-12 col-sm-1"><div class="alert alert-danger">1/12</div></div>
        <div class="col-12 col-sm-1"><div class="alert alert-danger">1/12</div></div>
      </div>
    </section>

    <hr>

    <section id="forms">
      <form>
        <div class="form-group">
          <label for="exampleFormControlInput1">Email address</label>
          <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
        </div>
        <div class="form-group">
          <label for="exampleFormControlSelect1">Example select</label>
          <select class="form-control" id="exampleFormControlSelect1">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
          </select>
        </div>
        <div class="form-group">
          <label for="exampleFormControlSelect2">Example multiple select</label>
          <select multiple class="form-control" id="exampleFormControlSelect2">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
          </select>
        </div>
        <div class="form-group">
          <label for="exampleFormControlTextarea1">Example textarea</label>
          <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
      </form>
      <div class="form-check">
        <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
        <label class="form-check-label" for="defaultCheck1">
          Default checkbox
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
        <label class="form-check-label" for="exampleRadios1">
          Default radio
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
        <label class="form-check-label" for="exampleRadios2">
          Second default radio
        </label>
      </div>
    </section>
  </div>
@endsection