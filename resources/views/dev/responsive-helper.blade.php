<div class="fixed left-0 bottom-0 bg-white p-2 z-50 font-normal">
  <span class="sm:hidden">Tiny</span>
  <span class="hidden sm:block md:hidden">SM</span>
  <span class="hidden md:block lg:hidden">MD</span>
  <span class="hidden lg:block xl:hidden">LG</span>
  <span class="hidden xl:block 2xl:hidden">XL</span>
  <span class="hidden 2xl:block">2XL</span>
</div>
