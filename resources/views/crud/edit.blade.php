@extends(config('enso.settings.layout'))

@section('content')
    <div class="container is-fluid">
        <header class="is-clearfix">
            <h1 class="title is-pulled-left">Editing {{ $crud->getNameSingular() }}</h1>

            @include($crud->getCrudView('lists.edit-actions'))
        </header>

        @include($crud->getCrudView('lists.edit-head'))

        @include('enso-crud::partials.alerts')

        @include($form_view, [
            'item'       => $item,
            'submitText' => 'Save',
            'csrf_token' => csrf_token(),
            'action'     => $action,
            'method'     => 'PATCH',
        ])
    </div>
@endsection
