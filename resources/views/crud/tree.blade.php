@extends(config('enso.settings.layout'))

@section('content')
    <div class="container is-fluid">
        <header class="is-clearfix">
          <h1 class="title is-pulled-left">{{ $crud->getNamePlural() }}</h1>

          <a href="{{ route($crud->getRoute() . '.index') }}" class="button is-pulled-right">Back</a>
        </header>

        <hr>

        <enso-crud-tree></enso-crud-tree>
    </div>
@endsection
