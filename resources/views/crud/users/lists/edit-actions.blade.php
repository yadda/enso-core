<div class="is-pulled-right">
    @if(!Auth::user()->is($item))
      <a href="{{ route('admin.users.login-as-user', $item->getKey()) }}" class="button is-info">Login as</a>
    @endif
    @include($crud->getCrudView('lists.edit-defaults'))
</div>
