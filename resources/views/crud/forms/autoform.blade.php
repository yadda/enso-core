<?php $form->setErrors($errors); ?>

@foreach($form->getSections() as $section)

    @foreach($section->getFields() as $field)

        {!! $field->render() !!}

    @endforeach

@endforeach
