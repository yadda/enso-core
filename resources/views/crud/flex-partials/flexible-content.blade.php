<div class="{{ $container_class }}">

    @foreach($flexible_field->getRows() as $row_index => $row)

        {{--
            Loop through each row, displaying either an override template or the
            default row data.
        --}}
        @if(View::exists("enso-crud::flex-partials.rows.{$row->getType()}"))

            @include("enso-crud::flex-partials.rows.{$row->getType()}")

        @else

            @include("enso-crud::flex-partials.default-row")

        @endif

    @endforeach

</div>
