<div class="{{ $row->getClasses() }}" id="{{ $id_prefix . '-' . $row_index }}">
    {{--
        Loop through the Blocks in the row, displaying them based on
        the type (which is derived from the Enso field component name)
    --}}
    @foreach($row->getBlocks() as $block)

        {{-- By Default, don't show blocks with no content --}}
        @if($block->notEmpty())

            @if(View::exists("enso-crud::flex-partials.blocks.{$row->getType()}.{$block->getType()}"))

                {{-- Allow for Overrides for Specific Row-Block combinations --}}
                @include("enso-crud::flex-partials.blocks.{$row->getType()}.{$block->getType()}")

            @else

                {{-- Fall back to specific block representations by type --}}
                @include("enso-crud::flex-partials.default-block")

            @endif
        @endif

    @endforeach
</div>
