@php
  $row_data = Yadda\Enso\Crud\Forms\Rows\Quote::unpack($row);
@endphp

<figure data-label="{{ $row_data->row_label }}" id="{{ $row_data->row_id }}" class="my-10 md:my-12 px-5 x-auto max-w-screen-sm text-2xl">
  @component('enso-crud::flex-partials.components.title', [
    'class' => 'mb-5 mt-10 md:mt-0'
  ])
    {{ $row_data->title }}
  @endcomponent
  <blockquote>
    {!! $row_data->text !!}
  </blockquote>
  <figcaption class="font-bold">
    {{ $row_data->author }}
  </figcaption>
</figure>
