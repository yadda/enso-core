@php
  $row_data = Yadda\Enso\Crud\Forms\Rows\Embed::unpack($row);

  switch ($row_data->size) {
    case 'small':
      $class = 'sm:max-w-sm';
      break;
    case 'large':
      $class = 'sm:max-w-lg';
      break;
    case 'medium':
    default:
      $class = 'sm:max-w-md';
  }
@endphp

<div class="px-5 my-10 mx-auto {{ $class }}">
  @component('enso-crud::flex-partials.components.title', [
    'class' => 'mb-5',
  ])
    {{ $row_data->title }}
  @endcomponent
  {!! $row_data->content !!}
</div>
