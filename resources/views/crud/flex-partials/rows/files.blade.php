@php
  $row_data = Yadda\Enso\Crud\Forms\Rows\Files::unpack($row);
@endphp

<div data-label="{{ $row_data->row_label }}" id="{{ $row_data->row_id }}" class="md:max-w-2xl md:max-w-screen-md lg:max-w-screen-lg mx-auto my-10 md:my-12 py-5">
  @component('enso-crud::flex-partials.components.title', [
    'class' => '',
  ])
    {{ $row_data->title }}
  @endcomponent
  <div class="flex flex-wrap mx-3 overflow-hidden">
    @foreach ($row_data->files as $file)
      <a href="{{ $file->getUrl() }}" class="my-3 px-3 overflow-hidden w-1/2 sm:w-1/3 md:w-1/4 lg:w-1/5" download>
        <div class="bg-gray-200 p-5">
          <img src="{{ asset('svg/file-download.svg') }}" alt="Download file" class="w-full h-auto">
        </div>
      </a>
    @endforeach
  </div>
</div>
