@php
    $row_data = \Yadda\Enso\Crud\Forms\Sections\ButtonsSection::unpack($row);
@endphp
@if ($row_data->buttons->isNotEmpty())
  <section class="md:max-w-2xl px-5 my-10 text-center" id="{{ $row_data->id }}">
    @if($row_data->title)
      @include('enso-crud::flex-partials.parts.title', [
        'title' => $row_data->title,
      ])
    @endif
    @include('enso-crud::flex-partials.blocks.buttons', [
      'button_group' => 'buttons is-centered',
    ])
  </section>
@endif
