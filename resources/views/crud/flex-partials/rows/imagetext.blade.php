@php
  $row_data = \Yadda\Enso\Crud\Forms\Sections\ImageTextSection::unpack($row);
  $id = $row_data->id ? $row_data->id : $id_prefix . '-' . $row_index;
@endphp

<section class="section" id="{{ $id }}">
  <div class="container">
    <div class="columns" style="{{ $row_data->alignment === 'right' ? '' : 'flex-direction:row-reverse' }}">
      <div class="column is-half">
        @if($row_data->image)
          <figure class="image is-square">
            <img src="{{ $row_data->image->getResizeUrl('1_1_640', true) }}" alt="{{ $row_data->image->alt_text }}">
          </figure>
        @endif
      </div>
      <div class="column is-half">
        <div class="content">
          @if (!empty($row_data->title))
            <h2>{{ $row_data->title }}</h2>
          @endif
          @if (!empty($row_data->section_title))
            <h3>{{ $row_data->section_title }}</h3>
          @endif
          {!! $row_data->content !!}
        </div>

        @include('enso-crud::flex-partials.blocks.buttons')
      </div>
    </div>
  </div>
</section>
