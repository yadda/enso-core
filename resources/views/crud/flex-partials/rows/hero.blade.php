@php
  $row_data = \Yadda\Enso\Crud\Forms\Rows\Hero::unpack($row);
  $hero_image = $row_data->images->isNotEmpty() ? $row_data->images->first() : null;
@endphp

<div data-label="{{ $row_data->row_label }}" id="{{  $row_data->row_id }}" class="relative bg-gray-300 w-full flex items-center justify-center py-48">
  @if ($row_data->video)
    <video class="full-bg-cover hidden md:block" {{ $hero_image ? 'poster="' . $hero_image->getResizeUrl('hero', true) .  '"' : '' }} autoplay muted loop>
      <source src="{{ $row_data->video->getUrl() }}" type="{{  $row_data->video->mimetype }}">
    </video>
  @endif

  @if($hero_image)
    <picture>
      <source
        srcset="{{ $hero_image->getResizeUrl('hero_mobile', true) }} 1x,
          {{ $hero_image->getResizeUrl('hero_mobile_2x', true) }} 2x"
        sizes="100vw"
        media="(max-width: 1024px)">
      <source
        srcset="{{ $hero_image->getResizeUrl('hero', true) }} 1024w,
              {{ $hero_image->getResizeUrl('hero_lg', true) }}  1920w,
              {{ $hero_image->getResizeUrl('hero_lg_2x', true) }} 3840w"
        sizes="100vw"
        media="(min-width: 1024px)">
      <img
        src="{{ $hero_image->getResizeUrl('hero', true) }}"
        alt="{{ $hero_image->alt_text }}"
        class="full-bg-cover {{ $row_data->video ? 'md:hidden' : '' }}">
    </picture>
  @endif

  <div class="text-center sm:max-w-xs md:max-w-2xl mx-10 z-10">
    @if ($row_data->title)
      <h2 class="text-4xl font-bold mb-3">
        {{ $row_data->title }}
      </h2>
    @endif

    @if ($row_data->content)
      {!!  $row_data->content !!}
    @endif

    @include('enso-crud::flex-partials.blocks.buttons', [
      'button_group' => 'mt-8',
    ])
  </div>
</div>
