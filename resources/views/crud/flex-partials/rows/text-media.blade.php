@php
  $row_data = Yadda\Enso\Crud\Forms\Rows\TextMedia::unpack($row);
@endphp

<div data-label="{{ $row_data->row_label }}" id="{{ $row_data->row_id }}" class="my-10 md:my-12 mx-auto px-5 lg:max-w-screen-lg">
  <div class="md:flex {{ $row_data->alignment === 'left' ? 'flex-row-reverse' : '' }}">
    <div class="md:flex-half">
      @component('enso-crud::flex-partials.components.media', [
        'video' => $row_data->video,
        'external_video' => $row_data->external_video,
        'images' => $row_data->images,
      ])
      @endcomponent
    </div>
    <div class="md:flex-half md:px-5">
      @component('enso-crud::flex-partials.components.title', [
        'class' => 'mb-5 mt-10 md:mt-0'
      ])
        {{ $row_data->title }}
      @endcomponent
      @component('enso-crud::flex-partials.components.content')
        {!! $row_data->content !!}
      @endcomponent
      @component('enso-crud::flex-partials.components.buttons', [
        'wrapper_class' => 'mt-2',
        'buttons' => $row_data->buttons,
      ])
      @endcomponent
    </div>
  </div>
</div>
