@php
  $row_data = Yadda\Enso\Crud\Forms\Rows\Accordion::unpack($row);
@endphp

<pre>
  {{ var_dump(
    $row_data->row_label,
    $row_data->row_id,
    $row_data->title,
    $row_data->accordion_rows
  ) }}
</pre>
