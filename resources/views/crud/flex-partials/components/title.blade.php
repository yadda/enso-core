@if ($slot)
  <h2 class="text-4xl font-bold leading-none {{ !empty($class) ? $class : '' }}">
    {{ $slot }}
  </h2>
@endif
