@php
  if (empty($class)) {
    $class = '';
  }
@endphp

@if ($slot)
  <div class="{{ $class }}">
    {!! $slot !!}
  </div>
@endif
