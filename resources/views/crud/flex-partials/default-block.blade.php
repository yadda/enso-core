<div class="{{ $block->getClasses() }}">
    @include("enso-crud::flex-partials.blocks.{$block->getType()}")
</div>
