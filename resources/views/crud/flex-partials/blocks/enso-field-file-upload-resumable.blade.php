@php
    $file = $block->getContent()->first();
@endphp

@if ($file)
  @php
      $type = EnsoMedia::typeFromMime($file->mimetype);
  @endphp
  <div>
    @if ($type === 'image')
        <img src="{{ $file->getResizeUrl('uploader_preview', true) }}" alt="{{ $file->alt_text }}">
    @else
        <a href="{{ $file->getUrl() }}" class="button" download>Download</a>
    @endif
  </div>
@endif
