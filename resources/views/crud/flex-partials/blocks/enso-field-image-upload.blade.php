@php
    $preset = config("enso.media.presets.{$row->getType()}", null) ? $row->getType() : 'thumbnail';
@endphp

@foreach($block->getContent() as $image)
    <img src="{{ $image->getResizeUrl($preset, true) }}">
@endforeach
