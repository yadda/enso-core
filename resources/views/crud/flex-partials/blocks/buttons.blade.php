@if ($row_data->buttons->isNotEmpty())
  <div class="{{ $button_group ?? 'buttons' }}">
    @foreach ($row_data->buttons as $button)
      <a href="{{ $button->link }}" rel="noopener noreferrer" class="{{ $class ?? 'button' }}" title="{{ $button->hover }}">{{ $button->label }}</a>
    @endforeach
  </div>
@endif
