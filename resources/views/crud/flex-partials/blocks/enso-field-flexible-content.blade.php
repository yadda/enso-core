@php
    $parser = resolve(Yadda\Enso\Crud\Contracts\FlexibleFieldParser::class);
    $sub_field = (new Yadda\Enso\Crud\Handlers\FlexibleField($parser));
    $sub_field->loadData($block->getContent(), $block->getClasses());
@endphp

@include('enso-crud::flex-partials.flexible-content', [
    'flexible_field' => $sub_field,
    'item' => $item
])
