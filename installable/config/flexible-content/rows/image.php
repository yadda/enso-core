<?php

return [

    'default-style' => 'wide',

    'styles' => [
        'narrow' => 'Narrow',
        'wide' => 'Wide',
    ],

];
