<?php

return [

    'default-style' => 'normal',

    'modules' => [
        'toolbar' => [
            ['bold', 'italic', 'underline', 'strike', 'link'],
            [['list' => 'ordered'], ['list' => 'bullet']],
            ['clean'],
        ],
    ],

    'styles' => [
        'normal' => 'Normal',
        'feature' => 'Feature',
    ],

];
