<?php

return [

    'default-size' => 'medium',

    'sizes' => [
        'small' => 'Small',
        'medium' => 'Medium',
        'large' => 'Large',
    ],

];
