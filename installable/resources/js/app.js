import Vue from 'vue';
import EnsoCarousel from 'enso-carousel';
import MainMenu from './enso/MainMenu.js';

new Vue({
  el: '#app',
  components: {
    EnsoCarousel,
    MainMenu,
  },
});
