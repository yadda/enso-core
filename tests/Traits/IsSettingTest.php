<?php

namespace Yadda\Enso\Tests\Traits;

use Yadda\Enso\Settings\Contracts\Model;
use Yadda\Enso\Settings\Contracts\Repository;

trait IsSettingTest
{
    /**
     * Gets an instance of the SettingRepository
     * 
     * This is used as an argument to get and set data via a CollectionSection
     *
     * @return Settings
     */
    protected function freshRepository()
    {
        return resolve(Repository::class);
    }
    
    /**
     * Gets a setting, by name, from the database, based on the
     *
     * @param [type] $setting_name
     * 
     * @return void
     */
    protected function getSetting($setting_name)
    {
        $setting_class = get_class(resolve(Model::class));

        return $setting_class::where('slug', $setting_name)->first();
    }

    /**
     * Gets Form data based on the settings currently in use.
     *
     * @return array
     */
    protected function getSettingFormData()
    {
        if (!$this->section) {
            $this->fail('$this->section not set up before running Settings Test');
        }

        $setting_repository = $this->freshRepository();

        return $this->section->getFormData($setting_repository);
    }

    /**
     * Takes data (as if it had come from a request) and create/updates settings
     * through the CollectionSection
     *
     * @param array $setting_data
     * 
     * @return void
     */
    protected function applySettingData($setting_data)
    {
        if (!$this->section) {
            $this->fail('$this->section not set up before running Settings Test');
        }

        $setting_repository = $this->freshRepository();

        $this->section->applyRequestData($setting_repository, $setting_data);
    }

    /**
     * Tests that the value of the setting with the given name is the same
     * as the expected value.
     * 
     * In cases where the value is considered 'empty' with php's empty() call,
     * use strict type-checking.
     *
     * @param mixed $expected_value
     * @param string $setting_name
     * 
     * @return void
     */
    protected function assertSettingValueEquals($expected_value, $setting_name)
    {
        $setting = $this->getSetting($setting_name);
        
        $this->assertNotNull($setting);

        if (empty($expected_value)) {
            $this->assertSame($expected_value, $setting->decoded);
        } else {
            $this->assertEquals($expected_value, $setting->decoded);
        }
    }

    /**
     * Test that a setting with the given name exists in the database.
     *
     * @param string $setting_name
     * 
     * @return void
     */
    protected function assertSettingExists($setting_name)
    {
        $this->assertNotNull($this->getSetting($setting_name));
    }

    /**
     * Tests that a setting with the given name does not exist
     *
     * @param string $setting_name
     * 
     * @return void
     */
    protected function assertSettingDoesNotExist($setting_name)
    {
        $this->assertNull($this->getSetting($setting_name));
    }
}
