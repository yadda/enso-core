<?php

namespace Yadda\Enso\Tests\Concerns;

// use Spatie\Translatable\HasTranslations;
use Yadda\Enso\Tests\Concerns\ModelJson;

class ModelJsonTrans extends ModelJson
{
    // use HasTranslations;

    protected $table = "test_models_json";

    protected $guarded = [];

    protected $casts = [
        'content' => 'array',
    ];

    public $translatable = [
        'content',
    ];
}
