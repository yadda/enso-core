<?php 

namespace Yadda\Enso\Tests\Concerns;

use Illuminate\Database\Eloquent\Model as BaseModel;

class ModelJson extends BaseModel
{
    protected $table = "test_models_json";

    protected $guarded = [];

    protected $casts = [
        'content' => 'array',
    ];
}