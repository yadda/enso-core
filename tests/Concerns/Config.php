<?php

namespace Yadda\Enso\Tests\Concerns;

use Yadda\Enso\Crud\Config as BaseConfig;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Tests\Concerns\Model as FakeModel;

class Config extends BaseConfig
{
    public function configure()
    {
        $this->setModel(FakeModel::class)
            ->setRoute('admin.tests')
            ->setViewsDir('tests')
            ->setNameSingular('Test')
            ->setNamePlural('Tests')
            ->setTableColumns([]);
    }

    public function create(Form $form)
    {
        return $form;
    }
}