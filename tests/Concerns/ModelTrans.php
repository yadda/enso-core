<?php 

namespace Yadda\Enso\Tests\Concerns;

use Illuminate\Database\Eloquent\Model as BaseModel;

use Spatie\Translatable\HasTranslations;

class ModelTrans extends BaseModel
{
    use HasTranslations;

    protected $table = "test_models_trans";

    protected $guarded = [];

    protected $casts = [
        'text' => 'array',
        'slug' => 'array',
        'email' => 'array',
        'textarea' => 'array',
    ];

    protected $dates = [
        // 'date',
    ];

    public $translatable = [
        'text',
        'slug',
        'email',
        'textarea',
    ];
}