<?php

namespace Yadda\Enso\Tests\Concerns\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Yadda\Enso\Users\Models\User;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        static $password;

        return [
            'username' => $this->faker->name(),
            'display_name' => $this->faker->name(),
            'real_name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => $password ?: $password = '$2y$10$TwarNTTf50vtTkWTiRGL6ujUjv/1W..jLSoPwBvJZtesH16AHWqB6', // bcrypt('secret')
            'remember_token' => Str::random(10),
        ];
    }
}
