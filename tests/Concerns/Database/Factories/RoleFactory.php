<?php

namespace Yadda\Enso\Tests\Concerns\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Yadda\Enso\Users\Models\Role;

class RoleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Role::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'slug' => $this->faker->unique()->slug(),
            'description' => $this->faker->sentence(10),
            'parent_id' => null,
            'left_id' => 0,
            'right_id' => 0,
            'depth' => 0,
        ];
    }
}
