<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Yadda\Enso\Users\Contracts\Role;
use Yadda\Enso\Users\Contracts\User;
use Yadda\Enso\Users\Models\Permission;

if (isset($factory)) {
    /** @var \Illuminate\Database\Eloquent\Factory $factory */
    $factory->define(get_class(App::make(User::class)), function (Faker\Generator $faker) {
        static $password;

        return [
            'username' => $faker->name,
            'email' => $faker->unique()->safeEmail,
            'display_name' => $faker->name,
            'real_name' => $faker->name,
            'password' => $password ?: $password = '$2y$10$TwarNTTf50vtTkWTiRGL6ujUjv/1W..jLSoPwBvJZtesH16AHWqB6', // bcrypt('secret')
            'remember_token' => Str::random(10),
        ];
    });

    /** @var \Illuminate\Database\Eloquent\Factory $factory */
    $factory->define(get_class(App::make(Role::class)), function (Faker\Generator $faker) {
        return [
            'name' => $faker->name,
            'slug' => $faker->unique()->slug,
            'description' => $faker->sentence(10),
            'parent_id' => null,
            'left_id' => 0,
            'right_id' => 0,
            'depth' => 0,
        ];
    });

    /** @var \Illuminate\Database\Eloquent\Factory $factory */
    $factory->define(Permission::class, function (Faker\Generator $faker) {
        return [
            'name' => $faker->name,
            'slug' => $faker->unique()->slug,
            'description' => $faker->sentence(10),
        ];
    });
}
