<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddsFakeModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_models', function (Blueprint $table) {
            $table->increments('id');
            $table->string('text')->nullable();
            $table->string('slug')->nullable();
            // $table->text('textarea')->nullable();
            // $table->text('wysiwyg')->nullable();
            // $table->json('wysiwyg_json')->nullable();
            // $table->string('amazon_product')->nullable();
            // $table->boolean('checkbox')->nullable();
            // $table->boolean('checkbox_2')->nullable();
            // $table->datetime('date')->nullable();
            // $table->string('select')->nullable();
            $table->datetime('publish_at')->nullable();
            $table->boolean('published')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('test_models_trans', function (Blueprint $table) {
            $table->increments('id');
            $table->json('text')->nullable();
            $table->json('slug')->nullable();
            // $table->json('textarea')->nullable();
            // $table->json('wysiwyg')->nullable();
            // $table->json('wysiwyg_json')->nullable();
            // $table->json('amazon_product')->nullable();
            // $table->json('checkbox')->nullable();
            // $table->json('checkbox_2')->nullable();
            // $table->json('date')->nullable();
            // $table->json('select')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('test_models_json', function (Blueprint $table) {
            $table->increments('id');
            $table->json('content')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_models_json');
        Schema::dropIfExists('test_models_trans');
        Schema::dropIfExists('test_models');
    }
}
