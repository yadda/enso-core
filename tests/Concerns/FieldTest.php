<?php

namespace Yadda\Enso\Tests\Concerns;

use \Illuminate\Support\Arr;
use Exception;
use Yadda\Enso\Crud\Forms\CollectionSection;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\JsonDataSection;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Tests\Concerns\Config;
use Yadda\Enso\Tests\Concerns\Model;
use Yadda\Enso\Tests\Concerns\ModelJson;
use Yadda\Enso\Tests\Concerns\ModelJsonTrans;
use Yadda\Enso\Tests\Concerns\ModelTrans;
use Yadda\Enso\Tests\TestCase;

abstract class FieldTest extends TestCase
{
    protected function setUpConfig()
    {
        $this->config = (new Config)
            ->setModel(Model::class);

        $this->section = (new Section('main'))
            ->addField($this->field);

        $this->form = (new Form)
            ->setAction('edit')
            ->setConfig($this->config)
            ->addSections([
                $this->section
            ]);
    }

    protected function setUpTranslatableConfig()
    {
        $this->config = (new Config)
            ->setModel(ModelTrans::class)
            ->setUseTranslations();

        $this->field->setTranslatable();

        $this->section = (new Section('main'))
            ->addField($this->field);

        $this->form = (new Form)
            ->setAction('edit')
            ->setConfig($this->config)
            ->addSections([
                $this->section
            ]);
    }

    protected function setUpJsonDataConfig()
    {
        $this->config = (new Config)
            ->setModel(ModelJson::class);

        $this->section = (new JsonDataSection('content'))
            ->addField($this->field);

        $this->form = (new Form)
            ->setAction('edit')
            ->setConfig($this->config)
            ->addSections([
                $this->section
            ]);
    }

    protected function setUpTranslatableJsonDataConfig()
    {
        $this->config = (new Config)
            ->setModel(ModelJsonTrans::class)
            ->setUseTranslations();

        $this->field->setTranslatable();

        $this->section = (new JsonDataSection('content'))
            ->addField($this->field);

        $this->form = (new Form)
            ->setAction('edit')
            ->setConfig($this->config)
            ->addSections([
                $this->section
            ]);
    }

    protected function setUpCollectionDataConfig()
    {
        $this->config = (new Config)
            ->setModel(resolve(\Yadda\Enso\Settings\Contracts\Model::class));

        $this->section = (new CollectionSection('content'))
            ->addField($this->field);

        $this->form = (new Form)
            ->setAction('edit')
            ->setConfig($this->config)
            ->addSections([
                $this->section
            ]);
    }

    protected function setUpTranslatableCollectionDataConfig()
    {
        $this->config = (new Config)
            ->setModel(ModelTrans::class)
            ->setUseTranslations();

        $this->field->setTranslatable();

        $this->section = (new CollectionSection('content'))
            ->addField($this->field);

        $this->form = (new Form)
            ->setAction('edit')
            ->setConfig($this->config)
            ->addSections([
                $this->section
            ]);
    }

    protected function assertTranslationsEqual($value, $model, $column, $field)
    {
        try {
            $this->assertEquals($value, Arr::get(json_decode($model->getAttributes()[$column], true), $field));
        } catch (Exception $e) {
            $this->fail('Unable to Assert Translation matches');
        }
    }
}
