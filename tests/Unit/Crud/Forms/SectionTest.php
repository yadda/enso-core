<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms;

use Illuminate\Support\Facades\Log;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Tests\TestCase;

class SectionTest extends TestCase
{
    protected $form;

    public function setUp(): void
    {
        parent::setUp();

        $this->section = new Section();
    }

    /** @test */
    public function a_field_can_be_added_to_a_form()
    {
        $field = new TextField('test_field');

        $this->assertEquals(0, $this->section->getFields()->count());

        $this->section->addField($field);

        $this->assertEquals(1, $this->section->getFields()->count());
        $this->assertEquals($field, $this->section->getFields()[0]);
    }

    /** @test */
    public function multiple_fields_can_be_added_at_once()
    {
        $field_one = new TextField('test_field_one');
        $field_two = new TextField('test_field_two');

        $this->assertEquals(0, $this->section->getFields()->count());

        $this->section->addFields([
            $field_one,
            $field_two
        ]);

        $this->assertEquals(2, $this->section->getFields()->count());

        $this->assertEquals($field_one, $this->section->getFields()[0]);
        $this->assertEquals($field_two, $this->section->getFields()[1]);
    }

    /** @test */
    public function a_field_can_be_added_before_a_specific_field()
    {
        $field_one = new TextField('first_added_field');
        $field_two = new TextField('second_added_field');
        $field_three = new TextField('third_added_field');

        $this->section->addField($field_one);
        $this->section->addField($field_two);

        $this->assertEquals($field_two, $this->section->getFields()[1]);

        $this->section->addFieldBefore('second_added_field', $field_three);

        $this->assertEquals($field_one, $this->section->getFields()[0]);
        $this->assertEquals($field_three, $this->section->getFields()[1]);
        $this->assertEquals($field_two, $this->section->getFields()[2]);
    }

    /** @test */
    public function a_field_can_be_added_after_a_specific_field()
    {
        $field_one = new TextField('first_added_field');
        $field_two = new TextField('second_added_field');
        $field_three = new TextField('third_added_field');

        $this->section->addField($field_one);
        $this->section->addField($field_two);

        $this->assertEquals($field_two, $this->section->getFields()[1]);

        $this->section->addFieldAfter('first_added_field', $field_three);

        $this->assertEquals($field_one, $this->section->getFields()[0]);
        $this->assertEquals($field_three, $this->section->getFields()[1]);
        $this->assertEquals($field_two, $this->section->getFields()[2]);
    }

    /** @test */
    public function a_field_can_be_moved_within_the_collection_after_another_field()
    {
        $field_one = new TextField('first_added_field');
        $field_two = new TextField('second_added_field');
        $field_three = new TextField('third_added_field');

        $this->section->addFields([
            $field_one,
            $field_two,
            $field_three
        ]);

        $this->assertEquals($field_one, $this->section->getFields()[0]);
        $this->assertEquals($field_two, $this->section->getFields()[1]);
        $this->assertEquals($field_three, $this->section->getFields()[2]);

        $this->section->moveFieldAfter('first_added_field', 'second_added_field');

        $this->assertEquals($field_two, $this->section->getFields()[0]);
        $this->assertEquals($field_one, $this->section->getFields()[1]);
        $this->assertEquals($field_three, $this->section->getFields()[2]);
    }

    /** @test */
    public function a_field_can_be_moved_within_the_collection_before_another_field()
    {
        $field_one = new TextField('first_added_field');
        $field_two = new TextField('second_added_field');
        $field_three = new TextField('third_added_field');

        $this->section->addFields([
            $field_one,
            $field_two,
            $field_three
        ]);

        $this->assertEquals($field_one, $this->section->getFields()[0]);
        $this->assertEquals($field_two, $this->section->getFields()[1]);
        $this->assertEquals($field_three, $this->section->getFields()[2]);

        $this->section->moveFieldBefore('third_added_field', 'second_added_field');

        $this->assertEquals($field_one, $this->section->getFields()[0]);
        $this->assertEquals($field_three, $this->section->getFields()[1]);
        $this->assertEquals($field_two, $this->section->getFields()[2]);
    }

    /** @test */
    public function a_field_can_be_prepended_to_the_field_list()
    {
        $field_one = new TextField('first_added_field');
        $field_two = new TextField('second_added_field');

        $this->section->addField($field_one);

        $this->assertEquals(1, $this->section->getFields()->count());

        $this->section->prependField($field_two);

        $this->assertEquals($field_two, $this->section->getFields()[0]);
        $this->assertEquals($field_one, $this->section->getFields()[1]);
    }

    /** @test */
    public function a_field_can_be_appended_to_the_field_list()
    {
        $field_one = new TextField('first_added_field');
        $field_two = new TextField('second_added_field');

        $this->section->addField($field_one);

        $this->assertEquals(1, $this->section->getFields()->count());

        $this->section->appendField($field_two);

        $this->assertEquals($field_one, $this->section->getFields()[0]);
        $this->assertEquals($field_two, $this->section->getFields()[1]);
    }

    /**
     * @test
     */
    public function get_field_by_name_is_deprecated()
    {
        Log::shouldReceive('warning')->once();

        $this->section->addField(TextField::make('test'));
        $this->section->getFieldByName('test');
    }

    /** @test */
    public function a_field_can_be_found_by_name()
    {
        Log::shouldReceive('warning')->never();

        $field_one = new TextField('first_added_field');
        $field_two = new TextField('second_added_field');
        $field_three = new TextField('third_added_field');

        $this->section->addFields([
            $field_one,
            $field_two,
            $field_three,
        ]);

        $this->assertEquals($field_two, $this->section->getField('second_added_field'));
    }

    /**
     * @test
     */
    public function remove_field_by_name_is_deprecated()
    {
        Log::shouldReceive('warning')->once();

        $this->section->addField(TextField::make('test'));
        $this->section->removeFieldByName('test');
    }

    /** @test */
    public function a_field_can_be_removed_by_name()
    {
        Log::shouldReceive('warning')->never();

        $field_one = new TextField('first_added_field');
        $field_two = new TextField('second_added_field');
        $field_three = new TextField('third_added_field');

        $this->section->addFields([
            $field_one,
            $field_two,
            $field_three,
        ]);

        $this->assertEquals(3, $this->section->getFields()->count());

        $this->section->removeField('second_added_field');

        $this->assertEquals(2, $this->section->getFields()->count());

        $this->assertEquals($field_one, $this->section->getFields()[0]);
        $this->assertEquals($field_three, $this->section->getFields()[1]);
    }

    /** @test */
    public function a_field_can_be_extracted_from_a_section()
    {
        $field_one = new TextField('first_added_field');
        $field_two = new TextField('second_added_field');
        $field_three = new TextField('third_added_field');

        $this->section->addFields([
            $field_one,
            $field_two,
            $field_three,
        ]);

        $this->assertEquals(3, $this->section->getFields()->count());

        $extracted = $this->section->extractField('second_added_field');
        $this->assertEquals($field_two, $extracted);

        $this->assertEquals(2, $this->section->getFields()->count());
        $this->assertEquals($field_one, $this->section->getFields()[0]);
        $this->assertEquals($field_three, $this->section->getFields()[1]);
    }
}
