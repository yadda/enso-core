<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\SlugField;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\Fields\SlugField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\Model;
use Yadda\Enso\Users\Models\Permission;
use Yadda\Enso\Users\Models\Role;
use Yadda\Enso\Users\Models\User;
use Yadda\Enso\Users\Repositories\PermissionRepository;
use Yadda\Enso\Users\Repositories\RoleRepository;

class AuthTest extends BaseFieldTest
{
    use DatabaseMigrations;

    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        // Slug field is a special case, as it contains logic to
        // enforce uniqueness of slug, and so requires being able to
        // check against a database.
        $this->loadMigrationsFrom(__DIR__ . '/../../../../../Concerns/Database/Migrations');

        Config::set('enso.users.permission_repository_concrete_class', PermissionRepository::class);
        Config::set('enso.users.role_repository_concrete_class', RoleRepository::class);
        Config::set('enso.users.classes.permission', Permission::class);

        $this->field = (new SlugField('slug'))
            ->setDefaultValue('Default Value');

        $this->setUpConfig();
    }

    /** @test */
    public function permissions_can_prevent_guests_from_writing_data()
    {
        Auth::logout();

        $model = new Model(['slug' => 'initial-value']);

        $this->field->setEditPermission('does-not-exist');

        $data = [
            'main' => [
                'slug' => 'updated-value'
            ]
        ];

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('updated-value', $model->slug);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function permissions_can_prevent_users_from_writing_data()
    {
        $user = User::factory()->createOne();

        $model = new Model(['slug' => 'initial-value']);

        $this->field->setEditPermission('does-not-exist');

        $data = [
            'main' => [
                'slug' => 'updated-value'
            ]
        ];

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('updated-value', $model->slug);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function user_with_permissions_can_write_data()
    {
        $user = User::factory()->createOne();
        $role = Role::factory()->createOne();
        $permission = Permission::factory()->createOne(['slug' => 'existing-permission']);

        $user->addPermission($permission);

        $model = new Model(['slug' => 'initial-value']);

        $this->field->setEditPermission('existing-permission');

        $data = [
            'main' => [
                'slug' => 'updated-value'
            ]
        ];

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('updated-value', $model->slug);
    }
}
