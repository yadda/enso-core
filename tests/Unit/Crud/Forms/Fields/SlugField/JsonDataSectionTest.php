<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\SlugField;

use Exception;
use Yadda\Enso\Crud\Forms\Fields\SlugField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\ModelJson;
use Yadda\Enso\Tests\Concerns\ModelJsonTrans;
use Yadda\Enso\Tests\Concerns\WordFilter;

/**
 * JsonDataSections get and set data differently enought form
 * regular sections that you can't just pass in a model
 * and directly set/get the data and have the results you would
 * get running it through the crud. It needs to take a step back
 * up the chain and have calls made directly on the Section object.
 */
class JsonDataSectionTest extends BaseFieldTest
{
    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        // Slug field is a special case, as it contains logic to
        // enforce uniqueness of slug, and so requires being able to
        // check against a database.
        $this->loadMigrationsFrom(__DIR__ . '/../../../../../Concerns/Database/Migrations');

        $this->field = (new SlugField('slug'))
            ->setSource('text')
            ->setRoute('%SLUG%')
            ->setDefaultValue('default-value');

        $this->setUpJsonDataConfig();
    }

    /** @test */
    public function gets_json_data_section_form_data_correctly()
    {
        $model = new ModelJson(['content' => ['slug' => 'initial-value']]);

        $expected_output = [
            'slug' => 'initial-value',
        ];

        $this->assertEquals($expected_output, $this->section->getFormData($model));
    }

    /** @test */
    public function gets_altered_json_data_section_form_data()
    {
        $model = new ModelJson(['content' => ['slug' => 'initial-value']]);

        $this->field->setAlterFormDataCallback(function ($value) {
            return 'modified-' . $value;
        });

        $expected_output = [
            'slug' => 'modified-initial-value',
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function gets_json_data_section_from_data_when_source_column_not_available()
    {
        $model = new ModelJson;

        $expected_output = [
            'slug' => null,
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default-value" in place of null');
    }

    /** @test */
    public function gets_json_data_section_from_data_when_source_field_not_available()
    {
        $model = new ModelJson(['content' => []]);

        $expected_output = [
            'slug' => null,
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default-value" in place of null');
    }

    /** @test */
    public function gets_json_data_section_form_data_when_data_falsey_but_should_still_be_used()
    {
        $model = new ModelJson(['content' => ['slug' => '0']]);

        $expected_output = [
            'slug' => '0',
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function gets_json_data_section_form_data_default_when_null()
    {
        $model = new ModelJson(['content' => ['slug' => null]]);

        $expected_output = [
            'slug' => null,
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default-value" in place of null');
    }

    /** @test */
    public function gets_json_data_section_form_data_default_when_empty_string()
    {
        $model = new ModelJson(['content' => ['slug' => '']]);

        $expected_output = [
            'slug' => '',
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default-value"');
    }

    /** @test */
    public function applies_data_through_a_json_data_section()
    {
        $model = new ModelJson;

        $data = [
            'content' => [
                'slug' => 'new-value',
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('new-value', $model->content['slug']);
    }

    /** @test */
    public function disabled_fields_dont_write_data()
    {
        $model = new ModelJson(['content' => ['slug' => 'initial-value']]);

        $this->field->setDisabled();

        $data = ['content' => ['slug' => 'updated-value']];

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('updated-value', $model->content['slug']);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function read_only_fields_dont_write_data()
    {
        $model = new ModelJson(['content' => ['slug' => 'initial-value']]);

        $this->field->setReadonly();

        $data = ['content' => ['slug' => 'updated-value']];

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('updated-value', $model->content['slug']);

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function updates_a_model_with_altered_input_data_through_a_json_data_section()
    {
        $model = new ModelJson(['content' => ['slug' => 'initial-value']]);
        $data = [
            'content' => [
                'slug' => 'new-value',
            ],
        ];

        $this->field->setAlterRequestDataCallback(function ($value) {
            return 'altered-' . $value;
        });

        $this->assertEquals('initial-value', $model->content['slug']);

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('new-value', $model->content['slug']);

        $this->markTestIncomplete('This field is not applying AlterRequestDataCallback. Value should be "altered-new-value"');
    }

    /** @test */
    public function sanitizion_can_be_used_on_request_data_through_a_json_data_section()
    {
        $model = new ModelJson(['content' => ['slug' => 'initial-value']]);
        $data = [
            'content' => [
                'slug' => 'new-value',
            ],
        ];

        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true);

        $this->assertEquals('initial-value', $model->content['slug']);

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('new-value', $model->content['slug']);

        $this->markTestIncomplete('This field is not applying any kind of sanitization. Value should be "<p>new-value</p>"');
    }

    /** @test */
    public function applies_filters_to_request_data_through_a_json_data_section()
    {
        $model = new ModelJson(['content' => ['slug' => 'initial-value']]);
        $data = [
            'content' => [
                'slug' => 'new-value',
            ],
        ];

        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('New'),
        ]);

        $this->assertEquals('initial-value', $model->content['slug']);

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('new-value', $model->content['slug']);

        $this->markTestIncomplete('This field is not applying any filters. Value should be "***-value"');
    }

    /** @test */
    public function exception_thrown_when_not_enough_data_provided()
    {
        $model = new ModelJson(['content' => ['slug' => 'initial-value']]);

        $data = [
            'content' => [
                'slug' => '',
                'text' => '',
            ]
        ];

        $this->expectException(Exception::class); // Laravel 5.4 >
        // $this->expectException(Exception::class); // Laravel 5.5 <

        $this->section->applyRequestData($model, $data);

        $this->markTestIncomplete('This should throw a CrudException');
    }

    /** @test */
    public function updates_a_model_through_a_json_data_section_with_0_input_data()
    {
        $model = new ModelJson(['content' => ['slug' => 'initial-value']]);
        $data = [
            'content' => [
                'slug' => '0',
                'text' => 'Fallback Value',
            ],
        ];

        $this->assertEquals('initial-value', $model->content['slug']);

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('fallback-value', $model->content['slug']);

        $this->markTestIncomplete('This functionality is incorrect. It should return "0"');
    }

    /** @test */
    public function updates_a_model_through_a_json_data_section_with_generated_fallback_value_when_given_null_input_data()
    {
        $model = new ModelJson(['content' => ['slug' => 'initial-value']]);
        $data = [
            'content' => [
                'slug' => null,
                'text' => 'Fallback Value'
            ],
        ];

        $this->assertEquals('initial-value', $model->content['slug']);

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('fallback-value', $model->content['slug']);
    }

    /** @test */
    public function updates_a_model_through_a_json_data_section_with_generated_fallback_value_when_given_empty_string_input_data()
    {
        $model = new ModelJson(['content' => ['slug' => 'initial-value']]);
        $data = [
            'content' => [
                'slug' => '',
                'text' => 'Fallback Value',
            ],
        ];

        $this->assertEquals('initial-value', $model->content['slug']);

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('fallback-value', $model->content['slug']);
    }

    /**
     * Translatable JsonDataSection
     */

    /** @test */
    public function gets_translatable_form_data_from_a_json_data_section()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans([
            'content' => [
                'slug' => [
                    "en" => "en-value",
                    "es" => "es-value",
                ],
            ]
        ]);

        $expected_output = [
            'slug' => [
                "en" => "en-value",
                "es" => "es-value",
            ],
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function gets_translatable_json_data_section_from_data_when_source_column_not_available()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans;

        $expected_output = [
            'slug' => [
                'en' => 'default-value',
            ],
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('It would be useful to be able so set an array of default values.');
    }

    /** @test */
    public function gets_translatable_json_data_section_from_data_when_source_field_not_available()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans(['content' => []]);

        $expected_output = [
            'slug' => [
                'en' => 'default-value',
            ],
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('It would be useful to be able so set an array of default values.');
    }

    /** @test */
    public function gets_translatable_default_form_data_from_a_json_data_section_when_value_is_null()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans([
            'content' => [
                'slug' => null,
            ]
        ]);

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = [
            'slug' => [
                'en' => 'default-value',
            ],
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('It would be useful to be able so set an array of default values.');
    }

    /** @test */
    public function gets_translatable_default_form_data_from_a_json_data_section_when_value_is_empty_string()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans([
            'content' => [
                'slug' => "",
            ]
        ]);

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = [
            'slug' => [
                'en' => 'default-value'
            ],
        ];

        $this->markTestIncomplete('This is broken, it cannont handle empty string');

        // $data = $this->section->getFormData($model);

        // $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function applies_translatable_request_data_through_a_json_data_section()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans;

        $data = [
            'content' => [
                'slug' => [
                    'en' => 'en-value',
                    'es' => 'es-value',
                ],
            ],
        ];

        $expected_output = [
            'en' => 'en-value',
            'es' => 'es-value',
        ];

        $this->section->applyRequestData($model, $data);

        // Need to use a workaround to circumvent HasTranslations overriding the getAttribute('content') method.
        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true)['slug']);
    }

    /** @test */
    public function disabled_translatable_fields_dont_write_data_through_a_json_data_section()
    {
        $this->setUpTranslatableJsonDataConfig();

        $this->field->setDisabled();

        $model = new ModelJsonTrans([
            'content' => [
                'slug' => [
                    'en' => 'en-value',
                    'es' => 'es-value',
                ]
            ]
        ]);

        $data = [
            'content' => [
                'slug' => [
                    'en' => 'new-en-value',
                    'es' => 'new-es-value',
                ],
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $expected_output = [
            'en' => 'new-en-value',
            'es' => 'new-es-value',
        ];

        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true)['slug']);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function read_only_translatable_fields_dont_write_data_through_a_json_data_section()
    {
        $this->setUpTranslatableJsonDataConfig();

        $this->field->setReadonly();

        $model = new ModelJsonTrans([
            'content' => [
                'slug' => [
                    'en' => 'en-value',
                    'es' => 'es-value',
                ]
            ]
        ]);

        $data = [
            'content' => [
                'slug' => [
                    'en' => 'new-en-value',
                    'es' => 'new-es-value',
                ],
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $expected_output = [
            'en' => 'new-en-value',
            'es' => 'new-es-value',
        ];

        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true)['slug']);

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function can_sanitize_translatable_data_when_writing_data()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans([
            'content' => [
                'slug' => [
                    'en' => 'en-value',
                    'es' => 'es-value',
                ]
            ]
        ]);

        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true);

        $data = [
            'content' => [
                'slug' => [
                    'en' => 'new-en-value',
                    'es' => 'new-es-value',
                ],
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $expected_output = [
            'en' => 'new-en-value',
            'es' => 'new-es-value',
        ];

        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true)['slug']);

        $this->markTestIncomplete('This is incorrect. It should be attempting to sanitize the data');

        $expected_output = [
            'en' => '<p>new-en-value</p>',
            'es' => '<p>new-es-value</p>',
        ];

        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true)['slug']);
    }

    /** @test */
    public function can_ultilise_alter_data_callbacks_for_translatable_data_when_writing_data()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans([
            'content' => [
                'slug' => [
                    'en' => 'en-value',
                    'es' => 'es-value',
                ]
            ]
        ]);

        $this->field->setAlterRequestDataCallback(function ($value) {
            return array_map(function ($single) {
                return 'altered-' . $single;
            }, $value);
        });

        $data = [
            'content' => [
                'slug' => [
                    'en' => 'new-en-value',
                    'es' => 'new-es-value',
                ],
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $expected_output = [
            'en' => 'new-en-value',
            'es' => 'new-es-value',
        ];

        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true)['slug']);

        $this->markTestIncomplete('This is incorrect. It should be running the data through the callback');

        $expected_output = [
            'en' => 'altered-new-en-value',
            'es' => 'altered-new-es-value',
        ];

        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true)['slug']);
    }

    /** @test */
    public function can_ultilise_filters_for_translatable_data_when_writing_data()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans([
            'content' => [
                'slug' => [
                    'en' => 'en-value',
                    'es' => 'es-value',
                ]
            ]
        ]);

        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('New'),
        ]);;

        $data = [
            'content' => [
                'slug' => [
                    'en' => 'new-en-value',
                    'es' => 'new-es-value',
                ],
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $expected_output = [
            'en' => 'new-en-value',
            'es' => 'new-es-value',
        ];

        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true)['slug']);

        $this->markTestIncomplete('This is broken. It should be attempting to apply filters');

        $expected_output = [
            'en' => '***-en-value',
            'es' => '***-es-value',
        ];

        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true)['slug']);
    }

    /** @test */
    public function creates_only_data_for_filled_localizations()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans;

        $data = [
            'content' => [
                'slug' => [
                    'en' => 'en-value',
                ],
                'text' => [
                    'en' => 'en-value',
                ],
            ],
        ];

        $expected_output = [
            'en' => 'en-value',
            'es' => 'en-value',
        ];

        $this->section->applyRequestData($model, $data);

        // Need to use a workaround to circumvent HasTranslations overriding the getAttribute('content') method.
        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true)['slug']);

        $this->markTestIncomplete('Should not have added an "es" value');
    }

    /** @test */
    public function doesnt_apply_translatable_request_data_through_a_json_data_section_when_data_is_wrong()
    {
        $this->setUpTranslatableJsonDataConfig();

        $initial_data = [
            'slug' => [
                'en' => "en-value",
                'es' => 'es-value',
            ],
            'text' => [
                'en' => 'En Fallback Value',
                'es' => 'Es Fallback Value'
            ]
        ];

        $model = new ModelJsonTrans([
            'content' => $initial_data
        ]);

        $data = [
            'content' => [
                'slug' => 'Bad Value',
                'text' => 'Bad Text'
            ],
        ];

        // Need to use a workaround to circumvent HasTranslations overriding the getAttribute('content') method.
        $this->assertEquals($initial_data, json_decode($model->getAttributes()['content'], true));

        $this->markTestIncomplete('This does not work');

        // $this->section->applyRequestData($model, $data);

        // // $this->assertEquals($initial_data, json_decode($model->getAttributes()['content'], true));
        // $this->markTestIncomplete('This is wrong. It shouldnt be overwriting good values when the values present are bad');
    }

    /** @test */
    public function applies_translatable_request_data_through_a_json_data_section_when_data_is_null()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans;

        $data = [
            'content' => [
                'slug' => null,
                'text' => [
                    'en' => 'En Fallback Value',
                    'es' => 'Es Fallback Value',
                ]
            ],
        ];

        $expected_output = [
            'slug' => [
                'en' => 'en-fallback-value',
                'es' => 'es-fallback-value',
            ],
        ];

        // Need to use a workaround to circumvent HasTranslations overriding the getAttribute('content') method.
        $this->assertArrayNotHasKey('content', $model->getAttributes());

        $this->section->applyRequestData($model, $data);

        // Need to use a workaround to circumvent HasTranslations overriding the getAttribute('content') method.
        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true));
    }

    /** @test */
    public function applies_translatable_request_data_through_a_json_data_section_when_data_is_empty_string()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans;

        $data = [
            'content' => [
                'slug' => '',
                'text' => 'En Fallback Value',
            ],
        ];

        // Need to use a workaround to circumvent HasTranslations overriding the getAttribute('content') method.
        $this->assertArrayNotHasKey('content', $model->getAttributes());

        $this->markTestIncomplete('This does not work');

        // $this->section->applyRequestData($model, $data);

        // $expected_output = [
        //     'slug' => [
        //         'en' => 'Default Value',
        //     ],
        // ];

        // // Need to use a workaround to circumvent HasTranslations overriding the getAttribute('content') method.
        // $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true));

        // $this->markTestIncomplete('Probably shouldn\'t do this. I think would expect it so set it to empty string');
    }
}
