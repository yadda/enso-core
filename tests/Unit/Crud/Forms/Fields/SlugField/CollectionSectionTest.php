<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\SlugField;

use Exception;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Yadda\Enso\Crud\Forms\Fields\SlugField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\WordFilter;

/**
 * CollectionSections get and set data differently enought form
 * regular sections that you can't just pass in a model
 * and directly set/get the data and have the results you would
 * get running it through the crud. It needs to take a step back
 * up the chain and have calls made directly on the Section object.
 */
class CollectionSectionTest extends BaseFieldTest
{
    use DatabaseMigrations;

    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->field = (new SlugField('slug'))
            ->setSource('text')
            ->setRoute('%SLUG%')
            ->setDefaultValue('default-value');

        $this->setUpCollectionDataConfig();
    }

    /** @test */
    public function gets_collection_section_form_data_correctly()
    {
        $this->setSetting('slug', 'initial-value');

        $expected_output = [
            'slug' => 'initial-value',
        ];

        $this->assertEquals($expected_output, $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class)));
    }

    /** @test */
    public function gets_altered_collection_section_form_data()
    {
        $this->setSetting('slug', 'setting-value');

        $this->field->setAlterFormDataCallback(function ($value) {
            return 'modified-' . $value;
        });

        $expected_output = [
            'slug' => 'modified-setting-value',
        ];

        $data = $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class));

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function gets_default_form_data_when_settings_doesnt_exist()
    {
        $expected_output = [
            'slug' => null,
        ];

        $data = $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class));

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default-value" in place of null');
    }

    /** @test */
    public function gets_collection_section_form_data_when_data_falsey_but_should_still_be_used()
    {
        $this->setSetting('slug', '0');

        $expected_output = [
            'slug' => '0',
        ];

        $data = $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class));

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function gets_collection_section_form_data_default_when_null()
    {
        $this->setSetting('slug', null);

        $expected_output = [
            'slug' => null,
        ];

        $data = $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class));

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default-value" in place of null');
    }

    /** @test */
    public function gets_collection_section_form_data_default_when_empty_string()
    {
        $this->setSetting('slug', '');

        $expected_output = [
            'slug' => '',
        ];

        $data = $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class));

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default-value"');
    }

    /** @test */
    public function applies_data_through_a_collection_section()
    {
        $setting_instance = new \Yadda\Enso\Settings\Settings;

        $data = [
            'content' => [
                'slug' => 'new-value',
            ],
        ];

        $this->section->applyRequestData($setting_instance, $data);

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);

        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('new-value', $setting->decoded);
    }

    /** @test */
    public function disabled_fields_dont_write_data()
    {
        $this->setSetting('slug', 'initial-value');
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        $this->field->setDisabled();

        $data = [
            'content' => [
                'slug' => 'updated-value'
            ]
        ];

        $this->section->applyRequestData($setting_repository, $data);

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);

        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('updated-value', $setting->decoded);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function readonly_fields_dont_write_data()
    {
        $this->setSetting('slug', 'initial-value');
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        $this->field->setReadonly();

        $data = [
            'content' => [
                'slug' => 'updated-value'
            ]
        ];

        $this->section->applyRequestData($setting_repository, $data);

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);

        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('updated-value', $setting->decoded);

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function updates_a_model_with_altered_input_data_through_a_collection_section()
    {
        $this->setSetting('slug', 'initial-value');
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        $data = [
            'content' => [
                'slug' => 'new-value',
            ],
        ];

        $this->field->setAlterRequestDataCallback(function ($value) {
            return 'altered-' . $value;
        });

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);

        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('initial-value', $setting->decoded);

        $this->section->applyRequestData($setting_repository, $data);

        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('new-value', $setting->decoded);

        $this->markTestIncomplete('This does not apply the AlterRequestDataCallback. It should return "altered-new-value"');
    }

    /** @test */
    public function sanitizes_request_data_through_a_collection_section()
    {
        $this->setSetting('slug', 'initial-value');
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        $data = [
            'content' => [
                'slug' => 'new-value',
            ],
        ];

        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true);

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);

        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('initial-value', $setting->decoded);

        $this->section->applyRequestData($setting_repository, $data);

        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('new-value', $setting->decoded);

        $this->markTestIncomplete('This does not apply any sanitization. It should return "<p>new-value</p>"');
    }

    /** @test */
    public function applies_filters_to_request_data_through_a_collection_section()
    {
        $this->setSetting('slug', 'initial-value');
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        $data = [
            'content' => [
                'slug' => 'new-value',
            ],
        ];

        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('new'),
        ]);

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);

        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('initial-value', $setting->decoded);

        $this->section->applyRequestData($setting_repository, $data);

        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('new-value', $setting->decoded);

        $this->markTestIncomplete('This does not apply any Filteres. It should return "***-value"');
    }

    /** @test */
    public function updates_a_model_through_a_collection_section_with_0_input_data()
    {
        $this->setSetting('slug', 'initial-value');
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        $data = [
            'content' => [
                'slug' => '0',
                'text' => 'Fallback Value'
            ],
        ];

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);

        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('initial-value', $setting->decoded);

        $this->section->applyRequestData($setting_repository, $data);

        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('fallback-value', $setting->decoded);

        $this->markTestIncomplete('This functionality is incorrect. It should return "0"');
    }

    /** @test */
    public function exception_thrown_when_neither_slug_nor_source_field_have_data()
    {
        $this->setSetting('slug', 'initial-value');
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        $data = [
            'content' => [
                'slug' => null,
                'text' => null,
            ],
        ];

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);

        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('initial-value', $setting->decoded);

        $this->expectException(Exception::class);

        $this->section->applyRequestData($setting_repository, $data);
    }

    /** @test */
    public function updates_a_model_through_a_collection_section_with_generated_fallback_value_when_given_null_input_data()
    {
        $this->setSetting('slug', 'initial-value');
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        $data = [
            'content' => [
                'slug' => null,
                'text' => 'Fallback Value'
            ],
        ];
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);

        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('initial-value', $setting->decoded);

        $this->section->applyRequestData($setting_repository, $data);

        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('fallback-value', $setting->decoded);
    }

    /** @test */
    public function updates_a_model_through_a_collection_section_with_default_value_when_given_empty_string_input_data()
    {
        $this->setSetting('slug', 'initial-value');
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        $data = [
            'content' => [
                'slug' => '',
                'text' => 'Fallback Value'
            ],
        ];
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);

        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('initial-value', $setting->decoded);

        $this->section->applyRequestData($setting_repository, $data);

        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('fallback-value', $setting->decoded);
    }

    /**
     * Translatable CollectionSection
     */

    /** @test */
    public function gets_translatable_form_data_from_a_collection_section()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en-value",
            "es" => "es-value",
        ];

        $this->setSetting('slug', [
            "en" => "en-value",
            "es" => "es-value",
        ]);

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $this->markTestIncomplete('Cannot properly read this value from the setting via getFormData');

        // $this->assertEquals(['slug' => $initial_state], $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class)));
    }

    /** @test */
    public function gets_translatable_default_form_data_when_setting_doesnt_exist()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $setting_repository = new \Yadda\Enso\Settings\Settings;

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = [
            'slug' => [
                "en" => "default-value",
            ],
        ];

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNull($setting);

        $data = $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class));

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('It would be useful to be able so set an array of default values.');
    }

    /** @test */
    public function gets_translatable_default_form_data_from_a_collection_section_when_data_is_null()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $this->setSetting('slug', null);
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = [
            'slug' => [
                "en" => "default-value",
            ],
        ];

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals(null, $setting->decoded);

        $data = $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class));

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('It would be useful to be able so set an array of default values.');
    }

    /** @test */
    public function gets_translatable_default_form_data_from_a_collection_section_when_data_is_empty_string()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $this->setSetting('slug', '');
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = [
            'slug' => [
                "en" => "default-value",
            ],
        ];

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('', $setting->decoded);

        $data = $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class));

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('It would be useful to be able so set an array of default values.');
    }

    /** @test */
    public function applies_translatable_request_data_through_a_collection_section()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en-value",
            "es" => "es-value",
        ];
        $this->setSetting('slug', $initial_state);

        $setting_repository = new \Yadda\Enso\Settings\Settings;

        // Test initial state
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $data = [
            'content' => [
                'slug' => [
                    'en' => 'new-en-value',
                    'es' => 'new-es-value',
                ],
            ],
        ];

        $expected_output = [
            'en' => 'new-en-value',
            'es' => 'new-es-value',
        ];

        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);
    }

    /** @test */
    public function disabled_fields_dont_write_data_through_a_collection_section()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en-value",
            "es" => "es-value",
        ];
        $this->setSetting('slug', $initial_state);

        $this->field->setDisabled();

        // Test initial state
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $data = [
            'content' => [
                'slug' => [
                    'en' => 'new-en-value',
                    'es' => 'new-es-value',
                ],
            ],
        ];

        $expected_output = [
            'en' => 'new-en-value',
            'es' => 'new-es-value',
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;
        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function read_only_fields_dont_write_data_through_a_collection_section()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en-value",
            "es" => "es-value",
        ];
        $this->setSetting('slug', $initial_state);

        $this->field->setReadonly();

        // Test initial state
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $data = [
            'content' => [
                'slug' => [
                    'en' => 'new-en-value',
                    'es' => 'new-es-value',
                ],
            ],
        ];

        $expected_output = [
            'en' => 'new-en-value',
            'es' => 'new-es-value',
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;
        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function can_sanitize_translatable_data_when_writing_data()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en-value",
            "es" => "es-value",
        ];
        $this->setSetting('slug', $initial_state);

        // Test initial state
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true);

        $data = [
            'content' => [
                'slug' => [
                    'en' => 'new-en-value',
                    'es' => 'new-es-value',
                ],
            ],
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;
        $this->section->applyRequestData($setting_repository, $data);

        $expected_output = [
            'en' => 'new-en-value',
            'es' => 'new-es-value',
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;
        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);

        $this->markTestIncomplete('This is incorrect. It should be attempting to sanitize the data');

        $expected_output = [
            'en' => '<p>new-en-value</p>',
            'es' => '<p>new-es-value</p>',
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;
        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);
    }

    /** @test */
    public function can_ultilise_alter_data_callbacks_for_translatable_data_when_writing_data()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en-value",
            "es" => "es-value",
        ];
        $this->setSetting('slug', $initial_state);

        // Test initial state
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $this->field->setAlterRequestDataCallback(function ($value) {
            return array_map(function ($single) {
                return 'altered-' . $single;
            }, $value);
        });

        $data = [
            'content' => [
                'slug' => [
                    'en' => 'new-en-value',
                    'es' => 'new-es-value',
                ],
            ],
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;
        $this->section->applyRequestData($setting_repository, $data);

        $expected_output = [
            'en' => 'new-en-value',
            'es' => 'new-es-value',
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;
        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);

        $this->markTestIncomplete('This is incorrect. It should be running the data through the callback');

        $expected_output = [
            'en' => 'altered-new-en-value',
            'es' => 'altered-new-es-value',
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;
        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);
    }

    /** @test */
    public function can_ultilise_filters_for_translatable_data_when_writing_data()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en-value",
            "es" => "es-value",
        ];
        $this->setSetting('slug', $initial_state);

        // Test initial state
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('New'),
        ]);;

        $data = [
            'content' => [
                'slug' => [
                    'en' => 'new-en-value',
                    'es' => 'new-es-value',
                ],
            ],
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;
        $this->section->applyRequestData($setting_repository, $data);

        $expected_output = [
            'en' => 'new-en-value',
            'es' => 'new-es-value',
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;
        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);

        $this->markTestIncomplete('This is broken. It should be attempting to apply filters');

        $expected_output = [
            'en' => '***-en-value',
            'es' => '***-es-value',
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;
        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);
    }

    /** @test */
    public function creates_only_data_for_filled_localizations()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en-value",
        ];

        $this->setSetting('slug', $initial_state);

        $setting_repository = new \Yadda\Enso\Settings\Settings;

        // Test initial state
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $data = [
            'content' => [
                'slug' => null,
                'text' => [
                    'en' => 'En Fallback Value',
                ],
            ],
        ];

        $expected_output = [
            'en' => 'en-fallback-value',
            'es' => 'en-fallback-value',
        ];

        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);

        $this->markTestIncomplete('This should not be creating an "es" value');
    }

    /** @test */
    public function applies_translatable_request_data_through_a_collection_section_when_data_is_null()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en-value",
            "es" => "es-value",
        ];
        $this->setSetting('slug', $initial_state);

        $setting_repository = new \Yadda\Enso\Settings\Settings;

        // Test initial state
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $data = [
            'content' => [
                'slug' => null,
                'text' => [
                    'en' => 'En Fallback Value',
                    'es' => 'Es Fallback Value',
                ],
            ],
        ];

        $expected_output = [
            'en' => 'en-fallback-value',
            'es' => 'es-fallback-value',
        ];

        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);
    }

    /** @test */
    public function applies_translatable_request_data_through_a_collection_section_when_data_is_empty_string()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en-value",
            "es" => "es-value",
        ];

        $this->setSetting('slug', $initial_state);

        $setting_repository = new \Yadda\Enso\Settings\Settings;

        // Test initial state
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'slug')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $data = [
            'content' => [
                'slug' => '',
                'text' => [
                    'en' => 'En Fallback Value',
                    'es' => 'Es Fallback Value',
                ],
            ],
        ];

        $expected_output = [
            'en' => 'en-fallback-value',
            'es' => 'es-fallback-value',
        ];

        $this->markTestIncomplete('This does not work');

        // $this->section->applyRequestData($setting_repository, $data);

        // $setting = get_class($setting_instance)::where('slug', 'slug')->first();
        // $this->assertNotNull($setting);
        // $this->assertEquals($expected_output, $setting->decoded);
    }
}
