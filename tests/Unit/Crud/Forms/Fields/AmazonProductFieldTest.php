<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Yadda\Enso\Crud\Forms\Fields\AmazonProductField;
use Yadda\Enso\Tests\Concerns\Field as BaseField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\Model;

class AmazonProductFieldTest extends BaseFieldTest
{
    // use DatabaseMigrations;

    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->field = (new AmazonProductField('amazon_asin'))
            ->setDefaultValue('default-value');

        $this->setUpConfig();
    }

    /** @test */
    public function has_correct_component()
    {
        $this->assertEquals('enso-field-amazon-product', $this->field->getComponent());
    }

    /** @test */
    public function gets_form_data_correctly()
    {
        $model = new Model(['amazon_asin' => 'Test Data']);

        $data = $this->field->getFormData($model);

        $this->assertEquals('Test Data', $data);
    }

    /** @test */
    public function gets_form_data_default_when_null()
    {
        $model = new Model(['amazon_asin' => null]);

        $data = $this->field->getFormData($model);

        $this->assertEquals(null, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default-value"');
    }

    /** @test */
    public function gets_from_data_default_when_empty_string()
    {
        $model = new Model(['amazon_asin' => '']);

        $data = $this->field->getFormData($model);

        $this->assertEquals('', $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default-value"');
    }

    /** @test */
    public function gets_from_data_default_when_0()
    {
        $model = new Model(['amazon_asin' => '0']);

        $data = $this->field->getFormData($model);

        $this->assertEquals('0', $data);
    }

    /** @test */
    public function updates_a_model_with_input_data()
    {
        $model = new Model(['amazon_asin' => 'initial value']);
        $data = [
            'main' => [
                'amazon_asin' => 'new value',
            ],
        ];

        $this->assertEquals('initial value', $model->amazon_asin);

        $this->field->getSection()->applyRequestData($model, $data);

        $this->assertEquals('new value', $model->amazon_asin);
    }

    /** @test */
    public function updates_a_model_with_default_value_when_given_empty_string_input_data()
    {
        $model = new Model(['amazon_asin' => 'initial value']);
        $data = [
            'main' => [
                'amazon_asin' => '',
            ],
        ];

        $this->assertEquals('initial value', $model->amazon_asin);

        $this->field->getSection()->applyRequestData($model, $data);

        $this->assertEquals('default-value', $model->amazon_asin);

        $this->markTestIncomplete('Not Sure it should be doing this');
    }

    /** @test */
    public function updates_a_model_with_default_value_when_given_null_input_data()
    {
        $model = new Model(['amazon_asin' => 'initial value']);
        $data = [
            'main' => [
                'amazon_asin' => null,
            ],
        ];

        $this->assertEquals('initial value', $model->amazon_asin);

        $this->field->getSection()->applyRequestData($model, $data);

        $this->assertEquals('default-value', $model->amazon_asin);

        $this->markTestIncomplete('Not Sure it should be doing this');
    }


    /** @test */
    public function updates_a_model_with_0_input_data()
    {
        $model = new Model(['amazon_asin' => 'initial value']);
        $data = [
            'main' => [
                'amazon_asin' => '0',
            ],
        ];

        $this->assertEquals('initial value', $model->amazon_asin);

        $this->field->getSection()->applyRequestData($model, $data);

        $this->assertEquals('default-value', $model->amazon_asin);

        $this->markTestIncomplete('This functionality is incorrect. It should return "0"');
    }
}
