<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Yadda\Enso\Crud\Forms\Fields\CheckboxField;
use Yadda\Enso\Tests\Concerns\Field as BaseField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\Model;

class CheckboxFieldWithOptionsTest extends BaseFieldTest
{
    // use DatabaseMigrations;

    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->field = (new CheckboxField('checkbox'))
            ->setDefaultValue([
                'checkbox' => false,
                'checkbox_2' => true,
                'checkbox_3' => true,
            ])
            ->setOptions([
                'checkbox' => 'Checkbox',
                'checkbox_2' => 'Checkbox 2',
                'checkbox_3' => 'Checkbox 3',
            ]);

        $this->setUpConfig();
    }

    /** @test */
    public function has_correct_component()
    {
        $this->assertEquals('enso-field-checkbox', $this->field->getComponent());
    }

    /** @test */
    public function gets_form_data_correctly()
    {
        $model = new Model([
            'checkbox' => true,
            'checkbox_2' => false,
            'checkbox_3' => true,
        ]);

        $expected_data = [
            "checkbox" => "checkbox",
            "checkbox_3" => "checkbox_3",
        ];

        $data = $this->field->getFormData($model);

        $this->assertEquals($expected_data, $data);

        $this->markTestIncomplete('This should label probably return true / false as the value for each element instead of returning only true ones');
    }

    /** @test */
    public function gets_form_data_default_when_null()
    {
        $model = new Model([
            'checkbox' => null,
            'checkbox_2' => null,
            'checkbox_3' => null,
        ]);

        $expected_data = [
            'checkbox' => false,
            'checkbox_2' => true,
            'checkbox_3' => true,
        ];

        $data = $this->field->getFormData($model);

        $this->assertEquals($expected_data, $data);

        $this->markTestIncomplete('This functionality does not use individual default values properly');
    }

    /** @test */
    public function gets_from_data_default_when_empty_string()
    {
        $model = new Model([
            'checkbox' => '',
            'checkbox_2' => '',
            'checkbox_3' => '',
        ]);

        $expected_data = [
            'checkbox' => false,
            'checkbox_2' => true,
            'checkbox_3' => true,
        ];

        $data = $this->field->getFormData($model);

        $this->assertEquals($expected_data, $data);

        $this->markTestIncomplete('This functionality does not use individual default values properly');
    }

    /** @test */
    public function updates_a_model_with_input_data()
    {
        $model = new Model(['checkbox' => 'initial value']);
        $data = [
            'main' => [
                'checkbox' => [
                    'checkbox' => true,
                    'checkbox_2' => false,
                    'checkbox_3' => true,
                ],
            ],
        ];

        $this->assertEquals('initial value', $model->checkbox);

        $this->field->getSection()->applyRequestData($model, $data);

        $this->assertEquals(true, $model->checkbox);
        $this->assertEquals(false, $model->checkbox_2);
        $this->assertEquals(true, $model->checkbox_3);
    }

    /** @test */
    public function updates_a_model_with_default_value_when_given_empty_string_input_data()
    {
        $model = new Model(['checkbox' => 'initial value']);
        $data = [
            'main' => [
                'checkbox' => [],
            ],
        ];

        $this->assertEquals('initial value', $model->checkbox);

        $this->field->getSection()->applyRequestData($model, $data);

        $this->assertEquals(false, $model->checkbox);
        $this->assertEquals(false, $model->checkbox_2);
        $this->assertEquals(false, $model->checkbox_3);

        $this->markTestIncomplete('This does not / can not currently use default values');
    }

    /** @test */
    public function updates_a_model_with_default_value_when_given_null_input_data()
    {
        $model = new Model(['checkbox' => 'initial value']);
        $data = [
            'main' => [
                'checkbox' => [],
            ],
        ];

        $this->assertEquals('initial value', $model->checkbox);

        $this->field->getSection()->applyRequestData($model, $data);

        $this->assertEquals(false, $model->checkbox);
        $this->assertEquals(false, $model->checkbox_2);
        $this->assertEquals(false, $model->checkbox_3);

        $this->markTestIncomplete('This does not / can not currently use default values');
    }
}
