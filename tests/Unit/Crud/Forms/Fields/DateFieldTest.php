<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Yadda\Enso\Crud\Forms\Fields\DateField;
use Yadda\Enso\Tests\Concerns\Field as BaseField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\Model;
use Yadda\Enso\Tests\Traits\IsDateTest;

class DateFieldTest extends BaseFieldTest
{
    use IsDateTest;

    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->field = (new DateField('date'))
            ->setDefaultValue($this->defaultDateInstance());

        $this->setUpConfig();
    }

    /** @test */
    public function has_correct_component()
    {
        $this->assertEquals('enso-field-date', $this->field->getComponent());
    }

    /** @test */
    public function gets_form_data_correctly()
    {
        $model = new Model(['date' => $this->dateInstance('2000-01-01 00:00:00')]);

        $data = $this->field->getFormData($model);

        $this->assertEquals($this->dateInstance('2000-01-01 00:00:00'), $data);
    }

    /** @test */
    public function gets_form_data_default_when_null()
    {
        $model = new Model(['date' => null]);

        $data = $this->field->getFormData($model);

        $this->assertEquals($this->defaultDateInstance(), $data);
    }

    /** @test */
    public function updates_a_model_with_input_data()
    {
        $model = new Model(['date' => $this->dateInstance('2000-01-01 00:00:00')]);

        $data = [
            'main' => [
                'date' => $this->validFormResponse(
                    $this->dateInstance('2000-01-01 23:59:59')
                ),
            ],
        ];

        $expected_value = $this->dateInstance('2000-01-01 23:59:59');

        $this->assertTrue($this->dateInstance('2000-01-01 00:00:00')->eq($model->date));

        $this->field->getSection()->applyRequestData($model, $data);

        $this->assertTrue($expected_value->eq($model->date));
    }

    /** @test */
    public function updates_a_model_with_default_value_when_given_empty_string_input_data()
    {
        $model = new Model(['date' => $this->dateInstance('2000-01-01 00:00:00')]);
        $data = [
            'main' => [
                'date' => '',
            ],
        ];

        $this->assertEquals($this->dateInstance('2000-01-01 00:00:00'), $model->date);

        $this->markTestIncomplete('This functionality is Broken. You cannot set a Carbon Instance as a default date.');

        // $this->field->getSection()->applyRequestData($model, $data);

        // $this->assertEquals($this->defaultDateInstance(), $model->date);
    }

    /** @test */
    public function updates_a_model_with_default_value_when_given_null_input_data()
    {
        $model = new Model(['date' => $this->dateInstance('2000-01-01 00:00:00')]);
        $data = [
            'main' => [
                'date' => null,
            ],
        ];

        $this->assertEquals($this->dateInstance('2000-01-01 00:00:00'), $model->date);

        $this->markTestIncomplete('This functionality is Broken. You cannot set a Carbon Instance as a default date.');

        // $this->field->getSection()->applyRequestData($model, $data);

        // $this->assertEquals($this->defaultDateInstance(), $model->date);
    }
}
