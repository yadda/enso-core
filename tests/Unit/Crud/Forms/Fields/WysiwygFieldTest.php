<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Yadda\Enso\Crud\Forms\Fields\WysiwygField;
use Yadda\Enso\Tests\Concerns\Field as BaseField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\Model;

class WysiwygFieldTest extends BaseFieldTest
{
    // use DatabaseMigrations;

    protected $field;

    protected function basicTextInsert($insert)
    {
        return "<p>{$insert}</p>";
    }

    protected function basicJsonInsert($insert)
    {
        return [
            'ops' => [
                [
                    "insert" => "$insert",
                ],
            ],
        ];
    }

    protected function basicInsert($insert)
    {
        return [
            'html' => $this->basicTextInsert($insert),
            'json' => $this->basicJsonInsert($insert),
        ];
    }

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->field = (new WysiwygField('wysiwyg'))
            ->setDefaultValue($this->basicInsert('Default Value'));

        $this->setUpConfig();
    }

    /** @test */
    public function has_correct_component()
    {
        $this->assertEquals('enso-field-wysiwyg', $this->field->getComponent());
    }

    /** @test */
    public function gets_form_data_correctly()
    {
        $model = new Model([
            'wysiwyg' => $this->basicTextInsert('Initial value'),
            'wysiwyg_json' => $this->basicJsonInsert('Initial value'),
        ]);

        $expected_output = $this->basicInsert('Initial value');

        $data = $this->field->getFormData($model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function gets_form_data_default_when_null()
    {
        $model = new Model([
            'wysiwyg' => null,
            'wysiwyg_json' => null,
        ]);

        $expected_output = [
            'html' => null,
            'json' => null,
        ];

        $data = $this->field->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return the default values');
    }

    /** @test */
    public function gets_from_data_default_when_empty_string()
    {
        $model = new Model([
            'wysiwyg' => '',
            'wysiwyg_json' => '',
        ]);

        $expected_output = [
            'html' => '',
            'json' => '',
        ];

        $data = $this->field->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return the default values');
    }

    /** @test */
    public function gets_from_data_default_when_0()
    {
        $model = new Model([
            'wysiwyg' => $this->basicTextInsert('0'),
            'wysiwyg_json' => $this->basicJsonInsert('0'),
        ]);

        $expected_output = $this->basicInsert('0');

        $data = $this->field->getFormData($model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function updates_a_model_with_input_data()
    {
        $model = new Model([
            'wysiwyg' => $this->basicTextInsert('Initial Value'),
            'wysiwyg_json' => $this->basicJsonInsert('Initial Value'),
        ]);

        $data = [
            'main' => [
                'wysiwyg' => $this->basicInsert('Test'),
            ],
        ];

        $this->assertEquals($this->basicTextInsert('Initial Value'), $model->wysiwyg);
        $this->assertEquals($this->basicJsonInsert('Initial Value'), $model->wysiwyg_json);

        $this->field->getSection()->applyRequestData($model, $data);

        $this->assertEquals($this->basicTextInsert('Test'), $model->wysiwyg);
        $this->assertEquals($this->basicJsonInsert('Test'), $model->wysiwyg_json);
    }

    /** @test */
    public function updates_a_model_with_default_value_when_given_empty_string_input_data()
    {
        $model = new Model([
            'wysiwyg' => $this->basicTextInsert('Initial Value'),
            'wysiwyg_json' => $this->basicJsonInsert('Initial Value'),
        ]);

        $data = [
            'main' => [
                'wysiwyg' => [
                    'html' => '',
                    'json' => [],
                ],
            ],
        ];

        $this->assertEquals($this->basicTextInsert('Initial Value'), $model->wysiwyg);
        $this->assertEquals($this->basicJsonInsert('Initial Value'), $model->wysiwyg_json);

        $this->field->getSection()->applyRequestData($model, $data);

        $this->assertEquals('', $model->wysiwyg);
        $this->assertEquals('[]', $model->wysiwyg_json);

        $this->markTestIncomplete('The final assertion values are not correct');
    }

    /** @test */
    public function updates_a_model_with_default_value_when_given_null_input_data()
    {
        $model = new Model([
            'wysiwyg' => $this->basicTextInsert('Initial Value'),
            'wysiwyg_json' => $this->basicJsonInsert('Initial Value'),
        ]);

        $data = [
            'main' => [
                'wysiwyg' => [
                    'html' => null,
                    'json' => null,
                ],
            ],
        ];

        $this->assertEquals($this->basicTextInsert('Initial Value'), $model->wysiwyg);
        $this->assertEquals($this->basicJsonInsert('Initial Value'), $model->wysiwyg_json);

        $this->field->getSection()->applyRequestData($model, $data);

        $this->assertEquals('', $model->wysiwyg);
        $this->assertEquals('[]', $model->wysiwyg_json);

        $this->markTestIncomplete('This is wrong, it should be an empty array, not a string of an array. If its working with nulls, it should set null');
    }
}
