<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Yadda\Enso\Tests\Concerns\Field as BaseField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\Model;

class FieldTest extends BaseFieldTest
{
    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->field = new BaseField('text');

        $this->setUpConfig();
    }

    /** @test */
    public function sentence_case_label_is_generated_from_name_if_label_not_set()
    {
        $this->field->setName('text_field');

        $this->assertEquals('Text Field', $this->field->getLabel());
    }
}