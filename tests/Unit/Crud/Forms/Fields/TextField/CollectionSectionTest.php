<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\TextField;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\WordFilter;
use Yadda\Enso\Tests\Traits\IsSettingTest;

/**
 * CollectionSections get and set data differently enought form
 * regular sections that you can't just pass in a model
 * and directly set/get the data and have the results you would
 * get running it through the crud. It needs to take a step back
 * up the chain and have calls made directly on the Section object.
 */
class CollectionSectionTest extends BaseFieldTest
{
    use DatabaseMigrations, IsSettingTest;

    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setSetting('text', 'Initial Value');

        $this->field = (new TextField('text'))
            ->setDefaultValue('Default Value');

        $this->setUpCollectionDataConfig();
    }

    /**
     * Data Reading
     */

    /** @test */
    public function reads_unaltered_form_data()
    {
        $expected_output = ['text' => 'Initial Value'];

        $this->assertEquals($expected_output, $this->getSettingFormData());
    }

    /** @test */
    public function reads_form_data_when_source_value_is_falsey_but_valid()
    {
        $this->setSetting('text', '0');

        $data = $this->getSettingFormData();

        $expected_output = ['text' => '0'];
        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_not_set()
    {
        $this->removeSetting('text');

        $data = $this->getSettingFormData();

        // Current State
        $expected_output = ['text' => null];
        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "Default Value" in place of null');

        // Desired State
        $expected_output = ['text' => 'Default Value'];
        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_null()
    {
        $this->setSetting('text', null);

        $data = $this->getSettingFormData();

        // Current State
        $expected_output = ['text' => null];
        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "Default Value" in place of null');

        // Desired State
        $expected_output = ['text' => 'Default Value'];
        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_empty_string()
    {
        $this->setSetting('text', '');

        $data = $this->getSettingFormData();

        // Current State
        $expected_output = ['text' => ''];
        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "Default Value"');

        // Desired State
        $expected_output = ['text' => 'Default Value'];
        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_altered_form_data()
    {
        $this->field->setAlterFormDataCallback(function ($value) {
            return 'Modified ' . $value;
        });

        $this->setSetting('text', 'Setting Value');

        $data = $this->getSettingFormData();

        $expected_output = ['text' => 'Modified Setting Value'];
        $this->assertEquals($expected_output, $data);
    }

    /**
     * Data Writing
     */

    /** @test */
    public function writes_unaltered_request_data()
    {
        $data = [
            'content' => [
                'text' => 'New Value',
            ],
        ];

        $this->assertSettingValueEquals('Initial Value', 'text');

        $this->applySettingData($data);

        $this->assertSettingValueEquals('New Value', 'text');
    }

    /** @test */
    public function writes_request_data_when_data_is_falsey_but_valid()
    {
        $data = [
            'content' => [
                'text' => '0',
            ],
        ];

        $this->assertSettingValueEquals('Initial Value', 'text');

        $this->applySettingData($data);

        // Current State
        $this->assertSettingValueEquals('Default Value', 'text');

        $this->markTestIncomplete('This functionality is incorrect. It should return "0"');

        // Desired State
        $this->assertSettingValueEquals('0', 'text');
    }

    /** @test */
    public function does_not_write_request_data_when_data_is_not_set()
    {
        $data = [
            'content' => [],
        ];

        $this->assertSettingValueEquals('Initial Value', 'text');

        $this->applySettingData($data);

        // Current State
        $this->assertSettingValueEquals('Default Value', 'text');

        $this->markTestIncomplete('This functionality is incorrect. It should leaving the setting in its initial state');

        // Desired State
        $this->assertSettingValueEquals('Initial Value', 'text');
    }

    /** @test */
    public function writes_request_data_when_data_is_null()
    {
        $data = [
            'content' => [
                'text' => null,
            ],
        ];

        $this->assertSettingValueEquals('Initial Value', 'text');

        $this->applySettingData($data);

        // Current State
        $this->assertSettingValueEquals('Default Value', 'text');

        $this->markTestIncomplete('This should be saving null Value');

        // Desired State
        $this->assertSettingValueEquals(null, 'text');
    }

    /** @test */
    public function writes_request_data_when_data_is_emtpy_string()
    {
        $data = [
            'content' => [
                'text' => '',
            ],
        ];

        $this->assertSettingValueEquals('Initial Value', 'text');

        $this->applySettingData($data);

        // Current State
        $this->assertSettingValueEquals('Default Value', 'text');

        $this->markTestIncomplete('This should be saving an empty string');

        // Desired State
        $this->assertSettingValueEquals('', 'text');
    }

    /** @test */
    public function can_ultilise_alter_data_callbacks_when_writing_request_data()
    {
        $this->field->setAlterRequestDataCallback(function ($value) {
            return 'Altered ' . $value;
        });

        $data = [
            'content' => [
                'text' => 'New Value',
            ],
        ];

        $this->assertSettingValueEquals('Initial Value', 'text');

        $this->applySettingData($data);

        $this->assertSettingValueEquals('Altered New Value', 'text');
    }

    /** @test */
    public function can_sanitize_data_when_writing_request_data()
    {
        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true)
            ->setPurifyHtml(true);

        $data = [
            'content' => [
                'text' => 'New Value',
            ],
        ];

        $this->assertSettingValueEquals('Initial Value', 'text');

        $this->applySettingData($data);

        $this->assertSettingValueEquals('<p>New Value</p>', 'text');
    }

    /** @test */
    public function can_ultilise_filters_when_writing_request_data()
    {
        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('New'),
        ]);

        $data = [
            'content' => [
                'text' => 'New Value',
            ],
        ];

        $this->assertSettingValueEquals('Initial Value', 'text');

        $this->applySettingData($data);

        $this->assertSettingValueEquals('*** Value', 'text');
    }

    /** @test */
    public function disabled_fields_dont_write_request_data()
    {
        $this->field->setDisabled();

        $data = [
            'content' => [
                'text' => 'Updated Value'
            ]
        ];

        $this->assertSettingValueEquals('Initial Value', 'text');

        $this->applySettingData($data);

        // Current State
        $this->assertSettingValueEquals('Updated Value', 'text');

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $this->assertSettingValueEquals('Initial Value', 'text');
    }

    /** @test */
    public function read_only_fields_dont_write_request_data()
    {
        $this->field->setReadonly();

        $data = [
            'content' => [
                'text' => 'Updated Value'
            ]
        ];

        $this->assertSettingValueEquals('Initial Value', 'text');

        $this->applySettingData($data);

        // Current State
        $this->assertSettingValueEquals('Updated Value', 'text');

        $this->markTestIncomplete('This is saving data, despite being field being read only. Furthermore, there is no way to test if the field actually is read only.');

        // Desired State
        $this->assertSettingValueEquals('Initial Value', 'text');
    }
}
