<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\TextField;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\Model;
use Yadda\Enso\Tests\Concerns\ModelTrans;
use Yadda\Enso\Tests\Concerns\WordFilter;

class SectionTest extends BaseFieldTest
{
    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new Model([
            'text' => 'Initial Value'
        ]);

        $this->field = (new TextField('text'))
            ->setDefaultValue('Default Value');

        $this->setUpConfig();
    }

    /**
     * Data Reading
     */

    /** @test */
    public function has_correct_component()
    {
        $this->assertEquals('enso-field-text', $this->field->getComponent());
    }

    /** @test */
    public function reads_unaltered_form_data()
    {
        $data = $this->field->getFormData($this->model);

        $this->assertEquals('Initial Value', $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_falsey_but_valid()
    {
        $this->model = new Model(['text' => '0']);

        $data = $this->field->getFormData($this->model);

        $this->assertEquals('0', $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_not_set()
    {
        $this->model = new Model();

        $data = $this->field->getFormData($this->model);

        // Current State
        $this->assertEquals(null, $data);

        $this->markTestIncomplete('This should be fetching a default Value');

        // Desired State
        $this->assertEquals('Default Value', $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_null()
    {
        $this->model = new Model(['text' => null]);

        $data = $this->field->getFormData($this->model);

        // Current State
        $this->assertEquals(null, $data);

        $this->markTestIncomplete('This should be fetching a default Value');

        // Desired State
        $this->assertEquals('Default Value', $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_empty_string()
    {
        $this->model = new Model(['text' => '']);

        $data = $this->field->getFormData($this->model);

        // Current State
        $this->assertEquals('', $data);

        $this->markTestIncomplete('This should be fetching a default Value');

        // Desired State
        $this->assertEquals('Default Value', $data);
    }

    /** @test */
    public function reads_altered_form_data()
    {
        $this->field->setAlterFormDataCallback(function ($value) {
            return 'Modified ' . $value;
        });

        $data = $this->field->getFormData($this->model);

        $this->assertEquals('Modified Initial Value', $data);
    }

    /**
     * Data Writing
     */

    /** @test */
    public function writes_unaltered_request_data()
    {
        $data = [
            'main' => [
                'text' => 'New Value',
            ],
        ];

        $this->assertEquals('Initial Value', $this->model->text);

        $this->field->applyRequestData($this->model, $data);

        $this->assertEquals('New Value', $this->model->text);
    }

    /** @test */
    public function writes_request_data_when_data_is_falsey_but_valid()
    {
        $data = [
            'main' => [
                'text' => '0',
            ],
        ];

        $this->assertEquals('Initial Value', $this->model->text);

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('Default Value', $this->model->text);

        $this->markTestIncomplete('This functionality is incorrect. It should save "0"');

        // Desired State
        $this->assertEquals('0', $this->model->text);
    }

    /** @test */
    public function does_not_write_request_data_when_data_is_not_set()
    {
        $data = [
            'main' => [],
        ];

        $this->assertEquals('Initial Value', $this->model->text);

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('Default Value', $this->model->text);

        $this->markTestIncomplete('It shouldnt be setting the modifying the value');

        // Desired State
        $this->assertEquals('Initial Value', $this->model->text);
    }

    /** @test */
    public function writes_request_data_when_data_is_null()
    {
        $data = [
            'main' => [
                'text' => null,
            ],
        ];

        $this->assertEquals('Initial Value', $this->model->text);

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('Default Value', $this->model->text);

        $this->markTestIncomplete('This should be saving null');

        // Desired State
        $this->assertEquals(null, $this->model->text);
    }

    /** @test */
    public function writes_request_data_when_data_is_emtpy_string()
    {
        $data = [
            'main' => [
                'text' => '',
            ],
        ];

        $this->assertEquals('Initial Value', $this->model->text);

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('Default Value', $this->model->text);

        $this->markTestIncomplete('This should be saving null');

        // Desired State
        $this->assertEquals(null, $this->model->text);
    }

    /** @test */
    public function can_ultilise_alter_data_callbacks_when_writing_request_data()
    {
        $data = [
            'main' => [
                'text' => 'New Value',
            ],
        ];

        $this->field->setAlterRequestDataCallback(function ($value) {
            return 'Altered ' . $value;
        });

        $this->assertEquals('Initial Value', $this->model->text);

        $this->field->applyRequestData($this->model, $data);

        $this->assertEquals('Altered New Value', $this->model->text);
    }

    /** @test */
    public function can_sanitize_data_when_writing_request_data()
    {
        $data = [
            'main' => [
                'text' => 'New Value',
            ],
        ];

        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true)
            ->setPurifyHtml(true);

        $sanitized = $this->field->getRequestData($data);

        $this->assertEquals('<p>New Value</p>', $sanitized);
    }

    /** @test */
    public function can_ultilise_filters_when_writing_request_data()
    {
        $data = [
            'main' => [
                'text' => 'New Test',
            ],
        ];

        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('New'),
            'mask_test' => (new WordFilter)->setWord('Test'),
        ]);

        $this->section->applyRequestData($this->model, $data);

        $this->assertEquals('*** ****', $this->model->text);
    }

    /** @test */
    public function disabled_fields_dont_write_request_data()
    {
        $this->field->setDisabled();

        $data = [
            'main' => [
                'text' => 'Updated Value'
            ]
        ];

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('Updated Value', $this->model->text);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $this->assertEquals('Initial Value', $this->model->text);
    }

    /** @test */
    public function read_only_fields_dont_write_request_data()
    {
        $this->field->setReadonly();

        $data = [
            'main' => [
                'text' => 'Updated Value'
            ]
        ];

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('Updated Value', $this->model->text);

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is read only.');

        // Desired State
        $this->assertEquals('Initial Value', $this->model->text);
    }
}
