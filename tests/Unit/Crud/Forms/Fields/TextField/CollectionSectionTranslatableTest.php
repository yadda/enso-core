<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\TextField;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\WordFilter;
use Yadda\Enso\Tests\Traits\IsSettingTest;

/**
 * CollectionSections get and set data differently enought form
 * regular sections that you can't just pass in a model
 * and directly set/get the data and have the results you would
 * get running it through the crud. It needs to take a step back
 * up the chain and have calls made directly on the Section object.
 */
class CollectionSectionTranslatableTest extends BaseFieldTest
{
    use DatabaseMigrations, IsSettingTest;

    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setSetting('text', ["en" => "Initial En Value", "es" => "Initial Es Value"]);

        $this->field = (new TextField('text'))
            ->setDefaultValue('Default Value');

        $this->setUpTranslatableCollectionDataConfig();
    }

    /**
     * Data Reading
     */

    /** @test */
    public function reads_unaltered_form_data()
    {
        $data = $this->getSettingFormData();

        $expected_output = ["en" => "Initial En Value", "es" => "Initial Es Value"];

        // Current State
        $this->assertEquals(['text' => ['en' => 'Default Value']], $data);

        $this->markTestIncomplete('Settings Currently Cannot return Translatable form data');

        // Desired State
        $this->assertEquals(['text' => $expected_output], $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_falsey_but_valid()
    {
        $this->setSetting('text', '0');

        $this->assertSettingValueEquals('0', 'text');

        $data = $this->getSettingFormData();

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.

        // Current State
        $expected_output = ['text' => [
            "en" => "Default Value"
        ]];

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This should be returning "0"');

        // Desired State
        $expected_output = ['text' => [
            "en" => "0"
        ]];

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_not_set()
    {
        $this->removeSetting('text');

        $this->assertSettingDoesNotExist('text');

        $data = $this->getSettingFormData();

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = ['text' => [
            "en" => "Default Value",
        ]];

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_null()
    {
        $this->setSetting('text', null);

        $this->assertSettingValueEquals(null, 'text');

        $data = $this->getSettingFormData();

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = ['text' => [
            "en" => "Default Value",
        ]];

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_empty_string()
    {
        $this->setSetting('text', '');

        $this->assertSettingValueEquals('', 'text');

        $data = $this->getSettingFormData();

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = ['text' => [
            "en" => "Default Value",
        ]];

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_altered_form_data()
    {
        $this->field->setAlterFormDataCallback(function ($value) {
            return array_map(function ($single) {
                return 'Modified ' . $single;
            }, $value);
        });

        $data = $this->getSettingFormData();

        // Current State
        $expected_output = ['text' => ['en' => 'Modified Default Value']];
        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('Cannot properly read this value from the setting via getFormData');

        // Desired State
        $expected_output = ['text' => ['en' => 'Initial En Value', 'es' => 'Initial Es Value']];
        $this->assertEquals($expected_output, $data);
    }

    /**
     * Data Writing
     */

    /** @test */
    public function writes_unaltered_request_data()
    {
        $data = [
            'content' => [
                'text' => ['en' => 'New En Value', 'es' => 'New Es Value'],
            ],
        ];

        $this->applySettingData($data);

        $expected_output = ['en' => 'New En Value', 'es' => 'New Es Value'];
        $this->assertSettingValueEquals($expected_output, 'text');
    }

    /** @test */
    public function writes_request_data_when_data_is_falsey_but_valid()
    {
        $data = [
            'content' => [
                'text' => [],
            ],
        ];

        $this->applySettingData($data);

        // Current State
        $expected_output = ['en' => 'Default Value'];
        $this->assertSettingValueEquals($expected_output, 'text');

        $this->markTestIncomplete('This should\'t do this. I would expect it to set the value to null or empty array, or delete the setting.');

        // Desired State
        $expected_output = ['en' => []];
        $this->assertSettingValueEquals($expected_output, 'text');
    }

    /** @test */
    public function does_not_write_request_data_when_data_is_not_set()
    {
        $data = [
            'content' => [],
        ];

        $this->applySettingData($data);

        // Current State
        $expected_output = ['en' => 'Default Value'];
        $this->assertSettingValueEquals($expected_output, 'text');

        $this->markTestIncomplete('The initial state should be unchanged');

        // Desired State
        $this->assertSettingValueEquals(["en" => "Initial En Value", "es" => "Initial Es Value"], 'text');
    }

    /** @test */
    public function writes_request_data_when_data_is_null()
    {
        $data = [
            'content' => [
                'text' => null,
            ],
        ];

        $this->applySettingData($data);

        // Current State
        $expected_output = ['en' => 'Default Value'];
        $this->assertSettingValueEquals($expected_output, 'text');

        $this->markTestIncomplete('This should\'t do this. I would expect it to set the value to null or empty array, or delete the setting.');

        // Desired State
        $expected_output = ['en' => null];
        $this->assertSettingValueEquals($expected_output, 'text');
    }

    /** @test */
    public function writes_request_data_when_data_is_emtpy_string()
    {
        $data = [
            'content' => [
                'text' => '',
            ],
        ];

        $this->applySettingData($data);

        // Current State
        $expected_output = ['en' => 'Default Value'];
        $this->assertSettingValueEquals($expected_output, 'text');

        $this->markTestIncomplete('This should\'t do this. I would expect it to set the value to null or empty array, or delete the setting.');

        // Desired State
        $expected_output = ['en' => null];
        $this->assertSettingValueEquals($expected_output, 'text');
    }

    /** @test */
    public function can_ultilise_alter_data_callbacks_when_writing_request_data()
    {
        $this->field->setAlterRequestDataCallback(function ($value) {
            return array_map(function ($single) {
                return 'Altered ' . $single;
            }, $value);
        });

        $data = [
            'content' => [
                'text' => ['en' => 'New En Value', 'es' => 'New Es Value'],
            ],
        ];

        $this->applySettingData($data);

        $expected_output = ['en' => 'Altered New En Value', 'es' => 'Altered New Es Value'];
        $this->assertSettingValueEquals($expected_output, 'text');
    }

    /** @test */
    public function can_sanitize_data_when_writing_request_data()
    {
        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true)
            ->setPurifyHtml(true);

        $data = [
            'content' => [
                'text' => ['en' => 'New En Value', 'es' => 'New Es Value'],
            ],
        ];

        $this->applySettingData($data);

        $expected_output = ['en' => '<p>New En Value</p>', 'es' => '<p>New Es Value</p>'];
        $this->assertSettingValueEquals($expected_output, 'text');
    }

    /** @test */
    public function can_ultilise_filters_when_writing_request_data()
    {
        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('New'),
        ]);
        ;

        $data = [
            'content' => [
                'text' => ['en' => 'New En Value', 'es' => 'New Es Value'],
            ],
        ];

        $this->applySettingData($data);

        $expected_output = ['en' => '*** En Value', 'es' => '*** Es Value'];
        $this->assertSettingValueEquals($expected_output, 'text');
    }

    /** @test */
    public function disabled_fields_dont_write_request_data()
    {
        $this->field->setDisabled();

        $data = [
            'content' => [
                'text' => ['en' => 'New En Value', 'es' => 'New Es Value'],
            ],
        ];

        $this->applySettingData($data);

        // Current State
        $expected_output = ['en' => 'New En Value', 'es' => 'New Es Value'];
        $this->assertSettingValueEquals($expected_output, 'text');

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $expected_output = ['en' => 'Initial En Value', 'es' => 'Initial Es Value'];
        $this->assertSettingValueEquals($expected_output, 'text');
    }

    /** @test */
    public function read_only_fields_dont_write_request_data()
    {
        $this->field->setReadonly();

        $data = [
            'content' => [
                'text' => ['en' => 'New En Value', 'es' => 'New Es Value'],
            ],
        ];

        $this->applySettingData($data);

        // Current State
        $expected_output = ['en' => 'New En Value', 'es' => 'New Es Value'];
        $this->assertSettingValueEquals($expected_output, 'text');

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is read only.');

        // Desired State
        $expected_output = ['en' => 'Initial En Value', 'es' => 'Initial Es Value'];
        $this->assertSettingValueEquals($expected_output, 'text');
    }
}
