<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\EmailField;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Yadda\Enso\Crud\Exceptions\CrudException;
use Yadda\Enso\Crud\Forms\Fields\EmailField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\WordFilter;

/**
 * CollectionSections get and set data differently enought form
 * regular sections that you can't just pass in a model
 * and directly set/get the data and have the results you would
 * get running it through the crud. It needs to take a step back
 * up the chain and have calls made directly on the Section object.
 */
class CollectionSectionTest extends BaseFieldTest
{
    use DatabaseMigrations;

    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->field = (new EmailField('email'))
            ->setDefaultValue('default@email.com');

        $this->setUpCollectionDataConfig();
    }

    /** @test */
    public function gets_collection_section_form_data_correctly()
    {
        $this->setSetting('email', 'initial@email.com');

        $expected_output = [
            'email' => 'initial@email.com',
        ];

        $this->assertEquals($expected_output, $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class)));
    }

    /** @test */
    public function gets_altered_collection_section_form_data()
    {
        $this->setSetting('email', 'initial@email.com');

        $this->field->setAlterFormDataCallback(function ($value) {
            return 'modified.' . $value;
        });

        $expected_output = [
            'email' => 'modified.initial@email.com',
        ];

        $data = $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class));

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function gets_default_form_data_when_settings_doesnt_exist()
    {
        $expected_output = [
            'email' => null,
        ];

        $data = $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class));

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default@email.com" in place of null');
    }

    /** @test */
    public function gets_collection_section_form_data_default_when_null()
    {
        $this->setSetting('email', null);

        $expected_output = [
            'email' => null,
        ];

        $data = $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class));

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default@email.com" in place of null');
    }

    /** @test */
    public function gets_collection_section_form_data_default_when_empty_string()
    {
        $this->setSetting('email', '');

        $expected_output = [
            'email' => '',
        ];

        $data = $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class));

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default@email.com"');
    }

    /** @test */
    public function applies_data_through_a_collection_section()
    {
        $setting_instance = new \Yadda\Enso\Settings\Settings;

        $data = [
            'content' => [
                'email' => 'new@email.com',
            ],
        ];

        $this->section->applyRequestData($setting_instance, $data);

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);

        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('new@email.com', $setting->decoded);
    }

    /** @test */
    public function disabled_fields_dont_write_data()
    {
        $this->setSetting('email', 'initial@email.com');
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        $this->field->setDisabled();

        $data = [
            'content' => [
                'email' => 'updated@email.com'
            ]
        ];

        $this->section->applyRequestData($setting_repository, $data);

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);

        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('updated@email.com', $setting->decoded);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function readonly_fields_dont_write_data()
    {
        $this->setSetting('email', 'initial@email.com');
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        $this->field->setReadonly();

        $data = [
            'content' => [
                'email' => 'updated@email.com'
            ]
        ];

        $this->section->applyRequestData($setting_repository, $data);

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);

        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('updated@email.com', $setting->decoded);

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function updates_a_model_with_altered_input_data_through_a_collection_section()
    {
        $this->setSetting('email', 'initial@email.com');
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        $data = [
            'content' => [
                'email' => 'new@email.com',
            ],
        ];

        $this->field->setAlterRequestDataCallback(function ($value) {
            return 'altered.' . $value;
        });

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);

        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('initial@email.com', $setting->decoded);

        $this->section->applyRequestData($setting_repository, $data);

        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('altered.new@email.com', $setting->decoded);
    }

    /** @test */
    public function sanitization_can_be_used_on_request_data_through_a_collection_section()
    {
        $this->setSetting('email', 'initial@email.com');
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        $data = [
            'content' => [
                'email' => 'new@email.com',
            ],
        ];

        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true);

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);

        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('initial@email.com', $setting->decoded);

        $this->section->applyRequestData($setting_repository, $data);

        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('<p>new@email.com</p>', $setting->decoded);
    }

    /** @test */
    public function applies_filters_to_request_data_through_a_collection_section()
    {
        $this->setSetting('email', 'initial@email.com');
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        $data = [
            'content' => [
                'email' => 'new@email.com',
            ],
        ];

        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('new'),
        ]);

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);

        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('initial@email.com', $setting->decoded);

        $this->expectException(CrudException::class);

        $this->section->applyRequestData($setting_repository, $data);
    }

    /** @test */
    public function updates_a_model_through_a_collection_section_with_default_value_when_given_null_input_data()
    {
        $this->setSetting('email', 'initial@email.com');
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        $data = [
            'content' => [
                'email' => null,
            ],
        ];
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);

        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('initial@email.com', $setting->decoded);

        $this->section->applyRequestData($setting_repository, $data);

        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('default@email.com', $setting->decoded);

        $this->markTestIncomplete('Not Sure it should be doing this');
    }

    /** @test */
    public function updates_a_model_through_a_collection_section_with_default_value_when_given_empty_string_input_data()
    {
        $this->setSetting('email', 'initial@email.com');
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        $data = [
            'content' => [
                'email' => '',
            ],
        ];
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);

        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('initial@email.com', $setting->decoded);

        $this->section->applyRequestData($setting_repository, $data);

        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('default@email.com', $setting->decoded);

        $this->markTestIncomplete('Not Sure it should be doing this');
    }

    /**
     * Translatable CollectionSection
     */

    /** @test */
    public function gets_translatable_form_data_from_a_collection_section()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en@email.com",
            "es" => "es@email.com",
        ];

        $this->setSetting('email', [
            "en" => "en@email.com",
            "es" => "es@email.com",
        ]);

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $this->markTestIncomplete('Cannot properly read this value from the setting via getFormData');

        // $this->assertEquals(['email' => $initial_state], $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class)));
    }

    /** @test */
    public function gets_translatable_default_form_data_when_setting_doesnt_exist()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $setting_repository = new \Yadda\Enso\Settings\Settings;

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = [
            'email' => [
                "en" => "default@email.com",
            ],
        ];

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNull($setting);

        $data = $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class));

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('It would be useful to be able so set an array of default values.');
    }

    /** @test */
    public function gets_translatable_default_form_data_from_a_collection_section_when_data_is_null()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $this->setSetting('email', null);
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = [
            'email' => [
                "en" => "default@email.com",
            ],
        ];

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals(null, $setting->decoded);

        $data = $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class));

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('It would be useful to be able so set an array of default values.');
    }

    /** @test */
    public function gets_translatable_default_form_data_from_a_collection_section_when_data_is_empty_string()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $this->setSetting('email', '');
        $setting_repository = new \Yadda\Enso\Settings\Settings;

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = [
            'email' => [
                "en" => "default@email.com",
            ],
        ];

        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals('', $setting->decoded);

        $data = $this->section->getFormData(resolve(\Yadda\Enso\Settings\Contracts\Model::class));

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('It would be useful to be able so set an array of default values.');
    }

    /** @test */
    public function applies_translatable_request_data_through_a_collection_section()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en@email.com",
            "es" => "es@email.com",
        ];
        $this->setSetting('email', $initial_state);


        $setting_repository = new \Yadda\Enso\Settings\Settings;

        // Test initial state
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $data = [
            'content' => [
                'email' => [
                    'en' => 'new.en@email.com',
                    'es' => 'new.es@email.com',
                ],
            ],
        ];

        $expected_output = [
            'en' => 'new.en@email.com',
            'es' => 'new.es@email.com',
        ];

        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);
    }

    /** @test */
    public function disabled_fields_dont_write_data_through_a_collection_section()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en@email.com",
            "es" => "es@email.com",
        ];
        $this->setSetting('email', $initial_state);

        $this->field->setDisabled();

        // Test initial state
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $data = [
            'content' => [
                'email' => [
                    'en' => 'new.en@email.com',
                    'es' => 'new.es@email.com',
                ],
            ],
        ];

        $expected_output = [
            'en' => 'new.en@email.com',
            'es' => 'new.es@email.com',
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;
        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function read_only_fields_dont_write_data_through_a_collection_section()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en@email.com",
            "es" => "es@email.com",
        ];
        $this->setSetting('email', $initial_state);

        $this->field->setReadonly();

        // Test initial state
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $data = [
            'content' => [
                'email' => [
                    'en' => 'new.en@email.com',
                    'es' => 'new.es@email.com',
                ],
            ],
        ];

        $expected_output = [
            'en' => 'new.en@email.com',
            'es' => 'new.es@email.com',
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;
        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function can_sanitize_translatable_data_when_writing_data()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en@email.com",
            "es" => "es@email.com",
        ];
        $this->setSetting('email', $initial_state);

        // Test initial state
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true);

        $data = [
            'content' => [
                'email' => [
                    'en' => 'new.en@email.com',
                    'es' => 'new.es@email.com',
                ],
            ],
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;
        $this->section->applyRequestData($setting_repository, $data);

        $expected_output = [
            'en' => '<p>new.en@email.com</p>',
            'es' => '<p>new.es@email.com</p>',
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;
        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);
    }

    /** @test */
    public function can_ultilise_alter_data_callbacks_for_translatable_data_when_writing_data()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en@email.com",
            "es" => "es@email.com",
        ];
        $this->setSetting('email', $initial_state);

        // Test initial state
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $this->field->setAlterRequestDataCallback(function ($value) {
            return array_map(function ($single) {
                return 'altered.' . $single;
            }, $value);
        });

        $data = [
            'content' => [
                'email' => [
                    'en' => 'new.en@email.com',
                    'es' => 'new.es@email.com',
                ],
            ],
        ];

        $expected_output = [
            'en' => 'new.en@email.com',
            'es' => 'new.es@email.com',
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;
        $this->section->applyRequestData($setting_repository, $data);

        $expected_output = [
            'en' => 'altered.new.en@email.com',
            'es' => 'altered.new.es@email.com',
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;
        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);
    }

    /** @test */
    public function can_ultilise_filters_for_translatable_data_when_writing_data()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en@email.com",
            "es" => "es@email.com",
        ];
        $this->setSetting('email', $initial_state);

        // Test initial state
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('New'),
        ]);;

        $data = [
            'content' => [
                'email' => [
                    'en' => 'new.en@email.com',
                    'es' => 'new.es@email.com',
                ],
            ],
        ];

        $expected_output = [
            'en' => 'new.en@email.com',
            'es' => 'new.es@email.com',
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;

        $this->expectException(CrudException::class);

        $this->section->applyRequestData($setting_repository, $data);

        $expected_output = [
            'en' => '***.en@email.com',
            'es' => '***.es@email.com',
        ];

        $setting_repository = new \Yadda\Enso\Settings\Settings;
        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);
    }

    /** @test */
    public function applies_translatable_request_data_through_a_collection_section_when_data_is_null()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en@email.com",
            "es" => "es@email.com",
        ];
        $this->setSetting('email', $initial_state);

        $setting_repository = new \Yadda\Enso\Settings\Settings;

        // Test initial state
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $data = [
            'content' => [
                'email' => null,
            ],
        ];

        $expected_output = [
            'en' => 'default@email.com',
        ];

        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);

        $this->markTestIncomplete('This should\'t do this. I would expect it to set the value to null, or delete the setting.');
    }

    /** @test */
    public function applies_translatable_request_data_through_a_collection_section_when_data_is_empty_string()
    {
        $this->setUpTranslatableCollectionDataConfig();

        $initial_state = [
            "en" => "en@email.com",
            "es" => "es@email.com",
        ];

        $this->setSetting('email', $initial_state);

        $setting_repository = new \Yadda\Enso\Settings\Settings;

        // Test initial state
        $setting_instance = resolve(\Yadda\Enso\Settings\Contracts\Model::class);
        $setting = $setting_instance::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($initial_state, $setting->decoded);

        $data = [
            'content' => [
                'email' => '',
            ],
        ];

        $expected_output = [
            'en' => 'default@email.com',
        ];

        $this->section->applyRequestData($setting_repository, $data);

        $setting = get_class($setting_instance)::where('slug', 'email')->first();
        $this->assertNotNull($setting);
        $this->assertEquals($expected_output, $setting->decoded);

        $this->markTestIncomplete('This should\'t do this. I would expect it to set the value to "", or delete the setting.');
    }
}
