<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\EmailField;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Exceptions\CrudException;
use Yadda\Enso\Crud\Forms\Fields\EmailField;
use Yadda\Enso\Tests\Concerns\Field as BaseField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\ModelJson;
use Yadda\Enso\Tests\Concerns\ModelJsonTrans;
use Yadda\Enso\Tests\Concerns\WordFilter;

/**
 * JsonDataSections get and set data differently enought form
 * regular sections that you can't just pass in a model
 * and directly set/get the data and have the results you would
 * get running it through the crud. It needs to take a step back
 * up the chain and have calls made directly on the Section object.
 */
class JsonDataSectionTest extends BaseFieldTest
{
    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->field = (new EmailField('email'))
            ->setDefaultValue('default@email.com');

        $this->setUpJsonDataConfig();
    }

    /** @test */
    public function gets_json_data_section_form_data_correctly()
    {
        $model = new ModelJson(['content' => ['email' => 'initial@email.com']]);

        $expected_output = [
            'email' => 'initial@email.com',
        ];

        $this->assertEquals($expected_output, $this->section->getFormData($model));
    }

    /** @test */
    public function gets_altered_json_data_section_form_data()
    {
        $model = new ModelJson(['content' => ['email' => 'initial@email.com']]);

        $this->field->setAlterFormDataCallback(function ($value) {
            return 'modified.' . $value;
        });

        $expected_output = [
            'email' => 'modified.initial@email.com',
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function gets_json_data_section_from_data_when_source_column_not_available()
    {
        $model = new ModelJson;

        $expected_output = [
            'email' => null,
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default@email.com" in place of null');
    }

    /** @test */
    public function gets_json_data_section_from_data_when_source_field_not_available()
    {
        $model = new ModelJson(['content' => []]);

        $expected_output = [
            'email' => null,
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default@email.com" in place of null');
    }

    /** @test */
    public function gets_json_data_section_form_data_default_when_null()
    {
        $model = new ModelJson(['content' => ['email' => null]]);

        $expected_output = [
            'email' => null,
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default@email.com" in place of null');
    }

    /** @test */
    public function gets_json_data_section_form_data_default_when_empty_string()
    {
        $model = new ModelJson(['content' => ['email' => '']]);

        $expected_output = [
            'email' => '',
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default@email.com"');
    }


    /** @test */
    public function applies_data_through_a_json_data_section()
    {
        $model = new ModelJson;

        $data = [
            'content' => [
                'email' => 'Text Value',
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('Text Value', $model->content['email']);
    }

    /** @test */
    public function disabled_fields_dont_write_data()
    {
        $model = new ModelJson(['content' => ['email' => 'initial@email.com']]);

        $this->field->setDisabled();

        $data = ['content' => ['email' => 'updated@email.com']];

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('updated@email.com', $model->content['email']);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function read_only_fields_dont_write_data()
    {
        $model = new ModelJson(['content' => ['email' => 'initial@email.com']]);

        $this->field->setReadonly();

        $data = ['content' => ['email' => 'updated@email.com']];

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('updated@email.com', $model->content['email']);

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function updates_a_model_with_altered_input_data_through_a_json_data_section()
    {
        $model = new ModelJson(['content' => ['email' => 'initial@email.com']]);
        $data = [
            'content' => [
                'email' => 'new@email.com',
            ],
        ];

        $this->field->setAlterRequestDataCallback(function ($value) {
            return 'altered.' . $value;
        });

        $this->assertEquals('initial@email.com', $model->content['email']);

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('altered.new@email.com', $model->content['email']);
    }

    /** @test */
    public function sanitization_can_be_used_through_a_json_data_section()
    {
        $model = new ModelJson(['content' => ['email' => 'initial@email.com']]);
        $data = [
            'content' => [
                'email' => 'new@email.com',
            ],
        ];

        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true);

        $this->assertEquals('initial@email.com', $model->content['email']);

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('<p>new@email.com</p>', $model->content['email']);
    }

    /** @test */
    public function applies_filters_to_request_data_through_a_json_data_section()
    {
        $model = new ModelJson(['content' => ['email' => 'initial@email.com']]);
        $data = [
            'content' => [
                'email' => 'new@email.com',
            ],
        ];

        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('new'),
        ]);

        $this->assertEquals('initial@email.com', $model->content['email']);

        $this->expectException(CrudException::class);

        $this->section->applyRequestData($model, $data);
    }

    /** @test */
    public function updates_a_model_through_a_json_data_section_with_default_value_when_given_null_input_data()
    {
        $model = new ModelJson(['content' => ['email' => 'initial@email.com']]);
        $data = [
            'content' => [
                'email' => null,
            ],
        ];

        $this->assertEquals('initial@email.com', $model->content['email']);

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('default@email.com', $model->content['email']);

        $this->markTestIncomplete('Not Sure it should be doing this');
    }

    /** @test */
    public function updates_a_model_through_a_json_data_section_with_default_value_when_given_empty_string_input_data()
    {
        $model = new ModelJson(['content' => ['email' => 'initial@email.com']]);
        $data = [
            'content' => [
                'email' => '',
            ],
        ];

        $this->assertEquals('initial@email.com', $model->content['email']);

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('default@email.com', $model->content['email']);

        $this->markTestIncomplete('Not Sure it should be doing this');
    }

    /**
     * Translatable JsonDataSection
     */

    /** @test */
    public function gets_translatable_form_data_from_a_json_data_section()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans([
            'content' => [
                'email' => [
                    "en" => "en@email.com",
                    "es" => "es@email.com",
                ],
            ]
        ]);

        $expected_output = [
            'email' => [
                "en" => "en@email.com",
                "es" => "es@email.com",
            ],
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function disabled_translatable_fields_dont_write_data_through_a_json_data_section()
    {
        $this->setUpTranslatableJsonDataConfig();

        $this->field->setDisabled();

        $model = new ModelJsonTrans([
            'content' => [
                'email' => [
                    'en' => 'en@email.com',
                    'es' => 'es@email.com',
                ]
            ]
        ]);

        $data = [
            'content' => [
                'email' => [
                    'en' => 'new.en@email.com',
                    'es' => 'new.es@email.com',
                ],
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $expected_output = [
            'en' => 'new.en@email.com',
            'es' => 'new.es@email.com',
        ];

        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true)['email']);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function read_only_translatable_fields_dont_write_data_through_a_json_data_section()
    {
        $this->setUpTranslatableJsonDataConfig();

        $this->field->setReadonly();

        $model = new ModelJsonTrans([
            'content' => [
                'email' => [
                    'en' => 'en@email.com',
                    'es' => 'es@email.com',
                ]
            ]
        ]);

        $data = [
            'content' => [
                'email' => [
                    'en' => 'new.en@email.com',
                    'es' => 'new.es@email.com',
                ],
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $expected_output = [
            'en' => 'new.en@email.com',
            'es' => 'new.es@email.com',
        ];

        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true)['email']);

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function can_sanitize_translatable_data_when_writing_data()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans([
            'content' => [
                'email' => [
                    'en' => 'en@email.com',
                    'es' => 'es@email.com',
                ]
            ]
        ]);

        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true);

        $data = [
            'content' => [
                'email' => [
                    'en' => 'new.en@email.com',
                    'es' => 'new.es@email.com',
                ],
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $expected_output = [
            'en' => '<p>new.en@email.com</p>',
            'es' => '<p>new.es@email.com</p>',
        ];

        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true)['email']);
    }

    /** @test */
    public function can_ultilise_alter_data_callbacks_for_translatable_data_when_writing_data()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans([
            'content' => [
                'email' => [
                    'en' => 'en@email.com',
                    'es' => 'es@email.com',
                ]
            ]
        ]);

        $this->field->setAlterRequestDataCallback(function ($value) {
            return array_map(function ($single) {
                return 'altered.' . $single;
            }, $value);
        });

        $data = [
            'content' => [
                'email' => [
                    'en' => 'new.en@email.com',
                    'es' => 'new.es@email.com',
                ],
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $expected_output = [
            'en' => 'altered.new.en@email.com',
            'es' => 'altered.new.es@email.com',
        ];

        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true)['email']);
    }

    /** @test */
    public function can_ultilise_filters_for_translatable_data_when_writing_data()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans([
            'content' => [
                'email' => [
                    'en' => 'en@email.com',
                    'es' => 'es@email.com',
                ]
            ]
        ]);

        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('new'),
        ]);;

        $data = [
            'content' => [
                'email' => [
                    'en' => 'new.en@email.com',
                    'es' => 'new.es@email.com',
                ],
            ],
        ];

        $this->expectException(CrudException::class);

        $this->section->applyRequestData($model, $data);

        $expected_output = [
            'en' => '***.en@email.com',
            'es' => '***.es@email.com',
        ];

        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true)['email']);
    }

    /** @test */
    public function gets_translatable_json_data_section_from_data_when_source_column_not_available()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans;

        $expected_output = [
            'email' => [
                'en' => 'default@email.com',
            ],
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('It would be useful to be able so set an array of default values.');
    }

    /** @test */
    public function gets_translatable_json_data_section_from_data_when_source_field_not_available()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans(['content' => []]);

        $expected_output = [
            'email' => [
                'en' => 'default@email.com',
            ],
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('It would be useful to be able so set an array of default values.');
    }

    /** @test */
    public function gets_translatable_default_form_data_from_a_json_data_section_when_value_is_null()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans([
            'content' => [
                'email' => null,
            ]
        ]);

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = [
            'email' => [
                'en' => 'default@email.com',
            ],
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('It would be useful to be able so set an array of default values.');
    }

    /** @test */
    public function gets_translatable_default_form_data_from_a_json_data_section_when_value_is_empty_string()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans([
            'content' => [
                'email' => "",
            ]
        ]);

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = [
            'email' => [
                'en' => 'default@email.com'
            ],
        ];

        $this->markTestIncomplete('This is broken, it cannont handle empty string');

        // $data = $this->section->getFormData($model);

        // $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function applies_translatable_request_data_through_a_json_data_section()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans;

        $data = [
            'content' => [
                'email' => [
                    'en' => 'en@email.com',
                    'es' => 'es@email.com',
                ],
            ],
        ];

        $expected_output = [
            'en' => 'en@email.com',
            'es' => 'es@email.com',
        ];

        $this->section->applyRequestData($model, $data);

        // Need to use a workaround to circumvent HasTranslations overriding the getAttribute('content') method.
        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true)['email']);
    }

    /** @test */
    public function doesnt_apply_translatable_request_data_through_a_json_data_section_when_data_is_wrong()
    {
        $this->setUpTranslatableJsonDataConfig();

        $initial_data = [
            'email' => [
                'en' => "en@email.com",
                'es' => 'es@email.com',
            ],
        ];

        $model = new ModelJsonTrans([
            'content' => $initial_data
        ]);

        $data = [
            'content' => [
                'email' => 'bad-value@email.com',
            ],
        ];

        // Need to use a workaround to circumvent HasTranslations overriding the getAttribute('content') method.
        $this->assertEquals($initial_data, json_decode($model->getAttributes()['content'], true));

        $this->section->applyRequestData($model, $data);

        // $this->assertEquals($initial_data, json_decode($model->getAttributes()['content'], true));
        $this->markTestIncomplete('This is wrong. It shouldnt be overwriting good values when the values present are bad');
    }

    /** @test */
    public function applies_translatable_request_data_through_a_json_data_section_when_data_is_null()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans;

        $data = [
            'content' => [
                'email' => null,
            ],
        ];

        // Need to use a workaround to circumvent HasTranslations overriding the getAttribute('content') method.
        $this->assertArrayNotHasKey('content', $model->getAttributes());

        $this->section->applyRequestData($model, $data);

        $expected_output = [
            'email' => [
                'en' => 'default@email.com',
            ],
        ];

        // Need to use a workaround to circumvent HasTranslations overriding the getAttribute('content') method.
        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true));

        $this->markTestIncomplete('Probably shouldn\'t do this. I think would expect it so set it to null string');
    }

    /** @test */
    public function applies_translatable_request_data_through_a_json_data_section_when_data_is_empty_string()
    {
        $this->setUpTranslatableJsonDataConfig();

        $model = new ModelJsonTrans;

        $data = [
            'content' => [
                'email' => '',
            ],
        ];

        // Need to use a workaround to circumvent HasTranslations overriding the getAttribute('content') method.
        $this->assertArrayNotHasKey('content', $model->getAttributes());

        $this->section->applyRequestData($model, $data);

        $expected_output = [
            'email' => [
                'en' => 'default@email.com',
            ],
        ];

        // Need to use a workaround to circumvent HasTranslations overriding the getAttribute('content') method.
        $this->assertEquals($expected_output, json_decode($model->getAttributes()['content'], true));

        $this->markTestIncomplete('Probably shouldn\'t do this. I think would expect it so set it to empty string');
    }
}
