<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\EmailField;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\Fields\EmailField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\Model;
use Yadda\Enso\Users\Models\Permission;
use Yadda\Enso\Users\Models\Role;
use Yadda\Enso\Users\Models\User;
use Yadda\Enso\Users\Repositories\PermissionRepository;
use Yadda\Enso\Users\Repositories\RoleRepository;

class AuthTest extends BaseFieldTest
{
    use DatabaseMigrations;

    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        Config::set('enso.users.permission_repository_concrete_class', PermissionRepository::class);
        Config::set('enso.users.role_repository_concrete_class', RoleRepository::class);
        Config::set('enso.users.classes.permission', Permission::class);

        $this->field = (new EmailField('email'))
            ->setDefaultValue('Default Value');

        $this->setUpConfig();
    }

    /** @test */
    public function permissions_can_prevent_guests_from_writing_data()
    {
        Auth::logout();

        $model = new Model(['email' => 'initial@email.com']);

        $this->field->setEditPermission('does-not-exist');

        $data = [
            'main' => [
                'email' => 'updated@email.com'
            ]
        ];

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('updated@email.com', $model->email);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function permissions_can_prevent_users_from_writing_data()
    {
        $user = User::factory()->createOne();

        $model = new Model(['email' => 'initial@email.com']);

        $this->field->setEditPermission('does-not-exist');

        $data = [
            'main' => [
                'email' => 'updated@email.com'
            ]
        ];

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('updated@email.com', $model->email);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function user_with_permissions_can_write_data()
    {
        $user = User::factory()->createOne();
        $role = Role::factory()->createOne();
        $permission = Permission::factory()->createOne(['slug' => 'existing-permission']);

        $user->addPermission($permission);

        $model = new Model(['email' => 'initial@email.com']);

        $this->field->setEditPermission('existing-permission');

        $data = [
            'main' => [
                'email' => 'updated@email.com'
            ]
        ];

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('updated@email.com', $model->email);
    }
}
