<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields;

use Yadda\Enso\Crud\Forms\Fields\OrderableRelationshipField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;

class OrderableRelationshipFieldTest extends BaseFieldTest
{
    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->field = (new OrderableRelationshipField('text'))
            ->setDefaultValue('default-value');

        $this->setUpConfig();
    }

    /** @test */
    public function has_correct_component()
    {
        $this->assertEquals('enso-field-orderable-relationship', $this->field->getComponent());
    }
}
