<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Yadda\Enso\Crud\Forms\Fields\DateTimeField;
use Yadda\Enso\Tests\Concerns\Field as BaseField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\Model;
use Yadda\Enso\Tests\Traits\IsDateTest;

class DateTimeFieldTest extends BaseFieldTest
{
    use IsDateTest;

    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->field = (new DateTimeField('datetime'))
            ->setDefaultValue($this->defaultDateInstance());

        $this->setUpConfig();
    }

    /** @test */
    public function has_correct_component()
    {
        $this->assertEquals('enso-field-datetime', $this->field->getComponent());
    }

    /** @test */
    public function gets_form_data_correctly()
    {
        $model = new Model(['datetime' => $this->dateInstance('2000-01-01 00:00:00')]);

        $data = $this->field->getFormData($model);

        $this->assertEquals($this->validFormResponse($this->dateInstance('2000-01-01 00:00:00')), $data);
    }

    /** @test */
    public function gets_form_data_default_when_null()
    {
        $model = new Model(['datetime' => null]);

        $data = $this->field->getFormData($model);

        $this->assertEquals($this->defaultDateInstance(), $data);
    }

    /** @test */
    public function updates_a_model_with_input_data()
    {
        $model = new Model(['datetime' => $this->dateInstance('2000-01-01 00:00:00')]);

        $data = [
            'main' => [
                'datetime' => $this->validFormResponse(
                    $this->dateInstance('2000-01-01 23:59:59')
                ),
            ],
        ];

        $expected_value = $this->dateInstance('2000-01-01 23:59:59');

        $this->assertEquals($this->dateInstance('2000-01-01 00:00:00'), $model->datetime);

        $this->field->getSection()->applyRequestData($model, $data);

        $this->assertEquals($expected_value, $model->datetime);
    }

    /** @test */
    public function updates_a_model_with_default_value_when_given_empty_string_input_data()
    {
        $model = new Model(['datetime' => $this->dateInstance('2000-01-01 00:00:00')]);
        $data = [
            'main' => [
                'datetime' => '',
            ],
        ];

        $this->assertEquals($this->dateInstance('2000-01-01 00:00:00'), $model->datetime);

        $this->markTestIncomplete('This functionality is Broken. You cannot set a Carbon Instance as a default date.');

        // $this->field->getSection()->applyRequestData($model, $data);

        // $this->assertEquals($this->defaultDateInstance(), $model->datetime);
    }

    /** @test */
    public function updates_a_model_with_default_value_when_given_null_input_data()
    {
        $model = new Model(['datetime' => $this->dateInstance('2000-01-01 00:00:00')]);
        $data = [
            'main' => [
                'datetime' => null,
            ],
        ];

        $this->assertEquals($this->dateInstance('2000-01-01 00:00:00'), $model->datetime);

        $this->markTestIncomplete('This functionality is Broken. You cannot set a Carbon Instance as a default date.');

        // $this->field->getSection()->applyRequestData($model, $data);

        // $this->assertEquals($this->defaultDateInstance(), $model->datetime);
    }
}
