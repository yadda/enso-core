<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\TextareaField;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\Fields\TextareaField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\WordFilter;
use Yadda\Enso\Tests\Traits\IsSettingTest;

/**
 * CollectionSections get and set data differently enought form
 * regular sections that you can't just pass in a model
 * and directly set/get the data and have the results you would
 * get running it through the crud. It needs to take a step back
 * up the chain and have calls made directly on the Section object.
 */
class CollectionSectionTranslatableTest extends BaseFieldTest
{
    use DatabaseMigrations, IsSettingTest;

    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setSetting('textarea', ["en" => "<p>Initial En Value</p>", "es" => "<p>Initial Es Value</p>"]);

        $this->field = (new TextareaField('textarea'))
            ->setDefaultValue('<p>Default Value</p>');

        $this->setUpTranslatableCollectionDataConfig();
    }

    /**
     * Data Reading
     */

    /** @test */
    public function reads_unaltered_form_data()
    {
        $data = $this->getSettingFormData();

        // // Current State
        $expected_output = ['textarea' => [
            "en" => "<p>Default Value</p>"
        ]];

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('Settings Currently Cannot return Translatable form data');

        // Desired State
        $expected_output = ['textarea' => [
            "en" => "<p>Initial En Value</p>", "es" => "<p>Initial Es Value</p>"
        ]];

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_falsey_but_valid()
    {
        $this->setSetting('textarea', '0');

        $this->assertSettingValueEquals('0', 'textarea');

        $data = $this->getSettingFormData();

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.

        // Current State
        $expected_output = ['textarea' => [
            "en" => "<p>Default Value</p>"
        ]];

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('It would be useful to be able so set an array of default values.');

        // Desired State
        $expected_output = ['textarea' => [
            "en" => "0"
        ]];

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_not_set()
    {
        $this->removeSetting('textarea');

        $this->assertSettingDoesNotExist('textarea');

        $data = $this->getSettingFormData();

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = ['textarea' => [
            "en" => "<p>Default Value</p>"
        ]];

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_null()
    {
        $this->setSetting('textarea', null);

        $this->assertSettingValueEquals(null, 'textarea');

        $data = $this->getSettingFormData();

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = ['textarea' => [
            "en" => "<p>Default Value</p>",
        ]];

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_empty_string()
    {
        $this->setSetting('textarea', '');

        $this->assertSettingValueEquals('', 'textarea');

        $data = $this->getSettingFormData();

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = ['textarea' => [
            "en" => "<p>Default Value</p>",
        ]];

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_altered_form_data()
    {
        $this->field->setAlterFormDataCallback(function ($value) {
            // Current State
            $expected_output = ["en" => "<p>Default Value</p>"];
            $this->assertEquals($expected_output, $value);

            $this->markTestIncomplete('Unable to get Formdata properly to further test this');

            // Desired State
            $expected_output = ['textarea' => ['en' => '<p>Initial En Value</p>', 'es' => '<p>Initial Es Value</p>']];
            $this->assertEquals($expected_output, $value);

            return ['en' => '<p>Modified En Value</p>', 'es' => '<p>Modified Es Value</p>'];
        });

        $data = $this->getSettingFormData();

        $expected_output = ['textarea' => ['en' => '<p>Modified En Value</p>', 'es' => '<p>Modified Es Value</p>']];
        $this->assertEquals($expected_output, $data);
    }

    /**
     * Data Writing
     */

    /** @test */
    public function writes_unaltered_request_data()
    {
        $data = [
            'content' => [
                'textarea' => ['en' => '<p>New En Value</p>', 'es' => '<p>New Es Value</p>'],
            ],
        ];

        $this->applySettingData($data);

        $expected_output = ['en' => '<p>New En Value</p>', 'es' => '<p>New Es Value</p>'];
        $this->assertSettingValueEquals($expected_output, 'textarea');
    }

    /** @test */
    public function writes_request_data_when_data_is_falsey_but_valid()
    {
        $data = [
            'content' => [
                'textarea' => [],
            ],
        ];

        $this->applySettingData($data);

        // Current State
        $expected_output = ['en' => '<p>Default Value</p>'];
        $this->assertSettingValueEquals($expected_output, 'textarea');

        $this->markTestIncomplete('This should\'t do this. I would expect it to set the value to null or empty array, or delete the setting.');

        // Desired State
        $expected_output = ['en' => []];
        $this->assertSettingValueEquals($expected_output, 'textarea');
    }

    /** @test */
    public function does_not_write_request_data_when_data_is_not_set()
    {
        $data = [
            'content' => [],
        ];

        $this->applySettingData($data);

        // Current State
        $expected_output = ['en' => '<p>Default Value</p>'];
        $this->assertSettingValueEquals($expected_output, 'textarea');

        $this->markTestIncomplete('The initial state should be unchanged');

        // Desired State
        $expected_output = ["en" => "<p>Initial En Value</p>", "es" => "<p>Initial Es Value</p>"];
        $this->assertSettingValueEquals($expected_output, 'textarea');
    }

    /** @test */
    public function writes_request_data_when_data_is_null()
    {
        $data = [
            'content' => [
                'textarea' => null,
            ],
        ];

        $this->applySettingData($data);

        // Current State
        $expected_output = ['en' => '<p>Default Value</p>'];
        $this->assertSettingValueEquals($expected_output, 'textarea');

        $this->markTestIncomplete('Thsis should\'t do this. I would expect it to set the value to null, or delete the setting.');

        // Desired State
        $expected_output = ['en' => null];
        $this->assertSettingValueEquals($expected_output, 'textarea');
    }

    /** @test */
    public function writes_request_data_when_data_is_emtpy_string()
    {
        $data = [
            'content' => [
                'textarea' => '',
            ],
        ];

        $this->applySettingData($data);

        // Current State
        $expected_output = ['en' => '<p>Default Value</p>'];
        $this->assertSettingValueEquals($expected_output, 'textarea');

        $this->markTestIncomplete('Thsis should\'t do this. I would expect it to set the value to null, or delete the setting.');

        // Desired State
        $expected_output = ['en' => ''];
        $this->assertSettingValueEquals($expected_output, 'textarea');
    }

    /** @test */
    public function can_ultilise_alter_data_callbacks_when_writing_request_data()
    {
        $this->field->setAlterRequestDataCallback(function ($value) {
            // Desired State
            $expected_output = ['en' => '<p>New En Value</p>', 'es' => '<p>New Es Value</p>'];
            $this->assertEquals($expected_output, $value);

            return ['en' => '<p>Altered En Value</p>', 'es' => '<p>Altered Es Value</p>'];
        });

        $data = [
            'content' => [
                'textarea' => ['en' => '<p>New En Value</p>', 'es' => '<p>New Es Value</p>'],
            ],
        ];

        $this->applySettingData($data);

        $expected_output = ['en' => '<p>Altered En Value</p>', 'es' => '<p>Altered Es Value</p>'];
        $this->assertSettingValueEquals($expected_output, 'textarea');
    }

    /** @test */
    public function can_sanitize_data_when_writing_request_data()
    {
        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true)
            ->setPurifyHtml(true);

        $data = [
            'content' => [
                'textarea' => ['en' => 'New En Value', 'es' => 'New Es Value'],
            ],
        ];

        $this->applySettingData($data);

        $expected_output = ['en' => '<p>New En Value</p>', 'es' => '<p>New Es Value</p>'];
        $this->assertSettingValueEquals($expected_output, 'textarea');
    }

    /** @test */
    public function can_ultilise_filters_when_writing_request_data()
    {
        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('New'),
        ]);
        ;

        $data = [
            'content' => [
                'textarea' => ['en' => '<p>New En Value</p>', 'es' => '<p>New Es Value</p>'],
            ],
        ];

        $this->applySettingData($data);

        $expected_output = ['en' => '<p>*** En Value</p>', 'es' => '<p>*** Es Value</p>'];
        $this->assertSettingValueEquals($expected_output, 'textarea');
    }

    /** @test */
    public function disabled_fields_dont_write_data_through_a_collection_section()
    {
        $this->field->setDisabled();

        $data = [
            'content' => [
                'textarea' => ['en' => '<p>New En Value</p>', 'es' => '<p>New Es Value</p>'],
            ],
        ];

        $this->applySettingData($data);

        // Current State
        $expected_output = ['en' => '<p>New En Value</p>', 'es' => '<p>New Es Value</p>'];
        $this->assertSettingValueEquals($expected_output, 'textarea');

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $expected_output = ['en' => '<p>Initial En Value</p>', 'es' => '<p>Initial Es Value</p>'];
        $this->assertSettingValueEquals($expected_output, 'text');
    }

    /** @test */
    public function read_only_fields_dont_write_data_through_a_collection_section()
    {
        $this->field->setReadonly();

        $data = [
            'content' => [
                'textarea' => ['en' => '<p>New En Value</p>', 'es' => '<p>New Es Value</p>'],
            ],
        ];

        $this->applySettingData($data);

        // Current State
        $expected_output = ['en' => '<p>New En Value</p>', 'es' => '<p>New Es Value</p>'];
        $this->assertSettingValueEquals($expected_output, 'textarea');

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is read only.');

        // Desired State
        $expected_output = ['en' => '<p>Initial En Value</p>', 'es' => '<p>Initial Es Value</p>'];
        $this->assertSettingValueEquals($expected_output, 'text');
    }
}
