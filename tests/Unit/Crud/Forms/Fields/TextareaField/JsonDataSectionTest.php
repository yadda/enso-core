<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\TextareaField;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\Fields\TextareaField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\ModelJson;
use Yadda\Enso\Tests\Concerns\ModelJsonTrans;
use Yadda\Enso\Tests\Concerns\WordFilter;

/**
 * JsonDataSections get and set data differently enought form
 * regular sections that you can't just pass in a model
 * and directly set/get the data and have the results you would
 * get running it through the crud. It needs to take a step back
 * up the chain and have calls made directly on the Section object.
 */
class JsonDataSectionTest extends BaseFieldTest
{
    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->field = (new TextareaField('textarea'))
            ->setDefaultValue('<p>Default Value</p>');

        $this->setUpJsonDataConfig();
    }

    /** @test */
    public function gets_json_data_section_form_data_correctly()
    {
        $model = new ModelJson(['content' => ['textarea' => '<p>Initial Value</p>']]);

        $expected_output = [
            'textarea' => '<p>Initial Value</p>',
        ];

        $this->assertEquals($expected_output, $this->section->getFormData($model));
    }

    /** @test */
    public function gets_altered_json_data_section_form_data()
    {
        $model = new ModelJson(['content' => ['textarea' => '<p>Initial Value</p>']]);

        $this->field->setAlterFormDataCallback(function ($value) {
            $this->assertEquals('<p>Initial Value</p>', $value);

            return '<p>Modified Value</p>';
        });

        $expected_output = [
            'textarea' => '<p>Modified Value</p>',
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function gets_json_data_section_from_data_when_source_column_not_available()
    {
        $model = new ModelJson;

        $expected_output = [
            'textarea' => null,
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "Default Value" in place of null');
    }

    /** @test */
    public function gets_json_data_section_from_data_when_source_field_not_available()
    {
        $model = new ModelJson(['content' => []]);

        $expected_output = [
            'textarea' => null,
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "Default Value" in place of null');
    }

    /** @test */
    public function gets_json_data_section_form_data_when_data_falsey_but_should_still_be_used()
    {
        $model = new ModelJson(['content' => ['textarea' => '0']]);

        $expected_output = [
            'textarea' => '0',
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function gets_json_data_section_form_data_default_when_null()
    {
        $model = new ModelJson(['content' => ['textarea' => null]]);

        $expected_output = [
            'textarea' => null,
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "Default Value" in place of null');
    }

    /** @test */
    public function gets_json_data_section_form_data_default_when_empty_string()
    {
        $model = new ModelJson(['content' => ['textarea' => '']]);

        $expected_output = [
            'textarea' => '',
        ];

        $data = $this->section->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "Default Value"');
    }

    /** @test */
    public function applies_data_through_a_json_data_section()
    {
        $model = new ModelJson;

        $data = [
            'content' => [
                'textarea' => '<p>New Value</p>',
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('<p>New Value</p>', $model->content['textarea']);
    }

    /** @test */
    public function disabled_fields_dont_write_data()
    {
        $model = new ModelJson(['content' => ['textarea' => '<p>Initial Value</p>']]);

        $this->field->setDisabled();

        $data = ['content' => ['textarea' => '<p>Updated Value</p>']];

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('<p>Updated Value</p>', $model->content['textarea']);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function read_only_fields_dont_write_data()
    {
        $model = new ModelJson(['content' => ['textarea' => '<p>Initial Value</p>']]);

        $this->field->setReadonly();

        $data = ['content' => ['textarea' => '<p>Updated Value</p>']];

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('<p>Updated Value</p>', $model->content['textarea']);

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function updates_a_model_with_altered_input_data_through_a_json_data_section()
    {
        $model = new ModelJson(['content' => ['textarea' => '<p>Initial Value</p>']]);
        $data = [
            'content' => [
                'textarea' => '<p>New Value</p>',
            ],
        ];

        $this->field->setAlterRequestDataCallback(function ($value) {
            $this->assertEquals('<p>New Value</p>', $value);

            return '<p>Altered Value</p>';
        });

        $this->assertEquals('<p>Initial Value</p>', $model->content['textarea']);

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('<p>Altered Value</p>', $model->content['textarea']);
    }

    /** @test */
    public function sanitizes_request_data_through_a_json_data_section()
    {
        $model = new ModelJson(['content' => ['textarea' => '<p>Initial Value</p>']]);
        $data = [
            'content' => [
                'textarea' => 'New Value',
            ],
        ];

        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true)
            ->setPurifyHtml(true);

        $this->assertEquals('<p>Initial Value</p>', $model->content['textarea']);

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('<p>New Value</p>', $model->content['textarea']);
    }

    /** @test */
    public function applies_filters_to_request_data_through_a_json_data_section()
    {
        $model = new ModelJson(['content' => ['textarea' => '<p>Initial Value</p>']]);
        $data = [
            'content' => [
                'textarea' => '<p>New Value</p>',
            ],
        ];

        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('New'),
            'mask_test' => (new WordFilter)->setWord('Value'),
        ]);

        $this->assertEquals('<p>Initial Value</p>', $model->content['textarea']);

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('<p>*** *****</p>', $model->content['textarea']);
    }

    /** @test */
    public function updates_a_model_through_a_json_data_section_with_0_input_data()
    {
        $model = new ModelJson(['content' => ['textarea' => '<p>Initial Value</p>']]);
        $data = [
            'content' => [
                'textarea' => '0',
            ],
        ];

        $this->assertEquals('<p>Initial Value</p>', $model->content['textarea']);

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('<p>Default Value</p>', $model->content['textarea']);

        $this->markTestIncomplete('This functionality is incorrect. It should return "<p>0</p>"');
    }

    /** @test */
    public function updates_a_model_through_a_json_data_section_with_default_value_when_given_null_input_data()
    {
        $model = new ModelJson(['content' => ['textarea' => '<p>Initial Value</p>']]);
        $data = [
            'content' => [
                'textarea' => null,
            ],
        ];

        $this->assertEquals('<p>Initial Value</p>', $model->content['textarea']);

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('<p>Default Value</p>', $model->content['textarea']);

        $this->markTestIncomplete('Not Sure it should be doing this');
    }

    /** @test */
    public function updates_a_model_through_a_json_data_section_with_default_value_when_given_empty_string_input_data()
    {
        $model = new ModelJson(['content' => ['textarea' => '<p>Initial Value</p>']]);
        $data = [
            'content' => [
                'textarea' => '',
            ],
        ];

        $this->assertEquals('<p>Initial Value</p>', $model->content['textarea']);

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('<p>Default Value</p>', $model->content['textarea']);

        $this->markTestIncomplete('Not Sure it should be doing this');
    }
}
