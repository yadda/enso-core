<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\TextareaField;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\Fields\TextareaField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\Model;
use Yadda\Enso\Users\Models\Permission;
use Yadda\Enso\Users\Models\Role;
use Yadda\Enso\Users\Models\User;
use Yadda\Enso\Users\Repositories\PermissionRepository;
use Yadda\Enso\Users\Repositories\RoleRepository;

class AuthTest extends BaseFieldTest
{
    use DatabaseMigrations;

    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        Config::set('enso.users.permission_repository_concrete_class', PermissionRepository::class);
        Config::set('enso.users.role_repository_concrete_class', RoleRepository::class);
        Config::set('enso.users.classes.permission', Permission::class);

        $this->model = new Model(['textarea' => '<p>Initial Value</p>']);

        $this->field = (new TextareaField('textarea'))
            ->setDefaultValue('Default Value');

        $this->setUpConfig();
    }

    /** @test */
    public function user_with_permissions_can_write_data()
    {
        $user = User::factory()->createOne();
        $role = Role::factory()->createOne();
        $permission = Permission::factory()->createOne(['slug' => 'existing-permission']);

        $user->addPermission($permission);

        $this->field->setEditPermission('existing-permission');

        $data = [
            'main' => [
                'textarea' => '<p>Updated Value</p>'
            ]
        ];

        $this->field->applyRequestData($this->model, $data);

        $this->assertEquals('<p>Updated Value</p>', $this->model->textarea);
    }

    /** @test */
    public function guests_can_not_write_data()
    {
        $this->field->setEditPermission('does-not-exist');

        $data = [
            'main' => [
                'textarea' => '<p>Updated Value</p>'
            ]
        ];

        $this->assertTrue(Auth::guest());

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('<p>Updated Value</p>', $this->model->textarea);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $this->assertEquals('<p>Initial Value</p>', $this->model->text);
    }

    /** @test */
    public function permissions_can_prevent_users_from_writing_data()
    {
        $user = User::factory()->createOne();

        $this->field->setEditPermission('does-not-exist');

        $data = [
            'main' => [
                'textarea' => '<p>Updated Value</p>'
            ]
        ];

        $this->field->applyRequestData($this->model, $data);

        $this->assertEquals('<p>Updated Value</p>', $this->model->textarea);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $this->assertEquals('<p>Initial Value</p>', $this->model->text);
    }
}
