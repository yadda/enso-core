<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\TextareaField;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\Fields\TextareaField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\WordFilter;
use Yadda\Enso\Tests\Traits\IsSettingTest;

/**
 * CollectionSections get and set data differently enought form
 * regular sections that you can't just pass in a model
 * and directly set/get the data and have the results you would
 * get running it through the crud. It needs to take a step back
 * up the chain and have calls made directly on the Section object.
 */
class CollectionSectionTest extends BaseFieldTest
{
    use DatabaseMigrations, IsSettingTest;

    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setSetting('textarea', '<p>Initial Value</p>');

        $this->field = (new TextareaField('textarea'))
            ->setDefaultValue('<p>Default Value</p>');

        $this->setUpCollectionDataConfig();
    }

    /**
     * Data Reading
     */

    /** @test */
    public function reads_unaltered_form_data()
    {
        $expected_output = ['textarea' => '<p>Initial Value</p>'];

        $this->assertEquals($expected_output, $this->getSettingFormData());
    }

    /** @test */
    public function reads_form_data_when_source_value_is_falsey_but_valid()
    {
        $this->setSetting('textarea', '0');

        $data = $this->getSettingFormData();

        $expected_output = ['textarea' => '0'];
        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_not_set()
    {
        $this->removeSetting('textarea');

        $data = $this->getSettingFormData();

        // Current State
        $expected_output = ['textarea' => null];
        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "<p>Default Value</p>"');

        // Desired State
        $expected_output = ['textarea' => '<p>Default Value</p>'];
        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_null()
    {
        $this->setSetting('textarea', null);

        $data = $this->getSettingFormData();

        // Current State
        $expected_output = ['textarea' => null];
        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "<p>Default Value</p>"');

        // Desired State
        $expected_output = ['textarea' => '<p>Default Value</p>'];
        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_empty_string()
    {
        $this->setSetting('textarea', '');

        $data = $this->getSettingFormData();

        // Current State
        $expected_output = ['textarea' => ''];
        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "<p>Default Value</p>"');

        // Default State
        $expected_output = ['textarea' => '<p>Default Value</p>'];
        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_altered_form_data()
    {
        $this->field->setAlterFormDataCallback(function ($value) {
            $this->assertEquals('<p>Initial Value</p>', $value);

            return '<p>Modified Value</p>';
        });

        $data = $this->getSettingFormData();

        $expected_output = ['textarea' => '<p>Modified Value</p>'];
        $this->assertEquals($expected_output, $data);
    }

    /**
     * Data Writing
     */

    /** @test */
    public function applies_data_through_a_collection_section()
    {
        $data = [
            'content' => [
                'textarea' => '<p>New Value</p>',
            ],
        ];

        $this->assertSettingValueEquals('<p>Initial Value</p>', 'textarea');

        $this->applySettingData($data);

        $this->assertSettingValueEquals('<p>New Value</p>', 'textarea');
    }

    /** @test */
    public function writes_request_data_when_data_is_falsey_but_valid()
    {
        $data = [
            'content' => [
                'textarea' => '0',
            ],
        ];

        $this->assertSettingValueEquals('<p>Initial Value</p>', 'textarea');

        $this->applySettingData($data);

        // Current State
        $this->assertSettingValueEquals('<p>Default Value</p>', 'textarea');

        $this->markTestIncomplete('This functionality is incorrect. It should return "0"');

        // Desired State
        $this->assertSettingValueEquals('0', 'textarea');
    }

    /** @test */
    public function does_not_write_request_data_when_data_is_not_set()
    {
        $data = [
            'content' => [],
        ];

        $this->assertSettingValueEquals('<p>Initial Value</p>', 'textarea');

        $this->applySettingData($data);

        // Current State
        $this->assertSettingValueEquals('<p>Default Value</p>', 'textarea');

        $this->markTestIncomplete('This functionality is incorrect. It should not be altering the setting');

        // Desired State
        $this->assertSettingValueEquals('<p>Initial Value</p>', 'textarea');
    }

    /** @test */
    public function writes_request_data_when_data_is_null()
    {
        $data = [
            'content' => [
                'textarea' => null,
            ],
        ];

        $this->assertSettingValueEquals('<p>Initial Value</p>', 'textarea');

        $this->applySettingData($data);

        // Current State
        $this->assertSettingValueEquals('<p>Default Value</p>', 'textarea');

        $this->markTestIncomplete('This functionality is incorrect. It should be emptying/deleting the setting');

        // Desired State
        $this->assertSettingValueEquals(null, 'textarea');
    }

    /** @test */
    public function writes_request_data_when_data_is_emtpy_string()
    {
        $data = [
            'content' => [
                'textarea' => '',
            ],
        ];

        $this->assertSettingValueEquals('<p>Initial Value</p>', 'textarea');

        $this->applySettingData($data);

        // Current State
        $this->assertSettingValueEquals('<p>Default Value</p>', 'textarea');

        $this->markTestIncomplete('This functionality is incorrect. It should be emptying/deleting the setting');

        // Desired State
        $this->assertSettingValueEquals('', 'textarea');
    }

    /** @test */
    public function can_ultilise_alter_data_callbacks_when_writing_request_data()
    {
        $this->field->setAlterRequestDataCallback(function ($value) {
            $this->assertEquals('<p>New Value</p>', $value);
            return '<p>Altered Value</p>';
        });

        $data = [
            'content' => [
                'textarea' => '<p>New Value</p>',
            ],
        ];

        $this->assertSettingValueEquals('<p>Initial Value</p>', 'textarea');

        $this->applySettingData($data);

        $this->assertSettingValueEquals('<p>Altered Value</p>', 'textarea');
    }

    /** @test */
    public function can_sanitize_data_when_writing_request_data()
    {
        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true)
            ->setPurifyHtml(true);

        $data = [
            'content' => [
                'textarea' => 'New Value',
            ],
        ];

        $this->assertSettingValueEquals('<p>Initial Value</p>', 'textarea');

        $this->applySettingData($data);

        $this->assertSettingValueEquals('<p>New Value</p>', 'textarea');
    }

    /** @test */
    public function can_ultilise_filters_when_writing_request_data()
    {
        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('New'),
        ]);

        $data = [
            'content' => [
                'textarea' => '<p>New Value</p>',
            ],
        ];

        $this->assertSettingValueEquals('<p>Initial Value</p>', 'textarea');

        $this->applySettingData($data);

        $this->assertSettingValueEquals('<p>*** Value</p>', 'textarea');
    }

    /** @test */
    public function disabled_fields_dont_write_request_data()
    {
        $this->field->setDisabled();

        $data = [
            'content' => [
                'textarea' => '<p>Updated Value</p>'
            ]
        ];

        $this->assertSettingValueEquals('<p>Initial Value</p>', 'textarea');

        $this->applySettingData($data);

        // Current State
        $this->assertSettingValueEquals('<p>Updated Value</p>', 'textarea');

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $this->assertSettingValueEquals('<p>Initial Value</p>', 'textarea');
    }

    /** @test */
    public function read_only_fields_dont_write_request_data()
    {
        $this->field->setReadonly();

        $data = [
            'content' => [
                'textarea' => '<p>Updated Value</p>'
            ]
        ];

        $this->assertSettingValueEquals('<p>Initial Value</p>', 'textarea');

        $this->applySettingData($data);

        // Current State
        $this->assertSettingValueEquals('<p>Updated Value</p>', 'textarea');

        $this->markTestIncomplete('This is saving data, despite being field being read only. Furthermore, there is no way to test if the field actually is read only.');

        // Desired State
        $this->assertSettingValueEquals('<p>Initial Value</p>', 'textarea');
    }
}
