<?php


namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\TextareaField;

use Yadda\Enso\Crud\Forms\Fields\TextareaField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\ModelTrans;
use Yadda\Enso\Tests\Concerns\WordFilter;

class SectionTranslatableTest extends BaseFieldTest
{
    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new ModelTrans([
            'textarea' => [
                'en' => '<p>En Value</p>',
                'es' => '<p>Es Value</p>',
            ],
        ]);

        $this->field = (new TextareaField('textarea'))
            ->setDefaultValue('<p>Default Value</p>');

        $this->setUpTranslatableConfig();
    }

    /**
     * Data Reading
     */

    /** @test */
    public function reads_unaltered_form_data()
    {
        $expected_output = [
            "en" => "<p>En Value</p>",
            "es" => "<p>Es Value</p>",
        ];

        $data = $this->field->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_valid_but_falsey()
    {
        $this->model = new ModelTrans([
            'textarea' => [],
        ]);

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = ['en' => '<p>Default Value</p>'];

        $data = $this->field->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_not_set()
    {
        $this->model = new ModelTrans;

        $data = $this->field->getFormData($this->model);

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = ['en' => '<p>Default Value</p>'];

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_null()
    {
        $this->model = new ModelTrans([
            'textarea' => null,
        ]);

        $data = $this->field->getFormData($this->model);

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = ['en' => '<p>Default Value</p>'];

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_empty_string()
    {
        $this->model = new ModelTrans([
            'textarea' => "",
        ]);

        $data = $this->field->getFormData($this->model);

        $this->assertEquals($this->field->getDefaultValue(), $data);
    }

    /** @test */
    public function reads_altered_form_data()
    {
        $this->field->setAlterFormDataCallback(function ($value) {
            $this->assertEquals([
                'en' => '<p>En Value</p>',
                'es' => '<p>Es Value</p>',
            ], $value);

            return [
                'en' => '<p>Modified En Value</p>',
                'es' => '<p>Modified Es Value</p>',
            ];
        });

        $expected_output = [
            "en" => "<p>Modified En Value</p>",
            "es" => "<p>Modified Es Value</p>",
        ];

        $data = $this->field->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /**
     * Data Writing
     */


    /** @test */
    public function writes_unaltered_request_data()
    {
        $data = [
            'main' => [
                'textarea' => [
                    'en' => '<p>En Value</p>',
                    'es' => '<p>Es Value</p>',
                ],
            ],
        ];

        $this->field->applyRequestData($this->model, $data);

        $this->assertEquals('<p>En Value</p>', $this->model->getTranslation('textarea', 'en', true));
        $this->assertEquals('<p>Es Value</p>', $this->model->getTranslation('textarea', 'es', true));
    }

    /** @test */
    public function writes_request_data_when_data_is_falsey_but_valid()
    {
        $data = [
            'main' => [
                'textarea' => [],
            ],
        ];

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('<p>Default Value</p>', $this->model->getTranslation('textarea', 'en', false));
        $this->assertEquals('<p>Es Value</p>', $this->model->getTranslation('textarea', 'es', false));

        $this->markTestIncomplete('It should be setting the values to null or empty arrays');

        // Desired State
        $this->assertEquals(null, $this->model->getTranslation('textarea', 'en', false));
        $this->assertEquals(null, $this->model->getTranslation('textarea', 'es', false));
    }

    /** @test */
    public function applies_translatable_request_data_correctly_when_data_is_null()
    {
        $data = [
            'main' => [
                'textarea' => null,
            ],
        ];

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('<p>Default Value</p>', $this->model->getTranslation('textarea', 'en', false));
        $this->assertEquals('<p>Es Value</p>', $this->model->getTranslation('textarea', 'es', false));

        $this->markTestIncomplete('It should be setting the values to null or empty arrays');

        // Desired State
        $this->assertEquals(null, $this->model->getTranslation('text', 'en', false));
        $this->assertEquals(null, $this->model->getTranslation('text', 'es', false));
    }

    /** @test */
    public function writes_request_data_when_data_is_emtpy_string()
    {
        $data = [
            'main' => [
                'textarea' => '',
            ],
        ];

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('<p>Default Value</p>', $this->model->getTranslation('textarea', 'en', false));
        $this->assertEquals('<p>Es Value</p>', $this->model->getTranslation('textarea', 'es', false));

        $this->markTestIncomplete('It should be setting the values to empty strings or arrays');

        // Current State
        $this->assertEquals(null, $this->model->getTranslation('textarea', 'en', false));
        $this->assertEquals(null, $this->model->getTranslation('textarea', 'es', false));
    }

    /** @test */
    public function can_ultilise_alter_data_callbacks_when_writing_request_data()
    {
        $this->field->setAlterRequestDataCallback(function ($value) {
            $this->assertEquals([
                'en' => '<p>New En Value</p>',
                'es' => '<p>New Es Value</p>',
            ], $value);

            return [
                'en' => '<p>Altered En Value</p>',
                'es' => '<p>Altered Es Value</p>',
            ];
        });

        $data = [
            'main' => [
                'textarea' => [
                    'en' => '<p>New En Value</p>',
                    'es' => '<p>New Es Value</p>',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        $this->assertEquals('<p>Altered En Value</p>', $this->model->getTranslation('textarea', 'en', true));
        $this->assertEquals('<p>Altered Es Value</p>', $this->model->getTranslation('textarea', 'es', true));
    }

    /** @test */
    public function can_sanitize_data_when_writing_request_data()
    {
        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true)
            ->setPurifyHtml(true);

        $data = [
            'main' => [
                'textarea' => [
                    'en' => 'New En Value',
                    'es' => 'New Es Value',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        $this->assertEquals('<p>New En Value</p>', $this->model->getTranslation('textarea', 'en', true));
        $this->assertEquals('<p>New Es Value</p>', $this->model->getTranslation('textarea', 'es', true));
    }

    /** @test */
    public function can_ultilise_filters_when_writing_request_data()
    {
        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('New'),
        ]);
        ;

        $data = [
            'main' => [
                'textarea' => [
                    'en' => '<p>New En Value</p>',
                    'es' => '<p>New Es Value</p>',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        $this->assertEquals('<p>*** En Value</p>', $this->model->getTranslation('textarea', 'en', true));
        $this->assertEquals('<p>*** Es Value</p>', $this->model->getTranslation('textarea', 'es', true));
    }

    /** @test */
    public function disabled_fields_dont_write_request_data()
    {
        $this->field->setDisabled();

        $data = [
            'main' => [
                'textarea' => [
                    'en' => '<p>New En Value</p>',
                    'es' => '<p>New Es Value</p>',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('<p>New En Value</p>', $this->model->getTranslation('textarea', 'en', true));
        $this->assertEquals('<p>New Es Value</p>', $this->model->getTranslation('textarea', 'es', true));

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $this->assertEquals('<p>En Value</p>', $this->model->getTranslation('textarea', 'en', true));
        $this->assertEquals('<p>Es Value</p>', $this->model->getTranslation('textarea', 'es', true));
    }

    /** @test */
    public function read_only_fields_dont_write_request_data()
    {
        $this->field->setReadonly();

        $data = [
            'main' => [
                'textarea' => [
                    'en' => '<p>New En Value</p>',
                    'es' => '<p>New Es Value</p>',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('<p>New En Value</p>', $this->model->getTranslation('textarea', 'en', true));
        $this->assertEquals('<p>New Es Value</p>', $this->model->getTranslation('textarea', 'es', true));

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $this->assertEquals('<p>En Value</p>', $this->model->getTranslation('textarea', 'en', true));
        $this->assertEquals('<p>Es Value</p>', $this->model->getTranslation('textarea', 'es', true));
    }
}
