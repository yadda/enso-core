<?php

namespace Yadda\Enso\Tests\Unit\Menu;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\EnsoMenu\Item;
use Yadda\Enso\Tests\TestCase;
use Yadda\Enso\Users\Models\Permission;
use Yadda\Enso\Users\Models\Role;
use Yadda\Enso\Users\Models\User;
use Yadda\Enso\Users\Repositories\PermissionRepository;
use Yadda\Enso\Users\Repositories\RoleRepository;

class AuthMenuTests extends TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();

        Config::set('enso.users.permission_repository_concrete_class', PermissionRepository::class);
        Config::set('enso.users.role_repository_concrete_class', RoleRepository::class);
        Config::set('enso.users.classes.permission', Permission::class);
        Config::set('enso.users.classes.role', get_class(resolve(Role::class)));
    }

    /** @test */
    public function a_guest_cannot_see_the_item_with_a_given_single_permission()
    {
        $item = new Item([
            'restrict' => 'permission-name',
        ]);

        $this->assertFalse($item->isAllowed());
    }

    /** @test */
    public function superusers_can_see_the_item_without_the_given_single_permission()
    {
        $user = User::factory()->createOne();
        $role = Role::factory()->createOne(['slug' => 'superuser']);

        $user->addRole($role);

        $user = $user->fresh()->load('roles');

        Auth::login($user);

        $item = new Item([
            'restrict' => 'existing-permission',
        ]);

        $this->assertTrue($item->isAllowed());
    }

    /** @test */
    public function a_user_without_the_given_single_permission_cannot_see_the_item()
    {
        $user = User::factory()->createOne();

        Auth::login($user);

        $item = new Item([
            'restrict' => 'existing-permission',
        ]);

        $this->assertFalse($item->isAllowed());
    }

    /** @test */
    public function a_user_with_the_given_single_permission_can_see_the_item()
    {
        $user = User::factory()->createOne();
        $permission = Permission::factory()->createOne(['slug' => 'existing-permission']);

        $user->addPermission($permission);

        Auth::login($user);

        $item = new Item([
            'restrict' => 'existing-permission',
        ]);

        $this->assertTrue($item->isAllowed());
    }

    /** @test */
    public function a_guest_cannot_see_the_item_with_a_given_array_permissions()
    {
        $item = new Item([
            'restrict' => ['first-permission', 'second-permission'],
        ]);

        $this->assertFalse($item->isAllowed());
    }

    /** @test */
    public function superusers_can_see_the_item_without_the_given_array_permissions()
    {
        $user = User::factory()->createOne();
        $role = Role::factory()->createOne(['slug' => 'superuser']);

        $user->addRole($role);

        $user = $user->fresh()->load('roles');

        Auth::login($user);

        $item = new Item([
            'restrict' => ['first-permission', 'second-permission'],
        ]);

        $this->assertTrue($item->isAllowed());
    }

    /** @test */
    public function a_user_without_any_of_the_given_array_permissions_cannot_see_the_item()
    {
        $user = User::factory()->createOne();

        Auth::login($user);

        $item = new Item([
            'restrict' => ['first-permission', 'second-permission'],
        ]);

        $this->assertFalse($item->isAllowed());
    }

    /** @test */
    public function a_user_with_any_of_the_given_array_permissions_can_see_the_item()
    {
        $user = User::factory()->createOne();
        $permission = Permission::factory()->createOne(['slug' => 'first-permission']);

        $user->addPermission($permission);

        Auth::login($user);

        $item = new Item([
            'restrict' => ['first-permission', 'second-permission'],
        ]);

        $this->assertTrue($item->isAllowed());
    }

    /** @test */
    public function a_user_with_all_of_the_given_array_permissions_can_see_the_item()
    {
        $user = User::factory()->createOne();
        $permission = Permission::factory()->createOne(['slug' => 'first-permission']);
        $permission_2 = Permission::factory()->createOne(['slug' => 'second-permission']);

        $user->addPermissions([$permission, $permission_2]);

        Auth::login($user);

        $item = new Item([
            'restrict' => ['first-permission', 'second-permission'],
        ]);

        $this->assertTrue($item->isAllowed());
    }
}
