<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Yadda\Enso\Categories\Models\Category;
use Yadda\Enso\Tests\Concerns\Database\Factories\CategoryFactory;
use Yadda\Enso\Tests\TestCase;

class CategoryTest extends TestCase
{
    use DatabaseMigrations;

    protected function categoryJsonData($attributes = [])
    {
        return array_merge([
            "main" => [
                "name" => 'Test Category',
                "slug" => 'test-category'
            ],
        ], $attributes);
    }

    protected function makeStoreRequest($user, $data = [])
    {
        return $this
            ->actingAs($user)
            ->post(
                'admin/categories',
                $this->categoryJsonData($data),
                [
                    'HTTP_X-Requested-With' => 'XMLHttpRequest'
                ]
            );
    }

    protected function makeUpdateRequest($user, $id, $data = [])
    {
        return $this
            ->actingAs($user)
            ->post(
                route('admin.categories.update', $id),
                $this->categoryJsonData($data),
                [
                    'HTTP_X-Requested-With' => 'XMLHttpRequest'
                ],
            );
    }

    /** @test */
    public function categories_can_have_a_name_and_slug()
    {
        Category::factory()->createOne();

        $category = Category::first();

        $this->assertEquals('Test Category', $category->name);
        $this->assertEquals('test-category', $category->slug);
    }

    /** @test */
    public function categories_can_have_child_categories()
    {
        $parent = Category::factory()->createOne([
            'name' => 'Parent Category',
        ]);

        $child = Category::factory()->createOne([
            'name' => 'Child Category'
        ]);

        $parent->appendNode($child);

        $parent = $parent->fresh();

        $this->assertCount(1, $parent->children);
    }

    /** @test */
    public function json_index_route_loads_single_level_by_default()
    {
        $parent = Category::factory()->createOne([
            'name' => 'Parent Category',
        ]);

        $child = Category::factory()->createOne([
            'name' => 'Child Category'
        ]);

        $parent->appendNode($child);

        $response = $this->get('json/categories');
        $data = $response->json();

        $this->assertCount(1, $data['data']);
        $this->assertArrayNotHasKey('children', $data['data'][0]);
    }

    /** @test */
    public function json_index_route_can_load_children()
    {
        $parent = Category::factory()->createOne([
            'name' => 'Parent Category',
        ]);

        $child = Category::factory()->createOne([
            'name' => 'Child Category'
        ]);

        $parent->appendNode($child);

        $response = $this->get('json/categories?include-children=1');
        $data = $response->json();

        $this->assertCount(1, $data['data']);
        $this->assertCount(1, $data['data'][0]['children']);
    }

    /** @test */
    public function json_show_route_gets_a_category()
    {
        $category = Category::factory()->createOne([
            'slug' => 'test-category',
        ]);

        $child = Category::factory()->createOne([
            'name' => 'Child Category'
        ]);

        $category->appendNode($child);

        $response = $this->get('/json/categories/test-category');
        $response->assertStatus(200);

        $data = $response->json();

        $this->assertEquals('test-category', $data['category']['slug']);
        $this->assertArrayNotHasKey('children', $data['category']);
    }

    /** @test */
    public function json_show_route_can_include_children()
    {
        $parent = Category::factory()->createOne([
            'slug' => 'test-category',
        ]);

        $child = Category::factory()->createOne([
            'slug' => 'child-category'
        ]);

        $parent->appendNode($child);

        $response = $this->get('json/categories/test-category?include-children=1');
        $response->assertStatus(200);

        $data = $response->json();

        $this->assertEquals('test-category', $data['category']['slug']);
        $this->assertEquals('child-category', $data['category']['children'][0]['slug']);
    }

    /** @test */
    public function json_create_route_creates_top_level_category_by_default()
    {
        $superuser = $this->makeSuperuser();
        $response = $this->makeStoreRequest($superuser);

        $response->assertStatus(200);
        $this->assertEquals(1, Category::count());
        $category = Category::first();
        $this->assertEquals('Test Category', $category->name);
        $this->assertNull($category->parent_id);
    }

    /** @test */
    public function json_create_route_creates_child_categories_if_requested()
    {
        $parent_category = Category::factory()->createOne();
        $superuser = $this->makeSuperuser();
        $response = $this->makeStoreRequest($superuser, [
            'main' => [
                'name' => 'Child Category',
                'slug' => 'child-category',
            ],
            'nesting' => [
                'child_of' => $parent_category->id,
            ],
        ]);

        $response->assertStatus(200);

        $parent_category = $parent_category->fresh();
        $parent_category->load('children');

        $this->assertCount(1, $parent_category->children);
        $this->assertEquals('Child Category', $parent_category->children->first()->name);
    }

    /** @test */
    public function json_create_route_can_insert_category_after_another_category()
    {
        $first_category = Category::factory()->createOne([
            'name' => 'First Category',
            'slug' => 'first-category',
        ]);
        $second_category = Category::factory()->createOne([
            'name' => 'Second Category',
            'slug' => 'second-category',
        ]);
        $superuser = $this->makeSuperuser();
        $response = $this->makeStoreRequest($superuser, [
            'main' => [
                'name' => 'Third Category',
                'slug' => 'third-category',
            ],
            'nesting' => [
                'insert_after' => $first_category->id,
            ],
        ]);

        $response->assertStatus(200);

        $categories = Category::orderBy('_lft')->get();

        $this->assertEquals('First Category', $categories->get(0)->name);
        $this->assertEquals('Third Category', $categories->get(1)->name);
        $this->assertEquals('Second Category', $categories->get(2)->name);
    }

    /** @test */
    public function json_create_route_can_insert_category_before_another_category()
    {
        $first_category = Category::factory()->createOne([
            'name' => 'First Category',
            'slug' => 'first-category',
        ]);
        $superuser = $this->makeSuperuser();
        $response = $this->makeStoreRequest($superuser, [
            'main' => [
                'name' => 'Second Category',
                'slug' => 'second-category',
            ],
            'nesting' => [
                'insert_before' => $first_category->id,
            ],
        ]);

        $response->assertStatus(200);

        $categories = Category::orderBy('_lft')->get();

        $this->assertEquals('Second Category', $categories->get(0)->name);
        $this->assertEquals('First Category', $categories->get(1)->name);
    }

    /** @test */
    public function json_update_route_can_move_a_category_after_another_category()
    {
        $first_category = Category::factory()->createOne([
            'name' => 'First Category',
            'slug' => 'first-category',
        ]);
        $second_category = Category::factory()->createOne([
            'name' => 'Second Category',
            'slug' => 'second-category',
        ]);
        $superuser = $this->makeSuperuser();
        $response = $this->makeUpdateRequest($superuser, $first_category->id, [
            'main' => [
                'name' => 'First Category',
                'slug' => 'first-category',
            ],
            'nesting' => [
                'insert_after' => $second_category->id,
            ],
        ]);

        $response->assertStatus(200);

        $categories = Category::orderBy('_lft')->get();

        $this->assertEquals('Second Category', $categories->get(0)->name);
        $this->assertEquals('First Category', $categories->get(1)->name);
    }

    /** @test */
    public function json_update_route_can_move_a_category_before_another_category()
    {
        $first_category = Category::factory()->createOne([
            'name' => 'First Category',
            'slug' => 'first-category',
        ]);
        $second_category = Category::factory()->createOne([
            'name' => 'Second Category',
            'slug' => 'second-category',
        ]);
        $superuser = $this->makeSuperuser();
        $response = $this->makeUpdateRequest($superuser, $second_category->id, [
            'main' => [
                'name' => 'Second Category',
                'slug' => 'second-category',
            ],
            'nesting' => [
                'insert_before' => $first_category->id,
            ],
        ]);

        $response->assertStatus(200);

        $categories = Category::orderBy('_lft')->get();

        $this->assertEquals('Second Category', $categories->get(0)->name);
        $this->assertEquals('First Category', $categories->get(1)->name);
    }

    /** @test */
    public function json_update_route_can_move_a_category_under_another_category()
    {
        $first_category = Category::factory()->createOne([
            'name' => 'First Category',
            'slug' => 'first-category',
        ]);
        $second_category = Category::factory()->createOne([
            'name' => 'Second Category',
            'slug' => 'second-category',
        ]);
        $superuser = $this->makeSuperuser();
        $response = $this->makeUpdateRequest($superuser, $second_category->id, [
            'main' => [
                'name' => 'Second Category',
                'slug' => 'second-category',
            ],
            'nesting' => [
                'child_of' => $first_category->id,
            ],
        ]);

        $response->assertStatus(200);

        $category = Category::whereNull('parent_id')->first();

        $this->assertEquals($first_category->id, $category->id);
        $this->assertCount(1, $first_category->children);
        $this->assertEquals($second_category->id, $first_category->children->first()->id);
    }

    /** @test */
    public function deleting_a_parent_item_will_move_its_children_up_into_its_place()
    {
        $parent = Category::factory()->createOne([
            'name' => 'Parent Item',
        ]);

        $child_1 = Category::factory()->createOne([
            'name' => 'Child 1',
        ]);

        $child_2 = Category::factory()->createOne([
            'name' => 'Child 2',
        ]);

        $grandchild_1 = Category::factory()->createOne([
            'name' => 'Grandchild 1',
        ]);

        // Note: Child 2 inserted first.
        $parent->appendNode($child_2);
        $parent->appendNode($child_1);
        $child_1->appendNode($grandchild_1);

        $parent->refresh();

        // Check they're in the order we inserted them in
        $children = $parent->children()->orderBy('_lft')->get();
        $this->assertCount(2, $children);
        $this->assertEquals('Child 2', $children->get(0)->name);
        $this->assertEquals('Child 1', $children->get(1)->name);
        $this->assertEquals('Grandchild 1', $children->get(1)->children->first()->name);

        $parent->delete();

        $categories = Category::whereIsRoot()->orderBy('_lft')->get();

        $this->assertCount(2, $categories);
        $this->assertEquals('Child 2', $categories->get(0)->name);
        $this->assertEquals('Child 1', $categories->get(1)->name);
        $this->assertEquals('Grandchild 1', $categories->get(1)->children->first()->name);
    }
}
