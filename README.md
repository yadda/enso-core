# Ensō Core

A CMS for Laravel

## Installation

See enso-docs.maya.agency

## Testing

```
./enso start
./enso composer install
./enso test
```
