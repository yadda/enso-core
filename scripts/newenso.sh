#!/usr/bin/env bash

# The idea is to create a new Laravel installation, create a .env file and then
# run this script from the root dir of your project. This will install enso-core
# publish the Docker files and boot Docker.

# Get Enso
docker run --rm -it \
  -v $(pwd):/opt \
  -v ~/.ssh:/root/.ssh \
  -w /opt shippingdocker/php-composer:latest \
  composer require yadda/enso-core:0.2

# Publish Docker files
docker run --rm -it \
  -v $(pwd):/opt \
  -w /opt shippingdocker/php-composer:latest \
  php artisan vendor:publish --provider='Yadda\Enso\EnsoServiceProvider' --tag=docker

# If you're a linux user, your files
# may be owned by user root
# Run the following if so:
ls -lah # Check owner of files in current dir
sudo chown -R $USER: . # Change owner:group if needed

# Start Docker
./enso start
