<?php

namespace Yadda\Enso\Users\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginAs extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()
            && $this->user()->hasPermission('impersonate');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
