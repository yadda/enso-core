<?php

namespace Yadda\Enso\Users\Contracts;

interface User
{
    /**
     * Return array data to fill a \Yadda\Enso\Crud\Tables\TextLink cell in an
     * Enso index table
     *
     * @return array
     */
    public function getNameColumnAttribute(): array;

    /**
     * Return a name for this use.
     *
     * @return string
     */
    public function getUsername(): string;

    /**
     * Deretmine whether this use is a Superuser
     *
     * @return boolean
     */
    public function isSuperuser(): bool;
}
