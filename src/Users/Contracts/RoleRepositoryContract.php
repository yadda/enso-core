<?php

namespace Yadda\Enso\Users\Contracts;

use Illuminate\Support\Collection;

interface RoleRepositoryContract
{
    /**
     * Set the current Role list
     *
     * @param Collection    $role_list  Role list to set
     */
    public function setRoleList(Collection $role_list);

    /**
     * Accepts a mixed set of data, and parses it to find the return a set array
     * of ids of all the matching Role items.
     *
     * @param  mixed    $identifiers        data to work from
     * @return array                        ids of matching Roles
     */
    public function findIdsFromMixed($identifiers);

    /**
     * Checks to see whether any of a given, mixed list of roles or identifiers
     * are present within current set of Roles.
     *
     * @param  mixed        $roles          List of roles to look for
     * @param  boolean      $all            Whether to match all or just one
     * @return boolean                      Success status
     */
    public function roleListContainsRoles($roles, $all = false);

    /**
     * Check to see whether the given role list contains a superuser role
     *
     * @return boolean                  Whether roles list contains a superuser
     *                                  role
     */
    public function roleListContainsSuperuser();
}
