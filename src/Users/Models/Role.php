<?php

namespace Yadda\Enso\Users\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Yadda\Enso\Crud\Contracts\IsCrudModel as ContractsIsCrudModel;
use Yadda\Enso\Crud\Traits\IsCrudModel;
use Yadda\Enso\Tests\Concerns\Database\Factories\RoleFactory;
use Yadda\Enso\Users\Interfaces\HasPermissions as HasPermissionsInterface;
use Yadda\Enso\Users\Traits\HasPermissions as HasPermissionsTrait;

/**
 * For user with the HasRoles and HasRolesAndPermissions traits.
 */
class Role extends Eloquent implements HasPermissionsInterface, ContractsIsCrudModel
{
    use HasPermissionsTrait,
        HasFactory,
        IsCrudModel;

    protected $fillable = [
        'name',
        'slug',
        'description',
        'parent_id',
        'left_id',
        'right_id'
    ];

    /**
     * Checks whether the current Role is a superuser role
     *
     * @return boolean          Whether role is superuser role
     */
    public function isSuperuserRole()
    {
        return is_null($this->parent_id);
    }

    /**
     * Query filter to return only roles that are superuser roles
     *
     * @param  Builder  $query  Original query
     * @return Builder          Modified query
     */
    public function scopeIsSuperuser($query)
    {
        return $query->whereNull('parent_id');
    }

    // @todo: Create IsHierarchichal trait

    public function getNameColumnAttribute()
    {
        return [
            'text' => $this->name,
            'url'  => route('admin.roles.edit', $this->id),
        ];
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    private static function newFactory()
    {
        return RoleFactory::new();
    }
}
