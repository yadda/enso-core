<?php

namespace Yadda\Enso\Users\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Yadda\Enso\Crud\Contracts\IsCrudModel as ContractsIsCrudModel;
use Yadda\Enso\Crud\Traits\IsCrudModel;
use Yadda\Enso\Tests\Concerns\Database\Factories\PermissionFactory;
use Yadda\Enso\Users\Interfaces\HasRoles as HasRolesInterface;
use Yadda\Enso\Users\Traits\HasRoles as HasRolesTrait;

/**
 * For use with the HasPermissions and HasRolesAndPermissions traits.
 */
class Permission extends Eloquent implements HasRolesInterface, ContractsIsCrudModel
{
    use HasRolesTrait,
        HasFactory,
        IsCrudModel;

    protected $fillable = [
        'name',
        'slug',
        'description'
    ];

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    private static function newFactory()
    {
        return PermissionFactory::new();
    }
}
