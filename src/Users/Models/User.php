<?php

namespace Yadda\Enso\Users\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Yadda\Enso\Crud\Contracts\IsCrudModel as ContractsIsCrudModel;
use Yadda\Enso\Crud\Traits\IsCrudModel;
use Yadda\Enso\Tests\Concerns\Database\Factories\UserFactory;
use Yadda\Enso\Users\Contracts\User as UserInterface;
use Yadda\Enso\Users\Interfaces\HasPermissions;
use Yadda\Enso\Users\Interfaces\HasRoles;
use Yadda\Enso\Users\Traits\HasRolesAndPermissions;

// @todo: Ensure that saving username and email are also validated to be
//        unique compares to BOTH columns in the database to prevent people from
//        subverting another user's account by entering an email address in a
//        username field.
//
//        Maybe make it so that a username can't be an email address, to
//        prevent pre-signing up with a username of a potential email address?

class User extends Authenticatable implements UserInterface, HasRoles, HasPermissions, ContractsIsCrudModel
{
    use HasRolesAndPermissions,
        IsCrudModel,
        Notifiable,
        HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'display_name',
        'real_name',
        'password',
        'remember_token',
        'updated_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The value to use at this Model's CRUD label.
     *
     * @return string
     */
    public function getCrudLabel(): string
    {
        return $this->getUsername();
    }

    /**
     * Return array data to fill a \Yadda\Enso\Crud\Tables\TextLink cell in an
     * Enso index table
     *
     * @return array
     */
    public function getNameColumnAttribute(): array
    {
        return [
            'text' => $this->username,
            'url'  => route('admin.users.edit', $this->id),
        ];
    }

    /**
     * Return a name for this use.
     *
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username ?? '';
    }

    /**
     * Deretmine whether this use is a Superuser
     *
     * @return boolean
     */
    public function isSuperuser(): bool
    {
        return $this->hasRole('superuser');
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    private static function newFactory()
    {
        return UserFactory::new();
    }
}
