<?php

use Yadda\Enso\Users\Models\Role;
use Yadda\Enso\Users\Models\User;

use Yadda\Enso\Users\Tests\TestCase;

class HasRolesTraitTest extends TestCase
{
    public function test_expected_failures()
    {
        $user = $this->factory->of(User::class)->times(1)->create();

        // Check expected exception on bad input
        $this->setExpectedException('ErrorException');
        $user->addRoles((object) ['invalid' => 'Invalid']);
    }

    /**
     * Tests that an instance of HasRole can add roles to it's relations list by
     * either instance of Role, role id or role slug.
     *
     * @return void
     */
    public function test_a_user_can_add_a_single_role()
    {
        $this->assertEquals(0, User::count());

        $user = $this->factory->of(User::class)->times(1)->create();
        $this->assertEquals($user->roles()->count(), 0);

        // Add Role by instance of Role
        $role = $this->factory->of(Role::class)->times(1)->create();
        $user->addRole($role);
        $this->assertEquals($user->roles->count(), 1);

        // Add role by role id
        $role = $this->factory->of(Role::class)->times(1)->create();
        $user->addRole($role->id);
        $this->assertEquals($user->roles->count(), 2);

        // Add role by role slug
        $role = $this->factory->of(Role::class)->times(1)->create();
        $user->addRole($role->slug);
        $this->assertEquals($user->roles->count(), 3);

        // Test that adding a duplicate does not fail nor add a new item
        $user->addRoles($role);
        $this->assertEquals($user->roles->count(), 3);

        // Check expected exception on bad input
        $this->setExpectedException('ErrorException');
        $user->addRoles((object) ['invalid' => 'Invalid']);
    }

    /**
     * Tests that an instance of HasRole can add multiple roles to it's relations,
     * by Collection or Role instances or an array of either instances of Role,
     * role ids, roles slugs, or a mixture of these.
     *
     * @return void
     */
    public function test_a_user_can_add_many_roles()
    {
        $this->assertEquals(0, User::count());

        $user = $this->factory->of(User::class)->times(1)->create();
        $this->assertEquals($user->roles->count(), 0);

        // Add Roles by Collection
        $roles_collection = $this->factory->of(Role::class)->times(2)->create();
        $user->addRoles($roles_collection);
        $this->assertEquals($user->roles->count(), 2);

        // Add roles by array of Role instances
        $role_1 = $this->factory->of(Role::class)->times(1)->create();
        $role_2 = $this->factory->of(Role::class)->times(1)->create();
        $user->addRoles([$role_1, $role_2]);
        $this->assertEquals($user->roles->count(), 4);

        // Add roles by array of Role ids
        $role_1 = $this->factory->of(Role::class)->times(1)->create();
        $role_2 = $this->factory->of(Role::class)->times(1)->create();
        $user->addRoles([$role_1->id, $role_2->id]);
        $this->assertEquals($user->roles->count(), 6);

        // Add roles by array of role slugs
        $role_1 = $this->factory->of(Role::class)->times(1)->create();
        $role_2 = $this->factory->of(Role::class)->times(1)->create();
        $user->addRoles([$role_1->slug, $role_2->slug]);
        $this->assertEquals($user->roles->count(), 8);

        // Add roles by array of mixed types
        $role_1 = $this->factory->of(Role::class)->times(1)->create();
        $role_2 = $this->factory->of(Role::class)->times(1)->create();
        $role_3 = $this->factory->of(Role::class)->times(1)->create();
        $user->addRoles([$role_1, $role_2->id, $role_3->slug]);
        $this->assertEquals($user->roles->count(), 11);

        // Defer to addRole when single provided
        $role_1 = $this->factory->of(Role::class)->times(1)->create();
        $user->addRoles($role_1);
        $this->assertEquals($user->roles->count(), 12);

        // Test that adding a duplicate does not fail nor add a new item
        $role_2 = $this->factory->of(Role::class)->times(1)->create();
        $user->addRoles([$role_1, $role_2]);
        $this->assertEquals($user->roles->count(), 13);
    }

    /**
     * Tests that a user has a given role
     *
     * @return void
     */
    public function test_a_user_has_a_given_role()
    {
        $user = $this->factory->of(User::class)->times(1)->create();
        $role = $this->factory->of(Role::class)->times(1)->create();

        // Check use doesn't have role by various methods. First entry passes
        // true to force refresh the relationship
        $this->assertFalse($user->hasRole($role));
        $this->assertFalse($user->hasRole($role->id));
        $this->assertFalse($user->hasRole($role->slug));

        // Add role to user
        $user->addRoles($role);

        // Add role and check user does have role by various methods. First entry
        // passes true to force refresh the relationship

        $this->assertTrue($user->hasRole($role));
        $this->assertTrue($user->hasRole($role->id));
        $this->assertTrue($user->hasRole($role->slug));
    }

    /**
     * Tests that a user belongs to any one of multiple roles
     *
     * @return void
     */
    public function test_a_user_has_one_of_an_array_of_roles()
    {
        $user = $this->factory->of(User::class)->times(1)->create();
        $roles = $this->factory->of(Role::class)->times(6)->create();

        // Adds last 3 roles, then refreshes the user instance
        $user->addRoles([$roles[3], $roles[4], $roles[5]]);

        // Test one of many from instance
        $this->assertFalse($user->hasRoles([$roles[0], $roles[1]]));
        $this->assertTrue($user->hasRoles([$roles[0], $roles[4]]));

        // Test one of many from id
        $this->assertFalse($user->hasRoles([$roles[1]->id, $roles[2]->id]));
        $this->assertTrue($user->hasRoles([$roles[1]->id, $roles[5]->id]));

        // Test one of many from slug
        $this->assertFalse($user->hasRoles([$roles[0]->slug, $roles[2]->slug]));
        $this->assertTrue($user->hasRoles([$roles[0]->slug, $roles[5]->slug]));

        // Test one of many from mixed
        $this->assertFalse($user->hasRoles([$roles[0], $roles[1]->id, $roles[2]->slug]));
        $this->assertTrue($user->hasRoles([$roles[0], $roles[1]->id, $roles[5]->slug]));

        // Test one of many with Single input
        $this->assertFalse($user->hasRoles($roles[0]));
        $this->assertTrue($user->hasRoles($roles[3]));
    }

    public function test_a_user_has_all_of_an_array_of_roles()
    {
        $user = $this->factory->of(User::class)->times(1)->create();
        $roles = $this->factory->of(Role::class)->times(6)->create();

        // Adds last 3 roles, then refreshes the user instance
        $user->addRoles([$roles[3], $roles[4], $roles[5]]);

        // Test all of from instance
        $this->assertFalse($user->hasAllRoles([$roles[0], $roles[4]]));
        $this->assertTrue($user->hasAllRoles([$roles[3], $roles[4]]));

        // Test all of from id
        $this->assertFalse($user->hasAllRoles([$roles[1]->id, $roles[4]->id]));
        $this->assertTrue($user->hasAllRoles([$roles[3]->id, $roles[4]->id]));

        // Test all of from slug
        $this->assertFalse($user->hasAllRoles([$roles[2]->slug, $roles[5]->slug]));
        $this->assertTrue($user->hasAllRoles([$roles[3]->slug, $roles[5]->slug]));

        // Test all of from mixed
        $this->assertFalse($user->hasAllRoles([$roles[1], $roles[2]->id, $roles[5]->slug]));
        $this->assertTrue($user->hasAllRoles([$roles[3], $roles[4]->id, $roles[5]->slug]));

        // Test all of with Single input
        $this->assertFalse($user->hasAllRoles($roles[0]));
        $this->assertTrue($user->hasAllRoles($roles[3]));
    }

    /**
     * Test that a single role can be removed from an instance of HasRole
     * @return [type] [description]
     */
    public function test_a_user_can_remove_a_single_role()
    {
        $user = $this->factory->of(User::class)->times(1)->create();
        $roles = $this->factory->of(Role::class)->times(4)->create();

        // Add 4 roles, then refreshes the user instance
        $user->addRoles($roles);

        // Test remove by Role instance
        $user->removeRole($roles[0]);
        $this->assertEquals($user->roles()->count(), 3);

        // Test remove by Role instance
        $user->removeRole($roles[1]->id);
        $this->assertEquals($user->roles()->count(), 2);

        // Test remove by Role instance
        $user->removeRole($roles[2]->slug);
        $this->assertEquals($user->roles()->count(), 1);

        // Test that passing a role that the user doesn't have makes no changes
        $user->removeRole($roles[0]);
        $this->assertEquals($user->roles()->count(), 1);
    }

    /**
     * Test removal of roles in bulk
     * @return [type] [description]
     */
    public function test_a_user_can_remove_many_roles()
    {
        $user = $this->factory->of(User::class)->times(1)->create();
        $roles = $this->factory->of(Role::class)->times(14)->create();

        $user->addRoles($roles);

        // Add Roles by Collection
        $roles_collection = collect([$roles[0], $roles[1]]);
        $user->removeRoles($roles_collection);
        $this->assertEquals($user->roles()->count(), 12);

        // Add roles by array of Role instances
        $user->removeRoles([$roles[2], $roles[3]]);
        $this->assertEquals($user->roles()->count(), 10);

        // Add roles by array of Role ids
        $user->removeRoles([$roles[4]->id, $roles[5]->id]);
        $this->assertEquals($user->roles()->count(), 8);

        // Add roles by array of role slugs
        $user->removeRoles([$roles[6]->slug, $roles[7]->slug]);
        $this->assertEquals($user->roles()->count(), 6);

        // Add roles by array of mixed types
        $user->removeRoles([$roles[8], $roles[9]->id, $roles[10]->slug]);
        $this->assertEquals($user->roles()->count(), 3);

        // Defer to addRole when single provide
        $user->removeRoles($roles[11]);
        $this->assertEquals($user->roles()->count(), 2);

        // Test that adding a duplicate does not fail nor add a new item
        $user->removeRoles([$roles[0], $roles[12]]);
        $this->assertEquals($user->roles()->count(), 1);
    }

    /**
     * Test that a user has a superuser role
     * @return void
     */
    public function test_that_a_user_is_a_superuser()
    {
        $superuser_role = $this->factory->of(Role::class)->times(1)->create([
            'left_id' => 1,
            'right_id' => 4
        ]);

        $sub_role = $this->factory->of(Role::class)->times(1)->create([
            'parent_id' => $superuser_role->id,
            'left_id' => 2,
            'right_id' => 3
        ]);

        $superuser = $this->factory->of(User::class)->times(1)->create();
        $superuser->addRole($superuser_role);

        $subuser = $this->factory->of(User::class)->times(1)->create();
        $subuser->addRole($sub_role);

        $this->assertTrue($superuser->isSuperuser());
        $this->assertFalse($subuser->isSuperuser());
    }

    // role model
    // Only ever one root role
    // only su can change edit children permission/state

    // hierarchy tree Trait - make hierarchy trait for it's own tests
}
