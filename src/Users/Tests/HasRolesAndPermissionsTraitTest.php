<?php

use Yadda\Enso\Users\Models\Role;
use Yadda\Enso\Users\Models\User;
use Yadda\Enso\Users\Models\Permission;
use Yadda\Enso\Users\Tests\TestCase;

class HasRolesAndPermissionsTraitTest extends TestCase
{
        /**
         * Test that an instance of hasRolesAndPermissions can have permissions
         * via roles and via direct permissions.
         *
         * @return void
         */
    public function test_a_user_has_a_single_permission_via_roles()
    {
        $user = $this->factory->of(User::class)->create();
        $role = $this->factory->of(Role::class)->create();
        $permission = $this->factory->of(Permission::class)->create();

        $role->addPermission($permission);
        $this->assertTrue($role->hasPermission($permission));

        // Ensures that permission doesn't exist on user
        $this->assertFalse($user->hasPermission($permission));

        $user->addRole($role);

        // Ensure that it does after adding role.
        $this->assertTrue($user->hasPermission($permission));
    }

    /**
     * Tests that a user can have one of many permissions via roles, direct
     * permissions and mixture of the two
     *
     * @return void
     */
    public function test_a_user_has_one_of_an_array_of_permissions_via_roles()
    {
        $permissions = $this->factory->of(Permission::class)->times(6)->create();
        // Make sure role has a parent id so it's not a superuser role
        $role = $this->factory->of(Role::class)->create(['parent_id' => 12]);
        $user = $this->factory->of(User::class)->create();

        $user->addPermissions([$permissions[0], $permissions[1]]);
        $role->addPermissions([$permissions[2], $permissions[3]]);

        // Test through permissions only
        $this->assertFalse($user->hasPermissions([$permissions[4], $permissions[5]]));
        $this->assertTrue($user->hasPermissions([$permissions[0], $permissions[4]]));

        $user->addRole($role);

        // Test through roles only
        $this->assertFalse($user->hasPermissions([$permissions[4], $permissions[5]]));
        $this->assertTrue($user->hasPermissions([$permissions[2], $permissions[4]]));

        // Test through neither, permission only, role only, either
        $this->assertFalse($user->hasPermissions([$permissions[4], $permissions[5]]));
        $this->assertTrue($user->hasPermissions([$permissions[0], $permissions[4]]));
        $this->assertTrue($user->hasPermissions([$permissions[2], $permissions[4]]));
        $this->assertTrue($user->hasPermissions([$permissions[1], $permissions[3]]));
    }

    /**
     * Test that a user can have all permissions in a set via roles, direct
     * permissions and a mixture of the two
     *
     * @return void
     */
    public function test_a_user_has_all_of_an_array_of_permissions_via_roles()
    {
        $user = $this->factory->of(User::class)->create();
        $role = $this->factory->of(Role::class)->create(['parent_id' => 12]);
        $permissions = $this->factory->of(Permission::class)->times(6)->create();

        $user->addPermissions([$permissions[0], $permissions[1]]);
        $role->addPermissions([$permissions[2], $permissions[3]]);

        // Test through permissions only
        $this->assertFalse($user->hasAllPermissions([$permissions[4], $permissions[5]]));
        $this->assertFalse($user->hasAllPermissions([$permissions[0], $permissions[4]]));
        $this->assertTrue($user->hasAllPermissions([$permissions[0], $permissions[1]]));

        $user->addRole($role);

        // Test through roles only
        $this->assertFalse($user->hasAllPermissions([$permissions[4], $permissions[5]]));
        $this->assertFalse($user->hasAllPermissions([$permissions[2], $permissions[4]]));
        $this->assertTrue($user->hasAllPermissions([$permissions[2], $permissions[3]]));

        // Test through neither, permission only, role only, both
        $this->assertFalse($user->hasAllPermissions([$permissions[4], $permissions[5]]));
        $this->assertFalse($user->hasAllPermissions([$permissions[0], $permissions[4]]));
        $this->assertTrue($user->hasAllPermissions([$permissions[0], $permissions[1]]));
        $this->assertFalse($user->hasAllPermissions([$permissions[2], $permissions[4]]));
        $this->assertTrue($user->hasAllPermissions([$permissions[2], $permissions[3]]));
        $this->assertFalse($user->hasAllPermissions([$permissions[0], $permissions[2], $permissions[4]]));
        $this->assertTrue($user->hasAllPermissions([$permissions[0], $permissions[2]]));
    }

    /**
     * Tests the adding of a permission to a role can affect the user it has been
     * applied to.
     *
     * @note:  Currently, this only works with refreshPermissionRepository being
     *         called on the parent model. Not ideal, but the time required to
     *         make a fix for this far outweight the requiredments of anything
     *         more than a callable function.
     *
     * @return void
     */
    public function test_adding_permissions_to_roles_affecting_parent()
    {
        $user = $this->factory->of(User::class)->create();
        $role = $this->factory->of(Role::class)->create(['parent_id' => 12]);
        $permission = $this->factory->of(Permission::class)->create();

        $user->addRole($role);

        $this->assertFalse($user->hasPermission($permission));

        $role->addPermission($permission);
        $user->refreshPermissionRepository();

        $this->assertTrue($user->hasPermission($permission));
    }

    /**
     * Tests the removing of a permission to a role can affect the user it has
     * been applied to.
     *
     * @note:  Currently, this only works with refreshPermissionRepository being
     *         called on the parent model. Not ideal, but the time required to
     *         make a fix for this far outweight the requiredments of anything
     *         more than a callable function.
     *
     * @return void
     */
    public function test_removal_of_permissions_from_roles_affecting_parent()
    {
        $user = $this->factory->of(User::class)->create();
        $role = $this->factory->of(Role::class)->create(['parent_id' => 12]);
        $permission = $this->factory->of(Permission::class)->create();

        $role->addPermission($permission);
        $user->addRole($role);

        $this->assertTrue($user->hasPermission($permission));

        $role->removePermission($permission);
        $user->refreshPermissionRepository();

        $this->assertFalse($user->hasPermission($permission));
    }
}
