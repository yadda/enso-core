<?php

namespace Yadda\Enso\Users\Traits;

use App;
use Illuminate\Support\Collection;
use Yadda\Enso\Users\Contracts\PermissionRepositoryContract;
use Yadda\Enso\Users\Traits\HasPermissions;
use Yadda\Enso\Users\Traits\HasRoles;

/**
 *
 */
trait HasRolesAndPermissions
{
    use HasRoles, HasPermissions;

    protected $all_permissions_repository;

    /**
     * Gets all the permissions from each of the roles of this item
     *
     * @return \Illuminate\Support\Collection List of permissions
     */
    protected function getPermissionsFromRoles()
    {
        $roles = $this->getRoleRepository()->getRoleList();

        // Don't do further work if no roles present
        if ($roles->count() === 0) {
            return new Collection();
        }

        $role_permission_sets = $roles->pluck('permissions')->reverse();
        $base_collection = $role_permission_sets->shift();

        // Reduces Collection of Collections into a single collection
        $role_permissions = $role_permission_sets->reduce(function ($accumulation, $permission_set) {
            if (empty($permission_set) || $permission_set->isEmpty()) {
                return $accumulation;
            }

            return $accumulation->merge($permission_set);
        }, $base_collection);

        return $role_permissions;
    }

    public function getAllPermissions()
    {
        $direct_permissions = $this->getPermissionRepository()->getPermissionList();
        $role_permissions = $this->getPermissionsFromRoles();

        return $direct_permissions->merge($role_permissions);
    }

    /**
     * Sets the permission repository to null to that on it's next call it will
     * recalculate it's permission list;
     *
     * @return HasRolesAndPermissions self
     */
    public function refreshPermissionRepository()
    {
        $this->role_repository = null;
        $this->load('roles', 'roles.permissions');

        $this->permission_repository = null;
        $this->load('permissions');

        $this->all_permissions_repository = null;

        return $this;
    }

    /**
     * Gets all the permissions fo the item
     *
     * @return \Illuminate\Support\Collection List of permissions
     */
    protected function getAllPermissionsRepository()
    {
        if (is_null($this->all_permissions_repository)) {
            $all_permissions = $this->getAllPermissions();

            $this->all_permissions_repository = App::make(PermissionRepositoryContract::class)
                ->setPermissionList($all_permissions);
        }

        return $this->all_permissions_repository;
    }

    /**
     * Test that the current HasPermissions instance has a given permission.
     *
     * @param mixed $permission Permission instance, Permission id, permission slug
     * @param bool  $refresh    Whether to force the model to reload it's relationship
     *
     * @return bool whether HasPermissions has Permission
     */
    public function hasPermission($permission, $refresh = false)
    {
        if (!$this->relationLoaded('roles') || $refresh) {
            $this->load('roles');
            $this->getRoleRepository()->setRoleList($this->roles);
        }

        // Superuser override
        if ($this->isSuperuser()) {
            return true;
        }

        if (!$this->relationLoaded('permissions') || $refresh) {
            $this->load('permissions');
            $this->getPermissionRepository()->setPermissionList($this->permissions);
        }

        if ($refresh) {
            $this->all_permissions_repository = null;
        }

        return $this->getAllPermissionsRepository()->permissionListContainsPermissions($permission);
    }

    /**
     * Tests that the current HasPermissions instance has the given permissions. By default,
     * it will return true if it has at least one of the given list. Setting $all
     * will require that it has all of them before returning true.
     *
     * @param mixed $permissions Permissions, permission ids, or permission slugs to test against
     * @param bool  $all         Whether all given items must match
     * @param bool  $refresh     Whether to force the model to reload it's relationship
     *
     * @return bool Successfull match status
     */
    public function hasPermissions($permissions, $all = false, $refresh = false)
    {
        if (!$this->relationLoaded('roles') || $refresh) {
            $this->load('roles');
            $this->getRoleRepository()->setRoleList($this->roles);
        }

        // Superuser override
        if ($this->isSuperuser()) {
            return true;
        }

        if (!$this->relationLoaded('permissions') || $refresh) {
            $this->load('permissions');
            $this->getPermissionRepository()->setPermissionList($this->permissions);
        }

        if ($refresh) {
            $this->all_permissions_repository = null;
        }

        return $this->getAllPermissionsRepository()->permissionListContainsPermissions($permissions, $all);
    }

    /**
     * Intuitive deferrer for hasPermissions($permissions, true, $refresh)
     *
     * @param mixed $permissions Permissions, permission ids, or permission slugs to test against
     * @param bool  $refresh     Whether to force the model to reload it's relationship
     *
     * @return bool Successfull match status
     */
    public function hasAllPermissions($permissions, $refresh = false)
    {
        return $this->hasPermissions($permissions, true, $refresh);
    }

    /**
     * Adds a given role to the current implementation of HasRoles.
     *
     * Uses the sync command on a relationship with the second arguments set to
     * false. This means that it adds it if it doesn't already exists, but
     * doesn't delete other records while doing so (which would happen if false
     * was left off).
     *
     * @param mixed   $role          Role instance, role id or role slug
     * @param boolean $refresh_roles Role instance, role id or role slug
     *
     * @return HasRoles self
     */
    public function addRole($role, $refresh_roles = true)
    {
        $id_array = $this->getRoleRepository()->findIdsFromMixed($role, true);

        if (!empty($id_array)) {
            $this->roles()->sync($id_array, false);
        }

        if ($refresh_roles) {
            $this->load('roles');
            $this->getRoleRepository()->setRoleList($this->roles);
            $this->all_permissions_repository = null;
        }

        return $this;
    }

    /**
     * Adds given roles to the current implementation of HasRoles. $roles may be
     * either a Collection of Roles or an mixed array of Role instances, Role ids
     * or Role slugs.
     *
     * Note: Defers to addRole if the argument is neither a Collection
     * nor an array.
     *
     * @param mixed   $roles         Collection or array of roles to add.
     * @param boolean $refresh_roles Role instance, role id or role slug
     *
     * @return HasRoles                 self
     */
    public function addRoles($roles, $refresh_roles = true)
    {
        $id_array = $this->getRoleRepository()->findIdsFromMixed($roles);

        if (!empty($id_array)) {
            $this->roles()->sync($id_array, false);
        }

        if ($refresh_roles) {
            $this->load('roles');
            $this->getRoleRepository()->setRoleList($this->roles);
            $this->all_permissions_repository = null;
        }

        return $this;
    }

    /**
     * Removes a single role from the current HasRoles instance
     *
     * @param mixed   $role          Role, role id, role slug to remove
     * @param boolean $refresh_roles Role instance, role id or role slug
     *
     * @return HasRoles self
     */
    public function removeRole($role, $refresh_roles = true)
    {
        $identifier = $this->getRoleRepository()->findIdsFromMixed($role, true);

        if (!is_null($identifier)) {
            $this->roles()->detach($identifier);
        }

        if ($refresh_roles) {
            $this->load('roles');
            $this->getRoleRepository()->setRoleList($this->roles);
            $this->all_permissions_repository = null;
        }

        return $this;
    }

    /**
     * Removes a number of given roles from the current HasUser instance. Roles
     * can be either a Collection or a mixed array of Role instances, role ids
     * and roles slugs.
     *
     * @param mixed   $roles         Roles to remove
     * @param boolean $refresh_roles Role instance, role id or role slug
     *
     * @return HasRoles self
     */
    public function removeRoles($roles, $refresh_roles = true)
    {
        $id_array = $this->getRoleRepository()->findIdsFromMixed($roles);

        if (!empty($id_array)) {
            $this->roles()->detach($id_array);
        }

        if ($refresh_roles) {
            $this->load('roles');
            $this->getRoleRepository()->setRoleList($this->roles);
            $this->all_permissions_repository = null;
        }

        return $this;
    }

    /**
     * Adds a given permission to the current implementation of HasPermissions.
     *
     * Uses the sync command on a relationship with the second arguments set to
     * false. This means that it adds it if it doesn't already exists, but
     * doesn't delete other records while doing so (which would happen if false
     * was left off).
     *
     * @param mixed $permission          Permission instance, permission id or permission slug
     * @param bool  $refresh_permissions Permission instance, permission id or permission slug
     *
     * @return hasPermissions self
     */
    public function addPermission($permission, $refresh_permissions = true)
    {
        $id_array = $this->getPermissionRepository()->findIdsFromMixed($permission, true);

        if (!empty($id_array)) {
            $this->permissions()->sync($id_array, false);
        }

        if ($refresh_permissions) {
            $this->load('permissions');
            $this->getPermissionRepository()->setPermissionList($this->permissions);
            $this->all_permissions_repository = null;
        }

        return $this;
    }

    /**
     * Adds given permissions to the current implementation of HasPermissions. $permissions may be
     * either a Collection of Permissions or an mixed array of Permission instances, Permission ids
     * or Permission slugs.
     *
     * Note: Defers to addPermission if the argument is neither a Collection
     * nor an array.
     *
     * @param mixed   $permissions         Collection or array of permissions to add.
     * @param boolean $refresh_permissions Permission instance, permission id or permission slug
     *
     * @return HasPermissions self
     */
    public function addPermissions($permissions, $refresh_permissions = true)
    {
        $id_array = $this->getPermissionRepository()->findIdsFromMixed($permissions);

        if (!empty($id_array)) {
            $this->permissions()->sync($id_array, false);
        }

        if ($refresh_permissions) {
            $this->load('permissions');
            $this->getPermissionRepository()->setPermissionList($this->permissions);
            $this->all_permissions_repository = null;
        }

        return $this;
    }

    /**
     * Removes a single permission from the current HasPermissions instance
     *
     * @param mixed $permission          Permission, permission id, permission slug to remove
     * @param bool  $refresh_permissions Permission instance, permission id or permission slug
     *
     * @return HasPermissions self
     */
    public function removePermission($permission, $refresh_permissions = true)
    {
        $identifier = $this->getPermissionRepository()->findIdsFromMixed($permission, true);

        if (!is_null($identifier)) {
            $this->permissions()->detach($identifier);
        }

        if ($refresh_permissions) {
            $this->load('permissions');
            $this->getPermissionRepository()->setPermissionList($this->permissions);
            $this->all_permissions_repository = null;
        }

        return $this;
    }

    /**
     * Removes a number of given permissions from the current HasUser instance. Permissions
     * can be either a Collection or a mixed array of Permission
     * instances, permission ids
     * and permissions slugs.
     *
     * @param mixed   $permissions         Permissions to remove
     * @param boolean $refresh_permissions Permission instance, permission id or permission slug
     *
     * @return HasPermissions self
     */
    public function removePermissions($permissions, $refresh_permissions = true)
    {
        $id_array = $this->getPermissionRepository()->findIdsFromMixed($permissions);

        if (!empty($id_array)) {
            $this->permissions()->detach($id_array);
        }

        if ($refresh_permissions) {
            $this->load('permissions');
            $this->getPermissionRepository()->setPermissionList($this->permissions);
            $this->all_permissions_repository = null;
        }

        return $this;
    }
}
