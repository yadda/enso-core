<?php

namespace Yadda\Enso\Users\Traits;

use App;
use Yadda\Enso\Users\Contracts\PermissionRepositoryContract;
use Yadda\Enso\Users\Models\Permission;

/**
 * Provides all the required functionality for an Eloquent model to have a
 * permissions relationship.
 *
 * Designed to fulfil the Yadda\Enso\Users\Interfaces\HasPermissions interface.
 */
trait HasPermissions
{

    protected $permission_repository;

    /**
     * Gets the table name for this relationship. It checks the model on which
     * this trait is being used for a definition first, then falls back to what
     * eloquent would default to.
     *
     * @return string Table name for relationship
     */
    protected function getPermissionsPivotTableName()
    {
        if (property_exists($this, 'permissions_pivot_table_name')) {
            return $this->permissions_pivot_table_name;
        } else {
            return $this->joiningTable(Permission::class);
        }
    }

    /**
     * Gets an instance of the Permission Repository to use
     *
     * @return \Yadda\Enso\Users\Contracts\PermissionRepositoryContract
     */
    public function getPermissionRepository()
    {
        if (empty($this->permission_repository)) {
            $this->permission_repository = App::make(PermissionRepositoryContract::class)
                ->setPermissionList($this->permissions);
        }

        return $this->permission_repository;
    }

    /**
     * Defines the Permissions relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class, $this->getPermissionsPivotTableName())->withTimestamps();
    }

    /**
     * Adds a given permission to the current implementation of HasPermissions.
     *
     * Uses the sync command on a relationship with the second arguments set to
     * false. This means that it adds it if it doesn't already exists, but
     * doesn't delete other records while doing so (which would happen if false
     * was left off).
     *
     * @param mixed   $permission          Permission instance, permission id or permission slug
     * @param boolean $refresh_permissions Permission instance, permission id or permission slug
     *
     * @return \Yadda\Enso\Users\Interfaces\HasPermissions self
     */
    public function addPermission($permission, $refresh_permissions = true)
    {
        $id_array = $this->getPermissionRepository()->findIdsFromMixed($permission, true);

        if (!empty($id_array)) {
            $this->permissions()->sync($id_array, false);
        }

        if ($refresh_permissions) {
            $this->load('permissions');
            $this->getPermissionRepository()->setPermissionList($this->permissions);
        }

        return $this;
    }

    /**
     * Adds given permissions to the current implementation of HasPermissions. $permissions may be
     * either a Collection of Permissions or an mixed array of Permission instances, Permission ids
     * or Permission slugs.
     *
     * Note: Defers to addPermission if the argument is neither a Collection
     * nor an array.
     *
     * @param mixed $permissions         Collection or array of permissions to add.
     * @param bool  $refresh_permissions Permission instance, permission id or permission slug
     *
     * @return \Yadda\Enso\Users\Interfaces\HasPermissions self
     */
    public function addPermissions($permissions, $refresh_permissions = true)
    {
        $id_array = $this->getPermissionRepository()->findIdsFromMixed($permissions);

        if (!empty($id_array)) {
            $this->permissions()->sync($id_array, false);
        }

        if ($refresh_permissions) {
            $this->load('permissions');
            $this->getPermissionRepository()->setPermissionList($this->permissions);
        }

        return $this;
    }

    /**
     * Test that the current HasPermissions instance has a given permission.
     *
     * @param mixed   $permission Permission instance, Permission id, permission slug
     * @param boolean $refresh    Whether to force the model to reload it's relationship
     *
     * @return boolean Whether HasPermissions has Permission
     */
    public function hasPermission($permission, $refresh = false)
    {
        if (!$this->relationLoaded('permissions') || $refresh) {
            $this->load('permissions');
            $this->getPermissionRepository()->setPermissionList($this->permissions);
        }

        return $this->getPermissionRepository()->permissionListContainsPermissions($permission);
    }

    /**
     * Tests that the current HasPermissions instance has the given permissions. By default,
     * it will return true if it has at least ONE of the given list. Setting $all
     * will require that it has all of them before returning true.
     *
     * @param mixed $permissions Permissions, permission ids, or permission slugs to test against
     * @param bool  $all         Whether all given items must match
     * @param bool  $refresh     Whether to force the model to reload it's  relationship
     *
     * @return bool Successfully matched status
     */
    public function hasPermissions($permissions, $all = false, $refresh = false)
    {
        if (!$this->relationLoaded('permissions') || $refresh) {
            $this->load('permissions');
            $this->getPermissionRepository()->setPermissionList($this->permissions);
        }

        return $this->getPermissionRepository()->permissionListContainsPermissions($permissions, $all);
    }

    /**
     * Intuitive deferrer for hasPermissions($permissions, true, $refresh)
     *
     * @param mixed $permissions Permissions, permission ids, or permission slugs to test against
     * @param bool  $refresh     Whether to force the model to reload it's relationship
     *
     * @return bool Successfully match status
     */
    public function hasAllPermissions($permissions, $refresh = false)
    {
        return $this->hasPermissions($permissions, true, $refresh);
    }

    /**
     * Removes a single permission from the current HasPermissions instance
     *
     * @param mixed   $permission          Permission, permission id, permission slug to remove
     * @param boolean $refresh_permissions Permission instance, permission id or permission slug
     *
     * @return \Yadda\Enso\Users\Interfaces\HasPermissions self
     */
    public function removePermission($permission, $refresh_permissions = true)
    {
        $identifier = $this->getPermissionRepository()->findIdsFromMixed($permission, true);

        if (!is_null($identifier)) {
            $this->permissions()->detach($identifier);
        }

        if ($refresh_permissions) {
            $this->load('permissions');
            $this->getPermissionRepository()->setPermissionList($this->permissions);
        }

        return $this;
    }

    /**
     * Removes a number of given permissions from the current HasUser instance. Permissions
     * can be either a Collection or a mixed array of Permission
     * instances, permission ids
     * and permissions slugs.
     *
     * @param mixed   $permissions         Permissions to remove
     * @param boolean $refresh_permissions Permission instance, permission id or permission slug
     *
     * @return \Yadda\Enso\Users\Interfaces\HasPermissions self
     */
    public function removePermissions($permissions, $refresh_permissions = true)
    {
        $id_array = $this->getPermissionRepository()->findIdsFromMixed($permissions);

        if (!empty($id_array)) {
            $this->permissions()->detach($id_array);
        }

        if ($refresh_permissions) {
            $this->load('permissions');
            $this->getPermissionRepository()->setPermissionList($this->permissions);
        }

        return $this;
    }
}
