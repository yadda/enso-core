<?php

namespace Yadda\Enso\Users\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Yadda\Enso\Users\Contracts\Role as RoleContract;
use Yadda\Enso\Users\Contracts\User as UserContract;

class UserSeeder extends Seeder
{
    public function run()
    {
        $role_class = get_class(resolve(RoleContract::class));
        $user_class = get_class(resolve(UserContract::class));

        $superuser = $role_class::where('slug', 'superuser')->first();

        $user = $user_class::create([
            'username' => 'maya',
            'display_name' => 'Maya',
            'real_name' => 'Maya Admin Account',
            'email' => 'studio@maya.agency',
            'password' => bcrypt('changeme'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $user->addRole($superuser);
        $user->save();
    }
}
