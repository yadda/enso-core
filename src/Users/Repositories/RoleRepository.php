<?php

namespace Yadda\Enso\Users\Repositories;

use Illuminate\Support\Collection;
use Yadda\Enso\Users\Contracts\Role as RoleContract;
use Yadda\Enso\Users\Contracts\RoleRepositoryContract;
use Yadda\Enso\Users\Models\Role;

class RoleRepository implements RoleRepositoryContract
{

    protected $role_list;

    protected $role_class;

    public function __construct(Collection $role_list = null)
    {
        $this->role_class = get_class(resolve(RoleContract::class));

        $role_list = $role_list ?: new Collection();
        $this->setRoleList($role_list);
    }

    /**
    * Set the current Role list
    *
    * @return \Illuminate\Support\Collection $role_list Role list to set
    */
    public function getRoleList()
    {
        return $this->role_list;
    }

    /**
     * Set the current Role list
     *
     * @param \Illuminate\Support\Collection $role_list Role list to set
     *
     * @return RoleRepository self
     */
    public function setRoleList(Collection $role_list)
    {
        $this->role_list = $role_list;

        return $this;
    }

    /**
     * Takes a mixed identifier and works out an id from it (or returns a slug
     * if not specified to lookup by slug)
     *
     * @param mixded  $identifier  Identifier to check
     * @param boolean $lookup_slug Whether to lookup models by slug and return id instead
     * @param boolean $lookup_id   Whether to lookup models by id to check for existence
     *
     * @return mixed Id or (string) case identifier it not looking up slugs
     */
    protected function findIdentifierFromMixedSingle($identifier, $lookup_slug = false, $lookup_id = false)
    {

        // Shouldn't need to do a lookup, the fact that is an instance of role
        // (with an id) mean it 'should' already exist.
        ;
        if ($identifier instanceof $this->role_class) {
            return $identifier->id;
        }

        // Otherwise, return an id (potentially running a lookup to pre-check)
        if (is_numeric($identifier)) {
            if ($lookup_id) {
                return is_null($role = $this->role_class::find($identifier)) ?
                null :
                $role->id;
            } else {
                return $identifier;
            }
        }

        // Optionally, lookup Role by slug and return it's id.
        if ($lookup_slug) {
            return is_null($role = $this->role_class::where('slug', $identifier)->first()) ?
                null :
                $role->id;
        }

        // Otherwise return as string. If a totally invalid argument it passed,
        // this should be expected to throw an exception.
        return (string) $identifier;
    }

    /**
     * Accepts a mixed set of data, and parses it to find the return a set array
     * of ids of all the matching Role items.
     *
     * Accepted types: Collection, Role instance, role id, role slug or an array
     * of any of the singular forms.
     *
     * @param mixed $identifiers data to work from
     *
     * @return array ids of matching Roles
     */
    public function findIdsFromMixed($identifiers)
    {
        if ($identifiers instanceof Collection) {
            return $identifiers->pluck('id')->all();
        }

        if (!is_array($identifiers)) {
            return [$this->findIdentifierFromMixedSingle($identifiers, true)];
        }

        // Gets all ids and slugs from the provided argumet
        $id_array = $slug_array = [];
        foreach ($identifiers as $role_argument) {
            $identifier = $this->findIdentifierFromMixedSingle($role_argument);

            // Occurs when instance of Role is passed but doesn't have an id.
            if (is_null($identifier)) {
                continue;
            }

            // Otherwise, place in correct data array.
            if (is_numeric($identifier)) {
                $id_array[] = $identifier;
            } else {
                $slug_array[] = $identifier;
            }
        }

        // Performs a query to get id's based on any slugs passed, them merges
        // with current list of ids from arguments.
        if (!empty($slug_array)) {
            $roles_from_slugs = $this->role_class::whereIn('slug', $slug_array)->get();
            if ($roles_from_slugs->count()) {
                $id_array = array_unique(array_merge($id_array, $roles_from_slugs->pluck('id')->all()));
            }
        }

        return $id_array;
    }

    /**
     * Checks to see whether any of a given, mixed list of roles or identifiers
     * are present within a set of Role.
     *
     * @param mixed $roles List of roles to look for
     * @param bool  $all   Whether to match all or just one
     *
     * @return bool Success status
     */
    public function roleListContainsRoles($roles, $all = false)
    {
        $id_array = $this->findIdsFromMixed($roles);

        $match_statuses = [];
        foreach ($id_array as $role) {
            $match_statuses[] = $this->role_list->contains($role);
        }

        // If no valid roles to match, then it implicity fails
        if (empty($match_statuses)) {
            return false;
        }

        // Shrink the array to just success and fail
        $statuses = array_unique($match_statuses);

        return $all ?
            $all && ! in_array(false, $statuses) : // if $all is set, then any fails should return false
            $all || in_array(true, $statuses); // if $all is not set, then any success should return true
    }

    /**
     * Check to see whether the given role list contains a superuser role
     *
     * @return boolean Whether roles list contains a superuser role
     */
    public function roleListContainsSuperuser()
    {
        return !! $this->role_list->filter(function ($role) {
            return $role->isSuperuserRole();
        })->count();
    }
}
