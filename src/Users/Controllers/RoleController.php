<?php

namespace Yadda\Enso\Users\Controllers;

use Yadda\Enso\Crud\Controller as BaseController;

/**
 * CRUD controller for User Roles
 */
class RoleController extends BaseController
{
    /**
     * Name of the CRUD
     *
     * @var string
     */
    protected $crud_name = 'role';
}
