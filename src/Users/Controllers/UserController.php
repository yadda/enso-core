<?php

namespace Yadda\Enso\Users\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use TorMorten\Eventy\Facades\Events as Eventy;
use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Controller as BaseController;
use Yadda\Enso\Users\Contracts\User;
use Yadda\Enso\Users\Requests\LoginAs;

class UserController extends BaseController
{
    protected $crud_name = 'user';

    protected function configureHooks()
    {
        Eventy::addFilter('crud.update.rules', [$this, 'dontRequirePasswordOnUpdate'], 20, 3);
        Eventy::addAction('crud.update.before-validation', [$this, 'dontSetPasswordIfEmpty'], 20, 2);
        Eventy::addFilter('crud.update.data', [$this, 'encryptPasswordIfSet'], 20, 3);
    }

    public function dontRequirePasswordOnUpdate(
        $rules,
        Config $config,
        Request $request
    ) {
        $rules['password'] = 'sometimes|alpha_num|min:6|confirmed';

        return $rules;
    }

    public function encryptPasswordIfSet(
        array $data,
        Config $config,
        Request $request
    ): array {
        if (isset($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        }

        return $data;
    }

    /**
     * Strips password data from request if values are empty, to prevent
     * validation un-required passwords
     *
     * @param Config $config
     * @param Request $request
     *
     * @return void
     */
    public function dontSetPasswordIfEmpty(Config $config, Request $request)
    {
        if (empty($request['password'])) {
            unset($request['password']);
        }

        if (empty($request['password'])) {
            unset($request['password_confirmation']);
        }
    }

    /**
     * If the current user has permission to impersonate another user, then log
     * them in as the given user and redirect to home.
     *
     * Support route model binding if implemented
     *
     * @param  User|string $user
     *
     * @return RedirectResponse
     */
    public function loginAsUser(LoginAs $request, $user): RedirectResponse
    {
        if (!$user instanceof Model) {
            if (is_null($user = App::make(User::class)->find($user))) {
                abort(404);
            }
        }

        Auth::login($user);

        return Redirect::to('/');
    }
}
