<?php

namespace Yadda\Enso\Users\Crud;

use Eventy;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Forms\Fields\BelongsToManyField;
use Yadda\Enso\Crud\Forms\Fields\EmailField;
use Yadda\Enso\Crud\Forms\Fields\PasswordField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Crud\Tables\Text;
use Yadda\Enso\Crud\Tables\TextLink;
use Yadda\Enso\Users\Contracts\Role as RoleContract;
use Yadda\Enso\Users\Contracts\User as UserContract;

/**
 * CRUD config for an User
 *
 * This represents any person that uses the website, not just people
 * with access to the Enso backend.
 */
class User extends Config
{
    /**
     * Configure this CRUD config
     *
     * @return void
     */
    public function configure()
    {
        // Gets Bound User class
        $user_class = get_class(resolve(UserContract::class));

        $this->model($user_class)
            ->route('admin.users')
            ->views('users')
            ->name('User')
            ->searchColumns(['username'])
            ->paginate(25)
            ->columns([
                (new TextLink('name_column'))
                    ->setLabel('Username')
                    ->orderableBy('username'),
                (new Text('display_name')),
                (new Text('real_name')),
                (new Text('email')),
            ])
            ->rules([
                'main.username' => 'required|alpha_dash',
                'main.email'    => 'required|email|unique:users,email',
                'main.password.0' => 'nullable|min:6|same:main.password.1',
                'main.password.1' => 'nullable|min:6',
            ])
            ->filters([
                'search' => \Yadda\Enso\Crud\Filters\UserFilter::make(),
            ]);

        Eventy::addFilter('crud.update.rules', function ($rules, Config $config, Request $request) {
            unset($rules['main.password']);
            return $rules;
        }, 10, 3);
    }

    /**
     * Create a CRUD Form
     *
     * @param \Yadda\Enso\Crud\Forms\Form $form Basic form to add configuration for
     *
     * @return \Yadda\Enso\Crud\Forms\Form
     */
    public function create(Form $form)
    {
        $role_model = get_class(resolve(RoleContract::class));

        $form
            ->allowAutocomplete(false)
            ->addSections([
                Section::make('main')->addFields([
                    TextField::make('username')
                        ->addFieldsetClass('is-6'),
                    EmailField::make('email')
                        ->addFieldsetClass('is-6'),
                    TextField::make('display_name')
                        ->addFieldsetClass('is-6')
                        ->setHelpText('Name to be displayed on frontend.'),
                    TextField::make('real_name')
                        ->addFieldsetClass('is-6'),
                    PasswordField::make('password')
                        ->addFieldsetClass('is-6'),
                    BelongsToManyField::make('roles')
                        ->useAjax(route('admin.roles.index'), $role_model),
                ]),
            ]);

        return $form;
    }

    /**
     * Applies a search term to a User query
     *
     * @param \Illuminate\Database\Query\Builder $query Search query
     * @param string                             $term  Search term
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function userSearch($query, $term)
    {
        return $query->where(function ($sub_query) use ($term) {
            return $sub_query->where('username', 'LIKE', '%' . $term . '%')
                ->orWhere('email', 'LIKE', '%' . $term . '%')
                ->orWhere('display_name', 'LIKE', '%' . $term . '%')
                ->orWhere('real_name', 'LIKE', '%' . $term . '%');
        });
    }
}
