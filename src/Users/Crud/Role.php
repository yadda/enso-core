<?php

namespace Yadda\Enso\Users\Crud;

use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Forms\Fields\SlugField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Crud\Tables\Text;
use Yadda\Enso\Crud\Tables\TextLink;
use Yadda\Enso\Users\Contracts\Role as RoleContract;

class Role extends Config
{
    public function configure()
    {
        // Gets bound Role Model
        $role_class = get_class(resolve(RoleContract::class));

        $this->model($role_class)
            ->route('admin.roles')
            ->views('roles')
            ->name('Role')
            ->paginate(25)
            ->columns([
                (new TextLink('name_column'))
                    ->setLabel('Name')
                    ->orderableBy('name'),
                (new Text('slug')),
                (new Text('description')),
            ])
            ->filters([
                'search' => [
                    'type' => 'text',
                    'label' => '',
                    'props' => [
                        'placeholder' => 'Search...',
                    ],
                    'callable' => [$this, 'roleSearch'],
                ],
            ]);
    }

    public function create(Form $form)
    {
        $form->addSections([
            (new Section('main'))
                ->addFields([
                    (new TextField('name'))
                        ->setPurifyHTML(false)
                        ->addFieldsetClass('is-6'),
                    (new SlugField('slug'))
                        ->setSource('name')
                        ->setRoute('%SLUG%')
                        ->addFieldsetClass('is-6'),
                    (new TextField('description')),
                ]),
        ]);

        return $form;
    }

    public function roleSearch($query, $term)
    {
        return $query->where(function ($sub_query) use ($term) {
            return $sub_query->where('name', 'LIKE', '%' . $term . '%')
                ->orWhere('slug', 'LIKE', '%' . $term . '%');
        });
    }
}
