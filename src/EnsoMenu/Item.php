<?php

namespace Yadda\Enso\EnsoMenu;

use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Yadda\Enso\EnsoMenu\Contracts\ItemInterface;
use Yadda\Enso\EnsoMenu\Traits\HasMenuItems;

class Item implements ItemInterface
{
    use HasMenuItems;

    // Internal collection of items.
    protected $items;

    // Hard coded url if route() helper not usable
    protected $url = '#';

    // Array of data to be passed into the Laravel route() helper
    protected $route;

    // Label for this menu item
    protected $label = 'No Label';

    // Class to add to the Icon for this Menu item
    protected $icon = 'fa fa-file-o';

    // Menu order of this item
    protected $order = 10;

    // A function to restrict access to this menu-item
    protected $restrictions;

    // Menu depth of this item.
    protected $item_depth = 1;

    public function __construct(array $config, $item_depth = null)
    {
        $this->items = new Collection();

        $this->applyUpdates($config);

        if (isset($config['items'])) {
            $this->addItems($config['items']);
        }

        if ($item_depth) {
            $this->item_depth = $item_depth;
        }
    }

    /**
     * Updates properties specified in the updates array
     *
     * @param array $updates
     *
     * @return self
     */
    public function applyUpdates(array $updates)
    {
        $this->setUrl($updates['url'] ?? $this->url);
        $this->setRoute($updates['route'] ?? $this->route);
        $this->setLabel($updates['label'] ?? $this->label);
        $this->setIcon($updates['icon'] ?? $this->icon);
        $this->setOrder($updates['order'] ?? $this->order);
        $this->setDepth($updates['depth'] ?? $this->item_depth);
        $this->setRestrictions($updates['restrict'] ?? $this->restrictions);

        return $this;
    }

    /**
     * Return true if this given item is the current url (or a child url)
     *
     * @return boolean
     */
    public function isActive()
    {
        return Request::is(substr(parse_url($this->getUrl(), PHP_URL_PATH), 1) . '*');
    }

    /**
     * Tests to see whether this item is restricted
     *
     * @return boolean
     */
    public function isAllowed()
    {
        if ($this->restrictions) {
            if (is_callable($this->restrictions)) {
                return call_user_func_array($this->restrictions, [$this]);
            }

            return Auth::check() && Auth::user()->hasPermissions((array) $this->restrictions);
        }

        return true;
    }

    /**
     * Gets the url that this menu item should use. If the 'route' property
     * is specified, try to build a route from the arguments. Otherwise, return
     * the hard coded url.
     *
     * @return string
     */
    public function getUrl()
    {
        if (!empty($this->route)) {
            try {
                return route(...$this->route);
            } catch (Exception $e) {
                throw new Exception('Menu Item can\'t create route from provided route data.');
            }
        }

        return $this->url;
    }

    /**
     * Sets the current 'url' property
     *
     * @param string $url
     *
     * @return self
     */
    protected function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * Gets the current route value. If it is set, it should
     * be an array with a route name and any arguments required
     * to build the route.
     *
     * @return array
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Sets the current route value
     *
     * @param array $route
     *
     * @return self
     */
    protected function setRoute($route)
    {
        $this->route = $route;
        return $this;
    }

    /**
     * Gets the current Label value
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Sets the current Label value
     *
     * @param string $label
     *
     * @return self
     */
    protected function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * Gets the current Icon value
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Sets the current Icon value
     *
     * @param string $icon
     *
     * @return self
     */
    protected function setIcon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * Gets the current Order value
     *
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Sets the current Order value
     *
     * @param integer $order
     *
     * @return self
     */
    protected function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * Gets the current Restrictions callable
     *
     * @return callable|null
     */
    public function getRestrictions()
    {
        return $this->restrictions;
    }

    /**
     * Sets the current restrictions callable
     *
     * @param integer $order
     *
     * @return self
     */
    protected function setRestrictions($restrictions)
    {
        $this->restrictions = $restrictions;
    }
}
