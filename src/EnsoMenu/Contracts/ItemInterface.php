<?php

namespace Yadda\Enso\EnsoMenu\Contracts;

interface ItemInterface
{
    /**
     * Updates properties specified in the updates array
     *
     * @param array $updates
     *
     * @return self
     */
    public function applyUpdates(array $updates);

    /**
     * Return true if this given item is the current url (or a child url)
     *
     * @return bool
     */
    public function isActive();

    /**
     * Tests to see whether this item is restricted
     *
     * @return bool
     */
    public function isAllowed();

    /**
     * Gets the url that this menu item should use.
     *
     * @return string
     */
    public function getUrl();

    /**
     * Gets the current route value. If it is set, it should
     * be an array with a route name and any arguments required
     * to build the route.
     *
     * @return array
     */
    public function getRoute();

    /**
     * Gets the current Label value
     *
     * @return string
     */
    public function getLabel();

    /**
     * Gets the current Icon value
     *
     * @return string
     */
    public function getIcon();

    /**
     * Gets the current Order value
     *
     * @return int
     */
    public function getOrder();

    /**
     * Gets the current Depth value.
     *
     * @return int
     */
    public function getDepth();

    /**
     * Gets the current Restrictions callable
     *
     * @return callable
     */
    public function getRestrictions();

    /**
     * Checks whether or not there are an child Items.
     *
     * @return bool
     */
    public function hasItems();

    /**
     * Gets all the items, ordered by their 'Order' property.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getItems();

    /**
     * Adds an Item to the item collection, by config
     * or instance of ItemInterface.
     *
     * @param array|\Yadda\Enso\Menu\Item $config
     *
     * @return self
     */
    public function addItem($item);

    /**
     * Adds a set of items to the item collection. Expects an array
     * of arrays
     *
     * @param array $item_configs
     *
     * @return self
     */
    public function addItems($item_configs);

    /**
     * Updates an Item in the items collection. Selection is by label,
     * Updates are expected as an array of vakues to update.
     *
     * You do NOT need to specify properties that are not changing.
     *
     * @param string $label
     * @param array  $updates
     *
     * @return self
     */
    public function updateItemByLabel($label, $updates);

    /**
     * Removes an Item in the items collection. Selection is by Label value.
     *
     * @param string $label
     *
     * @return self
     */
    public function removeItemByLabel($label);
}
