<?php

namespace Yadda\Enso\EnsoMenu\Traits;

use Illuminate\Support\Arr;
use TypeError;
use Yadda\Enso\EnsoMenu\Contracts\ItemInterface;
use Yadda\Enso\EnsoMenu\Exceptions\MenuException;
use Yadda\Enso\EnsoMenu\Item;

/**
 * Common functionality for both the Overarching Menu and for Menu items
 */
trait HasMenuItems
{
    /**
     * Gets the Depth of the Menu/Item.
     *
     * By Default, Menu doesn't have an item depth, and so is treated
     * as being as 0 depth.
     *
     * @return int
     */
    public function getDepth()
    {
        if (property_exists($this, 'item_depth')) {
            return $this->item_depth;
        }

        return 0;
    }

    /**
     * Sets the current Item Depth value
     *
     * @param int $item_depth
     *
     * @return self
     */
    protected function setDepth($item_depth)
    {
        $this->item_depth = $item_depth;

        return $this;
    }

    /**
     * Checks whether or not there are an child Items.
     *
     * @return bool
     */
    public function hasItems()
    {
        return (bool) $this->getItems()->count();
    }

    /**
     * Adds an Item to the item collection, by config. We don't allow
     * pre-build Items to be passed as an argument to better maintain
     * the depth structure of items.
     *
     * Throws an exception when this action would exceed the max menu depth
     *
     * @param array $config
     *
     * @return self
     */
    public function addItem($item)
    {
        if (($this->getDepth() + 1) > config('enso.menu.max_depth', 2)) {
            $label = Arr::get($item, 'label', 'No Label');
            throw new MenuException('Adding this menu item (' . $label . ') would exceed the max Menu depth.');
        }

        try {
            if ($item instanceof ItemInterface) {
                // Set correct depth for the insert location
                $item->applyUpdates(['depth' => ($this->getDepth() + 1)]);

                $this->items->push($item);
            } else {
                $this->items->push(new Item($item, ($this->getDepth() + 1)));
            }
        } catch (TypeError $e) {
            throw new MenuException('addItem expects an array of item data or an instance of an Item', 500, $e);
        }

        return $this;
    }

    /**
     * Adds a set of items to the item collection. Expects an array
     * of arrays
     *
     * @param array $item_configs
     *
     * @return self
     */
    public function addItems($item_configs)
    {
        foreach ($item_configs as $config) {
            $this->addItem($config);
        }

        return $this;
    }

    /**
     * Gets all the items, ordered by their 'Order' property.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getItems()
    {
        return $this->items->filter(function ($item) {
            return $item->isAllowed();
        })->sortBy(function ($item) {
            return $item->getLabel();
        });
    }

    /**
     * Gets the child item by its label, with the ability to
     * find nested items by using a period-separated string.
     *
     * @param string $label
     *
     * @return HasMenuItems|null
     */
    public function getItemByLabel($label)
    {
        $label_parts = explode('.', $label, 2);

        $first_label = array_shift($label_parts);

        $found_item = $this->items->first(function ($item) use ($first_label) {
            return $item->getLabel() === $first_label;
        });

        if ($found_item && count($label_parts) > 0) {
            $found_item = $found_item->getItemByLabel(Arr::first($label_parts));
        }

        return $found_item;
    }

    /**
     * Updates an Item in the items collection. Selection is by label,
     * Updates are expected as an array of items to update.
     *
     * You do NOT need to specify properties that are not changing.
     *
     * @param string $label
     * @param array  $updates
     *
     * @return self
     */
    public function updateItemByLabel($label, $updates)
    {
        $this->items->transform(function ($item) use ($label, $updates) {
            if ($item->getLabel() === $label) {
                $item->applyUpdates($updates);
            }

            return $item;
        });

        return $this;
    }

    /**
     * Removes an Item in the items collection. Selection is by Label value.
     *
     * @param string $label
     *
     * @return self
     */
    public function removeItemByLabel($label)
    {
        $this->items = $this->items->filter(function ($item) use ($label) {
            return $item->getLabel() !== $label;
        });

        return $this;
    }
}
