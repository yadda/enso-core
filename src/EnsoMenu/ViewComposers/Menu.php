<?php

namespace Yadda\Enso\EnsoMenu\ViewComposers;

use Illuminate\View\View;
use Yadda\Enso\Facades\EnsoMenu;

/**
 * Add EnsoMenu menu items to a view
 */
class Menu
{
    /**
     * Add items to view
     *
     * @param View $view The view to add items to
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('menu_items', EnsoMenu::getItems());
    }
}
