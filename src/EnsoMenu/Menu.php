<?php

namespace Yadda\Enso\EnsoMenu;

use Illuminate\Support\Collection;
use Yadda\Enso\EnsoMenu\Traits\HasMenuItems;

class Menu
{
    use HasMenuItems;

    protected $items;

    public function __construct()
    {
        $this->items = new Collection;
    }
}
