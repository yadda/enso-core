<?php

namespace Yadda\Enso;

use Alert;
use Illuminate\Support\Arr;

class Enso
{
    /**
     * Data to be passed for use as js variables
     *
     * @var array
     */
    protected $js_data = [];

    /**
     * Test whether any script data has been added
     */
    public function hasJSData(): bool
    {
        return !empty($this->js_data);
    }

    /**
     * Adds a piece of data to the $js_data array
     */
    public function setJSData(string $variable_name, $data): void
    {
        Arr::set($this->js_data, $variable_name, $data);
    }

    /**
     * Gets all JS data
     */
    public function getJSData(): array
    {
        $alerts = Alert::all();

        $flash_types = [
            'error',
            'info',
            'success',
            'warning',
        ];

        foreach ($flash_types as $type) {
            if (session()->has($type)) {
                $alerts[] = [
                    'type' => $type,
                    'message' => session()->get($type),
                ];
            }
        }

        if (session()->has('errors')) {
            foreach (session()->get('errors')->all() as $error) {
                $alerts[] = [
                    'type' => 'error',
                    'message' => $error,
                ];
            }
        }

        return array_merge(
            $this->js_data,
            ['alerts' => $alerts]
        );
    }
}
