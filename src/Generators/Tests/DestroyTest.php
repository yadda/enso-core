<?php

namespace Yadda\Enso\Generators\Tests;

use Illuminate\Foundation\Testing\TestResponse;
use Yadda\Enso\Generators\Tests\BaseTest;

/**
 * Provides common functionality for testing the Enso crud destroy route
 */
abstract class DestroyTest extends BaseTest
{
    /**
     * Route name suffix
     *
     * @return string
     */
    protected function getRouteNameSuffix(): string
    {
        return 'destroy';
    }

    /**
     * Makes valid request to correct endpoint
     *
     * @param array $parameters
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function makeRequest(array $parameters = []): TestResponse
    {
        return $this->delete(
            $this->getRoute([$this->getModelInstance()->getKey()])
        );
    }

    /**
     * Test that an Admin request to the index route
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function runAdminTest(): TestResponse
    {
        $model_class = $this->getModelClass();

        $this->setModelInstance(factory($model_class)->create());

        $this->assertEquals(1, $model_class::count());

        $response = $this->makeAdminRequest();

        $response->assertRedirect(
            route($this->getRouteNamePrefix() . '.index')
        );

        $this->assertEquals(0, $model_class::count());

        return $response;
    }

    /**
     * Test that the index route returns a login redirect response
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function runUnauthenticatedTest(): TestResponse
    {
        $model_class = $this->getModelClass();

        $this->setModelInstance(factory($model_class)->create());

        $this->assertEquals(1, $model_class::count());

        $response = $this->makeRequest();

        $response->assertRedirect('login');

        $this->assertEquals(1, $model_class::count());

        return $response;
    }
}
