<?php

namespace Yadda\Enso\Generators\Tests;

use Illuminate\Foundation\Testing\TestResponse;
use Yadda\Enso\Generators\Tests\BaseTest;

/**
 * Provides common functionality for testing the Enso crud edit route
 */
abstract class EditTest extends BaseTest
{
    /**
     * Name of the route this test covers
     *
     * @param array $parameters
     *
     * @return string
     */
    protected function getRoute(array $parameters = []): string
    {
        return route(
            $this->getRouteNamePrefix() . '.' . $this->getRouteNameSuffix(),
            $this->getModelInstance(true)->getKey()
        );
    }

    /**
     * Route name suffix
     *
     * @return string
     */
    protected function getRouteNameSuffix(): string
    {
        return 'edit';
    }

    /**
     * Test that an Admin request to the index route
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function runAdminTest(): TestResponse
    {
        $response = $this->makeAdminRequest();

        $response->assertStatus(200);

        return $response;
    }

    /**
     * Test that the index route returns a login redirect response
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function runUnauthenticatedTest(): TestResponse
    {
        $response = $this->makeRequest();

        $response->assertRedirect('login');

        return $response;
    }
}
