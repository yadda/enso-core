<?php

namespace Yadda\Enso\Generators\Tests;

use Illuminate\Foundation\Testing\TestResponse;
use Yadda\Enso\Generators\Tests\BaseTest;

/**
 * Provides common functionality for testing the Enso crud store route
 */
abstract class StoreTest extends BaseTest
{
    /**
     * Route name suffix
     *
     * @return string
     */
    protected function getRouteNameSuffix(): string
    {
        return 'store';
    }

    /**
     * Makes valid request to correct endpoint
     *
     * @param array $parameters
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function makeRequest(array $parameters = []): TestResponse
    {
        return $this->postJson(
            $this->getRoute(),
            $this->validModelData($parameters),
            $this->getJsonHeaders()
        );
    }

    /**
     * Test that an Admin request to the index route
     *
     * @param array $updates
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function runAdminTest(array $updates): TestResponse
    {
        $model_class = $this->getModelClass();

        $this->assertEquals(0, $model_class::count());

        $response = $this->makeAdminRequest($updates);

        $response->assertStatus(200);

        $this->assertEquals(1, $model_class::count());

        $created_model = $model_class::first();

        foreach ($updates as $property => $value) {
            $this->assertEquals($value, $created_model->$property);
        }

        return $response;
    }

    /**
     * Test that the index route returns a login redirect response
     *
     * @param array $updates
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function runUnauthenticatedTest(array $updates): TestResponse
    {
        $model_class = $this->getModelClass();

        $this->assertEquals(0, $model_class::count());

        $response = $this->makeRequest($updates);

        $response->assertStatus(401);

        $this->assertEquals(0, $model_class::count());

        return $response;
    }
}
