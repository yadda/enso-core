<?php

namespace Yadda\Enso\Meta\Traits;

use Yadda\Enso\Meta\Models\Meta;

trait HasMeta
{
    /**
     * Meta object relationship
     *
     * @return Meta
     */
    public function meta()
    {
        return $this->morphOne(Meta::class, 'metaable');
    }

    /**
     * Returns the Meta Object for this class
     *
     * @return Meta
     */
    public function getMeta()
    {
        if ($this->hasMeta()) {
            return $this->meta;
        }

        return new Meta;
    }

    /**
     * Checks to see whether this class has a Meta object
     *
     * @return boolean
     */
    public function hasMeta()
    {
        return !is_null($this->meta);
    }
}
