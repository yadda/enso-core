<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('meta')) {
            Schema::create('meta', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('metaable_id');
                $table->string('metaable_type');

                $table->unique(['metaable_id', 'metaable_type'], 'metaable_index');

                $table->string('meta_title')->nullable();
                $table->text('meta_description')->nullable();
                $table->integer('meta_image_id')->nullable();

                $table->string('facebook_title')->nullable();
                $table->text('facebook_description')->nullable();
                $table->integer('facebook_image_id')->nullable();

                $table->string('twitter_title')->nullable();
                $table->text('twitter_description')->nullable();
                $table->integer('twitter_image_id')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meta');
    }
}
