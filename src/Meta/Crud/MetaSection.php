<?php

namespace Yadda\Enso\Meta\Crud;

use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Forms\Fields\DividerField;
use Yadda\Enso\Crud\Forms\Fields\FileUploadFieldResumable;
use Yadda\Enso\Crud\Forms\Fields\SelectField;
use Yadda\Enso\Crud\Forms\Fields\TextareaField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Meta\Models\Meta;

class MetaSection extends Section
{
    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'meta';

    public function __construct(string $name = 'meta')
    {
        parent::__construct($name);

        $this->addFields([
            TextField::make('meta_title')
                ->setlabel('Title'),
            TextField::make('meta_keywords')
                ->setLabel('Keywords')
                ->setHelpText('A comma separated list of keywords.'),
            TextareaField::make('meta_description')
                ->setLabel('Description')
                ->addFieldsetClass('is-6')
                ->setSanitizationSetting('AutoFormat.AutoParagraph', false)
                ->setHelpText('A clear, concise description.'),
            FileUploadFieldResumable::make('meta_image_id')
                ->setLabel('Meta Image')
                ->setRelationshipName('metaImage')
                ->setUploadPath('pages/meta')
                ->addFieldsetClass('is-6'),
            DividerField::make('facebook_divider')
                ->setTitle('Facebook'),
            TextField::make('facebook_title')
                ->setHelpText('Leave this blank to use the default title.'),
            TextareaField::make('facebook_description')
                ->addFieldsetClass('is-6')
                ->setSanitizationSetting('AutoFormat.AutoParagraph', false)
                ->setHelpText('Leave this blank to use the default description.'),
            FileUploadFieldResumable::make('facebook_image_id')
                ->setLabel('Facebook Image')
                ->setRelationshipName('facebookImage')
                ->setUploadPath('pages/facebook')
                ->addFieldsetClass('is-6')
                ->setHelpText('Ideal size: 1200 x 630px. Minimum 600 x 315px. ' .
                    'Leave this blank to use the default image.'),
            DividerField::make('twitter_divider')
                ->setTitle('Twitter'),
            TextField::make('twitter_title')
                ->setHelpText('Leave this blank to use the default title.'),
            SelectField::make('twitter_card')
                ->setLabel('Twitter Card Type')
                ->setOptions([
                    'summary' => 'Summary',
                    'summary_large_image' => 'Summary with large image',
                ]),
            TextField::make('twitter_site')
                ->addFieldsetClass('is-half')
                ->setHelpText('The @username that will be shown in the card footer'),
            TextField::make('twitter_creator')
                ->addFieldsetClass('is-half')
                ->setHelpText('The @username of the content creator/author.'),
            TextareaField::make('twitter_description')
                ->addFieldsetClass('is-6')
                ->setSanitizationSetting('AutoFormat.AutoParagraph', false)
                ->setHelpText('Leave this blank to use the default description.'),
            FileUploadFieldResumable::make('twitter_image_id')
                ->setLabel('Twitter Image')
                ->setRelationshipName('twitterImage')
                ->setUploadPath('pages/twitter')
                ->addFieldsetClass('is-6')
                ->setHelpText(
                    'For "summary" card types use a square image between ' .
                        '144x144px and 4096x4096px. For "summary with large ' .
                        'image" card types, use a 2:1 image between 300x157px ' .
                        'and 4096x4096px. Leave this blank to use the default image.'
                ),
        ]);
    }

    /**
     * Applies data to the given item
     *
     * @param  Model    $item       Item to apply data to
     * @param  array    $data       All data
     */
    public function applyRequestData(&$item, array $data)
    {
        //
    }

    /**
     * Applies data to the given item
     *
     * @param  Model    $item       Item to apply data to
     * @param  array    $data       All data
     */
    public function applyRequestDataAfterSave(&$item, array $data)
    {
        $meta = $this->getItemMeta($item);

        foreach ($this->getFields() as $field) {
            $field->applyRequestData($meta, $data);
        }

        $meta->save();

        foreach ($this->getFields() as $field) {
            $field->applyRequestDataAfterSave($meta, $data);
        }
    }

    /**
     * Gets the data associated with each of the fields and stores in a
     * formatted structure for passing to the vue components
     *
     * @param  object   $item       data source
     * @return mixed                found data values
     */
    public function getFormData($item)
    {
        if (!is_object($item)) {
            return new stdClass;
        }

        $meta = $this->getItemMeta($item);

        $form_data = [];
        foreach ($this->getFields() as $field) {
            $form_data[$field->getName()] = $field->getFormData($meta);
        }

        if (empty($form_data)) {
            return new stdClass;
        }

        return $form_data;
    }

    /**
     * Gets the meta object from an item, or creates a new one if required
     *
     * @param  [type] $item [description]
     * @return [type]       [description]
     */
    protected function getItemMeta($item)
    {
        if (empty($item->id)) {
            return new Meta;
        }

        if (is_null($meta = $item->meta)) {
            $meta = new Meta;
            $meta->metaable_id = $item->getKey();

            // Use Laravel's morph map if available
            $meta->metaable_type = $item instanceof Model
                ? $item->getMorphClass()
                : get_class($item);
        }

        return $meta;
    }
}
