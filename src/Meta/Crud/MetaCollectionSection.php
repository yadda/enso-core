<?php

namespace Yadda\Enso\Meta\Crud;

use Yadda\Enso\Crud\Forms\CollectionSection;
use Yadda\Enso\Meta\Crud\MetaSection;

/**
 * The CRUD Form tab that provides Meta fields
 */
class MetaCollectionSection extends CollectionSection
{
    /**
     * Create a new MetaCollectionSection
     *
     * @param string $name Name of the section
     */
    public function __construct(string $name = 'meta')
    {
        parent::__construct($name);

        $other_meta = new MetaSection();

        $this->addFields($other_meta->getFields()->all());
    }
}
