<?php

namespace Yadda\Enso\Meta;

use Yadda\Enso\Meta\Models\Meta as MetaModel;

class Meta
{
    protected $meta;

    /**
     * Set the MetaModel to use
     *
     * @param MetaModel $meta
     * @return void
     */
    public function use(MetaModel $meta)
    {
        $this->meta = $meta;
    }

    /**
     * Get the current MetaModel object
     *
     * @return MetaModel
     */
    public function get()
    {
        if (is_null($this->meta)) {
            return new MetaModel;
        }

        return $this->meta;
    }
}
