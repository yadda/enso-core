<?php

namespace Yadda\Enso\Meta\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Yadda\Enso\Crud\Contracts\IsCrudModel as ContractsIsCrudModel;
use Yadda\Enso\Crud\Traits\IsCrudModel;
use Yadda\Enso\Media\Contracts\ImageFile;
use Yadda\Enso\Media\Traits\HasFilesTrait;
use Yadda\Enso\Settings\Facades\EnsoSettings;

class Meta extends Model implements ContractsIsCrudModel
{
    use HasFilesTrait, IsCrudModel;

    protected $table = 'meta';

    public $timestamps = false;

    protected $override_title;

    protected $override_description;

    protected $override_image;

    protected $override_facebook_image_url;

    protected $override_twitter_image_url;

    protected $override_keywords;

    protected $canonical_url;

    protected $fillable = [
        'meta_keywords',
        'meta_title',
        'meta_description',
        'meta_image_id',
        'facebook_title',
        'facebook_description',
        'facebook_image_id',
        'twitter_title',
        'twitter_description',
        'twitter_image_id',
        'twitter_card',
        'twitter_site',
        'twitter_creator',
    ];

    /**************************************************************************
     * General
     **************************************************************************/

    /**
     * Gets the Canonical Url for Meta object
     *
     * @return void
     */
    public function getCanonicalUrl()
    {
        if (!is_null($this->canonical_url)) {
            return $this->canonical_url;
        }

        return url()->full();
    }

    /**
     * Image relationship for general Meta Image.
     *
     * @return ImageFile
     */
    public function metaImage()
    {
        return $this->belongsTo(resolve(ImageFile::class), 'meta_image_id');
    }

    /**
     * Whether or not this Meta has any meta image attached, including fallbacks
     *
     * @return boolean
     */
    public function hasAnyMetaImage()
    {
        return !is_null($this->getMetaImage());
    }

    /**
     * Checks whether there is a Meta Image associated with this Meta
     *
     * @return boolean
     */
    public function hasMetaImage()
    {
        return !is_null($this->metaImage);
    }

    /**
     * Getter for general Meta Image
     *
     * @return ImageFile
     */
    public function getMetaImage()
    {
        if ($this->hasMetaImage()) {
            return $this->metaImage;
        }

        if (!empty($this->override_image)) {
            return $this->override_image;
        }

        if (!empty(EnsoSettings::get('meta_image_id'))) {
            return $this->getSettingImage('meta_image_id');
        }

        return null;
    }

    /**
     * Getter for general Meta Title
     *
     * @return string
     */
    public function getMetaTitle()
    {
        if ($this->hasMetaTitle()) {
            return $this->meta_title;
        }

        if ($this->hasOverrideTitle()) {
            return $this->override_title;
        }

        return EnsoSettings::get('meta_title', config('app.name', ''));
    }

    /**
     * Whether or not a title has been set on this meta
     *
     * @return boolean
     */
    public function hasMetaTitle()
    {
        return !empty($this->meta_title);
    }

    /**
     * Getter for general Meta Description
     *
     * @return string
     */
    public function getMetaDescription()
    {
        if ($this->hasMetaDescription()) {
            return $this->meta_description;
        }

        if ($this->hasOverrideDescription()) {
            return $this->override_description;
        }

        return EnsoSettings::get('meta_description');
    }

    /**
     * Whether or not a description has been set on this meta
     *
     * @return boolean
     */
    public function hasMetaDescription()
    {
        return !empty($this->meta_description);
    }

    /**
     * Getter for general Meta Keywords
     *
     * @return string
     */
    public function getMetaKeywords($as_array = false)
    {
        if ($this->hasMetaKeywords()) {
            $keywords = $this->meta_keywords;
        } elseif ($this->hasMetaKeywords()) {
            $keywords = $this->override_keywords;
        } else {
            $keywords = EnsoSettings::get('meta_keywords');
        }

        if ($as_array) {
            return (new Collection(explode(',', $keywords)))
                ->map(function ($keyword) {
                    return trim($keyword);
                });
        }

        return $keywords;
    }

    /**
     * Whether keywords have been set on this Meta
     *
     * @return boolean
     */
    public function hasMetaKeywords()
    {
        return !empty($this->meta_keywords);
    }

    /**************************************************************************
     * External Overrides
     **************************************************************************/

    /**
     * Set or get the title override
     *
     * @param string|null $title
     * @return self|null
     */
    public function overrideTitle($title = null)
    {
        if (is_null($title)) {
            return $this->override_title;
        }

        $this->override_title = $title;

        return $this;
    }

    /**
     * Whether or not there is a title override
     *
     * @return boolean
     */
    public function hasOverrideTitle()
    {
        return !is_null($this->override_title);
    }

    /**
     * Set or get the desccription override
     *
     * @param string|null $description
     * @return self|string
     */
    public function overrideDescription($description = null)
    {
        if (is_null($description)) {
            return $this->override_description;
        }

        $this->override_description = $description;

        return $this;
    }

    /**
     * Whether or not there is a description override
     *
     * @return boolean
     */
    public function hasOverrideDescription()
    {
        return !is_null($this->override_description);
    }

    /**
     * Set or get the keywords override
     *
     * @param string|null $keywords
     * @return self|string
     */
    public function overrideKeywords($keywords = null)
    {
        if (is_null($keywords)) {
            return $this->override_keywords;
        }

        $this->override_keywords = $keywords;

        return $this;
    }

    /**
     * Whether or not there is an keywords override set
     *
     * @return boolean
     */
    public function hasOverrideKeywords()
    {
        return !is_null($this->override_keywords);
    }

    /**
     * Set or get the override image
     *
     * @param ImageFile|null $image
     * @return self|ImageFile
     */
    public function overrideImage($image = null)
    {
        if (is_null($image)) {
            return $this->override_image;
        }

        $this->override_image = $image;

        return $this;
    }

    /**
     * Override the Facebook meta image with a URL
     *
     * @param string|null $url
     * @return void
     */
    public function overrideFacebookImageUrl(string $url = null)
    {
        if (is_null($url)) {
            return $this->override_facebook_image_url;
        }

        $this->override_facebook_image_url = $url;

        return $this;
    }

    public function overrideTwitterImageUrl(string $url = null)
    {
        if (is_null($url)) {
            return $this->override_twitter_image_url;
        }

        $this->override_twitter_image_url = $url;

        return $this;
    }

    /**
     * Whether or not an override image has been set
     *
     * @return boolean
     */
    public function hasOverrideImage()
    {
        return !is_null($this->override_image);
    }

    /**************************************************************************
     * Facebook
     **************************************************************************/

    /**
     * Image relation ship for Facebook Meta Image.
     *
     * @return ImageFile
     */
    public function facebookImage()
    {
        return $this->belongsTo(resolve(ImageFile::class), 'facebook_image_id');
    }

    /**
     * Getter for Facebook Meta Image. If empty, return the general Meta Image
     *
     * @return ImageFile
     */
    public function getFacebookImage()
    {
        if ($this->hasFacebookImage(false)) {
            return $this->facebookImage;
        }

        if ($this->hasMetaImage() || $this->hasOverrideImage()) {
            return $this->getMetaImage();
        }

        if (!empty(EnsoSettings::get('facebook_image_id'))) {
            return $this->getSettingImage('facebook_image_id');
        }

        return $this->getMetaImage();
    }

    /**
     * Whether or not there is a Facebook image
     *
     * @return boolean
     */
    public function hasFacebookImage($check_fallback = false)
    {
        if (!is_null($this->facebookImage)) {
            return true;
        }

        if (!$check_fallback) {
            return false;
        }

        if ($this->override_facebook_image_url) {
            return true;
        }

        if ($this->hasMetaImage() || $this->hasOverrideImage()) {
            return true;
        }

        if (!empty(EnsoSettings::get('facebook_image_id'))) {
            return true;
        }

        if (!empty(EnsoSettings::get('meta_image_id'))) {
            return true;
        }

        return false;
    }

    /**
     * Getter for Facebook Meta Title. If empty, return the general Meta Title
     *
     * @return string
     */
    public function getFacebookTitle()
    {
        if ($this->hasFacebookTitle()) {
            return $this->facebook_title;
        }

        if ($this->hasMetaTitle()) {
            return $this->meta_title;
        }

        if ($this->hasOverrideTitle()) {
            return $this->overrideTitle();
        }

        return EnsoSettings::get('facebook_title', $this->getMetaTitle());
    }

    /**
     * Whether or not this Meta has a facebook title.
     *
     * Does not check fallbacks.
     *
     * @return boolean
     */
    public function hasFacebookTitle()
    {
        return !empty($this->facebook_title);
    }

    /**
     * Getter for Facebook Meta Description. If empty, return the general Meta
     * Description
     *
     * @return string
     */
    public function getFacebookDescription()
    {
        if ($this->hasFacebookDescription()) {
            return $this->facebook_description;
        }

        if ($this->hasMetaDescription()) {
            return $this->meta_description;
        }

        if ($this->hasOverrideDescription()) {
            return $this->override_description;
        }

        return EnsoSettings::get('facebook_description', $this->getMetaDescription());
    }


    /**
     * Whether or not this Meta has a facebook description.
     *
     * Does not check fallbacks.
     *
     * @return boolean
     */
    public function hasFacebookDescription()
    {
        return !empty($this->facebook_description);
    }

    /**
     * The width of the facebook image
     *
     * @return int
     */
    public function getFacebookImageWidth()
    {
        return 1200;
    }

    /**
     * The height of the facebook image
     *
     * @return int
     */
    public function getFacebookImageHeight()
    {
        return 630;
    }

    /**
     * The Enso image preset name for the Facebook image
     *
     * @return string
     */
    public function getFacebookSizePreset()
    {
        return 'og_large';
    }

    /**
     * The URL of the Facebook image
     *
     * @return string
     */
    public function getFacebookImageUrl()
    {
        $size = $this->getFacebookSizePreset();
        $image = $this->getFacebookImage();

        if ($this->override_facebook_image_url) {
            return $this->override_facebook_image_url;
        } elseif ($image) {
            return $image->getResizeUrl($size, true);
        } else {
            return null;
        }
    }

    /**************************************************************************
     * Twitter
     **************************************************************************/

    /**
     * Image relation ship for twitter Meta Image.
     *
     * @return ImageFile
     */
    public function twitterImage()
    {
        return $this->belongsTo(resolve(ImageFile::class), 'twitter_image_id');
    }

    /**
     * Getter for Twitter Meta Image. If empty, return the general Meta Image
     *
     * @return ImageFile
     */
    public function getTwitterImage()
    {
        if ($this->hasTwitterImage(false)) {
            return $this->twitterImage;
        }

        if ($this->hasMetaImage() || $this->hasOverrideImage()) {
            return $this->getMetaImage();
        }

        if (!empty(EnsoSettings::get('twitter_image_id'))) {
            return $this->getSettingImage('twitter_image_id');
        }

        return $this->getMetaImage();
    }

    /**
     * Whether or not there is an image that can be used for sharing on Twitter
     *
     * @param boolean $check_fallback
     * @return boolean
     */
    public function hasTwitterImage($check_fallback = false)
    {
        if (!is_null($this->twitterImage)) {
            return true;
        }

        if (!$check_fallback) {
            return false;
        }

        if ($this->override_twitter_image_url) {
            return true;
        }

        if ($this->hasMetaImage() || $this->hasOverrideImage()) {
            return true;
        }

        if (!empty(EnsoSettings::get('twitter_image_id'))) {
            return true;
        }

        if (!empty(EnsoSettings::get('meta_image_id'))) {
            return true;
        }

        return false;
    }

    /**
     * Getter for Twitter Meta Title. If empty, return the general Meta Title
     *
     * @return string
     */
    public function getTwitterTitle()
    {
        if ($this->hasTwitterTitle()) {
            return $this->twitter_title;
        }

        if ($this->hasMetaTitle()) {
            return $this->meta_title;
        }

        if ($this->hasOverrideTitle()) {
            return $this->overrideTitle();
        }

        return EnsoSettings::get('twitter_title', $this->getMetaTitle());
    }

    /**
     * Whether or not there is a Twitter title set
     *
     * @return boolean
     */
    public function hasTwitterTitle()
    {
        return !empty($this->twitter_title);
    }

    /**
     * Getter for Twitter Meta Description. If empty, return the general Meta
     * Description
     *
     * @return string
     */
    public function getTwitterDescription()
    {
        if (!empty($this->twitter_description)) {
            return $this->twitter_description;
        }

        return EnsoSettings::get('twitter_description', $this->getMetaDescription());
    }

    /**
     * Whether or not there is a Twitter description set
     *
     * @return boolean
     */
    public function hasTwitterDescription()
    {
        return !empty($this->twitter_description);
    }

    /**
     * Get the Twitter card type that should be used
     *
     * @return string
     */
    public function getTwitterCardType()
    {
        if (!empty($this->twitter_card)) {
            return $this->twitter_card;
        }

        return EnsoSettings::get('twitter_card', 'summary');
    }

    /**
     * Whether or not the Twitter site account has been set o this Meta
     *
     * @return boolean
     */
    public function hasTwitterSite()
    {
        return !empty($this->getTwitterSite());
    }

    /**
     * Get the Twitter site account
     *
     * @return string|null
     */
    public function getTwitterSite()
    {
        if (!empty($this->twitter_site)) {
            return $this->twitter_site;
        }

        return EnsoSettings::get('twitter_site', null);
    }

    /**
     * Whether or not there is a Twitter creator set on this Meta
     *
     * @return boolean
     */
    public function hasTwitterCreator()
    {
        return !is_null($this->getTwitterCreator());
    }

    /**
     * Get the creator twitter account set on this meta
     *
     * @return string|null
     */
    public function getTwitterCreator()
    {
        if (!empty($this->twitter_creator)) {
            return $this->twitter_creator;
        }

        return EnsoSettings::get('twitter_creator', null);
    }

    /**
     * Get the image preset to use with the available image
     *
     * @todo add some logic to choose which image size is most appropriate
     * @return string
     */
    public function getTwitterImagePreset()
    {
        if ($this->getTwitterCardType() === 'summary') {
            return 'twitter_square_large';
        }

        return 'twitter_wide_large';
    }

    /**
     * Get a URL to an image to use when sharing on Twitter
     *
     * @return string
     */
    public function getTwitterImageUrl()
    {
        $size = $this->getTwitterImagePreset();
        $image = $this->getTwitterImage();

        if ($this->override_twitter_image_url) {
            return $this->override_twitter_image_url;
        } elseif ($image) {
            return $image->getResizeUrl($size, true);
        } else {
            return null;
        }
    }

    /**
     * Take an EnsoSetting slug and return the related ImageFile
     *
     * @param string $setting
     * @return ImageFile | null
     */
    protected function getSettingImage($setting)
    {
        $image_data = Arr::first(EnsoSettings::get($setting, []));
        $image_id = $image_data['id'] ?? null;
        $instance = resolve(ImageFile::class);

        return $instance::find($image_id);
    }
}
