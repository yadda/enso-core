<?php

namespace Yadda\Enso\Media\Crud;

use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Forms\Fields\DateTimeField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Crud\Tables\Text;
use Yadda\Enso\Crud\Tables\Thumbnail;

class File extends Config
{
    public function configure()
    {
        $file_model = config('enso.crud.file.model');

        $this->model($file_model)
            ->route('admin.media')
            ->views('file')
            ->name('File')
            ->paginate(24)
            ->order('created_at', 'desc')
            ->searchColumns(['filename', 'original_filename', 'title'])
            ->columns([
                Thumbnail::make('preview')
                    ->setLabel('')
                    ->orderableBy(null),
                Text::make('original_filename')
                    ->setLabel('Filename'),
                Text::make('mimetype'),
                Text::make('filesize')
                    ->setFormatter(function ($bytes) {
                        $base = log($bytes) / log(1024);
                        $suffix = array('B', 'KB', 'MB', 'GB', 'TB');
                        $f_base = floor($base);
                        return round(pow(1024, $base - floor($base)), 1) . $suffix[$f_base];
                    }),
            ])
            ->setBulkActions([
                'actions' => [
                    'delete' => [
                        'route' => route('admin.media.delete-many'),
                        'method' => 'DELETE',
                        'label' => 'Delete',
                    ],
                ]
            ]);
    }

    public function create(Form $form)
    {
        $form->addSections([
            0 => Section::make('main')
                ->addFields([
                    TextField::make('title'),
                    TextField::make('caption'),
                    TextField::make('alt_text'),
                    TextField::make('filename')
                        ->setDisabled(true)
                        ->setReadonly(true),
                    TextField::make('original_filename')
                        ->setReadonly(true)
                        ->setDisabled(true),
                    TextField::make('mimetype')
                        ->setDisabled(true)
                        ->setReadonly(true)
                        ->addFieldsetClass('is-half'),
                    TextField::make('filesize')
                        ->setDisabled(true)
                        ->setReadonly(true)
                        ->addFieldsetClass('is-half'),
                    DateTimeField::make('created_at')
                        ->setReadonly(true)
                        ->setDisabled(true),
                    DateTimeField::make('updated_at')
                        ->setReadonly(true)
                        ->setDisabled(true),
                ]),
        ]);

        return $form;
    }
}
