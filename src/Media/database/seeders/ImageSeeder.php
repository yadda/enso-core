<?php

namespace Yadda\Enso\Media\database\seeders;

use Illuminate\Database\Seeder;
use Yadda\Enso\Media\Contracts\ImageFile;
use Yadda\Enso\Utilities\Helpers;

class ImageSeeder extends Seeder
{
    protected $image_class;

    public function __construct()
    {
        $this->image_class = Helpers::getConcreteClass(ImageFile::class);
    }

    public function run()
    {
        $image_count = 10;

        for ($i=0; $i < $image_count; $i++) {
            $this->makeFile();
        }
    }

    protected function makeFile()
    {
        $image = new $this->image_class;
        $image->useRemoteFile('https://picsum.photos/1920/1080', '', 'testfile.jpg');
        $image->mimetype = 'image/jpeg';
        $image = $this->image_class::getCorrectModelType($image);
        $image->setDerivedProperties();
        $image->save();
    }
}
