<?php

namespace Yadda\Enso\Media\Models;

use Yadda\Enso\Media\Models\MediaFile;

/**
 * An extension of MediaFile specifically for audio files
 */
class AudioFile extends MediaFile
{
    /**
     * Base folder in which to put audio files
     *
     * @var string
     */
    protected $folder = 'audio';

    /**
     * Return the URL to a preview image
     *
     * @return string
     */
    public function getPreview(): string
    {
        return asset('svg/enso/icon-file-audio.svg');
    }

    /**
     * Return the URL for a detailed preview image for
     * when viewing the file's details
     *
     * @return string
     */
    public function getPreviewFull(): string
    {
        return asset('svg/enso/icon-file-audio.svg');
    }
}
