<?php

namespace Yadda\Enso\Media\Models;

use Exception;
use Intervention\Image\ImageManagerStatic as InterventionImageManager;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;
use Storage;
use Yadda\Enso\Media\Models\MediaFile;

/**
 * An extension of MediaFile specifically for video files
 */
class VideoFile extends MediaFile
{
    /**
     * Base folder in which to put video files
     *
     * @var string
     */
    protected $folder = 'videos';

    /**
     * Return the URL to a preview image
     *
     * @return string
     */
    public function getPreview(): string
    {
        if ($this->thumbnailExists()) {
            return $this->getThumbnailUrl();
        }

        if (!$this->canMakeThumbnail()) {
            return $this->defaultThumbnail();
        }

        if ($this->makeThumbnail()) {
            return $this->getThumbnailUrl();
        } else {
            return $this->defaultThumbnail();
        }
    }

    /**
     * Return the URL for a detailed preview image for
     * when viewing the file's details
     *
     * @return string
     */
    public function getPreviewFull(): string
    {
        return $this->getPreview();
    }

    /**
     * Check if we're able to generate thumbnails
     *
     * @return boolean
     */
    public function canMakeThumbnail(): bool
    {
        if (empty(trim(shell_exec('which ffmpeg')))) {
            return false;
        }

        if (empty(trim(shell_exec('which ffprobe')))) {
            return false;
        }

        return true;
    }

    /**
     * Generate the thumbnail file
     */
    public function makeThumbnail(): bool
    {
        if (!$this->canMakeThumbnail()) {
            return false;
        }

        $initial_memory = ini_get('memory_limit');
        ini_set('memory_limit', $this->memory_limit);

        try {
            $image = FFMpeg::fromDisk($this->disk)
                ->open($this->getPath())
                ->getFrameFromSeconds(0)
                ->export()
                ->toDisk($this->disk)
                ->save($this->getThumbnailPath());

            $content = Storage::disk($this->disk)->get($this->getThumbnailPath());
            $image = InterventionImageManager::make($content);
            $image->fit(200, 200);

            Storage::disk($this->disk)
                ->put($this->getThumbnailPath(), $image->stream());

            ini_set('memory_limit', $initial_memory);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * The URL to use for the thumbnail if no file exists or can be made
     */
    protected function defaultThumbnail(): string
    {
        return asset('svg/enso/icon-file-video.svg');
    }

    /**
     * Whether the thumbnail has already been created
     */
    public function thumbnailExists(): bool
    {
        try {
            $file_path = $this->getThumbnailPath();
            return Storage::disk($this->disk)->exists($file_path);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * The path to the thumbnail for this video
     */
    public function getThumbnailPath(string $preset_name = 'uploader_preview'): string
    {
        $filename = pathinfo($this->filename, PATHINFO_FILENAME) . '.jpg';

        return $this->concatenateFilepathSegments([
            config('enso.media.directory'),
            $this->folder,
            $this->path,
            $preset_name,
            $filename
        ]);
    }

    /**
     * The URL to the thumbnail
     */
    public function getThumbnailUrl(string $preset_name = 'uploader_preview'): string
    {
        $relative_path = $this->getThumbnailPath();
        return Storage::disk($this->disk)->url($relative_path);
    }
}
