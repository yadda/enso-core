<?php

namespace Yadda\Enso\Media\Models;

use Exception;
use File;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use Intervention\Image\Image;
use Intervention\Image\ImageManagerStatic as InterventionImageManager;
use Log;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;
use Storage;
use Yadda\Enso\Media\Colors;
use Yadda\Enso\Media\Contracts\ImageFile as ImageFileContract;
use Yadda\Enso\Media\Filters\Preset;
use Yadda\Enso\Media\Models\MediaFile;

/**
 * An extension of MediaFile specifically for audio files
 */
class ImageFile extends MediaFile implements ImageFileContract
{
    /**
     * An instance of the image as an Intervention Image
     *
     * @var Intervention\Image\Image
     */
    protected $image_instance;

    /**
     * Stash files' existence here so if the same call is made on this
     * ImageFile multiple times, it doesn't repeat the filesystem call
     *
     * @var array
     */
    protected $size_exists = [];

    /**
     * Base folder to put images in
     *
     * @var string
     */
    protected $folder = 'images';

    /**
     * Create a new image file model
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * Upload a given file into a given subdirectory of the media directory
     *
     * @param File   $file the uploaded file
     * @param string $path relative path to store the image
     *
     * @return void
     */
    public function useUpload($file, $path, $filename = null): void
    {
        $initial_memory = ini_get('memory_limit');
        ini_set('memory_limit', $this->memory_limit);

        if (is_null($filename)) {
            $this->original_filename = $file->getClientOriginalName();
        } else {
            $this->original_filename = $filename;
        }

        $this->path = $path;
        $dir = $this->getRelativeDirectory();

        // @todo split validation, EXIF correction etc out into separate functions

        try {
            $real_path = $file->getRealPath();
            $image = InterventionImageManager::make($real_path);
        } catch (Exception $e) {
            Log::error($e);
            throw new Exception('Image file uploaded that can\'t be instantiated');
        }

        $image = $this->fixRotation($image, $file);

        try {
            $image->save();
            $image_file = new \Illuminate\Http\File($real_path);
        } catch (Exception $e) {
            Log::error($e);
            throw new Exception('Image file uploaded that can\'t be instantiated');
        }

        $this->filename = $this->makeUniqueFilename();

        Storage::disk($this->disk)->putFileAs($dir, $image_file, $this->filename);

        ini_set('memory_limit', $initial_memory);
    }

    /**
     * A method to extend for defining filetype specific properties to the
     * fileinfo property of this file.
     *
     * @return void
     */
    protected function setDerivedFileinfoProperties(): void
    {
        $initial_memory = ini_get('memory_limit');
        ini_set('memory_limit', $this->memory_limit);

        try {
            if (empty($this->image_instance)) {
                $this->loadImage();
            }

            $this->setHeight($this->image_instance->height());
            $this->setWidth($this->image_instance->width());
        } catch (Exception $e) {
            Log::error($e);
        }

        ini_set('memory_limit', $initial_memory);
    }

    /**
     * Load an image from disk and create an InverventionImage instance
     *
     * @return void
     */
    protected function loadImage(): void
    {
        $content = Storage::disk($this->disk)->get($this->getPath());
        $this->image_instance = InterventionImageManager::make($content);
    }

    /**
     * Get the binary content of the file as a string
     *
     * @return string
     */
    protected function getImageContent(): string
    {
        return Storage::disk($this->disk)->get($this->getPath());
    }

    /**
     * Checks that a given preset name exists
     *
     * @param string $preset_name Preset name to tests
     *
     * @return bool Whether it exists
     */
    protected function checkPresetNameExists($preset_name): bool
    {
        if (array_key_exists($preset_name, Preset::$builtin_presets)) {
            return true;
        }

        return array_key_exists($preset_name, config('enso.media.presets'));
    }

    /**
     * returns the height of the given image
     *
     * @return int height of image
     */
    public function getHeightAttribute(): int
    {
        return $this->getFileinfoProperty('height');
    }

    /**
     * Prevents setting of height directly, as it should always be set via
     * setDerivedFileinfoProperties.
     *
     * @param mixed $irrelevant doesn't matter
     *
     * @throws Exception
     */
    public function setHeightAttribute($irrelevant): void
    {
        throw new Exception('You cannot directly set the height of an image');
    }

    /**
     * Set the height of the image
     *
     * @param integer $height height to set
     *
     * @return void
     */
    protected function setHeight(int $height): void
    {
        $this->setFileinfoProperty('height', $height);
    }

    /**
     * returns the width of the given image
     *
     * @return int width of image
     */
    public function getWidthAttribute(): int
    {
        return $this->getFileinfoProperty('width');
    }

    /**
     * Prevents setting of width directly, as it should always be set via
     * setDerivedFileinfoProperties
     *
     * @throws Exception
     *
     * @param mixed $irrelevant doesn't matter
     *
     * @return void
     */
    public function setWidthAttribute($irrelevant): void
    {
        throw new Exception('You cannot directly set the width of an image');
    }

    /**
     * Set the width of the image
     *
     * @param int $width width to set
     *
     * @return void
     */
    protected function setWidth(int $width): void
    {
        $this->setFileinfoProperty('width', $width);
    }

    /**
     * Creates resize images for each of the given presets. Can also pass a
     * string for a single preset
     *
     * @param mixed   $presets          preset resizes to make
     * @param boolean $force_regenerate whether to force regeneration
     * @param array   $force_regenerate Background color to use for images not
     *                                  set to 'cover' style
     *
     * @return void
     */
    public function makeResizes($presets, $force_regenerate = false, $extension = null): void
    {
        foreach ((array) $presets as $preset_name) {
            $this->makeResize($preset_name, $force_regenerate, $extension);
        }
    }

    /**
     * Makes a resized image based on the size of the image and the requested
     * preset size
     *
     * @param string $preset_name      Preset to make
     * @param bool   $force_regenerate If true, will only generate image if
     *                                 it doesn't already exist
     *
     * @todo either rename this function or refactor this. This now applies
     * other filters as well as resizing
     *
     * @return void
     */
    public function makeResize($preset_name, $force_regenerate = false, $extension = null): void
    {
        if (!$force_regenerate && $this->resizeExists($preset_name, $extension)) {
            return;
        }

        if (!$this->fileExists($this->getPath())) {
            Log::error('File does not exist. Id: ' . $this->id);
            return;
        }

        $initial_memory = ini_get('memory_limit');
        ini_set('memory_limit', $this->memory_limit);

        try {
            // This needs to be run each time that an image is processed or
            // applied filters will not have their opacity applied correctly.
            // This may be a bug in InterventionImage and if we can find a way
            // around that then we can change this to only load the image once
            $this->loadImage();

            $resized_instance = $this->image_instance->filter(
                new Preset($preset_name, $this->getFileinfoProperty('focus_x'), $this->getFileinfoProperty('focus_y'))
            );

            $relative_path = $this->getResizePath($preset_name, $extension);
            $relative_temp_path = 'temp/' . $relative_path;

            Storage::disk('local')->put($relative_temp_path, $resized_instance->stream($extension)->getContents());

            $temp_path = Storage::disk('local')->path($relative_temp_path);

            if (config('enso.media.optimize_images')) {
                $this->optimizeImage($temp_path);
            }

            Storage::disk($this->disk)->put($relative_path, Storage::disk('local')->get($relative_temp_path));
            Storage::disk('local')->delete($relative_temp_path);
        } catch (Exception $e) {
            Log::error('Error making resize "' . $preset_name . '" for Image with id: "' . $this->id . '"');
        }

        ini_set('memory_limit', $initial_memory);
    }

    /**
     * Take a path to an image and optimize it. If $output_path is given save
     * the resulting file there, otherwise overwrite the original file
     *
     * @param String $input_path  path of image to optimize
     * @param String $output_path path to store optimized image
     *
     * @return void
     */
    public function optimizeImage($input_path, $output_path = null): void
    {
        if (is_null($output_path)) {
            $output_path = $input_path;
        }

        ImageOptimizer::optimize($input_path, $output_path);
    }

    /**
     * Gets the url to the resized file that this model represents, as specified
     * by $preset_name.
     *
     * If the original file cannot be found a new file will not be generated
     * but the path will be returned anyway.
     *
     * @param string $preset_name        name of preset to get url for
     * @param string $generate_if_needed check for existence and generate if
     *                                   not found
     *
     * @return string full url of file
     */
    public function getResizeUrl($preset_name, $generate_if_needed = false, $extension = null): string
    {
        /**
         * Bypass resizing logic when file is not an image, as it is guaranteed
         * not to be able to be passed through Intervention. Implemented with
         * SVG's primarily in mind.
         */
        if (Arr::get(Config::get('enso.media.mime_types'), $this->mimetype, 'file') !== 'image') {
            return static::getCorrectModelType($this)->getUrl();
        }

        $relative_path = $this->getResizePath($preset_name, $extension);

        if ($generate_if_needed) {
            $this->makeResize($preset_name, false, $extension);
        }

        return Storage::disk($this->disk)->url($relative_path);
    }

    /**
     * Gets the relative path for the resized file that this model represents
     * as specified by $preset_name
     *
     * @param string $preset_name name of preset to get path for
     *
     * @return string full path to file
     */
    public function getResizePath($preset_name, $extension = null): string
    {
        if (!$this->checkPresetNameExists($preset_name)) {
            throw new Exception('Requesting path of image at an unknown size preset: ' . e($preset_name));
        }

        if (is_null($extension)) {
            $filename = $this->filename;
        } else {
            $filename = pathinfo($this->filename, PATHINFO_FILENAME) . '.' . $extension;
        }

        return $this->concatenateFilepathSegments([
            config('enso.media.directory'),
            $this->folder,
            $this->path,
            $preset_name,
            $filename
        ]);
    }

    /**
     * Return all potential paths for this file. These paths may not point to
     * existing files, it is an exhasutive list for making sure everything
     * gets deleted.
     *
     * @return array
     */
    public function getAllPaths(): array
    {
        $paths = parent::getAllPaths();

        foreach (array_merge(
            Preset::$builtin_presets,
            config('enso.media.presets')
        ) as $preset => $value) {
            $paths[] = $this->getResizePath($preset);

            // @todo this is not the best way to do this
            $paths[] = $this->getResizePath($preset, 'webp');
        }

        return $paths;
    }

    /**
     * Fix an images rotation based on its EXIF data
     *
     * @todo find out why we have the image in two formats here
     *
     * @param Image $image Image as an Image
     * @param File  $file  Image as a File
     *
     * @return Image
     */
    protected function fixRotation($image, $file): Image
    {
        $exif = [];

        try {
            $exif = exif_read_data($file->getPathName());
        } catch (Exception $e) {
            // exif_read_data throws an exception on failure. This might mean
            // that there is no exif data. It might also mean that
            // exif_read_data is actually failing (e.g. iPhones can give an
            // "illegal IFD size" error), so we fallback to InterventionImage's
            // implmentation. We could just use that but apparently that fails
            // in certain circumstances too.
        }

        if (empty($exif['Orientation'])) {
            try {
                $image = InterventionImageManager::make($file->getPathName());
                $exif = $image->exif();
            } catch (Exception $e) {
                // If there's an error, we don't rotate
            }
        }

        if (!empty($exif['Orientation'])) {
            switch ($exif['Orientation']) {
                case 2: // horizontal flip
                    $image->flip('h');
                    break;

                case 3: // 180 rotate left
                    $image->rotate(180);
                    break;

                case 4: // vertical flip
                    $image->flip('v');
                    break;

                case 5: // vertical flip + 90 rotate right
                    $image->flip('v');
                    $image->rotate(-90);
                    break;

                case 6: // 90 rotate right
                    $image->rotate(-90);
                    break;

                case 7: // horizontal flip + 90 rotate right
                    $image->flip('h');
                    $image->rotate(-90);
                    break;

                case 8: // 90 rotate left
                    $image->rotate(90);
                    break;

                case 1: // nothing
                default:
                    break;
            }
        }

        return $image;
    }

    /**
     * Checks whether a resize for a given image exists, keeping a record if
     * it does to prevent repeat checks
     *
     * @param string $preset_name name of preset to check
     *
     * @return boolean whether it exists
     */
    protected function resizeExists($preset_name, $extension = null): string
    {
        $format = $extension ? $extension : 'DEFAULT';

        if (!empty(Arr::get($this->size_exists, "{$format}.{$preset_name}"))) {
            return Arr::get($this->size_exists, "{$format}.{$preset_name}");
        }

        try {
            $file_path = $this->getResizePath($preset_name, $extension);
            $exists = Storage::disk($this->disk)->exists($file_path);
            Arr::set($this->size_exists, "{$format}.{$preset_name}", $exists);
            return $exists;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Return the URL to a preview image
     *
     * @return String
     */
    public function getPreview(): string
    {
        if ($this->isSvg()) {
            return $this->getUrl();
        }

        return $this->getResizeUrl('uploader_preview', true);
    }

    /**
     * Return the URL for a detailed preview image for
     * when viewing the file's details
     *
     * @return string
     */
    public function getPreviewFull(): string
    {
        if ($this->isSvg()) {
            return $this->getUrl();
        }

        return $this->getResizeUrl('uploader_preview_full', true);
    }

    /**
     * Rename the file and it's resized versions
     *
     * @param string $new_name The new filename. If null, a new filename is
     *                         created based on the original filename
     *
     * @return void
     */
    public function rename($new_name = null)
    {
        if (is_null($new_name)) {
            $new_name = $this->original_filename;
        }

        $new_name = $this->makeFilename($new_name);
        $from = $this->getPath();
        $to = pathinfo($from, PATHINFO_DIRNAME) . '/' . $new_name;

        if (Storage::disk($this->disk)->exists($from)) {
            Storage::disk($this->disk)->move($from, $to);
        }

        $sizes = array_keys(config('enso.media.presets', []));

        foreach ($sizes as $size) {
            $from = $this->getResizePath($size);
            $to = pathinfo($from, PATHINFO_DIRNAME) . '/' . $new_name;

            if (Storage::disk($this->disk)->exists($from)) {
                Storage::disk($this->disk)->move($from, $to);
            }
        }

        // Do the same but for WEBP files. There must be a better way to do this
        foreach ($sizes as $size) {
            $from = $this->getResizePath($size, 'webp');
            $to = pathinfo($from, PATHINFO_DIRNAME) . '/' . $new_name;

            if (Storage::disk($this->disk)->exists($from)) {
                Storage::disk($this->disk)->move($from, $to);
            }
        }

        $this->filename = $new_name;
        $this->save();
    }

    /**
     * Delete a given resize for this image if it exists
     *
     * @param string $preset_name
     *
     * @return void
     */
    public function deleteResize($preset_name, $extension = null)
    {
        if (!$this->resizeExists($preset_name, $extension)) {
            return;
        }

        $file_path = $this->getResizePath($preset_name, $extension);

        Storage::disk($this->disk)->delete($file_path);
    }

    /**
     * Delete all resized files for this image
     *
     * @return void
     */
    public function deleteAllResizes()
    {
        foreach (array_keys($this->getAllPresets()) as $preset_name) {
            $this->deleteResize($preset_name);
            $this->deleteResize($preset_name, 'webp');
        }
    }

    /**
     * Get an array of all custom and built in presets
     *
     * @return array
     */
    public function getAllPresets()
    {
        return array_merge(config('enso.media.presets'), Preset::$builtin_presets);
    }

    /**
     * Recreate any existing images resizes
     *
     * @return void
     */
    public function regenerateExistingResizes()
    {
        foreach (array_keys($this->getAllPresets()) as $preset_name) {
            if ($this->resizeExists($preset_name)) {
                $this->makeResize($preset_name, true);
            }

            if ($this->resizeExists($preset_name, 'webp')) {
                $this->makeResize($preset_name, true, 'webp');
            }
        }
    }

    /**
     * Ensures that this ImageFile has is_light fileinfo for a given preset (
     * or no preset if not specified) for a given set of areas.
     *
     * @param array  areas
     * @param string $preset
     *
     * @return ImageFileContract
     */
    public function lightDark(array $areas, string $preset = null): ImageFileContract
    {
        $areas = array_filter($areas, function ($area_key) use ($preset) {
            $base = $this->fileinfo['is_light'][$preset ?? 'no_preset'] ?? [];

            return !array_key_exists($area_key, $base);
        });

        if (!empty($areas)) {
            Colors::setLightDarkFor($this, $areas, $preset);
        }

        return $this;
    }
}
