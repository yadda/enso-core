<?php

namespace Yadda\Enso\Media\Controllers;

use EnsoMedia;
use Illuminate\Http\Response as ResponseClass;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Response;

class ImportController extends Controller
{
    /**
     * Searches the given directory path, and created and moves database images
     * 'as if' they were uploads. Deletes successfully imported images, to avoid
     * excessive data duplication.
     *
     * @param string $directory_path path to find files in
     *
     * @return \Illuminate\Http\Response response
     */
    public function importDirectory(string $directory_path = null)
    {
        // Default path is storage/imports
        if (is_null($directory_path)) {
            $directory_path = storage_path('imports');
        }

        // Creates a recursive iterator so that we can maintain the directory
        // structure of the imported files. This may be useful in future
        // iterations of Enso's media management.
        $dir_iterator = new RecursiveDirectoryIterator($directory_path);
        $iterator = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::CHILD_FIRST);

        $substr_len = strlen($directory_path);

        $import_subdirectory = config('enso.media.import_directory', 'imports');
        $import_responses = new Collection;

        foreach ($iterator as $fileinfo) {
            // Essentially, If not a file, do nothing and move on to the next.
            // Otherwise, fake a Laravel 'UploadFile' and pretend it has been
            // uploaded
            if ($fileinfo->isFile()) {
                $uploaded_file = new UploadedFile(
                    $fileinfo->getRealPath(),
                    $fileinfo->getFilename()
                );

                // Gets the relative directory from the path.
                $path = $import_subdirectory . substr($fileinfo->getPath(), ($substr_len));

                $response = EnsoMedia::uploadFile(
                    $uploaded_file,
                    EnsoMedia::typeFromMime($uploaded_file->getMimeType()),
                    $path
                );

                $import_responses->put($fileinfo->getRealPath(), $response);

                // Delete file to import on success
                if ($response->status() === ResponseClass::HTTP_OK) {
                    $filename = $fileinfo->getRealPath();
                    $fileinfo = null;
                    unlink($filename);
                }

                continue;
            }
        }

        return $this->buildImportDirectoryResponse($import_responses);
    }

    /**
     * Builds the resposne from attempting a directory import. Extend Controller
     * and override this function to provide a customised response.
     *
     * @return \Illuminate\Http\Response
     */
    protected function buildImportDirectoryResponse(Collection $file_statuses)
    {
        $response_code = ResponseClass::HTTP_OK;

        foreach ($file_statuses as $response) {
            if ($response->status() !== ResponseClass::HTTP_OK) {
                $response_code = ResponseClass::HTTP_ACCEPTED;
                break;
            }
        }

        return response()->json($file_statuses->toJson(), $response_code);
    }
}
