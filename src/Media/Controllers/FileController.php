<?php

namespace Yadda\Enso\Media\Controllers;

use Enso;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Yadda\Enso\Crud\Controller;
use Yadda\Enso\Media\Contracts\MediaFile as MediaFileContract;
use Yadda\Enso\Media\Exceptions\FileInUseException;

class FileController extends Controller
{
    /**
     * Name of this crud type
     *
     * This will be used to find config/model/controller
     *
     * @var string
     */
    protected $crud_name = 'file';

    /**
     * Show a file browser with previews
     *
     * @return \Illuminate\View\View
     */
    public function indexBrowser(Request $request)
    {
        $this->addCrudActionClass('browser');

        Enso::setJSData('crud', $this->getConfig()->getJSConfig());

        return view('enso-media::index', [
            'crud' => $this->getConfig(),
        ]);
    }

    /**
     * Add extra fields to the returned AJAX data that aren't defined
     * as index columns Crud but still need to be available in the
     * media browser view
     *
     * @param array                               $parsed previously parsed data
     * @param \Illuminate\Database\Eloquent\Model $item   Base item to get data from.
     *
     * @return array
     */
    public function addExtraFields($parsed, $item)
    {
        $item = resolve(MediaFileContract::class)::getCorrectModelType($item);

        $additional_fields = $item->toArray();

        return array_merge($parsed, $additional_fields);
    }

    /**
     * Updates the filename of a file, including all it's resizes.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $file_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateFilename(Request $request, $file_id)
    {
        $this->validate($request, [
            'new_filename' => ['required', 'string'],
        ]);

        $file = resolve(MediaFileContract::class)::getCorrectModelType(
            resolve(MediaFileContract::class)::findOrFail($file_id)
        );

        $this->moveFilesToNewFilename(
            $file,
            $request->get('new_filename')
        );

        return response()->json([
            'status' => 'success',
            'data' => [
                'filename' => $file->filename,
                'original_filename' => $file->original_filename,
                'preview' => $file->getPreview(),
                'url' => $file->getUrl(),
            ],
        ]);
    }

    /**
     * Updates all relevant files on the current Disk to use the new filename
     *
     * @param \Yadda\Enso\Media\Contracts\MediaFile $file
     * @param string                                $new_original_filename
     *
     * @return Yadda\Enso\Media\Contracts\MediaFile
     */
    protected function moveFilesToNewFilename(
        MediaFileContract $file,
        string $new_original_filename
    ): MediaFileContract {
        $new_filename = resolve(MediaFileContract::class)
            ->makeFilename($new_original_filename);

        foreach ($file->getAllPaths() as $path) {
            if (Storage::disk(config('enso.media.disk'))->exists($path)) {
                Storage::disk(config('enso.media.disk'))->move(
                    $path,
                    dirname($path) . '/' . $new_filename
                );
            }
        }

        $file->filename = $new_filename;
        $file->original_filename = $new_original_filename;
        $file->save();

        return $file;
    }

    /**
     * A list of transformer callables. Each of these should take an array and
     * an instance of the model. The function should then alter the array in
     * whatever way it likes and then return the array. This array will then be
     * passed to the next transformer, along with the original model.
     *
     * If you are overriding this, you probably want to add your own
     * transformers to the end of the array. The first transformer will recieve
     * an empty array as the first parameter.
     *
     * @return array
     */
    public function getTransformers()
    {
        return [
            [$this, 'applyFormatters'],
            [$this, 'setRowClasses'],
            [$this, 'addIds'],
            [$this, 'addExtraFields'],
        ];
    }

    /**
     * Delete a single file
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFile($file_id)
    {
        try {
            $this->performDeletion($file_id);
        } catch (FileInUseException $e) {
            return response()->json([
                'status' => 'fail',
                'message' => 'That image cannot be deleted as it is in use elsewhere on the site.',
                'data' => null,
            ], 422);
        }

        return response()->json([
            'status' => 'success',
            'data' => null,
        ]);
    }

    /**
     * Delete multiple files
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFiles(Request $request)
    {
        $file_ids = $request->input('values');

        $in_use_count = 0;
        $failed_ids = [];

        foreach ($file_ids as $file_id) {
            try {
                $this->performDeletion($file_id);
            } catch (FileInUseException $e) {
                $in_use_count++;
                $failed_ids[] = $file_id;
            }
        }

        if ($in_use_count === 0) {
            return response()->json([
                'status' => 'success',
                'data' => null,
            ]);
        } else {
            return response()->json([
                'status' => 'fail',
                'message' => 'Some images could not be deleted as they are in use elsewhere.',
                'data' => [
                    'failed_ids' => $failed_ids,
                ],
            ]);
        }
    }

    /**
     * Delete a file by id
     *
     * @param int $file_id
     *
     * @return void
     */
    protected function performDeletion($file_id)
    {
        $file = resolve(MediaFileContract::class)::findOrFail($file_id);
        $file = resolve(MediaFileContract::class)::getCorrectModelType($file);

        $file->delete();
    }

    /**
     * Permforms that actual update functionality using provided data.
     *
     * @param array $data Data to perform update with
     *
     * @return void
     */
    protected function performUpdate($data)
    {
        $response = parent::performUpdate($data);

        $form = $this->getConfig()->getEditForm();
        $item = $form->getModelInstance();

        $new_focus_x = Arr::get($data, 'main.focus_x', null);
        $new_focus_y = Arr::get($data, 'main.focus_y', null);

        $old_focus_x = $item->getFileinfoProperty('focus_x');
        $old_focus_y = $item->getFileinfoProperty('focus_y');

        $item->setFileinfoProperty('focus_x', $new_focus_x);
        $item->setFileinfoProperty('focus_y', $new_focus_y);
        $item->save();

        if ($new_focus_x !== $old_focus_x || $new_focus_y !== $old_focus_y) {
            $item = resolve(MediaFileContract::class)::getCorrectModelType($item);
            $item->regenerateExistingResizes();

            $item->setFileinfoProperty('is_light', []);
            $item->save();
        }

        return $response;
    }
}
