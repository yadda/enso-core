<?php

namespace Yadda\Enso\Media;

use Auth;
use Exception;
use Illuminate\Http\File;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Log;
use Yadda\Enso\Media\Contracts\AudioFile;
use Yadda\Enso\Media\Contracts\ImageFile;
use Yadda\Enso\Media\Contracts\MediaFile;
use Yadda\Enso\Media\Contracts\VideoFile;

/**
 * Enso Media manager class
 */
class Media
{
    /**
     * Mapping of file types to their classes
     *
     * @var array
     */
    protected $upload_types;

    /**
     * Create a new Media
     */
    public function __construct()
    {
        $this->upload_types = [
            'image' => get_class(resolve(ImageFile::class)),
            'audio' => get_class(resolve(AudioFile::class)),
            'video' => get_class(resolve(VideoFile::class)),
            'file' => get_class(resolve(MediaFile::class)),
        ];
    }

    /**
     * Get a file type from a given MIME type
     *
     * @param string $mime MIME type
     *
     * @return string
     */
    public function typeFromMime($mime): string
    {
        $mime_types = config('enso.media.mime_types');

        if (array_key_exists($mime, $mime_types)) {
            return $mime_types[$mime];
        }

        return 'file';
    }

    /**
     * Create a blank Model for a given type
     *
     * @param string $type
     * @return void
     */
    public function getModelFromType(string $type)
    {
        return $this->upload_types[$type];
    }

    /**
     * Take a given file and save it as a model of the given type
     *
     * @param UploadedFile $file
     * @param string $type Type of file (see $this->upload_types)
     * @param string $path Where to put the file
     * @return mixed The created model
     */
    public function uploadFile($file, $type, $path)
    {
        if (!$this->checkUploadType($type)) {
            $message = 'Unknown upload type given: "' . $type . '"';

            Log::error($message);

            return response()->json($message, Response::HTTP_NOT_IMPLEMENTED);
        }

        if (!$this->checkMimeType($file->getMimeType())) {
            Log::error('Invalid MIME type in uploaded file.');

            return response()->json('Unsupported media Type', Response::HTTP_UNSUPPORTED_MEDIA_TYPE);
        }

        $item = $this->storeFile($file, $type, $path);
        $item = $item::getCorrectModelType($item);

        if (is_null($item)) {
            return response()->json('There was a problem with that file.', Response::HTTP_INTERNAL_SERVER_ERROR);
        } else {
            return response()->json([
                'status' => 'success',
                'data' => [
                    'id' => $item->id,
                    'preview' => $item->getPreview(),
                    'filename' => $item->filename,
                    'filesize' => $item->filesize,
                ]
            ], Response::HTTP_OK);
        }
    }

    /**
     * Test that a given upload type is allowed
     *
     * @param String $type
     * @return Boolean
     */
    public function checkUploadType($type)
    {
        return array_key_exists($type, $this->upload_types);
    }

    /**
     * Check that a MIME type is accepted
     *
     * For more info see symphony docs for MimeTypeGuesser
     *
     * @param String $type
     * @return void
     */
    public function checkMimeType($type)
    {
        $accepted_mime_types = array_keys(config('enso.media.mime_types', []));

        return in_array($type, $accepted_mime_types);
    }

    /**
     * Store a file on disk and a record in the database
     *
     * @param File $file
     * @param String $type e.g. image, audio, file or video
     * @param String $path relative path within storage
     * @param String $filename the desired file name
     * @return Yadda\Enso\Media\Contracts\MediaFile or null
     */
    public function storeFile($file, $type, $path, $filename = null)
    {
        if (!is_null($filename)) {
            $filename = $this->removeDotSegments($filename);
        }

        try {
            $path = $this->removeDotSegments($path);
            $media_file = new $this->upload_types[$type];
            $media_file->user_id = Auth::check() ? Auth::id() : null;
            $media_file->useUpload($file, $path, $filename);
            $media_file->setDerivedProperties();
            $media_file->save();
        } catch (Exception $e) {
            $message = 'Failed creating new MediaFile from upload type "' . $type . '"';
            Log::error($message);
            Log::error($e->getMessage());

            return null;
        }

        return $media_file;
    }

    /**
     * As storeFile, but takes a file stored locally instead of an unploaded file
     *
     * @param String $file path to local file
     * @param String $type e.g. image, audio, file or video
     * @param String $path relative path within storage
     * @return Yadda\Enso\Media\Contracts\MediaFile or null
     */
    public function storeLocalFile($file_path, $type, $path)
    {
        $file = new File($file_path);

        try {
            $path = $this->removeDotSegments($path);
            $media_file = new $this->upload_types[$type];
            $media_file->user_id = Auth::check() ? Auth::id() : null;
            $media_file->useFile($file, $path);
            $media_file->setDerivedProperties();
            $media_file->save();
        } catch (Exception $e) {
            $message = 'Failed creating new MediaFile from upload type "' . $type . '"';
            Log::error($message . ': ' . $e->getMessage());

            return null;
        }

        return $media_file;
    }

    /**
     * As storeFile, but takes a URL to a remote file
     *
     * @param String $url
     * @param String $type
     * @param String $path
     * @param String $filename the desired file name
     * @return Yadda\Enso\Media\Contracts\MediaFile or null
     */
    public function storeRemoteFile($url, $type, $path, $filename = null)
    {
        if (!is_null($filename)) {
            $filename = $this->removeDotSegments($filename);
        }

        try {
            $media_file = new $this->upload_types[$type];
            $media_file->user_id = Auth::check() ? Auth::id() : null;
            $media_file->useRemoteFile($url, $path);
            $media_file->setDerivedProperties();
            $media_file->save();
        } catch (Exception $e) {
            throw $e;
            $message = 'Failed creating new MediaFile from upload type "' . $type . '"';
            Log::error($message . ': ' . $e->getMessage());

            return null;
        }

        return $media_file;
    }

    /**
     * Remove dot segments from URIs as per RFC 3986
     *
     * @see http://tools.ietf.org/html/rfc3986#section-5.2.4
     * @param string $input
     * @return string
     */
    public function removeDotSegments($input)
    {
        /**
         * 1.  The input buffer is initialized with the now-appended path
         *     components and the output buffer is initialized to the empty
         *     string.
         */
        $output = '';

        /**
         * 2.  While the input buffer is not empty, loop as follows:
         */
        while ($input !== '') {
            /**
             * A.  If the input buffer begins with a prefix of "`../`" or "`./`",
             *     then remove that prefix from the input buffer; otherwise,
             */
            if (($prefix = substr($input, 0, 3)) == '../' || ($prefix = substr($input, 0, 2)) == './'
            ) {
                $input = substr($input, strlen($prefix));
            } elseif (($prefix = substr($input, 0, 3)) == '/./' || ($prefix = $input) == '/.'
            ) {
                /**
                 * B.  if the input buffer begins with a prefix of "`/./`" or "`/.`",
                 *     where "`.`" is a complete path segment, then replace that
                 *     prefix with "`/`" in the input buffer; otherwise,
                 */
                $input = '/' . substr($input, strlen($prefix));
            } elseif (($prefix = substr($input, 0, 4)) == '/../' || ($prefix = $input) == '/..'
            ) {
                /**
                 * C.  if the input buffer begins with a prefix of "/../" or "/..",
                 *     where "`..`" is a complete path segment, then replace that
                 *     prefix with "`/`" in the input buffer and remove the last
                 *     segment and its preceding "/" (if any) from the output
                 *     buffer; otherwise,
                 */
                $input = '/' . substr($input, strlen($prefix));
                $output = substr($output, 0, strrpos($output, '/'));
            } elseif ($input == '.' || $input == '..') {
                /**
                 * D.  if the input buffer consists only of "." or "..", then remove
                 *     that from the input buffer; otherwise,
                 */
                $input = '';
            } else {
                /**
                 * E.  move the first path segment in the input buffer to the end of
                 *     the output buffer, including the initial "/" character (if
                 *     any) and any subsequent characters up to, but not including,
                 *     the next "/" character or the end of the input buffer.
                 */
                $pos = strpos($input, '/');
                if ($pos === 0) {
                    $pos = strpos($input, '/', $pos + 1);
                }
                if ($pos === false) {
                    $pos = strlen($input);
                }
                $output .= substr($input, 0, $pos);
                $input = (string) substr($input, $pos);
            }
        }

        // 3.  Finally, the output buffer is returned as the result of remove_dot_segments.
        return $output;
    }
}
