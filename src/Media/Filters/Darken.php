<?php

namespace Yadda\Enso\Media\Filters;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class Darken implements FilterInterface
{
    /**
     * Opacity of the tint.
     *
     * 0 for transparent, 1 for opaque
     *
     * @var float
     */
    protected $amount;

    /**
     * Create a new Darken
     *
     * @param float $amount
     */
    public function __construct($amount)
    {
        if (is_null($amount)) {
            $this->amount = 0.3;
        } else {
            $this->amount = $amount;
        }
    }

    /**
     * Applies filter to given image
     *
     * @param  \Intervention\Image\Image $image
     * @return \Intervention\Image\Image
     */
    public function applyFilter(Image $image)
    {
        $image->rectangle(0, 0, $image->width(), $image->height(), function ($draw) {
            $draw->background('rgba(0, 0, 0, ' . $this->amount . ')');
        });

        return $image;
    }
}
