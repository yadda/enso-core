<?php

namespace Yadda\Enso\Media\Filters;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class Resize implements FilterInterface
{
    /**
     * Desired width of the output image
     *
     * @var int
     */
    protected $width;

    /**
     * Desired height of the output image
     *
     * @var int
     */
    protected $height;

    /**
     * Type of cropping to use
     *
     * Either 'cover' (default), 'contain' or 'no-upscale',
     *
     * @var int
     */
    protected $crop_style;

    /**
     * Color to use when filling in excess space
     *
     * @var int
     */
    protected $background_color;

    /**
     * Horizontal focal point of the image.
     *
     * 0 is left, 1 is right. If not set, 0.5 will be used.
     *
     * @var float
     */
    protected $focus_x;

    /**
     * Vertical focal point of the image.
     *
     * 0 is top, 1 is bottom. If not set, 0.5 will be used.
     *
     * @var float
     */
    protected $focus_y;


    /**
     * Create a new Resize
     */
    public function __construct(
        $width,
        $height,
        $crop_style,
        $background_color = null,
        $focus_x = null,
        $focus_y = null
    ) {
        $this->width = $width;
        $this->height = $height;
        $this->crop_style = $crop_style;
        $this->background_color = $background_color ?? [255, 255, 255, 0];
        $this->focus_x = $focus_x ?? 0.5;
        $this->focus_y = $focus_y ?? 0.5;
    }

    /**
     * Applies filter to given image
     *
     * @param  \Intervention\Image\Image $image
     *
     * @return \Intervention\Image\Image
     */
    public function applyFilter(Image $image)
    {
        // Crop types that allow upscaling will fallback to 'contain'
        // if either dimension is null as the logic is the same
        if ((empty($this->width) || empty($this->height))
        && $this->crop_style !== 'no-upscale-forced-aspect'
        && $this->crop_style !== 'no-upscale'
        ) {
            $this->crop_style = 'contain';
        }

        switch ($this->crop_style) {
            // Resize the image to fit a specific aspect ratio. If the image
            // doesn't fit the given space, bars of $this->background_color
            // will be used to fill the gap
            case 'no-upscale-forced-aspect':
                $image->resize($this->width, $this->height, function ($constrain) {
                    $constrain->aspectRatio();
                    $constrain->upsize();
                });
                $image->resizeCanvas($this->width, $this->height, 'center', false, $this->background_color);
                break;
            // The same as 'cover' but won't make the image any bigger (in
            // either dimension) than the source material. This is useful in
            // places where a small image is preferable to a large pixelated
            // image. The flip-side is that the resulting image will be of
            // an indefinite aspect ratio. The image will center on the focus
            // point if one is provided.
            case 'no-upscale':
                $this->width = is_null($this->width) ? $image->width() : min($this->width, $image->width());
                $this->height = is_null($this->height) ? $image->height() : min($this->height, $image->height());
                $this->resize($image);
                break;
            // Create an image of the specified size, make the input image fit
            // inside that space and fill any gaps with $this->background_color
            case 'contain':
                $image->resize($this->width, $this->height, function ($constrain) {
                    $constrain->aspectRatio();
                });
                $image->resizeCanvas($this->width, $this->height, 'center', false, $this->background_color);
                break;
            // Create an image of the specified size and zoom the soure image to
            // fit it, centered on the a focus point if there is one.
            case 'cover':
            case null:
            default:
                $this->resize($image);
                break;
        }

        return $image;
    }

    /**
     * Calculates which of the axis to resize by to fill the requested image
     * with as little overflow as possible
     *
     * @param \Intervention\Image\Image $image
     *
     * @return void
     */
    protected function resize($image)
    {
        $too_narrow = false;
        $too_short = false;
        $resize_width = null;
        $resize_height = null;

        if (!is_null($this->width) && $image->width() < $this->width) {
            $too_narrow = true;
        }

        if (!is_null($this->height) && $image->height() < $this->height) {
            $too_short = true;
        }

        /**
         * This block calculates which of the axis to resize by to best fit the
         * requested image size
         */

        // If Image exeeds given resize bounds
        if (!$too_narrow && !$too_short) {
            // If both width and height bounds are specifed workout which
            // axis of a resize will meet the bounds first, and resize by
            // that axis
            if (!empty($this->width) && !empty($this->height)) {
                if (($this->width / $image->width()) < ($this->height / $image->height())) {
                    $resize_height = $this->height;
                } else {
                    $resize_width = $this->width;
                }
            // Otherwise, resize by whichever bound is set, constraining by
            // aspect ratio
            } elseif (!empty($this->width)) {
                $resize_width = $this->width;
            } else {
                $resize_height = $this->height;
            }
        // Image is too small in both directions. Scale to cover.
        } elseif ($too_narrow && $too_short) {
            if (($this->width / $image->width()) < ($this->height / $image->height())) {
                // Desired size is more landscape
                $resize_height = $this->height;
                $resize_width = null;
            } else {
                // Desired size is more portrait
                $resize_height = null;
                $resize_width = $this->width;
            }
        // If image is too narrow for the given resize bounds but is tall enough
        // to fit within it, then resize up by width.
        } elseif ($too_narrow) {
            $resize_width = $this->width;
        // If image is too short for the given resize bounds but is wide enough
        // to fit within it, then resize up by height.
        } elseif ($too_short) {
            $resize_height = $this->height;
        }

        // If $resize_width is non-null, we need to resize the image based on
        // the width and the aspect ratio.
        if (!is_null($resize_width)) {
            $image->resize($resize_width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        // If $resize_height is non-null, we need to resize the image based on
        // the height and the aspect ratio.
        if (!is_null($resize_height)) {
            $image->resize(null, $resize_height, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $crop_x = round(($this->focus_x * $image->width()) - ($this->width / 2));
        $crop_y = round(($this->focus_y * $image->height()) - ($this->height / 2));

        $crop_x = min(max(0, $crop_x), $image->width() - $this->width);
        $crop_y = min(max(0, $crop_y), $image->height() - $this->height);

        $image->crop(
            $this->width,
            $this->height,
            $crop_x,
            $crop_y
        );
    }
}
