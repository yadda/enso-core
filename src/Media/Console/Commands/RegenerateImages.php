<?php

namespace Yadda\Enso\Media\Console\Commands;

use Illuminate\Console\Command;
use Yadda\Enso\Media\Contracts\ImageFile as ImageFileContract;

class RegenerateImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'enso:images {preset} {--id=} {--path=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regenerate resized images from the original';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->option('id');
        $path = $this->option('path');
        $preset = $this->argument('preset');

        if (!$id && !$path) {
            $this->line('You must provide either:');
            $this->line('');
            $this->line('--id - Provide an image ID to resize a single image.');
            $this->line('  or');
            $this->line('--path - Resize multiple images by their path.');

            return;
        }

        $file_class = get_class(resolve(ImageFileContract::class));
        $query = $file_class::query();

        if ($id) {
            $query->where('id', $id);
        }

        if ($path) {
            $query->where('path', $path);
        }

        $count = $query->count();

        if (!$this->confirm('Found ' . $count . ' images. Continue?')) {
            $this->line('Aborting.');
            return;
        }

        $bar = $this->output->createProgressBar($count);

        $image = $query->chunk(100, function ($images) use ($bar, $preset) {
            foreach ($images as $image) {
                $image->makeResize($preset, true);
                $bar->advance();
            }
        });

        $bar->finish();

        $this->line('');
        $this->comment('Done.');
    }
}
