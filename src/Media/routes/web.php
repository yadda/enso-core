<?php

use Yadda\Enso\Facades\EnsoCrud;
use Yadda\Enso\Media\Contracts\UploadsController;
use Yadda\Enso\Utilities\Helpers;

Route::group([
    'middleware' => ['web', 'enso'],
], function () {
    $files_controller_class = EnsoCrud::controllerClass('file', true);
    $uploads_controller_class = Helpers::getConcreteClass(UploadsController::class);

    Route::group([
        'prefix' => 'admin/media',
    ], function () use ($files_controller_class, $uploads_controller_class) {
        Route::get('browser')
            ->uses($files_controller_class . '@indexBrowser')
            ->name('admin.media.browser');

        Route::get('files')
            ->uses($files_controller_class . '@files')
            ->name('admin.browser.files.index');

        Route::post('upload')
            ->uses($uploads_controller_class . '@uploadFile')
            ->name('admin.media.upload');

        Route::patch('{file}/update-filename')
            ->uses($files_controller_class . '@updateFilename')
            ->name('admin.media.update-filename');

        Route::get('upload-resumable')
            ->uses($uploads_controller_class . '@uploadFileResumableGet')
            ->name('admin.media.upload-resumable-get');

        Route::post('upload-resumable')
            ->uses($uploads_controller_class . '@uploadFileResumablePost')
            ->name('admin.media.upload-resumable');

        Route::delete('delete/{file}')
            ->uses($files_controller_class . '@deleteFile')
            ->name('admin.media.delete-file');

        Route::delete('delete-many')
            ->uses($files_controller_class . '@deleteFiles')
            ->name('admin.media.delete-many');
    });

    EnsoCrud::crudRoutes('admin/media', 'file', 'admin.media');
});
