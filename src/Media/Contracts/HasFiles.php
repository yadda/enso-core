<?php

namespace Yadda\Enso\Media\Contracts;

/**
 * For Models that have related files
 */
interface HasFiles
{
    /**
     * Files that have been attached to this model.
     *
     * This should not be altered manually but should be automatically updated
     * by Enso CRUD.
     *
     * @return Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function files();

    /**
     * Boot the HasFiles trait
     *
     * @return void
     */
    public static function bootHasFiles();

    /**
     * Detach all files from this model. This only remvoves files from the
     * "files" relationship not any other custom relationships that may exist
     * on the model so that they can be repopulated when updating from CRUD.
     *
     * You probaby don't want to call this method manually from outside Enso Core
     *
     * @return void
     */
    public function detachAllFiles();
}
