<?php

namespace Yadda\Enso\Media\Contracts;

use Yadda\Enso\Media\Contracts\MediaFile;

/**
 * Contract for binding to the correct AudioFile implementation
 */
interface AudioFile extends MediaFile
{
    //
}
