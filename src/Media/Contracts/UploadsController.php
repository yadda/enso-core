<?php

namespace Yadda\Enso\Media\Contracts;

use Illuminate\Http\Request;

interface UploadsController
{
    public function uploadFile(Request $request);
}
