<?php

namespace Yadda\Enso\Dev\Controllers;

use Illuminate\Routing\Controller;

class DevController extends Controller
{
    public function bulma()
    {
        return view('enso-dev::bulma-styles');
    }

    public function bootstrap4()
    {
        return view('enso-dev::bootstrap-4-styles');
    }
}
