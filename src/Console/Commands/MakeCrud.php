<?php

namespace Yadda\Enso\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

/**
 * Artisan command for generating an Ensō CRUD config file
 */
class MakeCrud extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:crud {name} {--f|force : Overwrite existing files} {--t|taxonomy : Create a taxonomy crud}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a CRUD';

    /**
     * The name for CRUD config
     * Should be singular, Capitalized
     *
     * @var string
     */
    protected $name;

    /**
     * The Filesystem
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * @var Composer
     */
    protected $composer;

    /**
     * Create a taxonomy Crud
     *
     * @var bool
     */
    protected $taxonomy;

    /**
     * Create a new command instance
     *
     * @param Filesystem $files
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
        $this->composer = app()['composer'];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->name = $this->argument('name');
        $this->taxonomy = $this->option('taxonomy');

        if (!$this->name) {
            return $this->error('You must provide a name. The name should be singular.');
        }

        $this->name = Str::studly($this->name);
        $this->name_plural = Str::plural($this->name);
        $name_lowercase = strtolower($this->name);
        $name_plural_lowercase = strtolower($this->name_plural);
        $slug = Str::slug($this->name_plural);

        $this->makeCrudConfig();
        $this->makeController();
        $this->makeTaxonomyJsonController();
        $this->makeModelAndMigration();
        $this->makeConfigEntry();

        $this->info('Dumping autoload...');
        $this->composer->dumpAutoloads();

        $this->line('');
        $this->info('Almost done!');

        //     $this->line('');
        //     $this->info('Add the following to config/enso/crud.php');
        //     $this->line('');
        //     $this->line("    '" . $name_lowercase . "' => [
        //     'controller'    => '\App\Http\Controllers\Admin\\" . $this->name . "Controller',
        //     'config'        => '\App\Crud\\" . $this->name . "',
        //     'model'         => '\App\\" . $this->name . "',
        // ],");

        $this->line('');
        $this->info('Add routes to routes/web.php');
        $this->line('');
        $this->line(
            "    EnsoCrud::crudRoutes('admin/" . $name_plural_lowercase . "', " .
                "'" . $name_lowercase . "', 'admin." . $name_plural_lowercase . "');"
        );

        if ($this->taxonomy) {
            $this->line("    Route::get('json/" . $slug . "', 'Json\\" . $this->name . "Controller@index')->name('json." . $slug . ".index');");
            $this->line("    Route::get('json/" . $slug . "/{" . $name_lowercase . "}', 'Json\\" . $this->name . "Controller@show')->name('json." . $slug . ".show');");
        }

        $this->line('');
        $this->info('Add a menu item in app/Providers/EnsoServiceProvider.php');

        $this->line('');
        $this->line("    EnsoMenu::addItem([
        Config::get('enso.crud." . $name_lowercase . ".menuitem')
    ]);");

        if ($this->taxonomy) {
            $this->line('');
            $this->info('Add nesting columns and name to your migration');
            $this->line('');
            $this->line('    $table->nestedSet();');
            $this->line('    $table->string(\'name\');');
            $this->line('    $table->string(\'slug\')->index();');
        }

        $this->line('');
        $this->info('Apply the following traits to your model');
        $this->line('    Yadda\Enso\Crud\Traits\IsCrudModel');
        if ($this->taxonomy) {
            $this->line('    Kalnoy\Nestedset\NodeTrait');
        }
        $this->line('');

        $this->info('Make your model implement the following interface');
        $this->line('    Yadda\Enso\Crud\Contracts\IsCrudModel');
        $this->line('');

        $this->info('Set some attributes as fillable on your model');
        $this->line('    protected $fillable = [');
        $this->line("        'name',");
        $this->line("        'slug',");
        $this->line('    ]');
        $this->line('');
    }

    protected function makeController(): void
    {
        $controller_path = $this->getAdminControllerPath();

        $this->info('Creating controller - ' . $controller_path);

        if ($this->files->exists($controller_path) && !$this->option('force')) {
            $this->error('Config file (' . $controller_path . ') already exists. Skipping.');
            return;
        }

        $this->files->put($controller_path, $this->compileController());

        $this->info('Controller created.');
    }

    protected function makeTaxonomyJsonController(): void
    {
        if (!$this->taxonomy) {
            return;
        }

        $dir = base_path('/app/Http/Controllers/Json');

        if (!file_exists($dir)) {
            mkdir($dir);
        }

        $path = base_path() . '/app/Http/Controllers/Json/' . $this->name . 'Controller.php';

        $this->info('Creating JSON taxonomy controller');

        if ($this->files->exists($path) && !$this->option('force')) {
            $this->error('Config file (' . $path . ') already exists. Skipping.');
            return;
        }

        $this->files->put($path, $this->compileJsonController());

        $this->info('Json taxonomy controller created.');
    }

    /**
     * Create a config file
     *
     * @return void
     */
    protected function makeCrudConfig(): void
    {
        $path = $this->getCrudConfigPath();

        $this->info('Creating CRUD configuration file - ' . $path);

        if ($this->files->exists($path) && !$this->option('force')) {
            $this->error('Config file (' . $path . ') already exists. Skipping.');
            return;
        }

        $this->files->put($path, $this->compileCrudConfig());

        $this->info('Config created.');
    }

    protected function makeModelAndMigration(): void
    {
        $model_path = $this->getModelPath();

        $this->info('Making model and migration - ' . $model_path);

        $model_name = $this->name;

        if ($this->files->exists($model_path) && !$this->option('force')) {
            $this->error('Model file (' . $model_path . ') already exists. Skipping.');
            return;
        }

        $this->call('make:model', [
            '-m' => true,
            'name' => $model_name,
            '--force' => $this->option('force'),
        ]);

        $this->info('Model and migration created.');
    }

    protected function getModelPath(): string
    {
        return base_path('/app/' . $this->name . '.php');
    }

    /**
     * Get the intended path to the config file
     *
     * @return string
     */
    protected function getCrudConfigPath(): string
    {
        return base_path('/app/Crud/' . $this->name . '.php');
    }

    /**
     * Get the intended path to the config file
     *
     * @return string
     */
    protected function getAdminControllerPath(): string
    {
        return base_path('/app/Http/Controllers/Admin/' . $this->name . 'Controller.php');
    }

    /**
     * Generate the code for the config file
     *
     * @return string
     */
    protected function compileCrudConfig(): string
    {
        $stub = $this->files->get(__DIR__ . '/../../../install-stubs/app/Crud/Config.stub');

        $stub = str_replace('{{views_dir}}', Str::kebab(strtolower($this->name)), $stub);
        $stub = str_replace('{{name_singular}}', $this->name, $stub);
        $stub = str_replace('{{name_plural}}', $this->name_plural, $stub);
        $stub = str_replace('{{route}}', Str::kebab(strtolower($this->name_plural)), $stub);

        if ($this->taxonomy) {
            $stub = str_replace('{{nested}}', "\n            ->nested()\n            ->order('_lft', 'ASC')", $stub);
        } else {
            $stub = str_replace('{{nested}}', '', $stub);
        }

        return $stub;
    }

    protected function compileController(): string
    {
        $stub = $this->files->get(__DIR__ . '/../../../install-stubs/app/Crud/Controller.stub');

        $stub = str_replace('{{name_singular}}', $this->name, $stub);

        return $stub;
    }

    protected function compileJsonController(): string
    {
        $stub = $this->files->get(__DIR__ . '/../../../install-stubs/app/Crud/TaxonomyController.stub');

        $stub = str_replace('{{name}}', $this->name, $stub);
        $stub = str_replace('{{slug}}', Str::slug($this->name), $stub);

        return $stub;
    }

    protected function makeMigration(): void
    {
        $this->info('Making migration...');

        $table_name = strtolower($this->name_plural);
        $migration_name = 'create_' . $table_name . '_table';

        $this->call('make:migration --create=' . $table_name . ' ' . $migration_name);

        $this->info('Migration created.');
    }

    /**
     * Get the path in which to store the config entry to register the CRUD
     */
    protected function getConfigPath(): string
    {
        return config_path('enso/crud/' . Str::snake($this->name) . '.php');
    }

    /**
     * Create an the contents for the Laravel config file
     * that registers the CRUD
     */
    protected function compileConfigEntry(): string
    {
        $stub = $this->files->get(__DIR__ . '/../../../install-stubs/app/Crud/ConfigEntry.stub');

        $stub = str_replace('{{name_singular}}', $this->name, $stub);
        $stub = str_replace('{{name_plural}}', $this->name_plural, $stub);
        $stub = str_replace('{{name_plural_lower}}', strtolower($this->name_plural), $stub);

        return $stub;
    }

    /**
     * Create the config file that registers the CRUD
     *
     * @return void
     */
    protected function makeConfigEntry(): void
    {
        $path = $this->getConfigPath();

        $this->info('Creating config entry - ' . $path);

        if ($this->files->exists($path) && !$this->option('force')) {
            $this->error('Config file (' . $path . ') already exists. Skipping.');
            return;
        }

        $this->files->put($path, $this->compileConfigEntry());

        $this->info('Config created.');
    }
}
