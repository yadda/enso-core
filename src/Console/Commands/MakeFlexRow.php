<?php

namespace Yadda\Enso\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

class MakeFlexRow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:flexrow {name} {--f|force : Overwrite existing files}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a Flexible Content Row Spec';

    /**
     * The name of the row
     *
     * Should be singular and Capitalized
     *
     * @var string
     */
    protected $name;

    /**
     * The Filesystem
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * Create a new command instance
     *
     * @param Filesystem $files
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
        $this->composer = app()['composer'];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->name = Str::studly($this->argument('name'));

        $this->createRowSpecFile();
        $this->createRowSpecTemplate();

        $this->composer->dumpAutoloads();
    }

    /**
     * Generate the contents of the row spec configuration file
     */
    protected function compileRowSpecFile(): string
    {
        $stub = $this->files->get(__DIR__ . '/../../../install-stubs/app/Crud/Row.stub');

        $stub = str_replace('{{name_singular}}', $this->name, $stub);
        $stub = str_replace('{{slug}}', Str::slug($this->name), $stub);

        return $stub;
    }

    protected function compilreRowSpecTemplateFile(): string
    {
        $stub = $this->files->get(__DIR__ . '/../../../install-stubs/app/Crud/FlexTemplate.stub');

        $stub = str_replace('{{name_singular}}', $this->name, $stub);

        return $stub;
    }

    /**
     * Create a row spec configuration file
     */
    protected function createRowSpecFile(): void
    {
        $dir_path = $this->getRowSpecPath();
        $file_path = $dir_path . $this->name . '.php';

        if ($this->files->exists($file_path) && !$this->option('force')) {
            $this->error('File already exists. Aborting.');
            exit;
        }

        $this->files->ensureDirectoryExists($dir_path);

        $this->files->put($file_path, $this->compileRowSpecFile());

        $this->info('Row Spec File created - ' . $file_path);
    }

    /**
     * Create a Row Spec template
     */
    protected function createRowSpecTemplate(): void
    {
        $dir_path = $this->getRowSpecTemplatePath();
        $file_path = $dir_path . Str::slug($this->name) . '.blade.php';

        if ($this->files->exists($file_path) && !$this->option('force')) {
            $this->error('File already exists. Aborting.');
            exit;
        }

        $this->files->ensureDirectoryExists($dir_path);

        $this->files->put($file_path, $this->compilreRowSpecTemplateFile());

        $this->info('Row Spec Template created - ' . $file_path);
    }

    /**
     * The path in which to put the generated Rowspec
     */
    protected function getRowSpecPath(): string
    {
        return app_path('Crud/Rows/');
    }

    protected function getRowSpecTemplatePath(): string
    {
        return resource_path('views/vendor/enso-crud/flex-partials/rows/');
    }
}
