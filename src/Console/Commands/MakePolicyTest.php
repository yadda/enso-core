<?php

namespace Yadda\Enso\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use Yadda\Enso\Facades\EnsoCrud;

class MakePolicyTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:policy-test
        {crud_name : Crud name of model to make policy test for}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates boilerplate templates for a Policy test';

    /**
     * crud_name argument
     *
     * @var string
     */
    protected $crud_name;

    /**
     * Class of the crud model
     *
     * @var string
     */
    protected $crud_model_class;

    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * Create a new controller creator command instance.
     *
     * @param  \Illuminate\Filesystem\Filesystem  $files
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->crud_name = $this->argument('crud_name');

        $this->crud_model_class = Config::get('enso.crud.' . $this->crud_name . '.model', null);

        if (empty($this->crud_model_class)) {
            $this->error('Crud name: ' . $this->crud_name . ' is invalid');
            return;
        }

        $this->createTests();
    }

    /**
     * Creates tests for the a set of actions
     *
     * @return void
     */
    protected function createTests(): void
    {
        $singular = class_basename(new $this->crud_model_class);
        $plural = Str::plural($singular);

        $replacements = [
            '{{singular}}' => $singular,
            '{{lc_singular}}' => Str::snake($singular),
            '{{lc_plural}}' => Str::snake($plural),
            '{{model_class}}' => $this->crud_model_class,
            '{{plural}}' => Str::plural($singular),
            '{{user_class}}' => EnsoCrud::modelClass('user'),
        ];

        $path = $this->getTestPath($plural, $singular);

        $exists = file_exists($path);

        if ($exists) {
            if (!$this->confirm('PolicyTest for `' . $singular . '` already exists. Overwrite?')
            ) {
                $this->info($path . ' not written.');
                return;
            }
        }

        $this->makeDirectory($path);

        $stub = $this->makeReplacements(
            $this->files->get($this->getStubPath()),
            $replacements
        );

        $this->files->put($path, $stub);

        $this->line('File ' . $path . ($exists ? ' overwritten' : ' created'));
    }

    /**
     * File path for the stub file
     *
     * @return string
     */
    protected function getStubPath(): string
    {
        return base_path() . '/vendor/yadda/enso-core/install-stubs/tests/Policy/PolicyTest.stub';
    }

    /**
     * File path for creating a test
     *
     * @param string $filename
     *
     * @return string
     */
    protected function getTestPath(string $namespace, string $model): string
    {
        return base_path() . '/tests/Unit/' . $namespace . '/' . $model . 'PolicyTest.php';
    }

    /**
     * Build the directory for the class, if necessary
     *
     * @param  string  $path
     *
     * @return string
     */
    protected function makeDirectory($path)
    {
        if (! $this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0777, true, true);
        }

        return $path;
    }

    /**
     * Makes a set of replacements on the given file content
     *
     * @param string $file_content
     * @param array $replacements
     *
     * @return string
     */
    protected function makeReplacements(string $file_content, array $replacements): string
    {
        return str_replace(array_keys($replacements), $replacements, $file_content);
    }
}
