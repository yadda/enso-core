<?php

namespace Yadda\Enso\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use Yadda\Enso\Users\Contracts\User;

class MakeCrudTests extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:crud-test
                           {crud_name : Internal name of the crud to make tests for}
                           {--a|all : Create all files}
                           {--c|crud : Create crud tests}
                           {--t|traits : Create additional test traits}
                           {--N|never-overwrite : Never overwrite existing files}
                           {--F|force : Always overwrite existing files}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates boilerplate templates for CRUD tests';

    /**
     * crud_name argument
     *
     * @var string
     */
    protected $crud_name;

    /**
     * Config for the given crud name
     *
     * @var \Yadda\Enso\Crud\Config
     */
    protected $crud_config;

    /**
     * Model for the given crud name
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $crud_model;

    /**
     * Name of the field to use for data storage actions
     *
     * @var string
     */
    protected $field_name;

    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * Crud actions
     */
    const CRUD_TESTS = [
        'create',
        'destroy',
        'edit',
        'index',
        'index_json',
        'store',
        'update',
    ];

    /**
     * Create a new controller creator command instance.
     *
     * @param  \Illuminate\Filesystem\Filesystem  $files
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->parseCrudName();

        $this->force = $this->option('force');

        if (empty($this->crud_model) || empty($this->crud_config)) {
            $this->error('Crud name: ' . $this->crud_name . ' is invalid');
            return;
        }

        if ($this->option('traits') || $this->option('all')) {
            $this->createCreatesUsersTrait();
        }

        if ($this->option('crud') || $this->option('all')) {
            $this->createCrudTrait();
            $this->createTests(static::CRUD_TESTS);
        }
    }

    /**
     * Creates tests for the a set of actions
     *
     * @return void
     */
    protected function createTests(array $actions): void
    {
        $replacements = [
            '{{base_name}}' => class_basename($this->crud_model),
            '{{crud_name}}' => $this->crud_name,
        ];

        foreach ($actions as $action) {
            $path = $this->getTestPath($action);
            $exists = file_exists($path);

            if ($exists && !$this->force) {
                if ($this->option('never-overwrite')
                    || !$this->confirm('Test for `' . $action . '` already exists. Overwrite?')
                ) {
                    continue;
                }
            }

            $this->makeDirectory($path);

            $stub = $this->makeReplacements(
                $this->files->get($this->getStubPath($action)),
                $replacements
            );

            if (in_array($action, ['index_json', 'store', 'update'])) {
                $stub = str_replace(
                    '{{field_name}}',
                    $this->getFieldName(),
                    $stub
                );
            }

            $this->files->put($path, $stub);

            $this->line('File ' . $path . ($exists ? ' overwritten' : ' created'));
        }
    }

    /**
     * Creates required traits for the CRUD tests
     */
    protected function createCreatesUsersTrait(): void
    {
        $user_model = App::make(Config::get('enso.crud.user.model'));

        $this->createTrait(
            'CreatesUsers',
            [
                '{{user_class}}' => get_class($user_model),
                '{{user_basename}}' => class_basename($user_model),
            ]
        );
    }

    /**
     * Creates required traits for the CRUD tests
     *
     * @return void
     */
    protected function createCrudTrait(): void
    {
        $this->createTrait(
            class_basename($this->crud_model) . 'CrudTrait',
            [
                '{{basename}}' => class_basename($this->crud_model),
                '{{crud}}' => get_class($this->crud_config),
                '{{model}}' => get_class($this->crud_model),
                '{{route}}' => $this->crud_config->getRoute(),
            ],
            'Traits/Admin',
            'CrudTrait',
        );
    }

    /**
     * Creates a trait from a stub file.
     *
     * @param string $name
     * @param array  $replacements
     * @param string $destination  Optional. Defaults to 'Traits'
     * @param string $stub         Optional. Defaults to $name
     *
     * @return void
     */
    protected function createTrait(
        string $name,
        array $replacements,
        string $destination = 'Traits',
        string $stub = null
    ): void {
        $path = $this->getTraitPath($name, $destination);
        $exists = file_exists($path);

        if ($exists && !$this->force) {
            if ($this->option('never-overwrite')
                || !$this->confirm('Testing trait `' . $name
                    . '` already exists. Overwrite?')
            ) {
                return;
            }
        }

        $this->makeDirectory($path);

        $this->files->put(
            $path,
            $this->makeReplacements(
                $this->files->get($this->getStubPath($stub ?? $name)),
                $replacements
            )
        );

        $this->line('File ' . $path . ($exists ? ' overwritten' : ' created'));
    }

    /**
     * Name of the field to use for testing updates
     *
     * @return string
     */
    protected function getFieldName(): string
    {
        if (is_null($this->field_name)) {
            $this->field_name = $this->ask('Specify name of text field to update');
        }

        return $this->field_name;
    }

    /**
     * Filename of the stub file for a test file with the given filename part
     *
     * @param string $filename
     *
     * @return string
     */
    protected function getStubPath(string $filename): string
    {
        return base_path() . '/vendor/yadda/enso-core/install-stubs/tests/crud/'
            . $filename . '.stub';
    }

    /**
     * Filename of the destination for a test with theh given filename part
     *
     * @param string $filename
     *
     * @return string
     */
    protected function getTestPath(string $filename): string
    {
        return base_path() . '/tests/Feature/Admin/'
            . class_basename($this->crud_model) . '/'
            . Str::studly($filename) . 'Test.php';
    }

    /**
     * Filename of the destination for a trait with the given filename part
     *
     * @param string $filename
     * @param string $destination
     *
     * @return string
     */
    protected function getTraitPath(string $filename, string $destination = 'Traits'): string
    {
        return base_path(
            '/tests/' . implode('/', array_filter([$destination, $filename])) . '.php'
        );
    }

    /**
     * Build the directory for the class, if necessary
     *
     * @param  string  $path
     *
     * @return string
     */
    protected function makeDirectory($path)
    {
        if (! $this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0777, true, true);
        }

        return $path;
    }

    /**
     * Makes a set of replacements on the given file content
     *
     * @param string $file_content
     * @param array $replacements
     *
     * @return string
     */
    protected function makeReplacements(string $file_content, array $replacements): string
    {
        return str_replace(array_keys($replacements), $replacements, $file_content);
    }

    /**
     * Uses the crud_name argument to populate the crud_config and crud_model
     * properties
     *
     * @return void
     */
    protected function parseCrudName(): void
    {
        $this->crud_name = $this->argument('crud_name');

        $crud_config = Config::get('enso.crud.' . $this->crud_name, null);

        $crud_model_class = Arr::get($crud_config, 'model', null);
        if (!empty($crud_model_class)) {
            $this->crud_model = new $crud_model_class;
        }

        $crud_config_class = Arr::get($crud_config, 'config', null);
        if (!empty($crud_model_class)) {
            $this->crud_config = new $crud_config_class;
        }
    }
}
