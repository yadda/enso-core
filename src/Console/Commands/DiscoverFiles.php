<?php

namespace Yadda\Enso\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yadda\Enso\Facades\EnsoMedia;
use Yadda\Enso\Media\Filters\Preset;

class DiscoverFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'enso:discover-files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Find files and create database records for them';

    /**
     * Name of Laravel Storage disk on which to find files
     *
     * @var string
     */
    protected $disk;

    /**
     * Number of files that already existed in the database
     */
    protected int $existing_count = 0;

    /**
     * Number of files discovered
     */
    protected int $new_count = 0;

    /**
     * Progress bar
     */
    protected $bar;

    /**
     * List of files that had 0 filesize
     *
     * @return array
     */
    protected $empty_files = [];

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->comment('Discovering files...');
        $this->line('');

        $this->bar = $this->output->createProgressBar();

        $this->disk = config('enso.media.disk');

        $preset_names = array_keys(array_merge(
            Preset::$builtin_presets,
            config('enso.media.presets')
        ));

        $directories = Storage::disk($this->disk)
            ->allDirectories('/');

        $directories = array_filter($directories, function ($dir) use ($preset_names) {
            $parts = explode('/', $dir);
            $last_elem = end($parts);
            return !in_array($last_elem, $preset_names);
        });

        foreach ($directories as $directory) {
            $this->handleDirectory($directory);
        }

        $this->bar->finish();

        $this->line('');
        $this->line('');
        $this->info('Finished!');
        $this->line('');

        if (!empty($this->empty_files)) {
            $this->comment('Skipped these ' . count($this->empty_files) . ' files as they have a filesize of 0:');
            foreach ($this->empty_files as $filename) {
                $this->comment('  - ' . $filename);
            }
        }

        $this->comment('Found ' . $this->new_count . ' files');

        if ($this->existing_count > 0) {
            $this->comment('Also found and ignored ' . $this->existing_count . ' files that were already in the database.');
        }
    }

    protected function handleDirectory($dir)
    {
        foreach (Storage::disk($this->disk)->files($dir) as $file) {
            $this->handleFile($file);
        }
    }

    protected function handleFile($file_path)
    {
        $this->bar->advance();

        $abs_path = Storage::disk($this->disk)->path($file_path);

        if (Storage::disk($this->disk)->size($file_path) === 0) {
            $this->empty_files[] = $file_path;
            return;
        }

        $mimetype = Storage::disk($this->disk)->mimeType($file_path);
        $type = EnsoMedia::typeFromMime($mimetype);
        $media_class = EnsoMedia::getModelFromType($type);
        $filename = pathinfo($file_path, PATHINFO_BASENAME);
        $filesize = Storage::disk($this->disk)->size($file_path);
        $media_dir = config('enso.media.directory');
        $abs_path = EnsoMedia::removeDotSegments($abs_path);
        $media_file_type = new $media_class;
        $path = substr(
            pathinfo($file_path, PATHINFO_DIRNAME),
            strlen($media_file_type->getFolder()) + strlen($media_dir) + 1
        );

        if (Str::startsWith($path, '/')) {
            $path = substr($path, 1);
        }

        $original_filename = pathinfo($filename, PATHINFO_FILENAME);
        $original_filename = implode('.', [
            substr($original_filename, 0, -14),
            pathinfo($filename, PATHINFO_EXTENSION),
        ]);

        if ($media_class::query()
            ->where('path', $path)
            ->where('filename', $filename)
            ->exists()) {
            $this->existing_count++;
            return;
        }

        $media_file = $media_class::make([
            'path' => $path,
            'filename' => $filename,
            'original_filename' => $original_filename,
            'filesize' => $filesize,
        ]);

        $media_file->setDerivedProperties();
        $media_file->save();

        $this->new_count++;
    }
}
