<?php

namespace Yadda\Enso\Categories\Crud;

use Yadda\Enso\Categories\Models\Category as CategoryModel;
use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Forms\Fields\SlugField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Crud\Tables\Text;

class Category extends Config
{
    /**
     * Configure the CRUD
     *
     * @return void
     */
    public function configure()
    {
        $this->model($this->getModel())
            ->route('admin.categories')
            ->views('categories')
            ->name('Category')
            ->searchColumns(['name'])
            ->nested()
            ->order('_lft', 'ASC')
            ->columns([
                Text::make('name'),
            ])
            ->rules([
                'main.name' => 'required|string',
            ]);
    }

    /**
     * The class name of the CRUD Model
     *
     * @return void
     */
    public function getModel()
    {
        return CategoryModel::class;
    }

    /**
     * Make a form for the 'create' action
     *
     * @param Form $form
     * @return Form
     */
    public function create(Form $form)
    {
        $form->addSections([
            Section::make('main')->addFields([
                TextField::make('name')
                    ->addFieldsetClass('is-half'),
                SlugField::make('slug')
                    ->addFieldsetClass('is-half')
                    ->setRoute($this->slugRoute())
                    ->setSource('name'),
            ]),
        ]);

        return $form;
    }
}
