<?php

$factory->define(Yadda\Enso\Categories\Models\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => 'Test Category',
        'slug' => 'test-category',
    ];
});
