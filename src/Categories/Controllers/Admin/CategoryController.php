<?php

namespace Yadda\Enso\Categories\Controllers\Admin;

use Yadda\Enso\Crud\Controller;

class CategoryController extends Controller
{
    protected $crud_name = 'category';
}
