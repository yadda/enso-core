<?php

namespace Yadda\Enso\Categories\Traits;

use Yadda\Enso\Categories\Models\Category;

trait BelongsToCategory
{
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
