<?php

namespace Yadda\Enso\Categories\Traits;

use Yadda\Enso\Categories\Models\Category;

trait BelongsToManyCategories
{
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
