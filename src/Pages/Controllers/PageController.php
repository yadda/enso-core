<?php

namespace Yadda\Enso\Pages\Controllers;

use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Controller;
use Yadda\Enso\Crud\Traits\Controller\HasProtectedRecords;
use Yadda\Enso\Crud\Traits\Controller\HasTemplates;
use Yadda\Enso\Crud\Traits\Controller\IsPublishable;
use Yadda\Enso\Crud\Traits\ModifiesBySlug;

class PageController extends Controller
{
    use HasProtectedRecords,
        HasTemplates,
        IsPublishable,
        ModifiesBySlug;

    /**
     * Configures the hooks and filters for this controller
     *
     * @return void
     */
    protected function configureHooks()
    {
        $this->configureModifyBySlug();
    }

    /**
     * Clone an item
     *
     * @param Model $item
     *
     * @return Model
     */
    protected function cloneItem(Model $item): Model
    {
        $clone = parent::cloneItem($item);

        $clone->slug = null;
        $clone->title = $clone->title . ' Copy';
        $clone->order = $item->query()->max('order') + 1;

        return $clone;
    }
}
