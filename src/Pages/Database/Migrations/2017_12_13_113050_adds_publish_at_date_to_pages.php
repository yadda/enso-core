<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddsPublishAtDateToPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->datetime('publish_at')->nullable()->index()->after('published');

            /**
             * Some sites had data added to the pages table prior to this update,
             * so only do so if required.
             */
            if (!Schema::hasColumn('pages', 'data')) {
                $table->json('data')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->dropColumn('publish_at');

            /**
             * Do not actively rollback the 'data' column, as in some earlier
             * builds this was added separately before this Ensō build was
             * released.
             */
        });
    }
}
