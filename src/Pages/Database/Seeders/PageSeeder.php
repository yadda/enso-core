<?php

namespace Yadda\Enso\Pages\Database\Seeders;

use App\Models\Page;
use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $max_order = Page::max('order') ?? 0;

        $home = Page::where('slug', 'home')->first();
        if (!$home) {
            $home = Page::create([
                'title' => 'Home',
                'slug' => 'home',
                'template' => 'default',
                'published' => true,
                'order' => ++$max_order,
            ]);
        }

        $holding_page = Page::where('slug', 'holding-page')->first();
        if (!$holding_page) {
            $holding_page = Page::create([
                'title' => 'Holding Page',
                'slug' => 'holding-page',
                'template' => 'default',
                'published' => true,
                'order' => ++$max_order,
            ]);
        }
    }
}
