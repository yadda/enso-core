<?php

namespace Yadda\Enso\Pages\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Yadda\Enso\Crud\Contracts\IsCrudModel as ContractsIsCrudModel;
use Yadda\Enso\Crud\Contracts\Model\IsPublishable as ModelIsPublishable;
use Yadda\Enso\Crud\Traits\HasFlexibleFields;
use Yadda\Enso\Crud\Traits\IsCrudModel;
use Yadda\Enso\Crud\Traits\Model\HasTemplates;
use Yadda\Enso\Crud\Traits\Model\IsPublishable;
use Yadda\Enso\Media\Contracts\ImageFile;
use Yadda\Enso\Media\Traits\HasFilesTrait;
use Yadda\Enso\Meta\Traits\HasMeta;
use Yadda\Enso\Pages\Contracts\Page as ContractsPage;

/**
 * Page Model
 */
class Page extends Model implements ModelIsPublishable, ContractsIsCrudModel, ContractsPage
{
    use HasFilesTrait,
        HasFlexibleFields,
        HasMeta,
        HasTemplates,
        IsCrudModel,
        IsPublishable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'content',
        'template',
        'thumbnail_id',
        'published',
        'publish_at',
        'publish_at_hours', // Temporary, non-db value until we have a DateTime picker
        'publish_at_minutes', // Temporary, non-db value until we have a DateTime picker
        'parent_id',
        'excerpt',
        'order',
        'data',
    ];

    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [
        'content' => '[]',
        'data' => '[]',
        'template' => 'default',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'content' => 'array',
        'data' => 'array',
        'publish_at' => 'datetime',
    ];

    /**
     * Child pages
     *
     * @return HasMany
     */
    public function children()
    {
        return $this->hasMany(static::class, 'parent_id')->orderBy('order', 'DESC');
    }

    /**
     * Helper for getting values from the JSON Data column
     *
     * @param string $attribute   Attribute name
     * @param string $data_column Name of Data column if not data
     *
     * @return mixed Attribute value, or null
     */
    public function getDataValue($attribute, $data_column = 'data')
    {
        try {
            return $this->{$data_column}[$attribute];
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Gets the name of the Publish At DateTime column on this publishable
     *
     * @return string|null
     */
    public function getPublishAtColumn(): ?string
    {
        return 'publish_at';
    }

    /**
     * Name of the permission that allows users to view a page based on
     * irrespective of publishing state.
     *
     * @return string|null
     */
    public function getPublishViewOverridePermission()
    {
        return 'view-unpublished-pages';
    }

    /**
     * The parent page
     *
     * @return BelongsTo
     */
    public function parentPage()
    {
        return $this->belongsTo(static::class, 'parent_id', 'id');
    }

    /**
     * A related thumbnail image
     *
     * @return BelongsTo
     */
    public function thumbnail()
    {
        $image_class = get_class(resolve(ImageFile::class));

        return $this->belongsTo($image_class, 'thumbnail_id');
    }

    /**
     * The URL at which this model's can be viewed
     *
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return route('pages.show', $this->slug);
    }
}
