<?php

namespace Yadda\Enso\Crud\Contracts;

interface IsCrudModel
{
    /**
     * Database column or attribute used to label the item
     */
    public function ensoLabelColumn(): string;

    /**
     * The value to use at this Model's CRUD label
     *
     * @return string
     */
    public function getCrudLabel(): string;

    /**
     * The URL at which this model's can be viewed
     *
     * @return string|null
     */
    public function getUrl(): ?string;
}
