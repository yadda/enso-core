<?php

namespace Yadda\Enso\Crud\Contracts;

interface IsPublishableCrud
{

    /**
     * Basic Select Filter options for Publishable items
     *
     * @return array
     */
    public function getPublishedOptions();

    /**
     * Basic callable for handling Publishing filters.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $value
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function filterPublishedState($query, $value);
}
