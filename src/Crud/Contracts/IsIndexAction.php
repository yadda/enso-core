<?php

namespace Yadda\Enso\Crud\Contracts;

interface IsIndexAction
{
    /**
     * Gets or sets the button content
     *
     * @param string|null $button_content
     *
     * @return string|self
     */
    public function buttonContent(string $button_content = null);

    /**
     * Gets or sets the button type
     *
     * @param string|null $button_type
     *
     * @return string|self
     */
    public function buttonType(string $button_type = null);

    /**
     * Gets or sets the component
     *
     * @param string|null $component
     *
     * @return string|self
     */
    public function component(string $component = null);

    /**
     * Gets or sets the condition
     *
     * @param callable|null $condition
     *
     * @return callable|null|self
     */
    public function condition(callable $condition = null);

    /**
     * Gets the button content property
     *
     * @return string
     */
    public function getButtonContent(): string;

    /**
     * Gets the button type property
     *
     * @return string
     */
    public function getButtonType(): string;

    /**
     * Gets the component property
     *
     * @return string
     */
    public function getComponent(): string;

    /**
     * Gets the condition property
     *
     * @return callable|null
     */
    public function getCondition(): ?callable;

    /**
     * Gets the order property
     *
     * @return int
     */
    public function getOrder(): int;

    /**
     * Gets the route property
     *
     * @return string
     */
    public function getRoute(): string;

    /**
     * Gets the title property
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * Gets the wrapper class property
     *
     * @return string
     */
    public function getWrapperClass(): string;

    /**
     * Return newly instantiated action
     *
     * @return IsIndexAction
     */
    public static function make(): IsIndexAction;

    /**
     * Gets or sets the order
     *
     * @param int|null $order
     *
     * @return int|self
     */
    public function order(string $order = null);

    /**
     * Gets or sets the route
     *
     * @param string|null $route
     *
     * @return string|self
     */
    public function route(string $route = null);

    /**
     * Sets the button content on this
     *
     * @param string $button_content
     *
     * @return self
     */
    public function setButtonContent(string $button_content): self;

    /**
     * Sets the button type on this
     *
     * @param string $button_type
     *
     * @return self
     */
    public function setButtonType(string $button_type): self;

    /**
     * Sets the component on this
     *
     * @param string $component
     *
     * @return self
     */
    public function setComponent(string $component): self;

    /**
     * Sets the condition on this
     *
     * @param callable $condition
     *
     * @return self
     */
    public function setCondition(callable $condition = null): self;

    /**
     * Sets the order on this
     *
     * @param string $order
     *
     * @return self
     */
    public function setOrder(string $order): self;

    /**
     * Sets the route on this
     *
     * @param string $route
     *
     * @return self
     */
    public function setRoute(string $route): self;

    /**
     * Sets the title on this
     *
     * @param string $title
     *
     * @return self
     */
    public function setTitle(string $title): self;

    /**
     * Sets the wrapper class on this
     *
     * @param string $wrapper_class
     *
     * @return self
     */
    public function setWrapperClass(string $wrapper_class): self;

    /**
     * Gets or sets the title
     *
     * @param string|null $title
     *
     * @return string|self
     */
    public function title(string $title = null);

    /**
     * Converts this instance into a data array for the Crud.
     *
     * @return array
     */
    public function toArray(): array;

    /**
     * Gets or sets the wrapper class
     *
     * @param string|null $wrapper_class
     *
     * @return string|self
     */
    public function wrapperClass(string $wrapper_class = null);
}
