<?php

namespace Yadda\Enso\Crud\Contracts;

/**
 * Makes a field useable in a CollectioSection
 *
 * This should probably be renamed and/or refactored as it's a bit confusing.
 *
 * @todo Refector this.
 */
interface HasCorrectTyping
{
    /**
     * Gets a data type array to be used to store items to a CollectionSection
     *
     * @return array Data type information
     */
    public function getDataType();

    /**
     * Gets the correct value for this field from the passed data for a
     * Collection Section, i.e. for use when storing in json, e.g. for settings
     *
     * @param object $item          data source
     * @param string $property_name override property name
     *
     * @return mixed                matched data
     */
    public function getCollectionSectionFormData($item, $property_name = null);
}
