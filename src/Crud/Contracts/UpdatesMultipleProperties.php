<?php

namespace Yadda\Enso\Crud\Contracts;

interface UpdatesMultipleProperties
{

    /**
     * Get the names of each of the potential values this can set
     *
     * @return array Names of values
     */
    public function getMultipleValueNames();

    /**
     * Gets the value that should be set when the option is specified in the
     * request data
     *
     * @param array $data request data
     *
     * @return mixed value to set
     */
    public function getDefaultSingleSetValue($data);

    /**
     * Gest the value that should be set when the option is not specified in the
     * request data
     *
     * @return mixed value to set
     */
    public function getDefaultSingleUnsetValue();
}
