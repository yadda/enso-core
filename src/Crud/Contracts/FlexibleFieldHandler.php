<?php

namespace Yadda\Enso\Crud\Contracts;

interface FlexibleFieldHandler
{
    /**
     * Loads the data into an interal array, passing it through a helper class
     * in order to expand the data
     *
     * @param array $data $field_data to handle
     *
     * @return void
     */
    public function loadData(array $data, $base_class);

    /**
     * Returns a collection of all the rows
     *
     * @return \Illuminate\Support\Collection
     */
    public function getRows();

    /**
     * Blade Directive static call (so we can use multiple arguments) cleanly in
     * the blade temlate
     *
     * @param object $item                Item to fetch flex content from
     * @param string $field_name          Name of field to get
     * @param string $template            Non-standard template name
     * @param string $template_field_name Template field name override
     *
     * @return string Rendered Flexible content
     */
    public static function getRowContent($item, $field_name, $template = null, $template_field_name = null);
}
