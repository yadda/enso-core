<?php

namespace Yadda\Enso\Crud\Contracts;

interface FlexibleFieldParser
{
    /**
     * Expands the given data where needed, from a shortened DB friendly format
     * to a a full representation of the data.
     *
     * @param array $data Data to expand
     *
     * @return array Expanded data
     */
    public function expand($data);

    /**
     * Compresses the given data where needed, from a full representation to a
     * shortened, DB friendly format (eg Referencing models by ID instead of
     * full model data)
     *
     * @param array $data Data to compress
     *
     * @return array Compressed data
     */
    public function compress($data);
}
