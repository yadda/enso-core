<?php

namespace Yadda\Enso\Crud\Contracts;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Yadda\Enso\Users\Contracts\User as UserContract;

interface IsPublishable
{
    /**
     * Attribute accessor format for the isPublished call
     *
     * @return bool
     */
    public function getIsPublishedAttribute(): bool;

    /**
     * Gets the name of the Published boolean column on this publishable
     *
     * @return string
     */
    public function getPublishedColumn(): string;

    /**
     * Gets the name of the Publish At DateTime column on this publishable
     *
     * @return string|null
     */
    public function getPublishAtColumn();

    /**
     * Gets the current Published boolean value from this publishable
     *
     * @return bool
     */
    public function getPublished(): bool;

    /**
     * Gets the current Publish At datetime value from this publishable.
     *
     * @return \DateTime|null
     */
    public function getPublishDate();

    /**
     * Name of the permission that allows users to view a page based on
     * irrespective of publishing state.
     *
     * @return string|null
     */
    public function getPublishViewOverridePermission();

    /**
     * Gets the name of the Publish At column on this publishable
     *
     * @return string
     */
    public function getUnpublishAtColumn();

    /**
     * Gets the Unpublish At datetime from this publishable.
     *
     * @return DateTime
     */
    public function getUnpublishDate();

    /**
     * Checks to see whether this Publishable is published
     *
     * @return bool
     */
    public function isPublished(): bool;

    /**
     * Publish this model
     */
    public function publish(?Carbon $publish_at = null): void;

    /**
     * Limits a publishable query to only those that should be visible to either
     * a passed in user, the current user if authenticated or a guest.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param \Yadda\Enso\Users\Contracts\User   $user
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeAccessibleToUser(Builder $query, UserContract $user = null): Builder;

    /**
     * Limits queries to only return item that are considered as published.
     *
     * @param \Illuminate\Database\Query\Builder $query
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopePublished(Builder $query): Builder;

    /**
     * Limits queries to only return item that are not labelled as published,
     * or which either have a publish_at date set in the future.
     *
     * @param \Illuminate\Database\Query\Builder $query
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeNotPublished(Builder $query): Builder;

    /**
     * Limits queries to only return items that are set to publish in the
     * future but are not yet published now
     *
     * @throws \Yadda\Enso\Crud\Exceptions\PublishableException if there is no Publish At column
     *
     * @param \Illuminate\Database\Query\Builder $query
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeWillPublishLater(Builder $query): Builder;

    /**
     * Unpublish this model
     */
    public function unpublish(): void;
}
