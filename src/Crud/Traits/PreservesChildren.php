<?php

namespace Yadda\Enso\Crud\Traits;

/**
 * When applied to the model of a nested crud, child items will be
 * moved up a level before the parent item is deleted
 */
trait PreservesChildren
{
    public static function bootPreservesChildren()
    {
        static::deleting(function ($model) {
            $model->children->sortBy('_lft')->each(function ($child) use ($model) {
                $child->insertBeforeNode($model);
            });

            $model->refreshNode();
        });
    }
}
