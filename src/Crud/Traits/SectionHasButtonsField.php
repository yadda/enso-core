<?php

namespace Yadda\Enso\Crud\Traits;

use Illuminate\Support\Collection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

/**
 * For sections that have buttons fields
 *
 * @deprecated - This has been retained for backwards compatability. You should
 *               instead use RowHasFlexibleContent trait.
 */
trait SectionHasButtonsField
{
    /**
     * Parses the buttons block content using the ButtonSection::unpack
     * function
     *
     * @param string      $block_name
     * @param FlexibleRow $row
     *
     * @return Collection
     */
    protected static function getButtons(FlexibleRow $row, $block_name): Collection
    {
        return static::flexibleFieldContent($block_name, $row);
    }
}
