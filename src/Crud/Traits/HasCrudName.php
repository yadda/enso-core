<?php

namespace Yadda\Enso\Crud\Traits;

use Yadda\Enso\Facades\EnsoCrud;

trait HasCrudName
{
    /**
     * Gets the crud name for this Crud Model
     *
     * @return string
     */
    public function getCrudName(): string
    {
        return EnsoCrud::crudName($this);
    }
}
