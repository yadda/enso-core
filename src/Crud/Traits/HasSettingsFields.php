<?php

namespace Yadda\Enso\Crud\Traits;

use Illuminate\Support\Collection;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Traits\HasFields;

/**
 * When using this trait, you should ensure that there is a property on the the
 * class that matches the property name returned by getSettingsFieldAttributeName, and
 * that it is an instance of \Illuminate\Support\Collection
 */
trait HasSettingsFields
{
    use HasFields;

    /**
     * Gets the proprety name of the fields collection
     *
     * @return string
     */
    protected function getSettingsFieldAttributeName(): string
    {
        return 'settings_fields';
    }

    /**
     * Check whether this a field with a given name already exists within
     * the fields collection.
     *
     * @param string $field_name
     *
     * @return bool
     */
    public function hasSettingsField(string $field_name): bool
    {
        return !!$this->baseHasField($this->getSettingsFieldAttributeName(), $field_name);
    }

    /**
     * Gets a field from the fields collection, by name
     *
     * @param string $field_name
     *
     * @return \Yadda\Enso\Crud\Forms\FieldInterface
     */
    public function getSettingsField(string $field_name): FieldInterface
    {
        return $this->baseGetField($this->getSettingsFieldAttributeName(), $field_name);
    }

    /**
     * Get the field collection
     *
     * @return \Illuminate\Support\Collection
     */
    public function getSettingsFields(): Collection
    {
        return $this->baseGetFields($this->getSettingsFieldAttributeName());
    }

    /**
     * Extracts and returns a field from the field collection.
     *
     * @param string $field_name
     *
     * @return \Yadda\Enso\Crud\Forms\FieldInterface
     */
    public function extractSettingsField(string $field_name): FieldInterface
    {
        return $this->baseExtractField($this->getSettingsFieldAttributeName(), $field_name);
    }

    /**
     * Remove a field from the field collection, by name
     *
     * @param string $field_name
     *
     * @return self
     */
    public function removeSettingsField(string $field_name)
    {
        return $this->baseRemoveField($this->getSettingsFieldAttributeName(), $field_name);
    }

    /**
     * Iteratively calls addField for each element in an array of fields
     *
     * @param array $fields
     *
     * @return self
     */
    public function addSettingsFields(array $fields)
    {
        return $this->baseAddFields($this->getSettingsFieldAttributeName(), $fields);
    }

    /**
     * Adds a given field to the field collection
     *
     * @param \Yadda\Enso\Crud\Forms\FieldInterface $field
     *
     * @return self
     */
    public function addSettingsField(FieldInterface $field)
    {
        return $this->baseAddField($this->getSettingsFieldAttributeName(), $field);
    }

    /**
     * Adds a Field after a field with the given name.
     *
     * @param string                                $field_name
     * @param \Yadda\Enso\Crud\Forms\FieldInterface $field
     *
     * @return self
     */
    public function addSettingsFieldAfter(string $field_name, FieldInterface $field)
    {
        return $this->baseAddFieldAfter(
            $this->getSettingsFieldAttributeName(),
            $field_name,
            $field
        );
    }

    /**
     * Adds a set of Fields after a field with the given name.
     *
     * @param string $field_name
     * @param array  $fields
     *
     * @return self
     */
    public function addSettingsFieldsAfter(string $field_name, array $fields)
    {
        return $this->baseAddFieldsAfter(
            $this->getSettingsFieldAttributeName(),
            $field_name,
            $fields
        );
    }

    /**
     * Adds a Field before a field with the given name.
     *
     * @param string                                $field_name
     * @param \Yadda\Enso\Crud\Forms\FieldInterface $field
     *
     * @return self
     */
    public function addSettingsFieldBefore(string $field_name, FieldInterface $field)
    {
        return $this->baseAddFieldBefore(
            $this->getSettingsFieldAttributeName(),
            $field_name,
            $field
        );
    }

    /**
     * Adds a set of Fields before a field with the given name.
     *
     * @param string $field_name
     * @param array  $fields
     *
     * @return self
     */
    public function addSettingsFieldsBefore(string $field_name, array $fields)
    {
        return $this->baseAddFieldsBefore(
            $this->getSettingsFieldAttributeName(),
            $field_name,
            $fields
        );
    }

    /**
     * Adds a field at the beginning of the field collection.
     *
     * @param \Yadda\Enso\Crud\Forms\FieldInterface $field
     *
     * @return self
     */
    public function prependSettingsField(FieldInterface $field)
    {
        return $this->basePrependField($this->getSettingsFieldAttributeName(), $field);
    }

    /**
     * Adds a field at the beginning of the field collection.
     *
     * @param array $fields
     *
     * @return self
     */
    public function prependSettingsFields(array $fields)
    {
        return $this->basePrependFields($this->getSettingsFieldAttributeName(), $fields);
    }

    /**
     * Adds a field at the end of the field collection.
     *
     * @param \Yadda\Enso\Crud\Forms\FieldInterface $field
     *
     * @return self
     */
    public function appendSettingsField(FieldInterface $field)
    {
        return $this->baseAppendField($this->getSettingsFieldAttributeName(), $field);
    }

    /**
     * Adds a field at the end of the field collection.
     *
     * @param array $fields
     *
     * @return self
     */
    public function appendSettingsFields(array $fields)
    {
        return $this->baseAppendFields($this->getSettingsFieldAttributeName(), $fields);
    }

    /**
     * Moves a named field to the position just before another named field
     *
     * @param string $source_name
     * @param string $destination_name
     *
     * @return self
     */
    public function moveSettingsFieldAfter(string $source_name, string $destination_name)
    {
        return $this->baseMoveFieldAfter(
            $this->getSettingsFieldAttributeName(),
            $source_name,
            $destination_name
        );
    }

    /**
     * Moves a named field to the position just before another named field
     *
     * @param string $source_name
     * @param string $destination_name
     *
     * @return self
     */
    public function moveSettingsFieldBefore(string $source_name, string $destination_name)
    {
        return $this->baseMoveFieldBefore(
            $this->getSettingsFieldAttributeName(),
            $source_name,
            $destination_name
        );
    }
}
