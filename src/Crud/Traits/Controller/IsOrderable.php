<?php

namespace Yadda\Enso\Crud\Traits\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TorMorten\Eventy\Facades\Eventy;
use Yadda\Enso\Crud\Config;

/**
 * This trait can be applied to an Ensō Crud Controller to provide ordering
 * functionality
 */
trait IsOrderable
{
    /**
     * Boot the trait
     *
     * @return void
     */
    public static function bootIsOrderable(): void
    {
        Eventy::addAction('crud.store.before', [static::class, 'isOrderableApplyNextOrder'], 20, 2);
    }

    /**
     * Adds an appropriate 'order' value to the Model being stored
     *
     * @param Config  $config
     * @param Request $request
     *
     * @return boolean
     */
    public static function isOrderableApplyNextOrder(Config $config, Request $request)
    {
        if ($config->isOrderable()) {
            $item = $config->getCreateForm()->getModelInstance();
            $order_column = $config->getOrderable();

            $item->{$order_column} = (($item->query()->max($order_column) ?? 0) + 1);
        }
    }

    /**
     * Change the order of a given item by the given amount
     *
     * @param Request $request Request to base action on.
     * @param mixed   $id      Identifier of item to reorder
     *
     * @return Response
     */
    public function reorder(Request $request, $id)
    {
        Eventy::action('crud.reorder.before', $this->getConfig(), $request);

        $change = (int) $request->input('change');

        $id = Eventy::filter('crud.reorder.id', $id, $this->getConfig(), $request);
        $change = Eventy::filter('crud.reorder.change', $change, $this->getConfig(), $request);

        if (!$change) {
            return [
                'status' => 'success',
                'data' => null,
                'message' => 'No action was taken.'
            ];
        }

        $order_column = $this->getConfig()->getOrderable();

        $item = $this->getItemById($id);

        // Amount to offset other items by to compensate for moving our item.
        // We are only moving one item, so the other items will have to move
        // by either +1 or -1
        $offset = $change <=> 0;

        $original_order = $item->order;

        // $x is the max/min id of the other items that need updating.
        $x = $original_order + $change + $offset;

        $order_min = min($original_order, $x);
        $order_max = max($original_order, $x);

        $updated = DB::table($item->getTable())
            ->where($order_column, '>', $order_min)
            ->where($order_column, '<', $order_max)
            ->select('id')
            ->get()
            ->map(function ($item) {
                return $item->id;
            });

        $updated->push((int) $id);

        DB::table($item->getTable())
            ->where($order_column, '>', $order_min)
            ->where($order_column, '<', $order_max)
            ->increment($order_column, -$offset);

        DB::table($item->getTable())
            ->where('id', $item->id)
            ->increment($order_column, $change);

        Eventy::action('crud.reorder.after', $updated, $this->getConfig(), $request);

        return [
            'status' => 'success',
            'data' => [
                'original_order' => $original_order,
                'order_min' => $order_min,
                'order_max' => $order_max,
                'x' => $x,
                'offset' => $offset,
                'change' => $change,
            ],
        ];
    }
}
