<?php

namespace Yadda\Enso\Crud\Traits\Controller;

use Eventy;
use Illuminate\Http\Request;
use Yadda\Enso\Crud\Config;

/**
 * This trait can be applied to an Ensō Crud Controller to provide protected
 * records functionality.
 */
trait HasProtectedRecords
{
    /**
     * Boot the trait
     *
     * @return void
     */
    public static function bootHasProtectedRecords(): void
    {
        Eventy::addAction('crud.edit', [static::class, 'hasProtectedRecordsDisableEditing'], 20, 2);
        Eventy::addAction('crud.update.before', [static::class, 'hasProtectedRecordsDisableEditing'], 20, 2);
    }

    /**
     * Disabled editing of a record's identifier field when the record's
     * identifier is determined to be "protected".
     *
     * @param Config  $config
     * @param Request $request
     *
     * @return void
     */
    public static function hasProtectedRecordsDisableEditing(Config $config, Request $request)
    {
        $model_instance = $config->getEditForm()->getModelInstance();
        $field_name = $config->hasProtectedRecordsFieldName();

        if (empty($model_instance->{$field_name})) {
            return;
        }

        if (!$config->hasProtectedRecordsIsEditable($model_instance)) {
            $form = $config->getEditForm();
            $form->getSection($config->hasProtectedRecordsSectionName())
                ->getField($field_name)
                ->setDisabled(true);
        }
    }
}
