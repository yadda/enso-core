<?php

namespace Yadda\Enso\Crud\Traits\Config;

use TorMorten\Eventy\Facades\Eventy;
use Yadda\Enso\Crud\Filters\PublishFilter;
use Yadda\Enso\Crud\Tables\Publish;
use Yadda\Enso\Facades\EnsoCrud;

/**
 * This trait can be applied to an Ensō Crud Config to provide publishing
 * functionality.
 */
trait IsPublishable
{
    /**
     * Boot the trait
     *
     * @return void
     */
    public static function bootIsPublishable(): void
    {
        $crud_name = EnsoCrud::crudName(static::class);

        Eventy::addFilter('enso.' . $crud_name . '.config.js-config', [static::class, 'isPublishableModifyJsConfig'], 20, 1);
    }

    /**
     * Initialize the trait
     *
     * @return void
     */
    public function initializeIsPublishable(): void
    {
        $this->addBulkActions($this->isPublishableBulkActions());
        $this->addItemFilter('published', $this->isPublishableFilter());
    }

    /**
     * BulkActions relevant to publish status
     *
     * @return array
     */
    protected function isPublishableBulkActions(): array
    {
        return [
            'publish' => [
                'route' => route($this->getRoute('publish')),
                'method' => 'POST',
                'label' => 'Publish selected',
            ],
            'unpublish' => [
                'route' => route($this->getRoute('unpublish')),
                'method' => 'POST',
                'label' => 'Unpublish selected',
            ],
        ];
    }

    /**
     * Crud Filter for published state
     *
     * @return PublishFilter
     */
    protected function isPublishableFilter(): PublishFilter
    {
        return PublishFilter::make($this->getCrudName());
    }

    /**
     * Modifes a JS Config data array
     *
     * @param array $config_data
     *
     * @return array
     */
    public static function isPublishableModifyJsConfig(array $config_data): array
    {
        $config_data['publishable'] = true;
        return $config_data;
    }

    /**
     * Publishing table cell to add to an index table
     *
     * @return Publish
     */
    protected function isPublishablePublishTableCell(): Publish
    {
        return Publish::make('is_published', $this)
            ->setLabel('Published')
            ->addThClass('is-narrow has-text-centered');
    }
}
