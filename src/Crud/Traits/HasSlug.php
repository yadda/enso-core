<?php

namespace Yadda\Enso\Crud\Traits;

trait HasSlug
{
    /**
     * Query scope to filter Models by a given slug
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $slug
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }
}
