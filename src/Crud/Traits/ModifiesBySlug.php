<?php

namespace Yadda\Enso\Crud\Traits;

use Eventy;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yadda\Enso\Crud\Config;

trait ModifiesBySlug
{
    /**
     * Whether to add Filter hooks for slug modifications
     *
     * @var bool
     */
    protected $adds_fields_by_slug = true;

    /**
     * Sets up events to allow for adding config
     * functions depending on the item's slug
     *
     * @return void
     */
    protected function configureModifyBySlug()
    {
        if ($this->adds_fields_by_slug) {
            Eventy::addAction('crud.edit', [$this, 'modifyBySlug'], 20, 2);
            Eventy::addAction('crud.update.before', [$this, 'modifyBySlug'], 10, 2);
        }
    }

    /**
     * Call a function on the config based on the slug of the Model instances
     * assigned to it, if it has been set up on the config.
     *
     * @param Config  $config  Original Config
     * @param Request $request Request (not used)
     *
     * @return void
     */
    public static function modifyBySlug(Config $config, Request $request)
    {
        $model_instance = $config->getEditForm()->getModelInstance();

        if (is_null($model_instance)) {
            throw new Exception('Cannot add ' . $config->getNameSingular() . ' fields to an edit form with no item');
        }

        // No action can be taken for a model with no slug.
        if (empty($model_instance->slug)) {
            return $config;
        }

        $method_name = 'add' . Str::studly($model_instance->slug) . 'Fields';

        if (method_exists($config, $method_name)) {
            $config->$method_name();
        }
    }
}
