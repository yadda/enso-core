<?php

namespace Yadda\Enso\Crud\Traits;

use Enso;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use TorMorten\Eventy\Facades\Eventy;

trait ControlsNestedModels
{
    public function rearrange(int $item_id, Request $request)
    {
        $item = $this->getItem($item_id);

        $this->applyNesting($item, $request);
    }

    /**
     * Relocate an item within a nested
     */
    public function applyNesting(Model $item, Request $request)
    {
        $options = $request->input('nesting');

        if (Arr::has($options, 'child_of')) {
            $parent_id = Arr::get($options, 'child_of');
            $parent = $this->getConfig()->getModel()::find($parent_id);

            if ($parent) {
                $item->appendToNode($parent)->save();
            }
        } elseif (Arr::has($options, 'insert_after')) {
            $sibling_id = Arr::get($options, 'insert_after');
            $sibling = $this->getconfig()->getModel()::find($sibling_id);

            if ($sibling) {
                $item->insertAfterNode($sibling);
            }
        } elseif (Arr::has($options, 'insert_before')) {
            $sibling_id = Arr::get($options, 'insert_before');
            $sibling = $this->getConfig()->getModel()::find($sibling_id);

            if ($sibling) {
                $item->insertBeforeNode($sibling);
            }
        }

        return $item;
    }

    /**
     * View a crud as an orderable tree if nesting is available
     */
    public function tree(Request $request)
    {
        $this->addCrudActionClass('tree');

        $config = $this->getConfig();

        Eventy::action('crud.tree.beforeRender', $request, $config);

        $view_name = $config->getCrudView('tree');

        Enso::setJSData('crud', $this->getConfig()->getJSConfig());

        return view(
            $view_name,
            [
                'crud' => $config,
            ]
        );
    }

    /**
     * Get a nested list of items for a tree reordering view
     */
    public function treeData(): JsonResponse
    {
        if (!$this->getConfig()->nested()) {
            abort(404);
        }

        $query = call_user_func($this->getConfig()->getModel() . '::query');
        $query->withoutGlobalScopes();

        $items = $query->defaultOrder()->get()->toTree();

        $items = $this->mapTree($items);

        return response()->json([
            'status' => 'success',
            'data' => [
                'items' => $items,
            ],
            'message' => null,
        ]);
    }

    protected function mapTree(Collection $tree): Collection
    {
        $items = new Collection;

        $tree->each(function ($item) use (&$items) {
            $items->push([
                'id' => $item->getKey(),
                'label' => $item->{$item->ensoLabelColumn()},
                '_lft' => $item->_lft,
                '_rgt' => $item->_rgt,
                'parent_id' => $item->parent_id,
                'children' => $this->mapTree($item->children),
            ]);
        });

        return $items;
    }
}
