<?php

namespace Yadda\Enso\Crud\Traits;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Yadda\Enso\Facades\EnsoCrud;
use Yadda\Enso\Facades\EnsoMeta;
use Yadda\Enso\Pages\Contracts\Page;

trait UsesPage
{
    /**
     * Rely on a page for this route. If the page doesn't exist, return a 404.
     * If it does, use the meta from the page.
     *
     * @param string $slug
     *
     * @return Page
     */
    public function usePage($slug): Page
    {
        $page_model = EnsoCrud::modelClass('page');

        $page = $page_model::where('slug', $slug)->accessibleToUser()->first();

        if (!$page) {
            Log::error('Tried to load a page that does not exist. Slug: ' . $slug);
            abort(404);
        }

        $meta = $page->getMeta();

        $meta->overrideTitle($page->title);

        EnsoMeta::use($meta);

        return $page;
    }

    /**
     * Rely on a page for this route. Unpublished pages will still work using
     * this function. If the page doesn't exist, return a 404. If it does, use
     * the meta from the page.
     *
     * @param string $slug
     *
     * @return Page
     */
    public function usePageAllowUnpublished(string $slug): Page
    {
        $page_model = EnsoCrud::modelClass('page');

        $page = $page_model::where('slug', $slug)->first();

        if (!$page) {
            Log::error('Tried to load a page that does not exist. Slug: ' . $slug);
            abort(404);
        }

        $meta = $page->getMeta();

        $meta->overrideTitle($page->title);

        EnsoMeta::use($meta);

        return $page;
    }

    /**
     * Rely on a page for this route. Unpublished pages will still work using
     * this function. If the page use the meta from the page. If not, provides
     * some logical defaults based on the slug.
     *
     * @param string $slug
     * @param array  $props
     *
     * @return Page
     */
    public function usePageAllowNotExists(string $slug, array $props = []): Page
    {
        $page_model = EnsoCrud::modelClass('page');

        $page = $page_model::where('slug', $slug)->first();

        if (empty($page)) {
            $page = new $page_model(array_merge(
                [
                    'title' => Str::title(Str::slug($slug, ' ')),
                    'slug' => $slug,
                    'template' => 'default',
                ],
                $props
            ));
        }

        $meta = $page->getMeta();

        $meta->overrideTitle($page->title);

        EnsoMeta::use($meta);

        return $page;
    }
}
