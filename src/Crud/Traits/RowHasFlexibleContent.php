<?php

namespace Yadda\Enso\Crud\Traits;

use Illuminate\Support\Collection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

trait RowHasFlexibleContent
{
    /**
     * Unpacks content from a named FlexibleContentField (which must use the
     * FieldHasRowSpecs trait)
     *
     * @param string $block_name
     * @param FlexibleRow $row
     *
     * @return Collection
     */
    public function getFlexibleContentFieldContent(string $block_name, FlexibleRow $row): Collection
    {
        return $this->getField($block_name)->getSubRowsData($row);
    }

    public static function flexibleFieldContent(string $block_name, FlexibleRow $row): Collection
    {
        return static::make()->getFlexibleContentFieldContent($block_name, $row);
    }
}
