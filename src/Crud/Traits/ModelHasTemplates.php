<?php

namespace Yadda\Enso\Crud\Traits;

use File;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Utilities\Helpers;

trait ModelHasTemplates
{
    /**
     * Gets a list of template files based on the template directory of the
     * model. If model not provided, try to ascertain which the model based on
     * Crud config.
     *
     * @return array                          Template list
     */
    protected function getTemplateList($model = null)
    {
        if (empty($model) && method_exists($this, 'getCrudName')) {
            $model = Helpers::getConcreteClass(Config::get(
                'enso.crud.' . $this->getCrudName() . '.model',
                []
            ));
        }

        $directory = (new $model)->getTemplateDirectory();

        $files = File::allFiles(resource_path("views/{$directory}"));

        $templates = array_map(function ($item) {
            return str_replace('.blade.php', '', $item->getBasename());
        }, $files);

        $templates = array_combine($templates, array_map(function ($template) {
            return ucfirst(str_replace('-', ' ', $template));
        }, $templates));

        return $templates;
    }
}
