<?php

namespace Yadda\Enso\Crud\Traits;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Yadda\Enso\Crud\Contracts\IsIndexAction;
use Yadda\Enso\Crud\Exceptions\CrudException;

trait HasIndexActions
{
    /**
     * Add Clone Index action if configured
     *
     * @return self
     */
    protected function addCloneIndexAction(): self
    {
        if ($this->allowCloning() && ($clone_action = $this->getCloneIndexAction())) {
            $this->addIndexAction('clone', $clone_action);
        }

        return $this;
    }

    /**
     * Add default Index actions for this Config
     *
     * @return void
     */
    protected function addDefaultIndexActions(): void
    {
        $this->addCloneIndexAction()
            ->addEditIndexAction()
            ->addDeleteIndexAction();
    }

    /**
     * Adds a delete button. This was originally part of the default config, but as it
     * 'could' be removed as a function call, it needs to be added that way to so we
     * can keep the setup in one place.
     *
     * @return self
     */
    protected function addDeleteIndexAction()
    {
        if ($delete_action = $this->getDeleteIndexAction()) {
            $this->addIndexAction('delete', $delete_action);
        }

        return $this;
    }

    /**
     * Add Edit Index action if configured
     *
     * @return self
     */
    protected function addEditIndexAction(): self
    {
        if ($edit_action = $this->getEditIndexAction()) {
            $this->addIndexAction('edit', $edit_action);
        }

        return $this;
    }

    /**
     * Adds a named index action to the list of actions.
     *
     * @param string              $name
     * @param array|IsIndexAction $index_action
     *
     * @return self
     */
    public function addIndexAction(string $name, $index_action)
    {
        if ($this->hasIndexAction($name)) {
            throw new CrudException(
                'Can not add IndexAction with name `' . $name . '`, it already exists'
            );
        }

        $this->{$this->getIndexActionsProperty()}[$name] = $index_action;

        return $this;
    }

    /**
     * Defers to addIndexAction for each element of an array.
     *
     * @param array $index_actions
     *
     * @return self
     */
    public function addIndexActions(array $index_actions)
    {
        foreach ($index_actions as $name => $action) {
            $this->addIndexAction($name, $action);
        }

        return $this;
    }

    /**
     * The Clone IndexAction for this Crud Config
     *
     * @return array|IsIndexAction
     */
    protected function getCloneIndexAction()
    {
        return \Yadda\Enso\Crud\IndexActions\CloneButton::make();
    }

    /**
     * The Delete index action for this Crud Config
     *
     * @return array|IsIndexAction
     */
    protected function getDeleteIndexAction()
    {
        return \Yadda\Enso\Crud\IndexActions\DeleteButton::make();
    }

    /**
     * The Edit IndexAction for this Crud Config
     *
     * @return array|IsIndexAction
     */
    protected function getEditIndexAction()
    {
        return \Yadda\Enso\Crud\IndexActions\EditButton::make();
    }

    /**
     * Returns the finalised index actions list
     *
     * @return array
     */
    public function getFinalIndexActions()
    {
        return (new Collection($this->getIndexActions()))->map(function ($action) {
            if ($action instanceof IsIndexAction) {
                $action = $action->toArray();
            }

            $action['route'] = route($this->getRoute('index')) . Arr::get($action, 'route');

            return $action;
        })->sortBy('order')->values();
    }

    /**
     * Gets the index actions that should be available for a given item.
     *
     * @param \Illuminate\Database\Eloquent\Model $item
     *
     * @return array
     */
    public function getIndexActionsForItem($item)
    {
        return collect($this->getFinalIndexActions())
            ->reduce(function ($carry, $action) use ($item) {
                if ((!isset($action['only_when'])) || (call_user_func($action['only_when'], $item))) {
                    $carry->push($action);
                }

                return $carry;
            }, new Collection)->filter()->values();
    }

    /**
     * Get index action, by name
     *
     * @param string $name
     *
     * @return array|IsIndexAction
     */
    public function getIndexAction(string $name)
    {
        return $this->hasIndexAction($name)
            ? $this->getIndexActions($name)
            : null;
    }

    /**
     * Get internal index actions array. If a name is provided as a string, get
     * that action. If name is provided as an array, get those actions.
     *
     * @param string|array|null
     *
     * @return array|IsIndexAction
     */
    public function getIndexActions($name = null)
    {
        if (is_null($name)) {
            return $this->{$this->getIndexActionsProperty()};
        } elseif (is_array($name)) {
            return array_intersect_key(
                $this->{$this->getIndexActionsProperty()},
                array_flip($name)
            );
        } else {
            return Arr::get($this->{$this->getIndexActionsProperty()}, $name);
        }
    }

    /**
     * Name of the index actions property
     *
     * @return string
     */
    protected function getIndexActionsProperty(): string
    {
        return 'index_actions';
    }

    /**
     * Boolean check to see whether any bulk actions are available for this item
     *
     * @return bool
     */
    public function hasIndexActions(): bool
    {
        return count($this->getIndexActions()) > 0;
    }

    /**
     * Whether a bulk actions is available
     *
     * @return bool
     */
    public function hasIndexAction(string $name): bool
    {
        return array_key_exists($name, $this->{$this->getIndexActionsProperty()});
    }

    /**
     * Removes an IndexAction by name
     *
     * @param string $name
     *
     * @return self
     */
    public function removeIndexAction(string $name)
    {
        if (key_exists($name, $this->{$this->getIndexActionsProperty()})) {
            unset($this->{$this->getIndexActionsProperty()}[$name]);
        }

        return $this;
    }

    /**
     * Removes IndexActions by name
     *
     * @param array $name
     *
     * @return self
     */
    public function removeIndexActions(array $names)
    {
        return $this->setIndexActions(
            array_diff_key(
                $this->getIndexActions(),
                array_flip($names)
            )
        );
    }

    /**
     * Set the entire index_actions array.
     *
     * @param array $index_actions
     *
     * @return self
     */
    public function setIndexActions(array $index_actions)
    {
        $this->{$this->getIndexActionsProperty()} = $index_actions;

        return $this;
    }

    /**
     * Updates a given Index Action with whatever array elements are passed. This accepts an
     * optional boolean to user array_replace_recursive in place array_replace, so that you can
     * replace both whole child arrays or just parts of them.
     *
     * @param string $name
     * @param array  $updates|null
     * @param bool   $deep
     *
     * @return self
     */
    public function updateIndexAction($name, $updates = null, $deep = false)
    {
        if (empty($this->{$this->getIndexActionsProperty()}[$name])) {
            throw new CrudException(
                'Index Action named `' . $name . '` cannot be updated as it does not exist.'
            );
        }

        if ($this->{$this->getIndexActionsProperty()}[$name] instanceof IsIndexAction) {
            throw new CrudException(
                'updateIndexAction can only be used with data-array index actions'
            );
        }

        if ($deep) {
            $this->{$this->getIndexActionsProperty()}[$name]
                = array_replace_recursive(
                    $this->{$this->getIndexActionsProperty()}[$name],
                    $updates
                );
        } else {
            $this->{$this->getIndexActionsProperty()}[$name]
                = array_replace(
                    $this->{$this->getIndexActionsProperty()}[$name],
                    $updates
                );
        }

        return $this;
    }

    /**
     * Updates a set of index actions. Should be key => value pairs that
     * represent the name of the action and it's action array. Does not support
     * selective replace vs replace_recursive on a per-action basis.
     *
     * @param array $updates
     * @param bool  $deep
     *
     * @return self
     */
    public function updateIndexActions($updates, $deep = false)
    {
        foreach ($updates as $name => $update) {
            $this->updateIndexAction($name, $update, $deep = false);
        }

        return $this;
    }
}
