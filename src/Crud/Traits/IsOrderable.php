<?php

namespace Yadda\Enso\Crud\Traits;

trait IsOrderable
{
    protected $orderable_order_by = 'order';
    protected $orderable_order_direction = 'DESC';

    /**
     * Sets the stored ordering column name. Default is 'order'.
     *
     * @param  string   $order_by           column name
     *
     * @return self
     */
    public function setOrderableOrderBy(string $order_by)
    {
        $this->orderable_order_by = $order_by;
        return $this;
    }

    /**
     * Gets the current stored ordering column name.
     *
     * @return string
     */
    public function getOrderableOrderBy()
    {
        return $this->orderable_order_by;
    }

    /**
     * Sets the stored ordering direction to 'DESC'.
     *
     * @return self
     */
    public function setOrderableDescending()
    {
        $this->orderable_order_direction = 'DESC';
        return $this;
    }

    /**
    * Gets the current stored ordering direction.
    *
    * @return string
    */
    public function isOrderableDescending()
    {
        return $this->orderable_order_direction === 'DESC';
    }

    /**
     * Sets the stored ordering direction to 'DESC'.
     *
     * @return self
     */
    public function setOrderableAscending()
    {
        $this->orderable_order_direction = 'ASC';
        return $this;
    }

    /**
    * Gets the current stored ordering direction.
    *
    * @return string
    */
    public function isOrderableAscending()
    {
        return $this->orderable_order_direction === 'ASC';
    }

    /**
     * Get the current order value of this item
     *
     * @return integer
     */
    public function getOrderableOrder()
    {
        return $this->{$this->getOrderableOrderBy()};
    }

    /**
     * Get the next item in order or null if this is the last
     *
     * @return Model|null
     */
    public function getOrderableNext()
    {
        return $this->where($this->getOrderableOrderBy(), '>', $this->getOrderableOrder())
            ->orderBy($this->getOrderableOrderBy(), 'ASC')
            ->first();
    }

    /**
     * Get the previous item in order or null if this is the first
     *
     * @return Model|null
     */
    public function getOrderablePrevious()
    {
        return $this->where($this->getOrderableOrderBy(), '<', $this->getOrderableOrder())
            ->orderBy($this->getOrderableOrderBy(), 'DESC')
            ->first();
    }
}
