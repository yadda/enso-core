<?php

namespace Yadda\Enso\Crud\Traits;

use LaravelLocalization;
use Yadda\Enso\Crud\Contracts\FlexibleFieldHandler as FlexibleFieldHandlerContract;

trait HasFlexibleFields
{
    protected $flexible_field_handler_instances = [];

    /**
     * Expands and renders a Flexible field, based on a given field name
     *
     * @param  string       $field_name         Name of field to render
     * @param  string       $base_class         Base class to base bem classes on
     * @param  string       $handler_override   [description]
     * @return mixed                            [description]
     */
    public function flexibleFieldContent($field_name, $base_class = null, $handler_override = null)
    {
        if (!isset($this->flexible_field_handler_instances[$field_name])) {
            if (empty($handler_override)) {
                $handler = resolve(FlexibleFieldHandlerContract::class);
            } else {
                $handler = new $handler_override;
            }

            // @todo it's 10pm and there is probably a better way to do this.
            if (isset($this->translatable) && in_array($field_name, $this->translatable)) {
                $content = $this->getTranslation($field_name, LaravelLocalization::getCurrentLocale());
                if (!$content) {
                    $content = [];
                }
                $handler->setInstance($this)->loadData($content, $base_class);
            } else {
                $handler->setInstance($this)->loadData($this->$field_name, $base_class);
            }

            $this->flexible_field_handler_instances[$field_name] = $handler;
        }

        return $this->flexible_field_handler_instances[$field_name];
    }
}
