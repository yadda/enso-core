<?php

namespace Yadda\Enso\Crud\Traits;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Trait that adds functionality to CrudControllers for publishing and
 * unpublishing items that use the IsPublishable trait.
 */
trait PublishesItems
{
    /**
     * Publishes a set of Models, based on given values as model ids.
     *
     * @param Request $request
     *
     * @return array
     */
    public function publish(Request $request)
    {
        $item_ids = $request->get('values', []);

        $config = $this->getConfig();

        $model = $config->getModel();

        $instance = new $model;

        try {
            $items = $model::withoutGlobalScopes()
                ->whereIn($instance->getKeyName(), $item_ids)
                ->get();

            if ($items->count() === 0) {
                return [
                    'status' => 'error',
                    'message' => 'No ' . $config->getNamePlural() . ' selected to publish.',
                ];
            }

            list($publishable, $unpublishable) = $this->filterUnpublishable($items);

            $this->publishItems($publishable);
        } catch (Exception $e) {
            DB::rollback();

            Log::error($e);

            return [
                'status' => 'error',
                'message' => 'Could not publish ' . $config->getNamePlural(),
            ];
        }

        if ($unpublishable->count() > 0) {
            $response = [
                'status' => 'warning',
                'title' => 'Warning!',
                'message' => 'Unable to publish some of the selected '
                    . $config->getNamePlural(),
            ];
        } else {
            $response = [
                'status' => 'success',
                'message' => 'Successfully published the selected '
                    . $config->getNamePlural(),
            ];
        }

        return array_merge(
            $response,
            [
                'success' => $publishable->pluck('id')->all(),
                'error' => $unpublishable->pluck('id')->all(),
            ]
        );
    }

    /**
     * Unpublishes a set of Models, based on given values as model ids.
     *
     * @param Request $request
     *
     * @return array
     */
    public function unpublish(Request $request)
    {
        $item_ids = $request->get('values', []);

        $config = $this->getConfig();

        $model = $config->getModel();

        $instance = new $model;

        try {
            $items = $model::withoutGlobalScopes()
                ->whereIn($instance->getKeyName(), $item_ids)
                ->get();

            if ($items->count() === 0) {
                return [
                    'status' => 'error',
                    'message' => 'No ' . $config->getNamePlural() . ' selected to unpublish.',
                ];
            }

            DB::beginTransaction();

            $items->each(function ($item) {
                $item->update([$item->getPublishedColumn() => false]);
            });
        } catch (Exception $e) {
            DB::rollback();

            Log::error($e);

            return [
                'status' => 'error',
                'message' => 'Could not unpublish ' . $config->getNamePlural(),
            ];
        }

        DB::commit();

        return [
            'status' => 'success',
            'message' => 'Successfully unpublished the selected '
                    . $config->getNamePlural(),
            'success' => $items->pluck('id')->all(),
        ];
    }

    /**
     * If the Models in this collection has a canPublish method, then filter them
     * by the result of that method. Otherwise, return all items as Publishable.
     *
     * @param Collection $items
     *
     * @return array
     */
    protected function filterUnpublishable(Collection $items): array
    {
        if (method_exists($items->first(), 'canPublish')) {
            return $items->partition(function ($item) {
                return $item->isPublished() || $item->canPublish();
            })->all();
        }

        return [$items, new Collection];
    }

    /**
     * Publishes items in the given collection, ignoring items already
     * published.
     *
     * @param Collection $items
     *
     * @return void
     */
    protected function publishItems(Collection $items)
    {
        $current_time = Carbon::now();

        DB::beginTransaction();

        $items->each(function ($item) use ($current_time) {
            if ($item->isPublished()) {
                return;
            }

            $item->fill([$item->getPublishedColumn() => true]);

            if ($item->getPublishAtColumn()) {
                if ($item->getPublishDate() && $item->getPublishDate()->gt($current_time)) {
                    $item->fill([$item->getPublishAtColumn() => $current_time]);
                }
            }

            $item->save();
        });

        DB::commit();
    }
}
