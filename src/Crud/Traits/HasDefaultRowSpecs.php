<?php

namespace Yadda\Enso\Crud\Traits;

use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\Rows\Accordion;
use Yadda\Enso\Crud\Forms\Rows\Files;
use Yadda\Enso\Crud\Forms\Rows\Hero;
use Yadda\Enso\Crud\Forms\Rows\Media;
use Yadda\Enso\Crud\Forms\Rows\Quote;
use Yadda\Enso\Crud\Forms\Rows\SignupForm;
use Yadda\Enso\Crud\Forms\Rows\Text;
use Yadda\Enso\Crud\Forms\Rows\TextMedia;

trait HasDefaultRowSpecs
{
    /**
     * Array of Site-wide default row specs.
     *
     * @return array
     */
    protected function defaultRowSpecs(): array
    {
        $signup_form_type = Config::get('enso.flexible-content.rows.signup-form.type');

        return array_filter([
            Accordion::make(),
            Files::make(),
            Hero::make(),
            Media::make(),
            Quote::make(),
            $signup_form_type ? SignupForm::make() : null,
            Text::make(),
            TextMedia::make(),
        ]);
    }
}
