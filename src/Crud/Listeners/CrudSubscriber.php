<?php

namespace Yadda\Enso\Crud\Listeners;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Yadda\Enso\Crud\Events\CrudBeforeCreate;
use Yadda\Enso\Crud\Events\CrudBeforeUpdate;
use Yadda\Enso\Crud\Events\CrudCreated;
use Yadda\Enso\Crud\Events\CrudDeleted;
use Yadda\Enso\Crud\Events\CrudUpdated;

class CrudSubscriber
{
    private $monolog;

    /**
     * Creates a new Logger instance so that we can log to a different file.
     */
    public function __construct()
    {
        $file_path = storage_path(Config::get('enso.events.crud.log_file', 'logs/crud.log'));

        $this->monolog = new Logger('crud');

        if (Config::get('enso.events.crud.use_daily_files', false)) {
            $files_to_keep = Config::get('enso.events.crud.files_to_keep', 0);
            $handler = new RotatingFileHandler($file_path, $files_to_keep);
        } else {
            $handler = new StreamHandler($file_path);
        }

        $this->monolog->setHandlers([$handler]);
    }

    /**
     * Handle user login events.
     */
    public function onItemBeforeCreate($event)
    {
        $ip = $this->getClientOriginalIp();
        $user_id = Auth::check() ? Auth::id() : 'NO USER';
        $event_name = 'Requested Create';
        $item_class = $event->class;

        $message = "{$ip} - {$event_name}: User `{$user_id}`, {$item_class}";

        $this->monolog->addRecord(Logger::INFO, $message);
    }

    /**
     * Handle user login events.
     */
    public function onItemCreated($event)
    {
        $ip = $this->getClientOriginalIp();
        $user_id = Auth::check() ? Auth::id() : 'NO USER';
        $event_name = 'Created';
        $item_class = get_class($event->item);
        $item_id = $event->item->getKey();

        $message = "{$ip} - {$event_name}: User `{$user_id}`, {$item_class} `{$item_id}`";

        $this->monolog->addRecord(Logger::INFO, $message);
    }

    /**
     * Handle user login events.
     */
    public function onItemBeforeUpdate($event)
    {
        $ip = $this->getClientOriginalIp();
        $user_id = Auth::check() ? Auth::id() : 'NO USER';
        $event_name = 'Requested Update';
        $item_class = get_class($event->item);
        $item_id = $event->item->getKey();

        $message = "{$ip} - {$event_name}: User `{$user_id}`, {$item_class} `{$item_id}`";

        $this->monolog->addRecord(Logger::INFO, $message);
    }

    /**
     * Handle user login events.
     */
    public function onItemUpdated($event)
    {
        $ip = $this->getClientOriginalIp();
        $user_id = Auth::check() ? Auth::id() : 'NO USER';
        $event_name = 'Updated';
        $item_class = get_class($event->item);
        $item_id = $event->item->getKey();

        $message = "{$ip} - {$event_name}: User `{$user_id}`, {$item_class} `{$item_id}`";

        $this->monolog->addRecord(Logger::INFO, $message);
    }

    /**
     * Handle user login events.
     */
    public function onItemDeleted($event)
    {
        $ip = $this->getClientOriginalIp();
        $user_id = Auth::check() ? Auth::id() : 'NO USER';
        $event_name = 'Deleted';
        $item_class = get_class($event->item);
        $item_id = $event->item->getKey();

        $message = "{$ip} - {$event_name}: User `{$user_id}`, {$item_class} `{$item_id}`";

        $this->monolog->addRecord(Logger::INFO, $message);
    }

    /**
     * Gets the IP address that initiated there request. If it has a different
     * x-forwarded-for header, include that as well.
     *
     * @return string
     */
    protected function getClientOriginalIp()
    {
        if (Request::header('x-forwarded-for')
            && Request::ip() !== Request::header('x-forwarded-for')
        ) {
            return Request::ip() . '(x-forwarded-for ' . Request::header('x-forwarded-for') . ')';
        }

        return Request::ip();
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        if (Config::get('enso.events.crud.before-create', false)) {
            $events->listen(CrudBeforeCreate::class, CrudSubscriber::class . '@onItemBeforeCreate');
        }

        if (Config::get('enso.events.crud.created', false)) {
            $events->listen(CrudCreated::class, CrudSubscriber::class . '@onItemCreated');
        }

        if (Config::get('enso.events.crud.before-update', false)) {
            $events->listen(CrudBeforeUpdate::class, CrudSubscriber::class . '@onItemBeforeUpdate');
        }

        if (Config::get('enso.events.crud.updated', false)) {
            $events->listen(CrudUpdated::class, CrudSubscriber::class . '@onItemUpdated');
        }

        if (Config::get('enso.events.crud.deleted', false)) {
            $events->listen(CrudDeleted::class, CrudSubscriber::class . '@onItemDeleted');
        }
    }
}
