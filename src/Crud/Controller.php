<?php

namespace Yadda\Enso\Crud;

use Alert;
use Carbon\Carbon;
use Enso;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\ValidationException;
use TorMorten\Eventy\Facades\Eventy;
use Yadda\Enso\Crud\Config as CrudConfig;
use Yadda\Enso\Crud\Contracts\Controller as ContractsController;
use Yadda\Enso\Crud\Contracts\CrudFilterContract;
use Yadda\Enso\Crud\Events\CrudBeforeCreate;
use Yadda\Enso\Crud\Events\CrudBeforeUpdate;
use Yadda\Enso\Crud\Events\CrudCreated;
use Yadda\Enso\Crud\Events\CrudDeleted;
use Yadda\Enso\Crud\Events\CrudUpdated;
use Yadda\Enso\Crud\Exceptions\CrudException;
use Yadda\Enso\Crud\Resources\ListResource;
use Yadda\Enso\Crud\Resources\TableResource;
use Yadda\Enso\Crud\Traits\Controller\IsOrderable;
use Yadda\Enso\Crud\Traits\ControlsNestedModels;
use Yadda\Enso\Crud\Traits\HasCrudName;
use Yadda\Enso\Media\Contracts\HasFiles;
use Yadda\Enso\Utilities\Traits\HasAutonomousTraits;
use Yadda\Enso\Utilities\Traits\SharesBodyClasses;

/**
 * Http controller for Ensō CRUD
 *
 * @author Jake Gully <jake@yadda.co.uk>
 * @author Andrew Ellender <andrew@yadda.co.uk>
 */
class Controller extends BaseController implements ContractsController
{
    use AuthorizesRequests,
        ControlsNestedModels,
        DispatchesJobs,
        HasAutonomousTraits,
        HasCrudName,
        IsOrderable,
        SharesBodyClasses,
        ValidatesRequests;

    /**
     * CrudConfig for this CRUD type
     *
     * @var CrudConfig
     */
    protected $config;

    /**
     * The current request
     *
     * @var Request
     */
    protected $request;

    /**
     * Array for determining Crud action permissions
     *
     * @var array
     */
    protected $permissions = [
        'create' => null,
        'read' => null,
        'update' => null,
        'destroy' => null,
    ];

    /**
     * Create a new instance of CrudController
     *
     * @return void
     */
    public function __construct()
    {
        $this->bootIfNotBooted();

        $this->bodyClasses('crud-type-' . $this->getCrudName());

        if (is_null($this->getConfig())) {
            $this->loadConfig();
        }

        $this->configureHooks();

        Enso::setJSData('show_file_in_use_message', config('enso.media.show_file_in_use_message'));

        $this->initializeTraits();
    }

    /**
     * Gets the correct crud action class for the given action name
     *
     * @param string $action_name Action name to resolve class for
     *
     * @return string
     */
    protected function addCrudActionClass($action_name)
    {
        $this->bodyClasses('crud-action-' . $action_name);
    }

    /**
     * Gets the config location for the Config
     *
     * @return void
     */
    protected function getConfigLocation()
    {
        return 'enso.crud.' . $this->getCrudName() . '.config';
    }

    /**
     * Attempt to load a CrudConfig based on this controller name
     *
     * @return CrudConfig
     */
    protected function loadConfig()
    {
        $config_location = $this->getConfigLocation();
        $config_class = config($config_location);

        if (!class_exists($config_class)) {
            throw new Exception(
                'CRUD config class cannot be loaded. Trying to load class: '
                    . var_export($config_class, true) . ' which was found in config: '
                    . var_export($config_location, true) . '.'
            );
        }

        $this->setConfig(new $config_class());

        return $this->getConfig();
    }

    /**
     * Get the CRUD config for this controller
     *
     * @return CrudConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Sets the CRUD config instance on this contorller
     *
     * @param CrudConfig $config Config instance to set
     *
     * @return self
     */
    public function setConfig(CrudConfig $config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Configures the hooks and filters for this controller
     *
     * @return void
     */
    protected function configureHooks()
    {
        //
    }

    /**
     * Alter the items before they are sent back via AJAX
     *
     * @param Model $item Model to pull data from.
     *
     * @return array
     */
    public function transformItems($item)
    {
        $parsed = [];

        foreach ($this->getTransformers() as $transformer) {
            $parsed = call_user_func($transformer, $parsed, $item);
        }

        return $parsed;
    }

    /**
     * Apply any column formatters that have been defined in the Crud config
     *
     * @param array $parsed Array of parsed model data
     * @param Model $item   Model to pull data from.
     *
     * @return array
     */
    public function applyFormatters($parsed, $item)
    {
        $col_names = $this->config->getColumnNames();
        $col_configs = $this->config->getTableColumns();

        foreach ($col_names as $col_name) {
            if (!is_null($col_configs[$col_name]->formatter)) {
                $parsed[$col_name] = ($col_configs[$col_name]->formatter)($item->{$col_name}, $item);
            } else {
                $parsed[$col_name] = $item->{$col_name};
            }
        }

        return $parsed;
    }

    /**
     * Adds index items as defined in the crud
     *
     * @param array $parsed Array of parsed model data
     * @param Model $item   Model to use to compute actions
     *
     * @return array
     */
    public function addIndexActions($parsed, $item)
    {
        $parsed['index_actions'] = $this->config->getIndexActionsForItem($item);

        return $parsed;
    }

    /**
     * If a row classes callback has been set, apply classes to items
     *
     * @param array $parsed The item in its current array form
     * @param Model $item   The original model
     *
     * @return array        $parsed but maybe with extra row_classes applied
     */
    public function setRowClasses($parsed, $item)
    {
        if ($this->config->hasRowClassesCallback()) {
            if (is_callable($this->config->getRowClassesCallback())) {
                $parsed['row_classes'] = call_user_func($this->config->getRowClassesCallback(), $item);
            } elseif (is_string($this->config->getRowClassesCallback())) {
                $parsed['row_classes'] = call_user_func([$item, $this->config->getRowClassesCallback()]);
            } else {
                throw new CrudException('The row_classes callable hasn\'t been configured correctly');
            }
        }

        return $parsed;
    }

    /**
     * Add an Id from an Eloquent model to a subset of data
     *
     * @param array $parsed Parsed items
     * @param Model $item   Item to get Id from
     *
     * @return array
     */
    public function addIds($parsed, $item)
    {
        if (!isset($parsed['id'])) {
            $parsed['id'] = $item->{$item->getKeyName()};
        }

        return $parsed;
    }

    /**
     * Transformer for adding children.
     *
     * This is recursive.
     */
    public function addChildren(array $parsed, Model $item): array
    {
        if (!$this->getConfig()->isNested()) {
            return $parsed;
        }

        $parsed['children'] = [];

        foreach ($item->children as $child) {
            $parsed['children'][] = $this->transformItems($child);
        }

        return $parsed;
    }

    /**
     * A list of transformer callables. Each of these should take an array and
     * an instance of the model. The function should then alter the array in
     * whatever way it likes and then return the array. This array will then be
     * passed to the next transformer, along with the original model.
     *
     * If you are overriding this, you probably want to add your own
     * transformers to the end of the array. The first transformer will recieve
     * an empty array as the first parameter.
     *
     * @return array
     */
    public function getTransformers()
    {
        return [
            [$this, 'applyFormatters'],
            [$this, 'setRowClasses'],
            [$this, 'addIds'],
            [$this, 'addIndexActions'],
        ];
    }

    /**
     * Determines if permissions are available to access this crud route.
     *
     * @param string $action Name of the action
     * @param Model  $item   Model that might be needed for limiting crud route
     *                       availability by specific content
     *
     * @return boolean
     */
    protected function hasPermission($action, $item = null)
    {
        $permission_comparator = $this->permissions[$action];

        if (is_null($permission_comparator)) {
            return true;
        } elseif (is_callable($permission_comparator)) {
            return call_user_func_array(
                $permission_comparator,
                [
                    $this->getConfig(),
                    $item
                ]
            );
        } elseif (is_array($permission_comparator)) {
            return Auth::check()
                && Auth::user()->hasPermissions($permission_comparator);
        } else {
            return Auth::check()
                && Auth::user()->hasPermission($permission_comparator);
        }
    }

    /**
     * Sets a permission for a given action type. Expected permissions are:
     *   a string (permission slug)
     *   an array (array of permission slugs, any of which will allow access)
     *   a callable (should return true|false do inpart permission) and expect two
     *     argument (config, item)
     *
     * @param string $action     Name of action permission to set
     * @param mixed  $permission permission comparisson to set
     *
     * @return self
     */
    public function setPermission($action, $permission)
    {
        if (!in_array($action, array_keys($this->permissions))) {
            throw new CrudException('Invalid Crud action assignement: ' . $action);
        }

        $this->permissions[$action] = $permission;

        return $this;
    }

    /**
     * Sets a multiple permissions comparitors at once. See setPermission for
     * more details on allowed permission arguments.
     *
     * @param array $permissions array of actions => permissions to set
     *
     * @return self
     */
    public function setPermissions(array $permissions)
    {
        $this->permissions = array_merge(
            $this->permissions,
            array_filter(
                $permissions,
                function ($action_name) {
                    return in_array($action_name, array_keys($this->permissions));
                },
                ARRAY_FILTER_USE_KEY
            )
        );

        return $this;
    }

    /**
     * Show a list of CRUD items.
     *
     * @param Request $request Request to base query on
     *
     * @return String
     */
    public function index(Request $request)
    {
        if (!$this->hasPermission('read')) {
            abort(403);
        }

        $this->request = $request;

        if (!$request->ajax() && !$request->has('ajax')) {
            return $this->indexHtml($request);
        }

        try {
            $config = $this->getConfig();

            Eventy::action('crud.index.before', $request, $config);

            $orderby = explode(',', $request->input('orderby', 'order'));

            $args = [
                'orderby' => $orderby,
                'order'   => $request->input('order', 'desc'),
                'search'  => $request->input('search', null),
            ];

            $args = Eventy::filter('crud.index.overrideArgs', $args);

            $query = $this->getIndexQuery($args);

            $query = $this->applyFilters($query, $request->get('filters', []));

            $total = $query->count();

            $items = $this
                ->doPagination($query, $request)
                ->get();

            if ($request->has('table')) {
                $items->transform([$this, 'transformItems']);
            }

            Eventy::action(
                'crud.index.beforeAjax',
                $request,
                $this->getConfig(),
                $items
            );
        } catch (CrudException $e) {
            Log::error($e->getMessage());

            return response()->json(
                [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ],
                500
            );
        } catch (Exception $e) {
            Log::error($e);

            return response()->json(
                [
                    'status'  => 'error',
                    'message' => config('app.debug')
                        ? $e->getMessage()
                        : 'An unexpected error occured attempting to complete this action',
                ],
                500
            );
        }

        if ($request->has('table')) {
            return [
                'status' => 'success',
                'data' => [
                    'total' => $total,
                    'items' => $this->makeTableCollection($items),
                ],
            ];
        } else {
            return $this->makeListCollection($items);
        }
    }

    /**
     * Return the non-AJAX index route
     *
     * @param Request $request Request to base query on
     *
     * @return View
     */
    protected function indexHtml(Request $request)
    {
        $this->addCrudActionClass('index');

        $config = $this->getConfig();

        Eventy::action('crud.index.beforeRender', $request, $config);

        $view_name = $config->getCrudView('index');

        Enso::setJSData('crud', $this->getConfig()->getJSConfig());

        return view(
            $view_name,
            [
                'crud' => $config
            ]
        );
    }

    /**
     * Make the query for the index page
     *
     * @param array $args Argument set to apply to query
     *
     * @return Builder
     */
    protected function getIndexQuery($args)
    {
        $query = call_user_func($this->getConfig()->getModel() . '::query');

        $query->withoutGlobalScopes();
        $query = Eventy::filter('crud.index.query', $query);

        if (!is_null($args['search'])) {
            $query = $this->doSearch($query, $args['search']);
        }

        $query = $this->preloadRelationships($query);
        $query = $this->applyOrderScopes($query, $args['orderby']);

        if ($this->config->getJoinsCallback()) {
            $query = ($this->config->getJoinsCallback())($query);
        }

        for ($i = 0; $i < count($args['orderby']); $i++) {
            $query->orderBy($args['orderby'][$i], $args['order']);
        }

        return $query;
    }

    /**
     * Apply pagination to a query
     *
     * @param Builder $query   Initial query
     * @param Request $request Request to base query on
     *
     * @return Builder $query
     */
    protected function doPagination(Builder $query, Request $request)
    {
        if ($this->getConfig()->getPaginate()) {
            $per_page = $request->input('per_page', 25);
            $page = $request->input('page', 1);
            $offset = ($page - 1) * $per_page;

            return $query->offset($offset)->limit($per_page);
        }

        return $query;
    }

    /**
     * Preload relationships if needed
     *
     * @param Builder $query Initial query
     *
     * @return Builder
     */
    protected function preloadRelationships(Builder $query)
    {
        if (!empty($this->getConfig()->getPreloadRelationships())) {
            $query->with($this->getConfig()->getPreloadRelationships());
        }

        return $query;
    }

    /**
     * Apply scopes for when ordering
     *
     * @param Builder $query   Initial query
     * @param array   $orderby List of order scopes to apply
     *
     * @return Builder
     */
    protected function applyOrderScopes(Builder $query, $orderby)
    {
        $scopes = $this->getConfig()->getOrderColumnScopes();

        foreach ($orderby as $o) {
            if (array_key_exists($o, $scopes)) {
                $query = call_user_func($scopes[$o], $query);
            }
        }

        return $query;
    }

    /**
     * Apply a search term to a query
     *
     * @param Builder $query       Initial Query
     * @param String  $search_term String to search with
     *
     * @return Builder
     */
    protected function doSearch(Builder $query, $search_term)
    {
        $search_cols = $this->getConfig()->getSearchColumns();

        if ($this->config->getSearchJoinsCallback()) {
            $query = ($this->config->getSearchJoinsCallback())($query);
        }

        $query->where(
            function ($query) use ($search_cols, $search_term) {
                foreach ($search_cols as $col) {
                    $query->orWhere($col, 'LIKE', '%' . $search_term . '%');
                }
            }
        );

        return $query;
    }

    /**
     * Display the create form
     *
     * @param Request $request Request to base query on
     *
     * @return View
     */
    public function create(Request $request)
    {
        if (!$this->hasPermission('create')) {
            abort(403);
        }

        $this->addCrudActionClass('create');

        $item = $this->getConfig()->newModelInstance();

        $this->getConfig()
            ->getCreateForm()
            ->setModelInstance($item);

        $form_view = Eventy::filter(
            'crud.create.view',
            $this->getConfig()->getCrudView('forms.form'),
            $this->getConfig(),
            $request
        );

        Eventy::action('crud.create', $this->getConfig(), $request);

        $this->setJSCreateData();

        return view(
            $this->getConfig()->getCrudView('create'),
            [
                'crud'      => $this->config,
                'form_view' => $form_view,
                'item'      => $item,
            ]
        );
    }

    /**
     * Sets the JS Data for a create page.
     *
     * @return void
     */
    protected function setJSCreateData()
    {
        Enso::setJSData('crud', $this->getConfig()->getJSConfig());
        Enso::setJSData('crud.form', $this->getConfig()->getJsCreateForm());
        Enso::setJSData('crud.item', $this->getConfig()->getCreateForm()->getFormData());
    }

    /**
     * Duplicate one adventure and redirect to it's edit page
     *
     * @param Request $request
     * @param mixed   $id
     *
     * @return RedirectResponse
     */
    public function clone(Request $request, $id): RedirectResponse
    {
        $config = $this->getConfig();

        $item = $this->getItem($id);

        if (!$config->allowCloning($item)) {
            abort(404);
        }

        $clone = $this->cloneItem($item);

        Eventy::action('crud.clone.replciate-item', $this->getConfig(), $request, $clone);

        $clone->save();

        $this->replicateRelations($item, $clone);

        Eventy::action('crud.clone.replciate-relations', $this->getConfig(), $request, $item, $clone);

        return Redirect::route($config->getRoute('edit'), $clone->getKey());
    }

    /**
     * Clone an item
     *
     * @param Model $item
     *
     * @return Model
     */
    protected function cloneItem(Model $item): Model
    {
        return $item->replicate();
    }

    /**
     * Copy any relationship that need
     *
     * @param Model $item
     *
     * @return void
     */
    protected function replicateRelations(Model $item, Model $clone): void
    {
        return;
    }

    /**
     * Store a new item in the database
     *
     * @param Request $request Request to base action on.
     *
     * @return Response|array JSON string if AJAX, else redirect
     */
    public function store(Request $request)
    {
        try {
            if (!$this->hasPermission('create')) {
                abort(403);
            }

            event(new CrudBeforeCreate($this->getConfig()->getModel()));

            $item = $this->getConfig()->newModelInstance();

            $this->getConfig()
                ->getCreateForm()
                ->setModelInstance($item);

            Eventy::action('crud.store.before-validation', $this->getConfig(), $request);

            $this->validate(
                $request,
                Eventy::filter('crud.store.rules', $this->getConfig()->getCreateRules(), $this->getConfig(), $request),
                Eventy::filter('crud.store.messages', $this->getConfig()->getMessages(), $this->getConfig(), $request),
                $this->getConfig()->getCreateForm()->getFieldNames()
            );

            $data = Eventy::filter('crud.store.data', $request->all(), $this->getConfig(), $request);

            Eventy::action('crud.store.before', $this->getConfig(), $request);

            $this->performCreate($data);

            /**
             * @todo - Extract to Nestable Trait
             */
            if ($this->getConfig()->isNested()) {
                $this->applyNesting($item, $request);
            }

            Eventy::action('crud.store.after', $this->getConfig(), $request);

            event(new CrudCreated($item));
        } catch (CrudException $e) {
            if ($request->ajax()) {
                return response()->json(
                    [
                        'status'   => 'error',
                        'message'  => $e->getMessage(),
                    ],
                    500
                );
            } else {
                return redirect()->back()->withInput();
            }
        } catch (ValidationException $e) {
            throw $e;
        } catch (MassAssignmentException $e) {
            Log::error($e->getMessage());

            if ($request->ajax()) {
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'The field ' . $e->getMessage() . ' is not fillable.',
                    ],
                    500
                );
            } else {
                return redirect()->back()->withInput();
            }
        } catch (Exception $e) {
            Log::error($e);

            if ($request->ajax()) {
                return response()->json(
                    [
                        'status'   => 'error',
                        'message'  => 'An unexpected error occured attempting to complete this action',
                    ],
                    500
                );
            } else {
                return redirect()->back()->withInput();
            }
        }

        if ($request->ajax()) {
            return Eventy::filter(
                'crud.store.xhr-success',
                [
                    'status' => 'success',
                    'data' => [],
                    'buttons' => $this->getUpdateButtons($item),
                ],
                $this->getConfig(),
                $request
            );
        } else {
            return $this->getConfig()
                ->getRedirect('store')
                ->with(
                    'success',
                    $this->getConfig()->getNameSingular() . ' was created successfully.'
                );
        }
    }

    /**
     * Permforms that actual create functionality using provided data.
     *
     * @param array $data Data to apply
     *
     * @return void
     */
    protected function performCreate($data)
    {
        $this->getConfig()
            ->getCreateForm()
            ->applyRequestData($data, [$this, 'beforeSaveCallback'])
            ->saveModelInstance()
            ->applyRequestDataAfterSave($data, [$this, 'afterSaveCallback'])
            ->saveModelInstance()
            ->getModelInstance()
            ->refresh();
    }

    /**
     * A callback for after an model has been populated with new data but before it is saved
     */
    public function beforeSaveCallback(Model $model, array $data, string $action): Model
    {
        return $model;
    }

    /**
     * A callback for after an model has been populated with all new data and has been saved
     *
     * Note: the model will be saved again after this
     */
    public function afterSaveCallback(Model $model, array $data, string $action): Model
    {
        return $model;
    }

    /**
     * Returns an array of button definitions to return to with an ajax response
     * to a store request to populate the actions list on success notification.
     *
     * @param Model $item Item to use to decided which buttons are applicable
     *
     * @return array
     */
    protected function getStoreButtons($item)
    {
        return [
            [
                'label' => 'Close',
                'url' => route(
                    $this->getConfig()->getRoute('edit'),
                    $item->getKey()
                ),
                'class' => 'is-primary is-outlined',
                'icon' => 'fa fa-close',
            ],
            [
                'label' => 'Add New',
                'url' => route($this->getConfig()->getRoute('create')),
                'class' => 'is-success',
                'icon' => 'fa fa-plus',
            ],
            [
                'label' => 'Finish',
                'url' => route($this->getConfig()->getRoute('index')),
                'class' => 'is-info',
                'icon' => 'fa fa-check',
            ],
        ];
    }

    /**
     * Perform a response request and return the necessary data for a
     * regular edit request response and for a post-update response
     *
     * @param Request $request
     * @param Integer $id
     *
     * @return Array
     */
    public function makeEditResponseData(Request $request, $id)
    {
        $this->addCrudActionClass('edit');

        $item = $this->getItemById($id);

        if (!$this->hasPermission('update', $item)) {
            abort(403);
        }

        $this->getConfig()->getEditForm()->setModelInstance($item);

        $form_view = Eventy::filter(
            'crud.edit.view',
            $this->getConfig()->getCrudView('forms.form'),
            $this->getConfig(),
            $request
        );

        Eventy::action('crud.edit', $this->getConfig(), $request);

        $this->setJSEditData($request);

        return [
            'item'      => $item,
            'crud'      => $this->getConfig(),
            'action'    => $this->getUpdateRoute($item),
            'form_view' => $form_view,
            'view'      => $this->getConfig()->getCrudView('edit'),
        ];
    }

    /**
     * Show the edit form
     *
     * @param Request $request Request to base action on.
     * @param integer $id      id of the Model to find
     *
     * @return View
     */
    public function edit(Request $request, $id)
    {
        $response_data = $this->makeEditResponseData($request, $id);

        return view(
            $response_data['view'],
            [
                'form_view' => $response_data['form_view'],
                'item' => $response_data['item'],
                'crud' => $response_data['crud'],
                'action' => $response_data['action'],
            ]
        );
    }

    /**
     * Sets the JS Data for an edit page.
     *
     * @param Request $request
     *
     * @return void
     */
    protected function setJSEditData(Request $request)
    {
        Enso::setJSData('crud', $this->getConfig()->getJSConfig());

        Enso::setJSData(
            'crud.form',
            $this->getConfig()->getJsEditForm()
        );

        Enso::setJSData(
            'crud.item',
            $this->getConfig()->getEditForm()->getFormData()
        );
    }

    /**
     * Gets the route for updating a CRUD item
     *
     * @param Model $item Item to get route for
     *
     * @return string url for action
     */
    protected function getUpdateRoute($item)
    {
        return route($this->getConfig()->getRoute() . '.update', $item->id);
    }

    /**
     * Get an item to be updated
     *
     * @param int $id identifier of the item to find
     *
     * @return Model
     */
    protected function getItem($id)
    {
        return $this->getItemById($id);
    }

    /**
     * Update an item with the given ID using the details in the request
     *
     * @param Request $request Request to base action on.
     * @param int     $id      Identifier of the item to update
     *
     * @return Response|array
     */
    public function update(Request $request, $id)
    {
        try {
            $item = $this->getItem($id);

            if (!$this->hasPermission('update', $item)) {
                abort(403);
            }

            event(new CrudBeforeUpdate($item));

            $this->getConfig()
                ->getEditForm()
                ->setModelInstance($item);

            Eventy::action('crud.update.before-validation', $this->getConfig(), $request);

            $this->validate(
                $request,
                Eventy::filter('crud.update.rules', $this->getConfig()->getEditRules(), $this->getConfig(), $request),
                Eventy::filter('crud.update.messages', $this->getConfig()->getMessages(), $this->getConfig(), $request),
                $this->getConfig()->getEditForm()->getFieldNames()
            );

            $data = Eventy::filter('crud.update.data', $request->all(), $this->getConfig(), $request);

            Eventy::action('crud.update.before', $this->getConfig(), $request);

            $this->performUpdate($data);

            if ($this->getConfig()->isNested()) {
                $item = $this->applyNesting($item, $request);
            }

            Eventy::action('crud.update.after', $this->getConfig(), $request);

            event(new CrudUpdated($item));
        } catch (CrudException $e) {
            if ($request->ajax()) {
                return response()->json(
                    [
                        'status'   => 'error',
                        'message'  => $e->getMessage(),
                    ],
                    500
                );
            } else {
                return redirect()->back()->withInput();
            }
        } catch (ValidationException $e) {
            throw $e;
        } catch (MassAssignmentException $e) {
            Log::error($e->getMessage());

            if ($request->ajax()) {
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'The field `' . $e->getMessage() . '` is not fillable.',
                    ],
                    500
                );
            } else {
                return redirect()->back()->withInput();
            }
        } catch (Exception $e) {
            Log::error($e);

            if ($request->ajax()) {
                return response()->json(
                    [
                        'status'   => 'error',
                        'message'  => 'An unexpected error occured attempting to complete this action',
                    ],
                    500
                );
            } else {
                return redirect()->back()->withInput();
            }
        }

        $response_data = $this->makeEditResponseData($request, $id);

        if ($request->ajax()) {
            return Eventy::filter(
                'crud.update.xhr-success',
                [
                    'status' => 'success',
                    'data' => [
                        'item' => $response_data['crud']->getEditForm()->getFormData(),
                        'form' => $response_data['crud']->getJsEditForm(),
                    ],
                    'buttons' => $this->getUpdateButtons($item),
                ],
                $this->getConfig(),
                $request
            );
        } else {
            return $this->getConfig()
                ->getRedirect('update')
                ->with('success', $this->getConfig()->getNameSingular() . ' was updated successfully.');
        }
    }

    /**
     * Permforms that actual update functionality using provided data.
     *
     * @param array $data Data to perform update with
     *
     * @return void
     */
    protected function performUpdate($data)
    {
        $form = $this->getConfig()->getEditForm();
        $item = $form->getModelInstance();

        if ($item instanceof HasFiles) {
            Log::debug('Detaching all files...');
            $item->detachAllFiles();
        }

        $form->applyRequestData($data, [$this, 'beforeSaveCallback'])
            ->saveModelInstance()
            ->applyRequestDataAfterSave($data, [$this, 'afterSaveCallback'])
            ->saveModelInstance()
            ->getModelInstance()
            ->refresh();
    }

    /**
     * Returns an array of button definitions to return to with an ajax response
     * to an update request to populate the actions list on success notification.
     *
     * @param Model $item Item to determine which buttons to provide for
     *
     * @return array
     */
    protected function getUpdateButtons($item)
    {
        return $this->getStoreButtons($item);
    }

    /**
     * Delete the given item from the database
     *
     * @param Request $request Request to base action on.
     * @param Integer $id      Identifier of the item to delete
     *
     * @return Response JSON if AJAX request, else Redirect
     */
    public function destroy(Request $request, $id)
    {
        if (!$this->getConfig()->getDeletable()) {
            $message = 'You cannot delete ' . $this->getConfig()->getNamePlural() . '.';
            if ($request->ajax()) {
                return [
                    'status' => 'error',
                    'data' => null,
                    'message' => $message
                ];
            } else {
                Alert::error($message);
                \App::abort(302, '', ['Location' => route('admin.departures.index')]);
            }
        }

        $item = $this->getItemById($id);

        if (!$this->hasPermission('destroy', $item)) {
            abort(403);
        }

        event(new CrudDeleted($item));

        Eventy::action('crud.destroy.before', $this->getConfig(), $request, $item);

        $item->delete();

        Eventy::action('crud.destroy.after', $this->getConfig(), $request);

        if ($request->ajax()) {
            return Eventy::filter(
                'crud.delete.xhr-success',
                [
                    'status' => 'success',
                    'data'   => null,
                ],
                $this->getConfig(),
                $request
            );
        }

        Alert::success($this->getConfig()->getNameSingular() . ' has been deleted.');

        return $this->config
            ->getRedirect('destroy')
            ->with('success', $this->getConfig()->getNameSingular() . ' has been deleted.');
    }

    /**
     * Get an item by its id
     *
     * @param integer $id Identifier of item to find
     *
     * @return Model
     */
    public function getItemById($id)
    {
        $model = $this->getConfig()->getModel();

        return $model::query()->withoutGlobalScopes()->findOrFail($id);
    }

    /**
     * Applies a set of filters to a Builder query
     *
     * @param Builder $query   Initial query state
     * @param array   $filters Filters to apply
     *
     * @return Builder
     */
    protected function applyFilters($query, $filters)
    {
        $config = $this->getConfig();

        /**
         * NOTE: When coming to the index page via a GET request, this will
         * be a json eoncoded string.
         *
         * The second argument forces Objects to be parsed as associateive arrays
         */
        if (is_string($filters)) {
            $filters = json_decode($filters, true);
        }

        // If we've reached this point and don't have an array, there's not really any
        // hope for automatically fixing it. As such, the best we can do is just not
        // apply the filters?
        if (!is_array($filters)) {
            return $query;
        }

        foreach ($config->filters() as $name => $config_filter) {
            $filter_value = Arr::get($filters, $name, null);

            if (is_object($config_filter) && $config_filter instanceof CrudFilterContract) {
                /**
                 * First, attempt to filter using the new Filters classes, which
                 * provides a callable which represents how it should modify the
                 * query.
                 */
                $config_filter->callable($query, $filter_value);
            } elseif (isset($config_filter['callable'])) {
                /**
                 * Next, check if a filter array has a callable to apply.
                 */
                $config_filter['callable']($query, $filter_value);
            } else {
                /**
                 * Otherwise, fall back to basic filtering logic based on type
                 */
                if (is_null($filter_value)) {
                    continue;
                }

                switch (Arr::get(
                    $config_filter,
                    'type',
                    'text'
                )) {
                    case 'select':
                        $item_key = Arr::get($config_filter, 'props.settings.track_by', 'id');
                        $item_value = Arr::get($filter_value, $item_key);
                        if (!empty($item_value)) {
                            $query->where($name, $item_value);
                        }
                        break;
                    case 'checkbox':
                        foreach ($filter_value ?? [] as $column => $value) {
                            $query->where($column, !empty($value));
                        }
                        break;
                    case 'date':
                        try {
                            $date = Carbon::createFromFormat(
                                'Y-m-d H:i:s.u',
                                Arr::get($filter_value, 'date'),
                                Arr::get($filter_value, 'timezone')
                            );

                            // Default will be to include the provided date.
                            $date->setTime(0, 0, 0);

                            $query->where($name, '>=', $date);
                        } catch (Exception $e) {
                            // Unable to parse date...
                            // Will just leave query as is.
                        }
                        break;
                    case 'text':
                    default:
                        if (strlen($filter_value) > 0) {
                            $query->where($name, 'LIKE', '%' . $filter_value . '%');
                        }
                }
            }
        }

        return $query;
    }

    /**
     * ResourceCollection representing models as data required to fill select
     * lists
     *
     * @param Collection $items
     *
     * @return ResourceCollection
     */
    public function makeListCollection(Collection $items): ResourceCollection
    {
        return ListResource::collection($items);
    }

    /**
     * ResourceCollection representing models as data required to fill table
     * rows on a CRUD index table
     *
     * @param Collection $items
     *
     * @return ResourceCollection
     */
    public function makeTableCollection(Collection $items): ResourceCollection
    {
        return TableResource::collection($items);
    }
}
