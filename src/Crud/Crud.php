<?php

namespace Yadda\Enso\Crud;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config as ConfigFacade;
use Illuminate\Support\Facades\Route;
use ReflectionClass;
use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Contracts\Model\IsPublishable;
use Yadda\Enso\Crud\Exceptions\InvalidCrudClassException;
use Yadda\Enso\Media\Traits\HasFilesTrait;
use Yadda\Enso\Utilities\Helpers;

class Crud
{
    /**
     * Crud name list for determining crud names from object classes
     *
     * @var array
     */
    protected $crud_name_list = [];

    /**
     * Create a new instance of a crud config for a given crud name
     *
     * @param string
     *
     * @return Config
     */
    public function config($crud_name)
    {
        $class_name = $this->configClass($crud_name);

        if (empty($class_name)) {
            throw new InvalidCrudClassException('Config could not be found for crud name : [' . (string) $crud_name . ']');
        }

        return new $class_name;
    }

    /**
     * Get the config class for a given crud name
     *
     * @param string $name
     *
     * @return string
     */
    public function configClass($crud_name)
    {
        return ConfigFacade::get('enso.crud.' . $crud_name . '.config');
    }

    /**
     * Get the controller class for a given crud name. Can be optionally set to
     * ensure that a preceding slash is added if it isn't already present.
     *
     * @param string $name
     * @param bool   $force_preceding_slash
     *
     * @return string
     */
    public function controllerClass(string $crud_name, bool $force_preceding_slash = false)
    {
        $class = ConfigFacade::get('enso.crud.' . $crud_name . '.controller');

        if ($force_preceding_slash === true) {
            $class = '\\' . trim($class, '\\');
        }

        return $class;
    }

    /**
     * Determines the crud name of a given object instance or class string.
     *
     * @throws InvalidCrudClassException
     *
     * @param object|string $crud
     *
     * @return string
     */
    public function crudName($crud): string
    {
        $class = is_object($crud) ? get_class($crud) : $crud;

        if ($this->getCrudNameList()->has($class)) {
            return $this->getCrudNameList()->get($class);
        }

        throw new InvalidCrudClassException('Unable to determine crud name of : [' . (string) $class . ']');
    }

    /**
     * Add routes for a Crud type
     *
     * @todo Once Enso has been updated to properly handle route-model binding,
     *       we can re-enable the Route::resource() call, so have left the
     *       $resource_prefix command commented for now.
     *
     * @deprecated
     * @throws InvalidCrudClassException
     * @param  string $path
     * @param  string $class
     * @return void
     */
    public function crudRoutes($path, $crud_name, $name)
    {
        // $resource_prefix = $this->getResourcePrefix($name);

        $controller_class = $this->controllerClass($crud_name, true);
        if (empty($controller_class)) {
            throw new InvalidCrudClassException(
                'An empty controller class was provided to Crud::routes. Path: ' . (string) $path
            );
        }

        $model_class = $this->modelClass($crud_name);
        if (empty($model_class)) {
            throw new InvalidCrudClassException(
                'An empty model class was provided to Crud::routes. Path: ' . (string) $path
            );
        }

        if (is_subclass_of($model_class, IsPublishable::class)) {
            Route::post($path . '/publish', $controller_class . '@publish')->name($name . '.publish');
            Route::post($path . '/unpublish', $controller_class . '@unpublish')->name($name . '.unpublish');
        }

        Route::get($path, $controller_class . '@index')->name($name . '.index');
        Route::post($path, $controller_class . '@store')->name($name . '.store');
        Route::get($path . '/create', $controller_class . '@create')->name($name . '.create');
        Route::get($path . '/tree', $controller_class . '@tree')->name($name . '.tree');
        Route::get($path . '/tree-data', $controller_class . '@treeData')->name($name . '.tree-data');
        Route::post($path . '/{id}/rearrange', $controller_class . '@rearrange')->name($name . '.rearrange');
        Route::delete($path . '/{id}', $controller_class . '@destroy')->name($name . '.destroy');
        Route::patch($path . '/{id}', $controller_class . '@update')->name($name . '.update');
        Route::get($path . '/{id}', $controller_class . '@show')->name($name . '.show');
        Route::get($path . '/{id}/clone', $controller_class . '@clone')->name($name . '.clone');
        Route::get($path . '/{id}/edit', $controller_class . '@edit')->name($name . '.edit');
        Route::post($path . '/{id}/reorder', $controller_class . '@reorder')->name($name . '.reorder');
    }

    /**
     * Undocumented function
     *
     * @return Collection
     */
    protected function getCrudNameList(): Collection
    {
        if (empty($this->crud_name_list)) {
            if (ConfigFacade::get('enso.settings.cache_crud_name_list', true)) {
                $this->crud_name_list = Cache::rememberForever('enso-crud-name-list', function () {
                    return $this->generateCrudNameList();
                });
            } else {
                $this->crud_name_list = $this->generateCrudNameList();
            }
        }

        return $this->crud_name_list;
    }

    /**
     * A resource controller generates names for resources based on the last section of the
     * URI that is generated e.g. 'admin/mailer/campaigns' => 'campaigns'
     *
     * The name that we pass in to prefix the reorder route will cannot be used as the 'as' option
     * in the resource controller, because it will have it's last section duplicated.
     * e.g. $name = 'admin.campaigns' => Resource creates 'admin.campaigns.campaigns.index'
     *
     * This function is the result, which trims the last section (which is the part that gives
     * the overlap) and returns that.
     *
     * @param string $name
     *
     * @return string
     */
    protected function getResourcePrefix($name)
    {
        $parts = explode('.', $name);
        $x = array_pop($parts);
        return implode('.', $parts);
    }

    /**
     * Generates ans stashes the crud name list based on config values.
     *
     * @todo - Classes with a preceding slash have been deprecated, but this code
     *         accommodates them by automatically removing it. Code can be
     *         simplified when we no longer want to support this functionality
     *
     * @return Collection
     */
    protected function generateCrudNameList(): Collection
    {
        return collect(ConfigFacade::get('enso.crud'))->flatMap(function ($value, $key) {
            $results = [];

            foreach (['controller', 'model', 'config'] as $selection) {
                if ($binding = Arr::get($value, $selection)) {
                    $results[Helpers::getConcreteClass(trim($binding, '\\'))] = $key;
                }
            }

            return $results;
        });
    }

    /**
     * A Check to see whether to use the files() relationship
     * to attempt to track relationships that files have with other
     * models.
     *
     * @return boolean
     */
    public function maintainFileList($class)
    {
        $has_trait = in_array(
            HasFilesTrait::class,
            array_keys((new ReflectionClass($class))->getTraits())
        );

        return ConfigFacade::get('enso.media.maintain_file_list', true) && $has_trait;
    }

    /**
     * Get the model class for a given crud name
     *
     * @param string $name
     * @return string
     */
    public function modelClass($crud_name)
    {
        return ConfigFacade::get('enso.crud.' . $crud_name . '.model');
    }

    /**
     * Get a query builder for the given crud type
     */
    public function query(string $crud_name): Builder
    {
        return ($this->modelClass($crud_name))::query();
    }

    /**
     * Add routes for a Crud type
     *
     * This is deprecated. crudRoutes() should be used instead.
     *
     * @deprecated
     * @throws InvalidCrudClassException
     * @param  string $path
     * @param  string $class
     * @return void
     */
    public function routes($path, $class, $name)
    {
        if (empty($class)) {
            throw new InvalidCrudClassException('An empty controller class was provided to Crud::routes. Path: ' . (string) $path);
        }

        Route::get($path, $class . '@index')->name($name . '.index');
        Route::post($path, $class . '@store')->name($name . '.store');
        Route::get($path . '/create', $class . '@create')->name($name . '.create');
        Route::get($path . '/tree', $class . '@tree')->name($name . '.tree');
        Route::get($path . '/tree-data', $class . '@treeData')->name($name . '.tree-data');
        Route::post($path . '/{id}/rearrange', $class . '@rearrange')->name($name . '.rearrange');
        Route::delete($path . '/{id}', $class . '@destroy')->name($name . '.destroy');
        Route::patch($path . '/{id}', $class . '@update')->name($name . '.update');
        Route::get($path . '/{id}', $class . '@show')->name($name . '.show');
        Route::get($path . '/{id}/clone', $class . '@clone')->name($name . '.clone');
        Route::get($path . '/{id}/edit', $class . '@edit')->name($name . '.edit');
        Route::post($path . '/{id}/reorder', $class . '@reorder')->name($name . '.reorder');
    }
}
