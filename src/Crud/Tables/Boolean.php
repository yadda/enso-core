<?php

namespace Yadda\Enso\Crud\Tables;

use Exception;

class Boolean extends Column implements ColumnInterface
{
    const DISPLAY_TYPES = [
        'check-cross',
        'boolean',
        'yes-no',
    ];

    protected $component = 'enso-cell-boolean';

    protected $display_type = 'check-cross';

    /**
     * Helper function for basic setting / getting of displayType
     * value/
     *
     * @param string|null $argument
     * @return string|self
     */
    public function displayType($argument = null)
    {
        if (is_null($argument)) {
            return $this->getDisplayType();
        }

        return $this->setDisplayType($argument);
    }

    /**
     * Sets the Display Type
     *
     * @param string $display_type
     *
     * @return self
     */
    public function setDisplayType($display_type)
    {
        if (!in_array($display_type, self::DISPLAY_TYPES)) {
            throw new Exception('Invalid Display Type set');
        }

        $this->display_type = $display_type;

        return $this;
    }

    /**
     * Gets the current Display Type
     *
     * @return string
     */
    public function getDisplayType()
    {
        return $this->display_type;
    }

    /**
     * Gets Cell properties set on this column
     *
     * @return array
     */
    public function getProps(): Array
    {
        return array_merge($this->cell_props, ['display-type' => $this->getDisplayType()]);
    }
}
