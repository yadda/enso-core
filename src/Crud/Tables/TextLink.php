<?php

namespace Yadda\Enso\Crud\Tables;

class TextLink extends Column implements ColumnInterface
{
    /**
     * Name of the Vue component to use to render this cell
     *
     * @var String
     */
    protected $component = 'enso-cell-text-link';

    /**
     * The target attribute for the <a> element
     *
     * @var String
     */
    protected $target;

    /**
     * Set the target attribute for the <a> element
     *
     * @param  String $target
     * @return self
     */
    public function setTarget($target)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get the target attribute for the <a> element
     *
     * @return String
     */
    public function getTarget()
    {
        return $this->target;
    }
}
