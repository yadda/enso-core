<?php

namespace Yadda\Enso\Crud\Tables;

class Thumbnail extends Column implements ColumnInterface
{
    protected $component = 'enso-cell-thumbnail';
}
