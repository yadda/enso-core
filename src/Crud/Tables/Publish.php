<?php

namespace Yadda\Enso\Crud\Tables;

use Yadda\Enso\Crud\Config;

class Publish extends Column implements ColumnInterface
{
    /**
     * Vue component to render this cell
     *
     * @var string
     */
    protected $component = 'enso-cell-publish';

    /**
     * Route to publish item
     *
     * @var string
     */
    protected $publish_route;

    /**
     * Route to unpublish item
     *
     * @var string
     */
    protected $unpublish_route;

    /**
     * Model attribute that marks the model as published or not
     *
     * Note: This may take into account a publish_at date
     *
     * @var string
     */
    protected $published_attribute = 'is_published';

    /**
     * Name of identifying attribute
     *
     * @var string
     */
    protected $key_name = 'id';

    /**
     * Create a new Publish
     */
    public function __construct(string $name, ?Config $config = null)
    {
        parent::__construct($name, $config);

        if ($config) {
            $this->publish_route = $config->getRoute('publish');
            $this->unpublish_route = $config->getRoute('unpublish');
        }
    }

    /**
     * Get or set the publish route
     */
    public function publishRoute(?string $route = null)
    {
        if (is_null($route)) {
            return $this->publish_route;
        } else {
            $this->publish_route = $route;

            return $this;
        }
    }

    /**
     * Get or set the unpublish route
     */
    public function unpublishRoute(?string $route = null)
    {
        if (is_null($route)) {
            return $this->unpublish_route;
        } else {
            $this->unpublish_route = $route;

            return $this;
        }
    }

    /**
     * Get the content of the column to display in each row
     */
    public function getContent($item, Config $crud): string
    {
        return $item->getKey();
    }

    /**
     * Get or set the name of the key of the item. E.g. 'id'
     */
    public function keyName(?string $name = null)
    {
        if (is_null($name)) {
            return $this->key_name;
        } else {
            $this->key_name = $name;

            return $this;
        }
    }

    /**
     * Get or set the name of the 'published' attribute
     */
    public function publishedAttribute(?string $attribute = null)
    {
        if (is_null($attribute)) {
            return $this->published_attribute;
        } else {
            $this->published_attribute = $attribute;

            return $this;
        }
    }

    /**
     * Get Cell properties set on this attribute
     */
    public function getProps(): array
    {
        return array_merge(
            $this->cell_props,
            [
                'publish-url' => route($this->publish_route),
                'unpublish-url' => route($this->unpublish_route),
                'published-attribute' => $this->published_attribute,
                'key-name' => $this->key_name,
            ]
        );
    }
}
