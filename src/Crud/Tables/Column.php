<?php

namespace Yadda\Enso\Crud\Tables;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Yadda\Enso\Crud\Config;

/**
 * A CRUD index page column
 */
abstract class Column implements ColumnInterface
{
    /**
     * The name of the column.
     *
     * Corresponds to an attribute on the model. This doesn't have to be a
     * database column, it can be an attribute accessor method.
     *
     * @var string
     */
    protected $name;

    /**
     * Label for the column heading
     *
     * @var string
     */
    protected $label;

    /**
     * Name of vue component to display this column
     *
     * @var string
     */
    protected $component;

    /**
     * Column to order by when requesting data. Defaults to $name.
     *
     * @var string
     */
    protected $orderable_column;

    /**
     * Reference to the Crud Config, to allow more complex calculations
     * and access to more data.
     *
     * @var Config|null
     */
    protected $config = null;

    /**
     * Properties to pass through to be bound on the Vue component
     *
     * @var Array
     */
    protected $cell_props = [];

    /**
     * A callback to format this column's data
     *
     * @var Callable
     */
    public $formatter;

    /**
     * Classes to be applied to the TH element
     *
     * @var Array
     */
    public $classes = [];

    /**
     * Create a new Column
     *
     * @param string $name   name of this column
     * @param mixed  $config Crud config
     */
    public function __construct(string $name, ?Config $config = null)
    {
        $this->setName($name);
        $this->orderable_column = $name;
        $this->config = $config;
    }

    /**
     * Create a new Column
     *
     * @param string $name
     * @param array  $config
     *
     * @return Column
     */
    public static function make(string $name, $config = null)
    {
        return App::make(static::class, [
            'name' => $name,
            'config' => $config
        ]);
    }

    /**
     * Set the config for the column
     *
     * @param Config $config Cruds config for this column
     *
     * @return self
     */
    public function setConfig(Config $config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Get the config for the column.
     *
     * @return Config
     */
    public function getConfig(): Config
    {
        return $this->config;
    }

    /**
     * Set the name of the column
     *
     * @param string $name Name of the column
     *
     * @return self
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the name of the column.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set the label for the column heading
     *
     * @param string $label Label for the column
     *
     * @return self
     */
    public function setLabel(string $label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get the label for the column heading
     *
     * @return string
     */
    public function getLabel(): string
    {
        if (is_null($this->label)) {
            $this->setLabel(Str::title(preg_replace('/_/', ' ', $this->getName())));
        }

        return $this->label;
    }

    /**
     * Get the content of the column to display in each row
     *
     * @param  Mixed   The item to render content for
     * @param  Config  The crud configuration for this item type
     * @return string
     */
    public function getContent($item, Config $crud): string
    {
        return $item->{$this->getName()};
    }

    /**
     * Get the name of Vue component to use
     *
     * @return string
     */
    public function getComponent()
    {
        return $this->component;
    }

    /**
     * Get the database column to order by when sorting data by this column
     *
     * @return Mixed Database column name or null
     */
    public function getOrderable(): ?string
    {
        return $this->orderable_column;
    }

    /**
     * Set the db column to order by when sorting data by this column.
     *
     * If $column is set to null then the data won't be orderable by this column.
     *
     * @param  string $column
     * @return self
     */
    public function orderableBy($column)
    {
        $this->orderable_column = $column;

        return $this;
    }

    /**
     * Set a callback for formatting output
     *
     * @param Callable $callback Callable for formatting output
     *
     * @return void
     */
    public function setFormatter(callable $callback)
    {
        $this->formatter = $callback;

        return $this;
    }

    /**
     * Gets properties set on this column
     *
     * @return array
     */
    public function getProps(): array
    {
        return $this->cell_props;
    }

    /**
     * Sets properties set on this column
     *
     * @param array $props
     *
     * @return self
     */
    public function setProps(array $props)
    {
        $this->cell_props = $props;

        return $this;
    }

    /**
     * Set the clases
     *
     * @param string|array $classes Classes for the TH element
     *
     * @return self
     */
    public function setThClasses($classes)
    {
        if (is_array($classes)) {
            $this->classes = $classes;
        } elseif (is_string(($classes))) {
            $this->classes = explode(' ', $classes);
        } else {
            throw new \Exception('Invalid type passed to setClasses ' . gettype($classes));
        }

        return $this;
    }

    /**
     * Add one or more classes for use on the TH element
     *
     * @param string|array $classes classes to add
     * @return self
     */
    public function addThClass($classes)
    {
        if (is_string($classes)) {
            $classes = explode(' ', $classes);
        }

        $this->classes = array_merge($this->classes, $classes);

        return $this;
    }

    /**
     * Remove one or more classes from TH element
     *
     * @param string|array $classes classes to remove
     *
     * @return self
     */
    public function removeThClass($classes)
    {
        if (is_string($classes)) {
            $classes = explode(' ', $classes);
        }

        $this->classes = array_diff($this->classes, $classes);

        return $this;
    }

    /**
     * Get the classes
     *
     * @return array
     */
    public function getThClasses(): array
    {
        return $this->classes;
    }

    /**
     * Helper method for setting/getting classes
     *
     * @param string|array|null $classes
     * @return array|self
     */
    public function thClasses($classes = null)
    {
        if (is_null($classes)) {
            return $this->getClasses();
        } else {
            return $this->setClasses($classes);
        }
    }

    /**
     * Get array to pass to the vue component
     *
     * @return array
     */
    public function getJsConfig(): array
    {
        return [
            'title' => $this->getLabel(),
            'name' => $this->getName(),
            'component' => $this->getComponent(),
            'orderable_by' => $this->getOrderable(),
            'props' => $this->getProps(),
            'th_classes' => $this->getThClasses(),
        ];
    }
}
