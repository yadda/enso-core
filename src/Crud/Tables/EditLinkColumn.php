<?php

namespace Yadda\Enso\Crud\Tables;

use Yadda\Enso\Crud\Config;

/**
 * Text column that provides a link to the edit route for the given item
 */
class EditLinkColumn extends Column implements ColumnInterface
{
    protected $component = 'enso-cell-text-link';
}
