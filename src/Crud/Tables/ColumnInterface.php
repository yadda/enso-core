<?php

namespace Yadda\Enso\Crud\Tables;

use Yadda\Enso\Crud\Config;

/**
 * CRUD index table column
 */
interface ColumnInterface
{
    /**
     * Create a new column
     *
     * @param string $name HTML friendly, no spaces or quotes
     */
    public function __construct(string $name, ?Config $config = null);

    /**
    * Set the name of the column
    *
    * @param string $name Name of the column
    *
    * @return self
    */
    public function setName(string $name);

    /**
     * Get the name of the column.
     *
     * @return String
     */
    public function getName();

    /**
     * Set the label for the column heading
     *
     * @param string $name Label for the column
     *
     * @return self
     */
    public function setLabel(string $name);

    /**
     * Get the label for the column heading
     *
     * @return String
     */
    public function getLabel();

    /**
     * Get the content of the column to display in each row
     */
    public function getContent($item, Config $crud) :string;

    /**
     * Get the name of Vue component to use
     *
     * @return String
     */
    public function getComponent();

    /**
     * Set the clases
     *
     * @param string|array $classes
     *
     * @return self
     */
    public function setThClasses($classes);

    /**
     * Add one or more classes
     *
     * @param string|array $classes
     *
     * @return self
     */
    public function addThClass($classes);

    /**
     * Remove one or more classes
     *
     * @param string|array $classes
     *
     * @return self
     */
    public function removeThClass($classes);

    /**
     * Get the classes
     *
     * @return array
     */
    public function getThClasses();

    /**
     * Helper method for setting/getting classes
     *
     * @param string|array|null $classes
     *
     * @return array|self
     */
    public function thClasses($classes = null);
}
