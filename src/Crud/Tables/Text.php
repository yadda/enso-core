<?php

namespace Yadda\Enso\Crud\Tables;

class Text extends Column implements ColumnInterface
{
    protected $component = 'enso-cell-text';
}
