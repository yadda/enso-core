<?php

namespace Yadda\Enso\Crud\Controllers;

use Amazon\ProductAdvertisingAPI\v1\ApiException;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\api\DefaultApi;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\GetItemsRequest;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\Item;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\PartnerType;
use Amazon\ProductAdvertisingAPI\v1\Configuration;
use Exception;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Http\Request as Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

/**
 * Handle checking Amazon APAI codes when using AmazonProductField
 */
class AmazonProductController extends Controller
{
    /**
     * Amazon API access key
     *
     * @var string
     */
    protected $accessKey;

    /**
     * Am,azon API secret key
     *
     * @var string
     */
    protected $secretKey;

    /**
     * Amazon API associate tag
     *
     * @var string
     */
    protected $associateTag;

    /**
     * Amazon API default locale
     *
     * @var string
     */
    protected $default_locale;

    /**
     * Create a new AmazonProductController
     */
    public function __construct()
    {
        $this->default_locale = config('services.amazon.apai.default_locale');
        $this->accessKey = config('services.amazon.apai.access_key_id');
        $this->secretKey = config('services.amazon.apai.secret_access_key');

        // Locale might have a dot in it, so we need to do this the long way round...
        $locales = config('services.amazon.apai.locales');

        if (!isset($locales[$this->default_locale])) {
            throw new Exception(
                'You must set a default locale in your config (e.g. ' .
                    'services.amazon.apai.default_locale) for Amazon Product ' .
                    'fields to work. This must correspond to a locale setting ' .
                    '(e.g. services.amazon.apai.locales.com)'
            );
        }

        $this->associateTag = $locales[$this->default_locale]['associate_tag'];
        $this->region = $locales[$this->default_locale]['region'];
    }

    /**
     * Looks up a given ASIN (Amazon Standard Identification Number)
     *
     * @param Request $request HTTP request
     *
     * @return Response
     */
    public function asinCheck(Request $request)
    {
        $asin = $request->input('asin');

        $conf = $this->createApaiConfig();

        $apiInstance = new DefaultApi(new GuzzleClient(), $conf);

        $lookup = new GetItemsRequest();
        $lookup->setItemIds([$asin]);
        $lookup->setPartnerTag($this->associateTag);
        $lookup->setPartnerType(PartnerType::ASSOCIATES);

        try {
            $response = $apiInstance->getItems($lookup);
        } catch (ApiException $e) {
            return Response::json([
                'product_exists' => false,
            ]);
        } catch (Exception $e) {
            Log::error($e);
            abort(500, 'Invalid response from Amazon.');
        }

        $item = Arr::first(
            $response->getItemsResult()->getItems(),
            function ($item) use ($asin) {
                return $item->getASIN() === $asin;
            }
        );

        if ($item) {
            return Response::json([
                'product_exists' => true,
                'product' => $this->parseItemToData($item),
            ]);
        } else {
            return Response::json([
                'product_exists' => false,
            ]);
        }
    }

    /**
     * Creates a basic Configuration instance for this App
     *
     * @return Configuration
     */
    protected function createApaiConfig(): Configuration
    {
        return (new Configuration())
            ->setAccessKey($this->accessKey)
            ->setSecretKey($this->secretKey)
            ->setHost('webservices.amazon.' . $this->default_locale)
            ->setRegion($this->region);
    }

    /**
     * Parses an Item into an array of data.
     *
     * @param Item $item
     *
     * @return array
     */
    protected function parseItemToData(Item $item): array
    {
        return [
            'url' => $item->getDetailPageURL(),
            'title' => $item->getItemInfo()->getTitle()->getDisplayValue(),
            'asin' => $item->getASIN(),
        ];
    }
}
