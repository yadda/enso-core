<?php

namespace Yadda\Enso\Crud\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Config;

/**
 * Should only be used with an instance or collection of
 * \Yadda\Enso\Crud\Contracts\IsCrudModel.
 */
class ListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $label_by = Config::get('enso.settings.select_label_by', 'label');

        return [
            'id' => $this->resource->getKey(),
            $label_by => $this->resource->getCrudLabel(),
            'url' => $this->resource->getUrl(),
        ];
    }
}
