<?php

namespace Yadda\Enso\Crud\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Should only be used with an instance or collection of
 * \Yadda\Enso\Crud\Contracts\IsCrudModel.
 */
class TableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
