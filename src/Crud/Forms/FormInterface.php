<?php

namespace Yadda\Enso\Crud\Forms;

interface FormInterface
{
    /**
     * Add a section
     *
     * @param CrudSection $section
     */
    public function addSection(SectionInterface $section);

    /**
     * Get the attached sections
     *
     * @return array
     */
    public function getSections();
}
