<?php

namespace Yadda\Enso\Crud\Forms;

use Yadda\Enso\Crud\Forms\RelationshipSection;

class HasOneSection extends RelationshipSection
{
    /**
     * Gets the data associated with each of the fields and stores in a
     * formatted structure for passing to the vue components
     *
     * @param object $item data source
     *
     * @return mixed found data values
     */
    public function getFormData($item)
    {
        if (!is_object($item)) {
            return new stdClass;
        }

        $relation = $this->getRelatedModel($item, false);

        $form_data = [];
        foreach ($this->getFields() as $field) {
            $form_data[$field->getName()] = $field->getFormData($relation);
        }

        if (empty($form_data)) {
            return new stdClass;
        }

        return $form_data;
    }

    /**
     * Applies data to the given item
     *
     * @param  Model    $item           Item to apply data to
     * @param  array    $data           All data
     */
    public function applyRequestData(&$item, array $data)
    {
        // Everything should happen after the initial save, so that this Section
        // Can be used for relations on create pages.

        return;
    }

    /**
     * Applies data to the given item AFTER a save has been completed, in order
     * to provide the scope to make pivot_table relationships and other updates
     * that required the item to have an id set.
     *
     * @param  Model    $item           Item to apply data to
     * @param  array    $data           All data
     */
    public function applyRequestDataAfterSave(&$item, array $data)
    {
        $relation = $this->getRelatedModel($item, $this->hasRelationData($data));

        if (is_null($relation)) {
            return;
        }

        foreach ($this->getFields() as $field) {
            if (!$field->shouldWriteData()) {
                continue;
            }

            $apply_data = true;

            // If there is a post-save callback, then this allows you to break out of
            // saving any at to the relation.
            if ($field->getCallbackAfterSave()) {
                $apply_data = $field->getCallbackAfterSave()($relation, $data, $field);
            }

            // Need to apply both before and after save data here so that all
            // field types work
            if ($apply_data === true) {
                $field->applyRequestData($relation, $data);
                $field->applyRequestDataAfterSave($relation, $data);
            }
        }

        /**
         * Run a callable on the relation item after applying data to it, to allow
         * for custom modification or to prevent saving of the relation under certain
         * conditions.
         */
        if ($this->getRelationCallback()) {
            $response = $this->getRelationCallback()($relation, $item, $data);

            if ($response === false) {
                return;
            }
        }

        $relation->save();
    }

    /**
     * Gets the related HasOne model from the item, or generates
     * a blank Model of the correct class
     *
     * @param Model   $item              Eloquent model to get relation item from
     * @param boolean $has_relation_data Whether any of the relation section fields were filled
     *
     * @return Model
     */
    private function getRelatedModel($item, $has_relation_data)
    {
        $relationship = $item->{$this->getName()}();

        if (!$this->allowNullRelation() || $has_relation_data) {
            $relationship->withDefault($item);
        }

        return $relationship->getResults();
    }
}
