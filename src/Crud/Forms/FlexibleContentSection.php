<?php

namespace Yadda\Enso\Crud\Forms;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Yadda\Enso\Crud\Forms\FieldCollection;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Handlers\FlexibleRow;
use Yadda\Enso\Crud\Traits\HasSettingsFields;
use Yadda\Enso\Crud\Traits\RowHasFlexibleContent;

class FlexibleContentSection extends Section
{
    use HasSettingsFields, RowHasFlexibleContent;

    /**
     * If false, this row type will be hidden from the options when adding a new row
     *
     * @var boolean
     */
    protected $allow_new = true;

    /**
     * if false, this row type will be be deletable
     *
     * @var boolean
     */
    protected $allow_deletion = true;

    /**
     * Allow duplicating this row type
     *
     * @var boolean
     */
    protected $allow_duplication = true;

    /**
     * Maximum instances of this row allowed
     *
     * @var int
     */
    protected $max_instances = null;

    /**
     * Settings fields
     *
     * @var FieldCollection
     */
    protected $settings_fields;

    /**
     * Name of field to use for the row excerpt - shown in the row title bar
     *
     * @var string
     */
    protected $excerpt_field = null;

    public function __construct(string $name = 'main')
    {
        parent::__construct($name);

        $this->settings_fields = (new FieldCollection)->setSection($this);

        $this->addCommonFields();
    }

    /**
     * Adds fields that should be common for all rowspecs
     *
     * @return void
     */
    public function addCommonFields(): void
    {
        $this->addSettingsFields([
            TextField::make('row_label')
                ->addFieldsetClass('is-6'),
            TextField::make('row_id')
                ->addFieldsetClass('is-6'),
        ]);
    }

    /**
     * Set whether to allow deleting rows of this type
     *
     * @param boolean $deletable
     * @return self
     */
    public function allowDeletion($deletable = true)
    {
        $this->allow_deletion = $deletable;

        return $this;
    }

    /**
     * Set whether rows of this type should have a duplicate button
     *
     * @param boolean $duplicatable
     * @return self
     */
    public function allowDuplication($duplicatable = true)
    {
        $this->allow_duplication = $duplicatable;

        return $this;
    }

    /**
     * Set whether to allow new rows of this type
     *
     * @param Boolean $allowed
     * @return self
     */
    public function allowNew($allowed = true)
    {
        $this->allow_new = $allowed;

        return $this;
    }

    /**
     * Get the maximum insetances of this row that can exist on one flexible
     * content field.
     *
     * @param int $max
     * @return self
     */
    public function getMaxInstances()
    {
        return $this->max_instances;
    }

    /**
     * Gets a field by it's name property from this section
     *
     * @deprecated v0.2.277
     *
     * @param string $name Name of field to get
     *
     * @return FieldInterface|null
     */
    public function getSettingsFieldByName($name)
    {
        Log::warning('`getSettingsFieldByName` has been Deprecated. Use `getSettingsField` instead');

        return $this->getSettingsField($name);
    }

    /**
     * Html content for a given, named wysiwyg block
     *
     * @param Collection $blocks
     * @param string $block_name
     *
     * @return string|null
     */
    protected static function getWysiwygHtml(Collection $blocks, string $block_name): ?string
    {
        if ($blocks->has($block_name)) {
            return Arr::get($blocks->get($block_name)->getContent(), 'html', null);
        }

        return null;
    }

    /**
     * Helper method for getting/setting maximum instances
     *
     * @param int $max
     * @return int|self
     */
    public function maxInstances($max = null)
    {
        if ($max) {
            return $this->setMaxInstances($max);
        } else {
            return $this->getMaxInstances();
        }
    }

    /**
     * Set the maximum instances of this row that can exist on one flexible
     * content field.
     *
     * @param int $max
     * @return self
     */
    public function setMaxInstances($max)
    {
        $this->max_instances = $max;

        return $this;
    }

    /**
     * Convert this section to an array
     *
     * @return void
     */
    public function toArray()
    {
        $output = parent::toArray();

        $output['settings_fields'] = $this->getSettingsFields()->toArray();

        $output['allow_new'] = $this->allow_new;
        $output['allow_deletion'] = $this->allow_deletion;
        $output['allow_duplication'] = $this->allow_duplication;
        $output['max_instances'] = $this->max_instances;
        $output['excerpt_field'] = $this->excerpt_field;

        return $output;
    }

    /**
     * Unpacks a row of parsed data, mergin common and row-specific fields
     * into a single object
     */
    public static function unpack(FlexibleRow $row): ?object
    {
        return (object) array_merge(
            static::getCommonContent($row),
            static::getRowContent($row),
        );
    }

    /**
     * Removes common fields from this FlexibleContentSection
     *
     * @return FlexibleContentSection
     */
    public function withoutCommon(): FlexibleContentSection
    {
        foreach (['row_id', 'row_label'] as $field_name) {
            $this->removeSettingsField($field_name);
        }

        return $this;
    }

    /**
     * Unpack common fields.
     *
     * @param FlexibleRow $row
     *
     * @return array
     */
    protected static function getCommonContent(FlexibleRow $row): array
    {
        return [
            'row_label' => $row->settingContent('row_label'),
            'row_type' => $row->getType(),
            'row_id' => $row->settingContent('row_id'),
        ];
    }

    /**
     * Unpack Row-specific fields. Should be overriden in Rowspecs that extend
     * this class.
     *
     * @param FlexibleRow $row
     *
     * @return array
     */
    protected static function getRowContent(FlexibleRow $row): array
    {
        return [];
    }

    /**
     * Set the name of the field to use for the row excerpt
     */
    public function excerptField(string $field_name = null)
    {
        if (is_null($field_name)) {
            return $this->excerpt_field;
        }

        $this->excerpt_field = $field_name;

        return $this;
    }
}
