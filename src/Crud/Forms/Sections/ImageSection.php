<?php

namespace Yadda\Enso\Crud\Forms\Sections;

use App;
use Illuminate\Support\Arr;
use Yadda\Enso\Crud\Forms\Fields\FileUploadFieldResumable;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;
use Yadda\Enso\Media\Contracts\ImageFile as ImageFileContract;

class ImageSection extends FlexibleContentSection
{
    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'image';

    public function __construct(string $name = 'image')
    {
        parent::__construct($name);

        $this
            ->setLabel('Image')
            ->addFields([
                FileUploadFieldResumable::make('image'),
            ]);
    }

    /**
     * Unpacks a row of parsed data, mergin common and row-specific fields
     * into a single object
     */
    public static function unpack(FlexibleRow $row): ?object
    {
        $type = Arr::get($row, 'type');

        $image_class = App::make(ImageFileContract::class);
        $image_ids = Arr::get($row, 'fields.image.content', null);

        if (!empty($image_ids)) {
            $image = $image_class::find(Arr::first($image_ids));
        }

        // If Image doesn't exist
        if (empty($image)) {
            return null;
        }

        $content = [
            'image' => $image
        ];

        return (object) compact('type', 'content');
    }
}
