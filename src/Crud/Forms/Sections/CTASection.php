<?php

namespace Yadda\Enso\Crud\Forms\Sections;

use Illuminate\Support\Arr;
use Yadda\Enso\Crud\Forms\Fields\SelectField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

class CTASection extends FlexibleContentSection
{
    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'cta';

    public function __construct(string $name = 'cta')
    {
        parent::__construct($name);

        $this
            ->setLabel('CTA')
            ->addFields([
                (new TextField('text')),
                (new TextField('link')),
                (new SelectField('target'))
                    ->setOptions([
                        '_blank' => 'New Tab/Window',
                        '_self' => 'Current Tab',
                    ]),
            ]);
    }

    /**
     * Unpacks a row of parsed data, mergin common and row-specific fields
     * into a single object
     */
    public static function unpack(FlexibleRow $row): ?object
    {
        $type = Arr::get($row, 'type');

        $text = Arr::get($row, 'fields.text.content', '');

        // If image not selected
        if (empty($text)) {
            return null;
        }

        $link = Arr::get($row, 'fields.link.content', '#');
        if ($link !== '#') {
            $link = $this->sanitizeLink($link);
        }

        $content = [
            'text' => $text,
            'link' => $link,
            'target' => Arr::get($row, 'fields.target.content.id', '_self'),
        ];

        return (object) compact('type', 'content');
    }

    protected function sanitizeLink($link)
    {
        if (!preg_match('#^(http(s)?:)?//#', $link)) {
            return '//' . $link;
        }

        return $link;
    }
}
