<?php

namespace Yadda\Enso\Crud\Forms\Sections;

use Yadda\Enso\Crud\Forms\Fields\ButtonsField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

class ButtonsSection extends FlexibleContentSection
{
    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'buttons';

    public function __construct(string $name = 'buttons')
    {
        parent::__construct($name);

        $this->setLabel('Buttons')
            ->addFields([
                TextField::make('title'),
                ButtonsField::make('buttons')->setLabel('')->setMaxRows(2),
            ])
            ->addSettingsFields([
                TextField::make('section_title')
                    ->setHelptext('This will be used in menus, lists of page sections, etc.')
                    ->addFieldsetClass('is-6'),
                TextField::make('id')
                    ->setLabel('Id Attribute')
                    ->addFieldsetClass('is-6'),
            ]);
    }

    /**
     * Unpacks a row of parsed data, mergin common and row-specific fields
     * into a single object
     */
    public static function unpack(FlexibleRow $row): ?object
    {
        return (object) [
            'id' => $row->settingContent('row_id'),
            'section_title' => $row->blockContent('section_title'),
            'title' => $row->blockContent('title'),
            'buttons' => static::flexibleFieldContent('buttons', $row),
        ];
    }
}
