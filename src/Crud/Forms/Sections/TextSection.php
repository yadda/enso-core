<?php

namespace Yadda\Enso\Crud\Forms\Sections;

use Illuminate\Support\Arr;
use Yadda\Enso\Crud\Forms\Fields\WysiwygField;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

class TextSection extends FlexibleContentSection
{
    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'text';

    public function __construct(string $name = 'text')
    {
        parent::__construct($name);

        $this
            ->setLabel('Text')
            ->addFields([
                (new WysiwygField('text')),
            ]);
    }

    /**
     * Unpacks a row of parsed data, mergin common and row-specific fields
     * into a single object
     */
    public static function unpack(FlexibleRow $row): ?object
    {
        $type = Arr::get($row, 'type');

        $text = Arr::get($row, 'fields.text.content.html', '');

        if (!$text) {
            return null;
        }

        $content = [
            'text' => $text,
        ];

        return (object) compact('type', 'content');
    }
}
