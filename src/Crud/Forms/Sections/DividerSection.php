<?php

namespace Yadda\Enso\Crud\Forms\Sections;

use Illuminate\Support\Arr;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

class DividerSection extends FlexibleContentSection
{
    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'divider';

    public function __construct(string $name = 'divider')
    {
        parent::__construct($name);

        $this
            ->setLabel('Divider')
            ->addFields([
                (new TextField('divider'))
                    ->setDisabled(true)
                    ->setHelpText('You do not need to add any data to this row'),
            ]);
    }

    /**
     * Unpacks a row of parsed data, mergin common and row-specific fields
     * into a single object
     */
    public static function unpack(FlexibleRow $row): ?object
    {
        $type = Arr::get($row, 'type');
        $content = [];

        return (object) compact('type', 'content');
    }
}
