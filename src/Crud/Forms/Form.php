<?php

namespace Yadda\Enso\Crud\Forms;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use stdClass;
use Yadda\Enso\Crud\Exceptions\FieldIntegrityException;
use Yadda\Enso\Facades\EnsoCrud;

class Form implements FormInterface
{
    /**
     * The attached sections for this form
     *
     * @var \Illuminate\Support\Collection A collection of items that extend SectionInterface
     */
    protected $sections;

    /**
     * Errors raised when completing the form
     *
     * @var \Illuminate\Support\ViewErrorBag
     */
    protected $errors;

    /**
     * The name of the vue component to use to display this form
     * e.g. <this-bit-here :foo="bar">
     *
     * @var string
     */
    protected $tag_name = 'enso-crud-form';

    /**
     * A reference to the config object that holds this form. Maintaining this
     * reference is required for relationship fields, and any other field that
     * needs information only present in the config.
     *
     * @param \Yadda\Enso\Crud\Config
     */
    protected $config_reference;

    /**
     * The stored models instance for this form.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model_instance;

    /**
     * Whether this form should allow general autocomplete.
     *
     * @var string
     */
    protected $autocomplete = true;

    public function __construct()
    {
        $this->sections = new Collection;
    }

    /**
     * Sets the local data instance to a given object
     *
     * @param object $item Data to set
     */
    public function setModelInstance($item)
    {
        if (!is_object($item)) {
            throw new Exception('You must pass an object to Form->setModelInstance()');
        }

        $this->model_instance = $item;

        return $this;
    }

    /**
     * Gets the local data instance of the form's model
     *
     * @return \Illuminate\Database\Eloquent\Model Model instance
     */
    public function getModelInstance()
    {
        return $this->model_instance;
    }

    /**
     * Saves the Model instance to the database
     *
     * @return self
     */
    public function saveModelInstance()
    {
        if (!$this->model_instance instanceof Model) {
            throw new Exception('You cannot save this Model, as it is not an instance of Eloquent\Model');
        }

        $this->model_instance->save();

        return $this;
    }

    /**
     * Sets the config reference
     *
     * @param \Yadda\Enso\Crud\Config &$config reference to containing crud config
     */
    public function setConfig(&$config)
    {
        $this->config_reference = $config;

        foreach ($this->getSections() as $section) {
            $section->setForm($this);
        }

        return $this;
    }

    /**
     * Returns the config reference
     *
     * @return \Yadda\Enso\Crud\Config reference to containing crud config
     */
    public function getConfig()
    {
        return $this->config_reference;
    }

    /**
     * Whether this form has a config set
     *
     * @return bool
     */
    public function hasConfig()
    {
        return !empty($this->config_reference);
    }

    /**
     * Gets the model from the containing Config
     *
     * @return string Model class
     */
    public function getModel()
    {
        if (!$this->hasConfig()) {
            throw new FieldIntegrityException('Cannot get a Model class from a form with no config');
        }

        return $this->getConfig()->getModel();
    }

    /**
     * Boolean check to see whether the form has a section of the given name
     *
     * @param string $name Section name
     *
     * @return bool Whether it exists
     */
    public function hasSection($name)
    {
        return !!$this->getSection($name);
    }

    /**
     * Get the attached sections
     *
     * @return array
     */
    public function getSections()
    {
        return $this->sections;
    }

    /**
     * Gets a section from the form by its name.
     *
     * DEPRECATED: Please use `getSection` instead
     *
     * @param string $name
     *
     * @return \Yadda\Enso\Crud\Forms\SectionInterface
     */
    public function getSectionByName($name)
    {
        Log::warning('`getSectionByName` has been Deprecated. Use `getSection` instead');

        return $this->getSection($name);
    }

    /**
     * Gets a section from the form by its name.
     *
     * @param string $name
     *
     * @return \Yadda\Enso\Crud\Forms\SectionInterface
     */
    public function getSection($name)
    {
        foreach ($this->getSections() as $section) {
            if ($section->getName() === $name) {
                return $section;
            }
        }

        return null;
    }

    /**
     * Extracts and returns a Section from the internal Sections list.
     *
     * @param string $name
     *
     * @return \Yadda\Enso\Crud\Forms\SectionInterface|null
     */
    public function extractSection($name)
    {
        $index = $this->getSectionIndex($name);

        if ($index !== false) {
            return $this->sections->splice($index, 1)->first();
        }

        return null;
    }

    /**
     * Remove a section from the form by its name.
     *
     * DEPRECATED: Please use `removeSection` instead
     *
     * @param string $name
     *
     * @return self
     */
    public function removeSectionByName($name)
    {
        Log::warning('`removeSectionByName` has been Deprecated. Use `removeSection` instead');

        $this->removeSection($name);

        return $this;
    }

    /**
     * Remove a section from the form by its name
     *
     * @param string $name
     *
     * @return self
     */
    public function removeSection($name)
    {
        $this->sections = $this->sections->reject(function ($item) use ($name) {
            return $item->getName() === $name;
        })->values();

        return $this;
    }

    /**
     * Adds an iterable collection of Sections to this Form instance
     *
     * @param iterable collection
     */
    public function addSections($sections)
    {
        foreach ($sections as $section) {
            $this->addSection($section);
        }

        return $this;
    }

    /**
     * Adds a given Section to the internal section array
     *
     * @param \Yadda\Enso\Crud\Forms\SectionInterface $section Section to add
     *
     * @return self
     */
    public function addSection(SectionInterface $section)
    {
        if ($this->hasSection($section->getName())) {
            throw new Exception('Cannot add section with duplicate name: \'' . $section->getName() . '\'');
        }

        if ($this->hasConfig()) {
            $section->setForm($this);
        }

        $this->sections->push($section);

        return $this;
    }

    /**
     * Adds a Section after a section with the given name.
     *
     * @param string                                  $section_name
     * @param \Yadda\Enso\Crud\Forms\SectionInterface $section
     *
     * @return self
     */
    public function addSectionAfter(string $section_name, SectionInterface $section)
    {
        $index = $this->getSectionIndex($section_name);

        if ($index === false) {
            throw new Exception('Cannot add section after a section that does not exist.');
        }

        if ($this->hasConfig()) {
            $section->setForm($this);
        }

        $this->sections->splice($index + 1, 0, [$section]);

        return $this;
    }

    /**
     * Adds a Section before a section with the given name.
     *
     * @param string                                  $section_name
     * @param \Yadda\Enso\Crud\Forms\SectionInterface $section
     *
     * @return self
     */
    public function addSectionBefore(string $section_name, SectionInterface $section)
    {
        $index = $this->getSectionIndex($section_name);

        if ($index === false) {
            throw new Exception('Cannot add section after a section that does not exist.');
        }

        if ($this->hasConfig()) {
            $section->setForm($this);
        }

        $this->sections->splice($index, 0, [$section]);

        return $this;
    }

    /**
     * Adds a section at the beginning of the section collection.
     *
     * @param \Yadda\Enso\Crud\Forms\SectionInterface $section
     *
     * @return self
     */
    public function prependSection(SectionInterface $section)
    {
        if ($this->hasConfig()) {
            $section->setForm($this);
        }

        $this->sections->prepend($section);

        return $this;
    }

    /**
     * Adds a section at the end of the section collection.
     *
     * @param \Yadda\Enso\Crud\Forms\SectionInterface $section
     *
     * @return self
     */
    public function appendSection(SectionInterface $section)
    {
        if ($this->hasConfig()) {
            $section->setForm($this);
        }

        $this->sections->push($section);

        return $this;
    }

    /**
     * Moves a named section to the position just before another named section
     *
     * @param string $source_name
     * @param string $destination_name
     *
     * @return self
     */
    public function moveSectionAfter(string $source_name, string $destination_name)
    {
        $transfer = $this->extractSection($source_name);

        if (is_null($transfer)) {
            return $this;
        }

        $destination_index = $this->getSectionIndex($destination_name);

        $this->sections->splice($destination_index + 1, 0, [$transfer]);

        return $this;
    }

    /**
     * Moves a named section to the position just before another named section
     *
     * @param string $source_name
     * @param string $destination_name
     *
     * @return self
     */
    public function moveSectionBefore(string $source_name, string $destination_name)
    {
        $transfer = $this->extractSection($source_name);

        if (is_null($transfer)) {
            return $this;
        }

        $destination_index = $this->getSectionIndex($destination_name);

        $this->sections->splice($destination_index, 0, [$transfer]);

        return $this;
    }

    /**
     * Iterates throught it's sections, applying data to the given item based on
     * methods provided by each field type.
     *
     * @param array $data Data to apply
     */
    public function applyRequestData(array $data, callable $callback = null)
    {
        if (is_null($this->model_instance)) {
            throw new Exception('Must set a model instance before applying request data to it');
        }

        if (EnsoCrud::maintainFileList(get_class($this->model_instance))) {
            $this->model_instance->files()->sync([]);
        }

        foreach ($this->getSections() as $section) {
            $section->applyRequestData($this->model_instance, $data);
        }

        if (!is_null($callback)) {
            $this->model_instance = $callback($this->model_instance, $data, $this->getAction());
        }

        return $this;
    }

    /**
     * Iterates throught it's sections, applying data to the given item based on
     * methods provided by each field type.
     *
     * @param array $data Data to apply
     */
    public function applyRequestDataAfterSave(array $data, callable $callback = null)
    {
        if (is_null($this->model_instance)) {
            throw new Exception('Must set a model instance before applying request data to it');
        }

        foreach ($this->getSections() as $section) {
            $section->applyRequestDataAfterSave($this->model_instance, $data);
        }

        if (!is_null($callback)) {
            $this->model_instance = $callback($this->model_instance, $data, $this->getAction());
        }

        return $this;
    }

    /**
     * Gets a formatted object of data to be passed to the vue components via js
     * from the attached data instance
     *
     * @return stdClass
     */
    public function getFormData()
    {
        $item = $this->model_instance;

        // If no item is set, just return the default;
        if (is_null($item)) {
            return new stdClass;
        }

        $data = [];

        foreach ($this->getSections() as $section) {
            $data[$section->getName()] = $section->getFormData($item);
        }

        // If no data at all, return a stdClass so that converting to json
        // still results in an object and doesn't convert to an empty array.
        if (empty($data)) {
            $data = new stdClass;
        }

        return $data;
    }

    /**
     * Set the action of the current request. E.g. create/update
     *
     * @param string $action
     *
     * @return self
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get the action of the current request. E.g. create/update
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Combined Accessor and setter function for the autocomplete
     * property.
     *
     * @param boolean|null $value
     *
     * @return self|boolean
     */
    public function allowAutocomplete($value = null)
    {
        if (!is_null($value)) {
            return $this->setAutocomplete($value);
        }

        return $this->getAutocomplete();
    }

    /**
     * Gets the current autocomplete status of this form
     *
     * @return bool
     */
    public function getAutocomplete()
    {
        return $this->autocomplete;
    }

    /**
     * Sets the current autocomplete status of this form.
     *
     * @param bool $value
     *
     * @return self
     */
    public function setAutocomplete($value)
    {
        $this->autocomplete = $value;

        return $this;
    }

    /**
     * Get an array of field names, e.g. 'main.name' mapped to
     * field labels, e.g. 'Name'.
     *
     * @return array
     */
    public function getFieldNames()
    {
        $sections = collect($this->getSections());

        return $sections->flatMap(function ($section) {
            return collect($section->getFields())->mapWithKeys(function ($field) use ($section) {
                return [$section->getName() . '.' . $field->getName() => $field->getLabel()];
            });
        })->toArray();
    }

    /**
     * Returns a formatted array version of this section for passingn to the
     * front end
     *
     * @return array Formatted version of this instance
     */
    public function toArray()
    {
        $output = [
            'sections'  => [],
            'component' => $this->tag_name,
            'autocomplete' => $this->getAutocomplete() ? 'on' : 'off',
        ];

        foreach ($this->sections as $section) {
            if ($section->isRestricted()) {
                continue;
            }

            $output['sections'][$section->getName()] = $section->toArray();
        }

        return $output;
    }

    /**
     * Gets the current index of a Section, base on it's name.
     *
     * @param string $section_name
     *
     * @return integer
     */
    protected function getSectionIndex($section_name)
    {
        return $this->sections->search(function ($section) use ($section_name) {
            return $section->getName() === $section_name;
        });
    }
}
