<?php

namespace Yadda\Enso\Crud\Forms\Rows;

use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\Fields\ButtonsField;
use Yadda\Enso\Crud\Forms\Fields\FileUploadFieldResumable;
use Yadda\Enso\Crud\Forms\Fields\GalleryField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Fields\WysiwygField;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

/**
 * Represents a purely text row withing a flexible content collection.
 */
class Hero extends FlexibleContentSection
{
    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'hero';

    public function __construct(string $name = 'hero')
    {
        parent::__construct($name);

        $this
            ->addFields([
                GalleryField::make('images')
                    ->setHelpText(
                        'If an External Video is chosen, only the first image will '
                        . 'be used. Image should be 16:9 ratio and at least 1920 x '
                        . '1080 px in size. If a video is provided, an image should '
                        . 'also be provided as a fallback.'
                    ),
                FileUploadFieldResumable::make('video')
                    ->setHelpText(
                        'This should be a short (~30sec) 1920 x 1080 px looping '
                        . 'MP4 video using the H264 codec. In order for the '
                        . 'browser to autoplay the video, it must NOT contain a '
                        . 'sound channel. Not just silent audio, but no audio '
                        . 'channel at all.'
                    ),
                TextField::make('title'),
                WysiwygField::make('content')
                    ->setLabel('Subtitle')
                    ->setModules(
                        Config::get('enso.flexible-content.rows.hero.modules', [])
                    ),
                ButtonsField::make('buttons')
                    ->setMaxRows(2),
            ]);

        $this->getField('images')->setMaxFiles(1);
    }

    /**
     * Unpack Row-specific fields.
     *
     * @param FlexibleRow $row
     *
     * @return array
     */
    protected static function getRowContent(FlexibleRow $row): array
    {
        return [
            'images' => $row->blockContent('images'),
            'video' => $row->blockContent('video')->first(),
            'buttons' => static::flexibleFieldContent('buttons', $row),
            'content' => static::getWysiwygHtml($row->getBlocks(), 'content'),
            'title' => $row->blockContent('title'),
        ];
    }
}
