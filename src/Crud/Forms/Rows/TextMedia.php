<?php

namespace Yadda\Enso\Crud\Forms\Rows;

use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Forms\Rows\Media;
use Yadda\Enso\Crud\Forms\Rows\Text;
use Yadda\Enso\Crud\Handlers\FlexibleRow;
use Yadda\Enso\Crud\Traits\RowHasAlignment;
use Yadda\Enso\Utilities\Helpers;

/**
 * Represents a purely text row withing a flexible content collection.
 */
class TextMedia extends FlexibleContentSection
{
    use RowHasAlignment;

    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'text-media';

    /**
     * Array of style options
     *
     * @var array
     */
    protected $styles;

    public function __construct(string $name = 'text-media')
    {
        parent::__construct($name);

        $this
            ->setLabel('Text / Media')
            ->addFields(
                Helpers::getConcreteClass(Text::class)::textFields()
            )
            ->addFieldAfter('title', $this->alignmentField()->addFieldsetClass('is-3'))
            ->addFields(
                Helpers::getConcreteClass(Media::class)::mediaFields()
            );

        $this->getField('title')->addFieldsetClass('is-9');

        $this->getField('content')
            ->setModules(
                Config::get('enso.flexible-content.rows.text-media.modules', [])
            );

        $this
            ->getField('images')
            ->setHelpText(
                'If a Video is chosen this image will be used as a fall-back ' .
                'for mobile devices and browsers that do not support video. ' .
                'Image should be 1:1 ratio and at least 768 x 768 px in size.'
            )
            // We don't have the ability to show non-full-screen-width carousels
            // so we can only accept a single image here.
            ->setMaxFiles(1);

        $this
            ->getField('video')
            ->setHelpText(
                'This should be a short (~30sec) 768 x 768 px looping MP4 '
                . 'video using the H264 codec. In order for the browser to '
                . 'autoplay the video, it must NOT contain a sound channel. '
                . 'Not just silent audio, but no audio channel at all.'
            );
    }

    /**
     * Unpack Row-specific fields. Should be overriden in Rowspecs that extend
     * this class.
     *
     * If style is not selected, default to the first style in the config array
     *
     * @param FlexibleRow $row
     *
     * @return array
     */
    protected static function getRowContent(FlexibleRow $row): array
    {
        return array_merge(
            Helpers::getConcreteClass(Text::class)::textContent($row),
            Helpers::getConcreteClass(Media::class)::mediaContent($row),
            [
                'alignment' => static::calculateAlignment($row),
            ]
        );
    }
}
