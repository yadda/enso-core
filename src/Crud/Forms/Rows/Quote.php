<?php

namespace Yadda\Enso\Crud\Forms\Rows;

use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Fields\WysiwygField;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

/**
 * Represents a purely text row withing a flexible content collection.
 */
class Quote extends FlexibleContentSection
{
    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'quote';

    /**
     * Array of style options
     *
     * @var array
     */
    protected $styles;

    public function __construct(string $name = 'quote')
    {
        parent::__construct($name);

        $this->addFields([
            TextField::make('title'),
            WysiwygField::make('text')
                ->setModules(static::modules()),
            TextField::make('author')
        ]);
    }

    /**
     * Modules for text rows
     *
     * @return array
     */
    public static function modules(): array
    {
        return Config::get('enso.flexible-content.rows.quote.modules', []);
    }

    /**
     * Unpack Row-specific fields. Should be overriden in Rowspecs that extend
     * this class.
     *
     * If style is not selected, default to the first style in the config array
     *
     * @param FlexibleRow $row
     *
     * @return array
     */
    protected static function getRowContent(FlexibleRow $row): array
    {
        return [
            'title' => $row->blockContent('title'),
            'text' => static::getWysiwygHtml($row->getBlocks(), 'text'),
            'author' => $row->blockContent('author'),
        ];
    }
}
