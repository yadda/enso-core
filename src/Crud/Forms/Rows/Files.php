<?php

namespace Yadda\Enso\Crud\Forms\Rows;

use Yadda\Enso\Crud\Forms\Fields\FileUploadFieldResumable;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

/**
 * Represents a purely text row withing a flexible content collection.
 */
class Files extends FlexibleContentSection
{
    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'files';

    public function __construct(string $name = 'files')
    {
        parent::__construct($name);

        $this->addFields([
            TextField::make('title'),
            FileUploadFieldResumable::make('files')
                ->setMaxFiles(null),
        ]);
    }

    /**
     * Unpack Row-specific fields.
     *
     * @param FlexibleRow $row
     *
     * @return array
     */
    protected static function getRowContent(FlexibleRow $row): array
    {
        return [
            'files' => $row->blockContent('files'),
            'title' => $row->blockContent('title'),
        ];
    }
}
