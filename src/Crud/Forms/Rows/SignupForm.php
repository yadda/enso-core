<?php

namespace Yadda\Enso\Crud\Forms\Rows;

use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\Fields\FileUploadFieldResumable;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Fields\WysiwygField;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

/**
 * Represents a SignupForm row within a flexible content collection.
 */
class SignupForm extends FlexibleContentSection
{
    /**
     * Default name for this section
     */
    const DEFAULT_NAME = 'signup-form';

    /**
     * Default button text for when none is provided.
     */
    const DEFAULT_BUTTON = 'Sign up';

    public function __construct(string $name = 'signup-form')
    {
        parent::__construct($name);

        $this->addFields([
            FileUploadFieldResumable::make('images')
                ->setMaxFiles(1)
                ->setHelpText('Optional background image'),
            TextField::make('title'),
            WysiwygField::make('content'),
            TextField::make('button')
                ->setLabel('Button text')
                ->setPlaceholder(static::DEFAULT_BUTTON),
        ]);
    }

    /**
     * Unpack Row-specific fields.
     *
     * @param FlexibleRow $row
     *
     * @return array
     */
    protected static function getRowContent(FlexibleRow $row): array
    {
        $button = $row->blockContent('button');

        $type = Config::get('enso.flexible-content.rows.signup-form.type');

        $base_form_data = [
            'button' => $button ? $button : static::DEFAULT_BUTTON,
            'content' => static::getWysiwygHtml($row->getBlocks(), 'content'),
            'images' => $row->blockContent('images'),
            'title' => $row->blockContent('title'),
            'type' => $type,
        ];

        $form_type_parser = Config::get('enso.flexible-content.rows.signup-form.types.' . $type);

        if (class_exists($form_type_parser)) {
            return array_merge(
                $base_form_data,
                $form_type_parser::getRowContent($row)
            );
        } else {
            return $base_form_data;
        }
    }
}
