<?php

namespace Yadda\Enso\Crud\Forms\Rows;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\Fields\SelectField;
use Yadda\Enso\Crud\Forms\Fields\TextareaField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

/**
 * Represents a purely text row withing a flexible content collection.
 */
class Embed extends FlexibleContentSection
{
    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'embed';

    /**
     * Array of style options
     *
     * @var array
     */
    protected $styles;

    public function __construct(string $name = 'embed')
    {
        parent::__construct($name);

        $default_size = SelectField::makeOption(
            Config::get('enso.flexible-content.rows.embed.default-size'),
            Arr::get(
                Config::get('enso.flexible-content.rows.embed.sizes'),
                Config::get('enso.flexible-content.rows.embed.default-size')
            )
        );

        $this->addFields([
            TextField::make('title')
                ->addFieldsetClass('is-9'),
            SelectField::make('style')
                ->setOptions(Config::get('enso.flexible-content.rows.embed.sizes'))
                ->setSettings(['allow_empty' => false])
                ->setDefaultValue($default_size)
                ->addFieldsetClass('is-3'),
            TextareaField::make('content')
                ->setPurifyHTML(false),
        ]);
    }

    /**
     * Unpack Row-specific fields. Should be overriden in Rowspecs that extend
     * this class.
     *
     * If style is not selected, default to the first style in the config array
     *
     * @param FlexibleRow $row
     *
     * @return array
     */
    protected static function getRowContent(FlexibleRow $row): array
    {
        $size_track_by = static::make()->getField('style')->getSetting('track_by');

        return [
            'title' => $row->blockContent('title'),
            'size' => Arr::get(
                $row->blockContent('style'),
                $size_track_by,
                Config::get('enso.flexible-content.rows.embed.default-size')
            ),
            'content' => $row->blockContent('content'),
        ];
    }
}
