<?php

namespace Yadda\Enso\Crud\Forms\Rows;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\Fields\GalleryField;
use Yadda\Enso\Crud\Forms\Fields\SelectField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

/**
 * Represents a purely text row withing a flexible content collection.
 */
class Image extends FlexibleContentSection
{
    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'image';

    /**
     * Constructor
     *
     * @param string $name
     */
    public function __construct(string $name = 'media')
    {
        parent::__construct($name);

        $this->addFields(array_merge(
            [
                TextField::make('title')
                    ->addFieldsetClass('is-two-thirds'),
                SelectField::make('style')
                    ->setDefaultValue(
                        static::getDefaultStyleValue()
                    )
                    ->setOptions(
                        static::getStyleOptions()
                    )
                    ->setSettings([
                        'allow_empty' => false,
                        'show_labels' => false,
                    ])
                    ->addFieldsetClass('is-one-third'),
            ],
            static::imageFields(),
        ));
    }

    /**
     * Image content for a given FlexibleRow
     *
     * @param FlexibleRow $row
     *
     * @return array
     */
    public static function imageContent(FlexibleRow $row): array
    {
        return [
            'images' => $row->blockContent('images'),
        ];
    }

    /**
     * Image specific fields for an image row.
     *
     * @return array
     */
    public static function imageFields(): array
    {
        return [
            GalleryField::make('images')
                ->setHelpText(
                    'Adding more than one image will result in an image carousel '
                    . 'being displayed'
                ),
        ];
    }

    /**
     * Default Style
     *
     * @return string
     */
    protected static function getDefaultStyle(): string
    {
        return Config::get('enso.flexible-content.rows.image.default-style', 'wide');
    }

    /**
     * Default values of the Style dropdown box
     *
     * @return array
     */
    protected static function getDefaultStyleValue(): array
    {
        $default_style = static::getDefaultStyle();

        return SelectField::makeOption(
            $default_style,
            Config::get('enso.flexible-content.rows.image.styles.' . $default_style, 'Wide')
        );
    }

    /**
     * Options for the Style dropdown field
     *
     * @return array
     */
    protected static function getStyleOptions(): array
    {
        return (new Collection(Config::get('enso.flexible-content.rows.image.styles', [])))
            ->map(function ($item, $key) {
                return SelectField::makeOption($key, $item);
            })->values()->toArray();
    }

    /**
     * Unpack Row-specific fields.
     *
     * @param FlexibleRow $row
     *
     * @return array
     */
    protected static function getRowContent(FlexibleRow $row): array
    {
        $style_track_by = static::make()->getField('style')->getSetting('track_by');

        return array_merge(
            [
                'title' => $row->blockContent('title'),
                'style' => Arr::get(
                    $row->blockContent('style'),
                    $style_track_by,
                    static::getDefaultStyle()
                ),
            ],
            static::imageContent($row),
        );
    }
}
