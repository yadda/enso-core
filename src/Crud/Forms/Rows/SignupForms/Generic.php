<?php

namespace Yadda\Enso\Crud\Forms\Rows\SignupForms;

use Yadda\Enso\Crud\Forms\Fields\DividerField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Handlers\FlexibleRow;
use Yadda\Enso\Settings\Contracts\ExtraSettings;
use Yadda\Enso\Settings\Facades\EnsoSettings;

/**
 * Handles Generic specific data for SignupForm flexible row specs.
 */
class Generic implements ExtraSettings
{
    /**
     * Generic specific content for a signup form.
     *
     * @param FlexibleRow $row
     *
     * @return array
     */
    public static function getRowContent(FlexibleRow $row): array
    {
        return [
            'form_url' => EnsoSettings::get('signup_form_button_url')
        ];
    }

    /**
     * Fields to add to EnsoSettings so that users can populate SignupForm details
     *
     * @param Form $form
     *
     * @return Form
     */
    public static function updateForm(Form $form): Form
    {
        $form
            ->getSection('main')
            ->addFields([
                DividerField::make('signup-form-options')
                    ->setTitle('Signup Form Options'),
                TextField::make('signup_form_button_url'),
            ]);

        return $form;
    }
}
