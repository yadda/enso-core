<?php

namespace Yadda\Enso\Crud\Forms\Rows;

use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Contracts\FlexibleFieldHandler;
use Yadda\Enso\Crud\Forms\Fields\ButtonsField;
use Yadda\Enso\Crud\Forms\Fields\FileUploadFieldResumable;
use Yadda\Enso\Crud\Forms\Fields\FlexibleContentField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Fields\WysiwygField;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

/**
 * Represents a purely text row withing a flexible content collection.
 */
class Accordion extends FlexibleContentSection
{
    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'accordion';

    public function __construct(string $name = 'accordion')
    {
        parent::__construct($name);

        $this->addFields([
            TextField::make('title'),
            FlexibleContentField::make('accordion')
                ->addRowSpecs([
                    static::singleRowSpec(),
                ])
        ]);
    }

    /**
     * Modules for text rows
     *
     * @return array
     */
    public static function modules(): array
    {
        return Config::get('enso.flexible-content.rows.accordion.modules', []);
    }

    /**
     * Unpack Row-specific fields.
     *
     * @param FlexibleRow $row
     *
     * @return array
     */
    protected static function getRowContent(FlexibleRow $row): array
    {
        $handler = app(FlexibleFieldHandler::class);
        $handler->loadData($row->blockContent('accordion'), '');

        return [
            'title' => $row->blockContent('title'),
            'accordion_rows' => $handler->getRows()->map(function ($flexible_row) {
                return (object) [
                    'title' => $flexible_row->blockContent('title'),
                    'content' => static::getWysiwygHtml($flexible_row->getBlocks(), 'content'),
                    'file' => $flexible_row->blockContent('file')->first(),
                    'buttons' => static::flexibleFieldContent('buttons', $flexible_row),
                ];
            }),
        ];
    }

    /**
     * Row spec representing a single accordion row.
     *
     * @return FlexibleContentSection
     */
    protected static function singleRowSpec(): FlexibleContentSection
    {
        return FlexibleContentSection::make('accordion_row')
            ->addFields([
                TextField::make('title'),
                WysiwygField::make('content')
                    ->setModules(static::modules()),
                ButtonsField::make('buttons')
                    ->singleButton(),
                FileUploadFieldResumable::make('file')
                    ->setMaxFiles(1),
            ])->withoutCommon();
    }
}
