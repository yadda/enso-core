<?php

namespace Yadda\Enso\Crud\Forms\Rows;

use Illuminate\Support\Arr;
use Yadda\Enso\Crud\Forms\Fields\FileUploadFieldResumable;
use Yadda\Enso\Crud\Forms\Fields\GalleryField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Fields\VideoEmbedField;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

/**
 * Represents a purely text row withing a flexible content collection.
 */
class Media extends FlexibleContentSection
{
    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'media';

    public function __construct(string $name = 'media')
    {
        parent::__construct($name);

        $this->addField(TextField::make('title'));
        $this->addFields(static::mediaFields());
    }

    /**
     * Media content for a given FlexibleRow
     *
     * @param FlexibleRow $row
     * @param string      $suffix
     *
     * @return array
     */
    public static function mediaContent(FlexibleRow $row, $suffix = ''): array
    {
        $external_video = $row->blockContent('external_video');

        return [
            'external_video' . $suffix => Arr::get($external_video, 'id', null)
                ? ((array) $external_video)
                : null,
            'images' . $suffix => $row->blockContent('images'),
            'video' . $suffix => $row->blockContent('video')->first(),
        ];
    }

    /**
     * Fields for a media block
     *
     * @return array
     */
    public static function mediaFields($suffix = '')
    {
        return [
            GalleryField::make('images' . $suffix)
                ->setHelpText(
                    'If an External Video is chosen, only the first image will '
                    . 'be used. Image should be 16:9 ratio and at least 1920 x '
                    . '1080 px in size.'
                ),
            FileUploadFieldResumable::make('video' . $suffix)
                ->setHelpText(
                    'This should be a short (~30sec) 1920 x 1080 px looping '
                    . 'MP4 video using the H264 codec. In order for the '
                    . 'browser to autoplay the video, it must NOT contain a '
                    . 'sound channel. Not just silent audio, but no audio '
                    . 'channel at all.'
                )
                ->addFieldsetClass('is-6'),
            VideoEmbedField::make('external_video' . $suffix)
                ->addFieldsetClass('is-6'),
        ];
    }

    /**
     * Unpack Row-specific fields.
     *
     * @param FlexibleRow $row
     *
     * @return array
     */
    protected static function getRowContent(FlexibleRow $row): array
    {
        return array_merge(
            static::mediaContent($row),
            [
                'title' => $row->blockContent('title'),
            ]
        );
    }
}
