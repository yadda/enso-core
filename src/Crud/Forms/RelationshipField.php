<?php

namespace Yadda\Enso\Crud\Forms;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Exceptions\FieldIntegrityException;
use Yadda\Enso\Crud\Forms\Field;

/**
 * Base relationship field. Used for all fields that define a relationship
 * E.g. images, files as well as plain relationship dropdowns
 */
class RelationshipField extends Field implements HasCorrectTyping
{

    // Name of the relationship, if different from the set name
    protected $relationship_name;

    // Unique key for this relationship item's table
    protected $relationship_key = 'id';

    protected $relationship_class;

    /**
     * Callback to be run on options. This is designed to reduce the amount of
     * data transfered from server to client when using objects and eloquent
     * models with unnecessary data for the operation.
     *
     * @param callable
     */
    protected $data_callback;

    /**
     * Create a new field
     *
     * @param String $name
     */
    public function __construct($name)
    {
        parent::__construct($name);

        $this->data_callback = function ($item) {

            // Unset all relationships to minimise data passed back. If these
            // are actually needed, you could change this callback, but chances
            // are that you need to make something more custom anyway.
            if ($item instanceof EloquentModel) {
                $item->setRelations([]);
            }

            return $item;
        };
    }

    /**
     * Sets the stored name of the relationship
     *
     * @param string    $relationship_name  name to set
     */
    public function setRelationshipName($relationship_name)
    {
        $this->relationship_name = $relationship_name;

        return $this;
    }

    /**
     * Gets the currently set n ame of the relationship
     *
     * @return string                       relationship name
     */
    public function getRelationshipName()
    {
        if (is_null($this->relationship_name)) {
            return Str::camel($this->getName());
        }

        return $this->relationship_name;
    }

    /**
     * Sets the stored name of the unique idenifier field
     *
     * @param string    $relationship_key   name to set
     */
    public function setRelationshipKey($relationship_key)
    {
        $this->relationship_key = $relationship_key;

        return $this;
    }

    /**
     * Gets the column name of the unique identifier field
     *
     * @return string                   column name
     */
    public function getRelationshipKey()
    {
        return $this->relationship_key;
    }

    public function setRelationshipClass($class)
    {
        $this->relationship_class = $class;
        return $this;
    }

    public function getRelationshipClass()
    {
        if (empty($this->relationship_class)) {
            throw new Exception(
                'You are trying to use a RelationshipField to get a class, ' .
                    'but have not yet set the class'
            );
        }

        return $this->relationship_class;
    }

    /**
     * Sets the callback to run over this relation's items in order to convert
     * them into a desired format
     *
     * @param callable  $callback       parsing callback
     */
    public function setDataCallback(callable $callback)
    {
        $this->data_callback = $callback;

        return $this;
    }

    /**
     * Gets the correct value for this field from the passed data
     *
     * @param  object   $item           Data source
     * @param  string   $property_name  Override property name
     * @return mixed                    matched data
     */
    public function getFormData($item, $property_name = null)
    {
        if (!is_object($item)) {
            return $this->getDefaultValue();
        }

        if (isset($property_name)) {
            $property_value = $item->$property_name;

            if (is_null($property_value)) {
                return $this->getDefaultValue();
            }

            if (is_array($property_value)) {
                // @todo - Refactor this to distinguish between data
                //         called for JsonDataSections and Normal ones

                // @todo =======================================================
                // We're hitting this when using this in page data and the image
                // is not getting populated properly....
                // =============================================================

                $value = $property_value;
            } else {
                if (is_array($item->$property_name)) {
                    $value = array_map($this->data_callback, $item->property_name);
                } else {
                    $value = $item->$property_name->map($this->data_callback);
                }
            }
        } else {
            $relationship_name = $this->getRelationshipName();

            $relation = $item->$relationship_name;
            if ($relation instanceof Collection) {
                $value = $relation->map($this->data_callback);
            } elseif (!is_null($relation)) {
                $value = [call_user_func($this->data_callback, $relation)];
            } else {
                $value = $this->getDefaultValue();
            }
        }

        if (!is_null($this->getAlterFormDataCallback())) {
            $value = call_user_func($this->getAlterFormDataCallback(), $value);
        }

        return $value;
    }

    /**
     * Converts this Field into an array of data
     *
     * @return array
     */
    public function toArray()
    {
        $output = parent::toArray();

        $output['props']['relationship'] = $this->getRelationshipName();
        $output['props']['relationship_key'] = $this->getRelationshipKey();

        return $output;
    }

    /**
     * Extendable Field Integrity Check. All fields should extend this, call
     * parent versions and then add their own checks.
     *
     * @return void
     */
    protected function fieldIntegrityCheck()
    {
        parent::fieldIntegrityCheck();

        if (empty($this->getRelationshipName())) {
            throw new FieldIntegrityException(
                'Relationship Fields Must implement a relationship name. ' .
                    'Set this with `setRelationshipName($name)`'
            );
        }
    }
}
