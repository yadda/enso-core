<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use DB;
use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\Fields\HasManyField as BaseRelationshipField;
use Yadda\Enso\Crud\Traits\IsOrderable;

/**
 * An actual relationship field. Allows selecting other item[s] from a dropdown.
 */
class OrderableHasManyField extends BaseRelationshipField implements FieldInterface
{
    use IsOrderable;

    /**
     * Name of the custom vue component
     *
     * @var string
     */
    protected $tag_name = 'enso-field-orderable-relationship';

    /**
     * This relationship requires that the id of the current item be set, and
     * as such must happen after a save for the create route
     *
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     *
     * @return self
     */
    public function applyRequestDataAfterSave(Model &$item, $data)
    {
        $data = $this->getRequestData($data);

        $relation_class = $this->getRelationshipForeignClass();
        $relation_instance = new $relation_class;

        $property_name = $this->getName();
        if (!$item->relationLoaded($property_name)) {
            $item->load($property_name);
        }

        $previous_value = $item->$property_name;

        // Summary: If there is no change, do nothing.
        if (($previous_value->count() === 0 && empty($data)) ||
            ($this->matchingData($previous_value->pluck($this->getSetting('track_by'))->toArray(), $data))
        ) {
            return $this;
        }

        $bindings = [];

        $statement = 'UPDATE `' . $relation_instance->getTable() . '` SET `' .
            $this->getRelationshipForeignKey() . '` = ';
        $statement .= $this->getRelationKeyCaseStatement($item, $data, $bindings);
        $statement .= ', `' . $this->getOrderableOrderBy() . '` = ';
        $statement .= $this->getOrderCaseStatement($item, $data, $bindings);

        $query = DB::statement($statement, $bindings);

        // Make the orders for the new relation-set
        $count = $this->isOrderableDescending() ? count($data) : 0;

        foreach ($data as $relation_id) {
            $relation = $relation_class::find($relation_id);

            $relation->update([$this->getOrderableOrderBy() => $count]);

            if ($this->isOrderableDescending()) {
                $count--;
            } else {
                $count++;
            }
        }

        return $this;
    }

    /**
     * Determines whether two sets of data match.
     *
     * This has been override from it's parent because it is important that
     * a collection of the same keys is only a match if the order is also a
     * match for this type of Field.
     *
     * @param  array    $previous       First data
     * @param  array    $data           Second data
     *
     * @return boolean                  Whether they have the same values
     */
    protected function matchingData(array $previous, array $data)
    {
        return $previous === $data;
    }

    protected function getRelationKeyCaseStatement(Model $item, array $data, array &$bindings)
    {
        // Generates the 'Case' for updating relation foreign keys.
        $statement = 'CASE ';

        if (!empty($data)) {
            foreach ($data as $binding) {
                $bindings[] = $binding;
            }
            $bindings[] = $item->getKey();

            // Creates a string of ?'s with a number equal to the length of data,
            // so that we can use setBindings properly.
            $binding_string = implode(',', array_fill(0, count($data), '?'));
            $statement .= 'WHEN `' . $this->getSetting('track_by') . '` IN (' . $binding_string . ') THEN ? ';
        }

        $bindings[] = $item->getKey();

        $statement .= 'WHEN `' . $this->getRelationshipForeignKey() . '` = ? THEN null ';
        $statement .= 'ELSE `' . $this->getRelationshipForeignKey() . '` END';

        return $statement;
    }

    protected function getOrderCaseStatement(Model $item, array $data, array &$bindings)
    {
        // Generates the 'Case' for updating relation foreign keys.
        $statement = 'CASE ';

        if (!empty($data)) {
            foreach ($data as $binding) {
                $bindings[] = $binding;
            }

            // Creates a string of ?'s with a number equal to the length of data,
            // so that we can use setBindings properly.
            $binding_string = implode(',', array_fill(0, count($data), '?'));
            $statement .= 'WHEN `' . $this->getSetting('track_by') . '` IN (' . $binding_string . ') THEN 0 ';
        }

        $bindings[] = $item->getKey();

        $statement .= 'WHEN `' . $this->getRelationshipForeignKey() . '` = ? THEN 0 ';
        $statement .= 'ELSE `' . $this->getOrderableOrderBy() . '}` END';

        return $statement;
    }
}
