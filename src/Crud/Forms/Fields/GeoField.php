<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Grimzy\LaravelMysqlSpatial\Types\Point;
use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\SectionInterface;

/**
 * Base class for all geospatial field types
 */
class GeoField extends Field implements
    FieldInterface,
    HasCorrectTyping
{
    protected $tag_name = 'enso-field-map';

    protected $application_validation_rules = ['required'];

    protected $use_location      = false;
    protected $use_boundary_box  = false;
    protected $use_zoom          = false;
    protected $use_polygon       = false;
    protected $use_circle_radius = false;

    protected $location_column      = 'map_location';
    protected $boundary_box_column  = 'map_boundary_box';
    protected $zoom_column          = 'map_zoom';
    protected $polygon_column       = 'map_polygon';
    protected $circle_radius_column = 'map_circle_radius';

    /**
     * Center point of the map if no location has been chosen
     *
     * @var Grimzy\LaravelMysqlSpatial\Types\Point
     */
    protected $default_location = null;

    /**
     * Zoom level of the map on page load
     *
     * @var Integer
     */
    protected $default_zoom = 13;

    /**
     * Set the default map location
     *
     * @param Float $lat
     * @param Float $lng
     * @return self
     */
    public function setDefaultLocation($lat, $lng)
    {
        $this->default_location = new Point($lat, $lng);

        return $this;
    }

    /**
     * Get the default location
     *
     * @return Grimzy\LaravelMysqlSpatial\Types\Point
     */
    public function getDefaultLocation()
    {
        return $this->default_location;
    }

    /**
     * Set the default map zoom
     *
     * @param Integer $zoom
     * @return self
     */
    public function setDefaultZoom($zoom)
    {
        $this->default_zoom = $zoom;

        return $this;
    }

    /**
     * Get the default zoom level
     *
     * @return Integer
     */
    public function getDefaultZoom()
    {
        return $this->default_zoom;
    }

    /**
     * Convert the field to an array
     *
     * @return Array
     */
    public function toArray()
    {
        $array = parent::toArray();

        if ($this->default_location) {
            $array['props']['default_location'] = [
                'lat' => $this->default_location->getLat(),
                'lng' => $this->default_location->getLng(),
            ];
        } else {
            $array['props']['default_location'] = null;
        }
        $array['props']['default_zoom'] = $this->default_zoom;

        return $array;
    }
}
