<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use DB;
use Exception;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\Fields\HasOneField;

/**
 * An actual relationship field. Allows selecting other item[s] from a dropdown.
 */
class HasManyField extends HasOneField implements FieldInterface
{
    /**
     * The default value to return if no other value is available
     *
     * @param mixed
     */
    protected $default = [];

    public function __construct($name)
    {
        parent::__construct($name);

        $this->setSettings([
            'multiple' => true,
            'hide_selected' => true,
        ], true);
    }

    /**
     * This relationship requires that the id of the current item be set, and
     * as such must happen after a save for the create route
     *
     * @param Model $item  Item to set data on
     * @param mixed $value Data to set
     *
     * @return self
     */
    public function applyRequestDataAfterSave(Model &$item, $data)
    {
        $data = $this->getRequestData($data);
        $property_name = $this->getName();

        if (!$item->relationLoaded($property_name)) {
            $item->load($property_name);
        }

        $previous_value = $item->$property_name;

        // Summary: If there is no change, do nothing.
        if (($previous_value->count() === 0 && empty($data)) ||
            ($this->matchingData($previous_value->pluck('id')->toArray(), $data))
        ) {
            return $this;
        }

        $raw = 'CASE ';

        if (!empty($data)) {
            $bindings = $data;

            // Creates a string of ?'s with a number equal to the length of data,
            // so that we can use setBindings properly.
            $binding_string = implode(',', array_fill(0, count($data), '?'));
            $raw .= 'WHEN `' . $this->getSetting('track_by') .
                '` IN (' . $binding_string . ') THEN ' . $item->getKey() . ' ';
        }
        $raw .= 'WHEN `' . $this->getRelationshipForeignKey() . '` = ' . $item->getKey() . ' THEN null ';
        $raw .= 'ELSE `' . $this->getRelationshipForeignKey() . '` END';

        $class = $this->getRelationshipForeignClass();
        $instance = new $class;
        $query = DB::table($instance->getTable());

        if (isset($bindings)) {
            $query->setBindings($bindings);
        }

        $query->update([$this->getRelationshipForeignKey() => DB::raw($raw)]);

        return $this;
    }

    /**
     * Determines whether two sets of data match. Data its
     *
     * @param  array    $previous       First data
     * @param  array    $data           Second data
     *
     * @return boolean                  Whether they have the same values
     */
    protected function matchingData(array $previous, array $data)
    {
        sort($previous);
        sort($data);

        return $previous === $data;
    }

    /**
     * Hook for modifying the request data, if required
     *
     * @param  mixed    $data           Original data
     * @return mixed                    Modified data
     */
    protected function modifyRequestData($data)
    {
        if (empty($data) || !is_array($data)) {
            return $this->getDefaultValue();
        }

        $keys = [];
        foreach ($data as $single) {
            $keys[] = $single[$this->getSetting('track_by')];
        }

        return $keys;
    }

    /**
     * Gets the correct value for this field from the passed data
     *
     * @param  object       $item               Data source
     * @param  string       $property_name      Override property name
     *
     * @return mixed                            matched data
     */
    public function getFormData($item, $property_name = null)
    {
        $this->doFieldIntegrityCheck();

        $property_name = $property_name ?? $this->getName();

        $relation = $this->getRelationItemsFromItem($item, $property_name);

        if ($relation->count() === 0) {
            return $this->getDefaultValue();
        } else {
            return $relation->map($this->data_callback);
        }
    }

    /**
     * Attempt to get the correct relationship data for this field's item
     *
     * @param mixed  $item
     * @param string $property_name
     *
     * @return EloquentCollection
     */
    protected function getRelationItemsFromItem($item, $property_name): EloquentCollection
    {
        $relation_class = $this->getRelationshipForeignClass();

        // Get Relation data from item
        try {
            $relation = $item->$property_name;

            // If collection, return that.
            if ($relation instanceof Collection) {
                return $relation;
            }

            // if NOT an array, (given this is a hasManyField), we can assume it's missing
            // or broken.
            if (!is_array($relation) || count($relation) === 0) {
                throw new Exception;
            }
        } catch (Exception $e) {
            return (new $relation_class)->newCollection();
        }

        return $relation_class::whereIn($this->getSetting('track_by'), $relation)->get();
    }
}
