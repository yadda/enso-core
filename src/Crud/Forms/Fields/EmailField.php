<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\SectionInterface;
use Yadda\Enso\Crud\Traits\PurifiesHtml;

class EmailField extends Field implements
    FieldInterface,
    HasCorrectTyping
{
    // This field should usually be validated to only accept valid email
    // address anyway, but if that isn't done (and it's not automatically)
    // then at least this isn't an attack vector.
    use PurifiesHtml;

    protected $tag_name = 'enso-field-email';

    protected $applicable_validation_rules = ['required'];
}
