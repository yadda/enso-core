<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\Fields\FileUploadField;
use Yadda\Enso\Crud\Forms\SectionInterface;
use Yadda\Enso\Media\Models\VideoFile;

class VideoUploadField extends FileUploadField implements FieldInterface
{
    protected $accepted_file_types = [
        'video/*'
    ];

    /**
     * Name of the custom vue component
     *
     * @var string
     */
    protected $tag_name = 'enso-field-video-upload';

    // model that this upload field type instantiated
    protected $file_model = VideoFile::class;

    /**
     * Laravel validation rules which can be converted to html validation rules
     * for the form input
     *
     * @var array
     */
    protected $applicable_validation_rules = [
        'required'
    ];
}
