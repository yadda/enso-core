<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Purifier;
use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Traits\PurifiesHtml;

class WysiwygField extends Field implements
    FieldInterface,
    HasCorrectTyping
{
    use PurifiesHtml;

    protected $tag_name = 'enso-field-wysiwyg';

    protected $applicable_validation_rules = ['required'];

    protected $default = [
        'html' => '',
        'json' => [],
    ];

    protected static $flexible_field = true;

    /**
     * Props to apply to the fields Vue component
     *
     * @var array
     */
    protected $props = [
        'theme' => 'snow',
        'modules' => [
            'toolbar' => [
                [['header' => [1, 2, 3, 4, 5, 6, false]]],
                ['blockquote', 'code-block'],
                ['bold', 'italic', 'underline', 'strike', 'link'],
                [['align' => []]],
                [['color' => []], ['background' => []]],
                ['clean'],
            ],
        ],
    ];

    /**
     * Applies data to a given item. Can be overridden to provide functionality
     * for non-simple data.
     *
     * Note that the JSON is sent back and forth as a string, otherwise Laravel
     * middleware strips out strings that contain only a newline which
     * completely removes a lot of styling. Good times.
     *
     * @todo this doesn't allow setting a value to 0 - it just sets it to default instead
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     */
    public function applyRequestData(Model &$item, $data)
    {
        $field_name = $this->getName();
        $json_field_name = $field_name . '_json';

        $value = $this->getRequestData($data);

        // @todo this could be simplified by keeping wysiwyg html and json
        //       together in a single JSON column
        if ($this->getTranslatable()) {
            $htmls = [];
            $jsons = [];

            foreach ($value as $language => $translation) {
                $htmls[$language] = $translation['html'];
                $jsons[$language] = $translation['json'];
            }

            $item->setTranslations($field_name, $htmls);
            $item->setTranslations($json_field_name, $jsons);
        } else {
            if (!$value['json']) {
                $value['json'] = '[]';
            }

            $item->fill([
                $field_name => $value['html'],
                $json_field_name => $value['json'],
            ]);
        }

        $this->setDirtiness($item->isDirty($field_name));
    }

    /**
     * Get settings for data sanitization
     *
     * @return mixed
     */
    public function getSanitizationSettings()
    {
        return array_merge(
            Config::get('enso.flexible-content.fields.wysiwyg.sanitization-settings', []),
            $this->sanitization_settings
        );
    }

    /**
     * Clean the data before inserting in the database
     *
     * @param mixed $data
     *
     * @return string
     */
    protected function sanitizeData($data)
    {
        if (!$this->getPurifyHTML()) {
            return $data;
        }

        // Fix to prevent fields using this trait from becoming unable to set a
        // value to null.
        if (is_null($data)) {
            return $data;
        }

        $settings = array_merge(
            // Purifier defaults
            Config::get('purifier.settings.default'),
            // Enso forced override
            [
                'Attr.AllowedFrameTargets' => ['_blank'],
                'AutoFormat.AutoParagraph' => false,
            ],
            // Implementation specific
            $this->getSanitizationSettings()
        );

        if ($this->getTranslatable()) {
            foreach ($data as &$value) {
                $value['html'] = Purifier::clean($value['html'], $settings);
            }
        } else {
            $data['html'] = Purifier::clean($data['html'], $settings);
        }

        return $data;
    }

    /**
     * Get the value of this field. For populating forms.
     *
     * @param  object   $item           Data source
     * @param  string   $property_name  Override property name
     * @return mixed                    matched data
     */
    public function getFormData($item, $property_name = null)
    {
        if (empty($property_name)) {
            $property_name = $this->getName();
        }

        // Call the correct section-specific method for getting
        // Form Data.
        $section = $this->getSection();
        $method_name = 'get' . class_basename($section) . 'FormData';
        if (method_exists($this, $method_name)) {
            return $this->$method_name($item, $property_name);
        } else {
            /**
             * This doesn't seem to ever actualy get hit. Flexible Content Sections
             * currently just grab and stash data in the raw format that it's passed in
             * (barring expanding/compressing models to ids)
             *
             * I believe this code has become redundant.
             */
            $response = [];

            try {
                if (property_exists($item, $property_name) && isset($item->$property_name['html'])) {
                    $response['html'] = $item->$property_name['html'];
                } else {
                    $response['html'] = $this->getDefaultValue()['html'];
                }
            } catch (Exception $e) {
                $response['html'] = '';
            }

            try {
                if (property_exists($item, $property_name) && isset($item->$property_name['json'])) {
                    $response['json'] = $item->{$property_name}['json'];
                } else {
                    $response['json'] = $this->getDefaultValue()['json'];
                }
            } catch (Exception $e) {
                $response['json'] = '';
            }
        }

        return $response;
    }

    /**
     * Gets the Correct Data for use in a field that is a child of a
     * regular 'Section'.
     *
     * NOTE: Currently this is stored as two separate database columns,
     *       once with the field name, and onen with the field name, appended
     *       with '_json'. This may disappear in the future in order to keep
     *       wysiwyg fields stored in the same format across Enso, but for now
     *       it is what it is.
     *
     * @param Model $item
     * @param string $property_name
     *
     * @return array
     */
    protected function getSectionFormData($item, $property_name)
    {
        if ($this->getTranslatable()) {
            $htmls = parent::getFormData($item, $property_name);
            $jsons = parent::getFormData($item, $property_name . '_json');

            foreach ($this->getLanguages() as $code => $language) {
                $html = $item->getTranslation($property_name, $code);
                $json = $item->getTranslation($property_name . '_json', $code);

                $response[$code] = [
                    'html' => !empty($html) ? $html : '',
                    'json' => !empty($json) ? $json : [],
                ];
            }
        } else {
            try {
                $html = $item->$property_name;
                $json = $item->{$property_name . '_json'};
                $response = compact('html', 'json');
            } catch (Exception $e) {
                $response = $this->getDefaultValue();
            }
        }

        return $response;
    }

    /**
     * Flexible Content Specific version of getting content for this Field Type
     *
     * @todo This doesn't appear to actually ever get used... need investigating as
     *       to when FlexibleContentFields ceased using their component fields as
     *        data parsers, and whether that's a problem.
     *
     * @param stdClass $item
     * @param string $property_name
     *
     * @return array
     */
    protected function getFlexibleContentSectionData($item, $property_name)
    {
        $response = [];

        try {
            if (property_exists($item, $property_name)) {
                $response['html'] = $item->$property_name;
            } else {
                $response['html'] = $this->getDefaultValue()['html'];
            }
        } catch (Exception $e) {
            $response['html'] = '';
        }

        try {
            if (property_exists($item, $property_name . '_json')) {
                $response['json'] = $item->{$property_name . '_json'};
            } else {
                $response['json'] = $this->getDefaultValue()['json'];
            }
        } catch (Exception $e) {
            $response['json'] = '';
        }

        return $response;
    }

    /**
     * Gets a saved data in the correct format for populating the form
     * e.g. for Page specific content slots. At present data is saved
     * as if it were on a model, so the only thing to alter is the
     * default value, as this 'can' be null if the field doesn't
     * exist.
     *
     * @param Model $item
     * @param string $property_name
     *
     * @return array Data
     */
    protected function getJsonDataSectionFormData($item, $property_name)
    {
        $response = parent::getFormData($item, $property_name);

        if (is_null($response)) {
            $response = $this->getDefaultValue();
        }

        return $response;
    }

    /**
     * Gets a saved data in the correct format for populating the form
     * e.g. for storing in settings
     *
     * @todo Make this work with translations
     *
     * @param Model $item
     * @param string $property_name
     *
     * @return array                Data
     */
    public function getCollectionSectionFormData($item, $property_name = null)
    {
        $form_data = parent::getFormData($item, $property_name);

        if (!$form_data) {
            $data = $this->getDefaultValue();
        } else {
            $html = $form_data['html'] ?? null;
            $json = $form_data['json'] ?? null;

            /**
             * The Wysiwyg field should be able to reconstitue the field data
             * if just one of these two is available, so only return the
             * default value is neither are set.
             */
            if (!$html && !$json) {
                $data = $this->getDefaultValue();
            } else {
                $data = compact('html', 'json');
            }
        }

        return $data;
    }

    /**
     * Set the theme that the editor will use
     *
     * @param string $theme
     * @return self
     */
    public function setTheme(string $theme)
    {
        $this->props['theme'] = $theme;

        return $this;
    }

    /**
     * Get the theme that the editor will use
     *
     * @return string
     */
    public function getTheme()
    {
        return $this->props['theme'];
    }

    /**
     * Helpler method for getting/setting the editor theme
     *
     * @param string $theme
     * @return self|string
     */
    public function theme($theme = null)
    {
        if ($theme) {
            return $this->setTheme($theme);
        } else {
            return $this->getTheme();
        }
    }

    /**
     * Set the editors modules config array
     *
     * @param array $modules
     * @return self
     */
    public function setModules(array $modules)
    {
        $this->props['modules'] = $modules;

        return $this;
    }

    /**
     * Get the editor modules config array
     *
     * @return array
     */
    public function getModules()
    {
        return $this->props['modules'];
    }

    /**
     * Helper method for getting/setting the editor modules config array
     *
     * @param null|array $modules
     * @return self|array
     */
    public function modules($modules = null)
    {
        if ($modules) {
            return $this->setModules($modules);
        } else {
            return $this->getModules();
        }
    }

    /**
     * Set the allowed editor formats
     *
     * @see https://quilljs.com/docs/formats/
     * @param array|null $formats Pass null to reset to default
     * @return self
     */
    public function setFormats(array $formats)
    {
        if (is_null($formats)) {
            unset($this->props['formats']);
        } else {
            $this->props['formats'] = $formats;
        }

        return $this;
    }

    /**
     * Get the allowed editor formats
     *
     * @return array|null
     */
    public function getFormats()
    {
        if (isset($this->props['formats'])) {
            return $this->props['formats'];
        }

        return null;
    }

    /**
     * Helper method for setting/getting the allowed editor formats
     *
     * @param null|array $formats
     * @return self|array
     */
    public function formats($formats = null)
    {
        if ($formats) {
            return $this->setFormats($formats);
        } else {
            return $this->getFormats();
        }
    }

    /**
     * Filters
     */

    /**
     * Gets data to be filtered by text filters from the passed data
     *
     * @param mixed $data
     *
     * @return mixed
     */
    public function getTextData($data)
    {
        return $data['html'];
    }

    /**
     * Returns the filtered data in the correct format
     *
     * @param mixed $data
     * @param mixed $original
     *
     * @return mixed
     */
    public function setTextData($data, $original)
    {
        $original['html'] = $data;

        return $original;
    }
}
