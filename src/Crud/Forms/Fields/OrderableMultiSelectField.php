<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\Fields\MultiSelectField;

class OrderableMultiSelectField extends MultiSelectField implements FieldInterface
{
    /**
     * Name of the custom vue component
     *
     * @var string
     */
    protected $tag_name = 'enso-field-orderable-relationship';
}
