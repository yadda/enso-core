<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Exception;
use Yadda\Enso\Crud\Exceptions\FieldIntegrityException;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\Fields\SelectField;

class MultiSelectField extends SelectField implements FieldInterface
{
    /**
     * The default value to return if no other value is available
     *
     * @param mixed
     */
    protected $default = [];

    /**
     * Can this field be used inside a flexible content field
     *
     * @var boolean
     */
    protected static $flexible_field = true;

    protected $data_packing_type = 'object';

    public function __construct($name, $type = null)
    {
        parent::__construct($name);

        if (!is_null($type)) {
            $this->data_packing_type = $type;
        };

        // Set relevant settings for ths field type (ignoreing deprecation
        // warnings)
        $this->setSettings([
            'multiple' => true,
            'hide_selected' => true,
        ], true);

        $this->pack_callback = function ($data) {
            switch ($this->data_packing_type) {
                case 'fullobject':
                    return collect($data)->map(function ($item) {
                        return (object) $item;
                    });
                case 'object':
                default:
                    return collect($data)->map(function ($item, $index) {
                        return ((object)$item)->{$this->getSetting('track_by')};
                    })->values()->toArray();
            }
        };

        $this->unpack_callback = function ($data) {
            switch ($this->data_packing_type) {
                case 'fullobject':
                case 'object':
                    return collect($data)->map(function ($datum) {
                        return collect(array_filter($this->getFinalOptions(), function ($item) use ($datum) {
                            return ((object)$item)->{$this->getSetting('track_by')} === $datum;
                        }))->first();
                    })->filter()->values()->toArray();
                default:
                    if (class_exists($this->data_packing_type)) {
                        // Find items from the data
                        $items = $this->data_packing_type::whereIn($this->getSetting('track_by'), $data)->get();

                        // Map the data to get the items in the correct order.
                        return collect($data)->map(function ($datum) use ($items) {
                            return $items->first(function ($item) use ($datum) {
                                return $item->{$this->getSetting('track_by')} === $datum;
                            });
                        })->filter()->map($this->data_callback);
                    }
            }

            return $data;
        };

        $this->data_callback = function ($item) {
            return [
                $this->getSetting('track_by') => ((object) $item)->{$this->getSetting('track_by')},
                $this->getSetting('label') => ((object) $item)->{$this->getSetting('label')}
            ];
        };
    }

    /**
     * Setter for Ajax specific functionality
     *
     * @param  string   $route      route to call for ajax items
     * @param  mixed    $method     Model class, or callback
     * @return self
     */
    public function useAjax($route, $method)
    {
        $this->settings['ajax_url'] = $route;

        // Accepts either classname or a callable.
        if (is_string($method)) {
            $this->ajax_callback = function ($value) use ($method) {
                $items = $method::whereIn($this->getSetting('track_by'), $value)->get();

                if (is_null($items)) {
                    return $this->getDefaultValue();
                }

                return $items->map($this->data_callback);
            };
            return $this;
        } elseif (is_callable($method)) {
            $this->ajax_callback = $method;
            return $this;
        }

        throw new FieldIntegrityException('Unknown parameter type '. gettype($method) .' passed to useAjax()');
    }

    /**
     * Gets the correct value for this field from the passed data
     *
     * @param  object       $item               Data source
     * @param  string       $property_name      Override property name
     *
     * @return mixed                            matched data
     */
    public function getFormData($item, $property_name = null)
    {
        $this->doFieldIntegrityCheck();

        $property_name = $property_name ?? $this->getName();

        try {
            $property_value = $item->$property_name;
        } catch (Exception $e) {
            $property_value = $this->getDefaultValue();
        } finally {
            if (empty($property_value)) {
                return $this->getDefaultValue();
            }
        }

        if ($this->isAjax()) {
            return call_user_func($this->ajax_callback, $property_value);
        } else {
            return call_user_func($this->unpack_callback, $property_value);
        }
    }

    /**
     * Hook for modifying the request data, if required
     *
     * @param  mixed    $data           Original data
     * @return mixed                    Modified data
     */
    protected function modifyRequestData($data)
    {
        if (empty($data) || !is_array($data)) {
            return $this->getDefaultValue();
        }

        return call_user_func($this->pack_callback, $data);
    }

    /**
     * Gets a data type array to be used to store items to a CollectionSection
     *
     * @return array                Data type information
     */
    public function getDataType()
    {
        $data_type = [
            'type' => 'multiple',
            'content' => [
                'key' => $this->getSetting('track_by'),
            ],
        ];

        if ($this->getOptions() instanceof Builder) {
            $data_type['content']['class'] = get_class($this->getOptions()->getModel());
        }

        return $data_type;
    }
}
