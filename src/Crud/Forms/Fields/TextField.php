<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Traits\PurifiesHtml;

class TextField extends Field implements
    FieldInterface,
    HasCorrectTyping
{
    use PurifiesHtml;

    protected $default = '';

    protected $tag_name = 'enso-field-text';

    protected $applicable_validation_rules = ['required'];

    protected static $flexible_field = true;

    protected $purify_html = false;

    /**
     * Filters
     */

    /**
     * Gets data to be filtered by text filters from the passed data
     *
     * @param mixed $data
     *
     * @return mixed
     */
    public function getTextData($data)
    {
        return $data;
    }

    /**
     * Returns the filtered data in the correct format
     *
     * @param mixed $data
     * @param mixed $original
     *
     * @return mixed
     */
    public function setTextData($data, $original)
    {
        return $data;
    }
}
