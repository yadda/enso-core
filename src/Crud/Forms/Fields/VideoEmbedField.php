<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;

class VideoEmbedField extends Field implements FieldInterface, HasCorrectTyping
{
    protected $default = [
        'type' => null,
        'id' => null,
        'canonical_url' => null,
        'options' => [],
    ];

    protected $tag_name = 'enso-field-video-embed';

    protected static $flexible_field = true;
}
