<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Yadda\Enso\Crud\Exceptions\FieldIntegrityException;
use Yadda\Enso\Crud\Forms\Fields\FlexibleContentField;

abstract class RelationshipRepeaterField extends FlexibleContentField
{
    protected $default = [];

    protected $relation_parsing_callback;
    protected $request_parsing_callback;
    protected $request_application_callback;
    protected $relation_application_callback;

    protected $blank_field_model;
    protected $blank_relationship_relation;
    protected $relation_primary_key;

    protected $delete_removed = false;

    /**
     * Create a new field
     *
     * @param String $name
     */
    public function __construct($name)
    {
        parent::__construct($name);

        $this->setRelationParsingCallback(function ($item, $field_names) {
            return $item;
        });

        $this->setRequestParsingCallback(function ($data) {
            return $data;
        });

        $this->setRequestApplicationCallback(function ($parent, $item, $data, $index) {
            $item->fill($data)->save();
            return $item;
        });
    }

    /**
     * Gets a blank version on the Model that this field is action on
     *
     * @param  boolean      $reset              Whether to force reset the local
     *                                          stored data for this option
     *
     * @return Model
     */
    protected function getBlankFieldModel($reset = false)
    {
        if (is_null($this->blank_field_model) || $reset) {
            if (!class_exists($model_class = $this->getSection()->getModel())) {
                throw new FieldIntegrityException('Class `' . $model_class . '` does not exist');
            }

            $this->blank_field_model = new $model_class;
        }

        return $this->blank_field_model;
    }

    /**
     * Gets the blank instance of the related model
     *
     * @param  boolean      $reset              Whether to force reset the local
     *                                          stored data for this option
     *
     * @return Model
     */
    protected function getBlankRelationshipModel($reset = false)
    {
        if (is_null($this->blank_relationship_relation) || $reset) {
            $model_instance = $this->getBlankFieldModel();
            $property_name = $this->getName();
            $this->blank_relationship_relation = $model_instance->$property_name()->getRelated();
        }

        return $this->blank_relationship_relation;
    }

    /**
     * Gets the name of the Primary key of the related Model
     *
     * @param  boolean      $reset              Whether to force reset the local
     *                                          stored data for this option
     *
     * @return string
     */
    protected function getRelationPrimaryKeyName($reset = false)
    {
        if (is_null($this->relation_primary_key) || $reset) {
            $this->relation_primary_key = $this->getBlankRelationshipModel()->getKeyName();
        }

        return $this->relation_primary_key;
    }

    /**
     * Returns the row spec pertinient to this relatinoship repeated field.
     *
     * @return RowSpec
     */
    public function getRelationshipRowSpec()
    {
        if (!$this->hasRowSpec($this->getName())) {
            throw new Exception('Field Name AND Row Spec name should be the relationship name');
        }

        return $this->getRowSpec($this->getName());
    }

    public function setRelationParsingCallback(callable $callback)
    {
        $this->relation_parsing_callback = $callback;
        return $this;
    }

    public function getRelationParsingCallback()
    {
        return $this->relation_parsing_callback;
    }

    public function setRequestParsingCallback(callable $callback)
    {
        $this->request_parsing_callback = $callback;
        return $this;
    }

    public function getRequestParsingCallback()
    {
        return $this->request_parsing_callback;
    }

    public function setRequestApplicationCallback(callable $callback)
    {
        $this->request_application_callback = $callback;
        return $this;
    }

    public function getRequestApplicationCallback()
    {
        return $this->request_application_callback;
    }

    public function setRelationApplicationCallback(callable $callback)
    {
        $this->relation_application_callback = $callback;
        return $this;
    }

    public function getRelationApplicationCallback()
    {
        return $this->relation_application_callback;
    }

    public function getDeleteRemoved()
    {
        return $this->delete_removed;
    }

    public function setDeleteRemoved($value)
    {
        $this->delete_removed = $value;
        return $this;
    }

    /**
     * Gets the correct value for this field to populate the form
     *
     * @param  object   $item           Data source
     * @param  string   $property_name  Override property name
     * @return mixed                    matched data
     */
    public function getFormData($item, $property_name = null)
    {
        $this->doFieldIntegrityCheck();

        $rowspec = $this->getRelationshipRowSpec();

        if (!method_exists($item, $this->getName())) {
            throw new Exception('Field Name Must be A Relationship that Exists on the Model Instance');
        }

        $field_names = array_map(function ($field) {
            return $field->getName();
        }, $rowspec->getFields()->all());

        $field_data = [];

        $relationship = $item->{$this->getName()};

        if (is_null($this->getRelationParsingCallback())) {
            throw new Excpetion(
                'Must Specify a callback to convert Relation Instance into data array for FlexibleField'
            );
        }

        foreach ($relationship as $related_item) {
            $item_data = [
                'type' => $rowspec->getName(),
                'fields' => [],
            ];

            $related_item = call_user_func($this->getRelationParsingCallback(), $related_item, $field_names);

            foreach ($rowspec->getFields() as $field) {
                try {
                    $value = $field->getFormData($related_item);
                } catch (Exception $e) {
                    throw new Exception('RowSpec Field Name must match a property on the Related Model Instances');
                }

                $item_data['fields'][$field->getName()] = [
                    'class' => '',
                    'field' => get_class($field),
                    'content' => $value,
                    'component' => $field->getComponent(),
                ];
            }

            $field_data[] = $item_data;
        }

        return $field_data;
    }

    /**
     * Gets the appropriate piece of data for saving this field to the database.
     * Data comes from the data array, based on field_name.
     *
     * @param  array    $data           data to find value from
     * @param  array    $section_name   Section name override
     * @param  string   $field_name     data name to fetch if not using current
     *                                  field's name
     * @return mixed                    found value
     */
    public function getRequestData($data, $section_name = null, $field_name = null)
    {
        $data = parent::getRequestData($data, $section_name, $field_name);

        $data = array_map(function ($row_data) {
            $content = [];

            foreach ($row_data['fields'] as $field_name => $field_data) {
                $content[$field_name] = $field_data['content'];
            }

            return $content;
        }, $data);

        if (!is_null($this->getRequestParsingCallback())) {
            foreach ($data as $index => $data_set) {
                $data[$index] = call_user_func($this->getRequestParsingCallback(), $data_set);
            }
        }

        return $data;
    }

    /**
     * Applies data to a given item. Can be overridden to provide functionality
     * for non-simple data.
     *
     * @todo this doesn't allow setting a value to 0 - it just sets it to default instead
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     */
    public function applyRequestData(Model &$item, $data)
    {
        //
    }

    /**
     * Applies data to a given item after a save event has occured. This is to
     * provide a place to perform relationship updated
     *
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     */
    public function applyRequestDataAfterSave(Model &$item, $data)
    {
        $field_name = $this->getName();

        $data = $this->getRequestData($data);

        $current_relations = $item->{$field_name};
        $new_current_relations = new Collection;

        foreach ($data as $index => $relation_data) {
            if ($current_relations->contains(
                $this->getRelationPrimaryKeyName(),
                $relation_data[$this->getRelationPrimaryKeyName()]
            )) {
                $relation = $current_relations->first(function ($model) use ($relation_data) {
                    // Keys can be integers (when using classic `id`) or strings
                    // (when using uuids), so non-strict compare required
                    return $model->getKey() == $relation_data[$this->getRelationPrimaryKeyName()];
                });
            } else {
                $relation = clone ($this->getBlankRelationshipModel());
            }

            $relation = call_user_func($this->getRequestApplicationCallback(), $item, $relation, $relation_data, $index);

            $new_current_relations->put($relation->getKey(), $relation);
        }

        $removed_relations = $current_relations
            ->keyBy($this->getRelationPrimaryKeyName())
            ->diffKeys($new_current_relations);

        call_user_func($this->getRelationApplicationCallback(), $item, $new_current_relations, $removed_relations);

        // Shouldn't need to set Dirty here, as it's relation data is stored on
        // other tables and is already done
        // $this->setDirtiness($item->isDirty($field_name));
    }
}
