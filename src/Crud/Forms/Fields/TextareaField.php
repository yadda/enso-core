<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Traits\PurifiesHtml;

class TextareaField extends Field implements
    FieldInterface,
    HasCorrectTyping
{
    use PurifiesHtml;

    protected $tag_name = 'enso-field-textarea';

    protected $applicable_validation_rules = ['required'];

    protected $rows = 10; // Default value

    protected $default = '';

    protected static $flexible_field = true;

    protected $sanitization_settings = [
        'HTML.Doctype'             => 'HTML 4.01 Transitional',
        'HTML.Allowed'             => 'div,b,strong,i,em,u,a[href|title],ul,ol,' .
            'li,p[style],br,span[style],img[width|height|alt|src]',
        'CSS.AllowedProperties'    => 'font,font-size,font-weight,font-style,' .
            'font-family,text-decoration,padding-left,color,background-color,text-align',
        'AutoFormat.AutoParagraph' => false,
        'AutoFormat.RemoveEmpty'   => true,
    ];

    protected $purify_html = false;

    /**
     * Sets the number of rows that a text area should take up
     *
     * @param integer   $rows       number of rows to show
     */
    public function setRows($rows)
    {
        $this->rows = (int) $rows;
        return $this;
    }

    /**
     * Returns the currently set number of rows
     *
     * @return integer              number of rows
     */
    protected function getRows()
    {
        return $this->rows;
    }

    /**
     * Convert the field to an array
     *
     * @return Array
     */
    public function toArray()
    {
        $output = parent::toArray();
        $output['rows'] = $this->rows;

        return $output;
    }

    /**
     * Filters
     */

    /**
     * Gets data to be filtered by text filters from the passed data
     *
     * @param mixed $data
     *
     * @return mixed
     */
    public function getTextData($data)
    {
        return $data;
    }

    /**
     * Returns the filtered data in the correct format
     *
     * @param mixed $data
     * @param mixed $original
     *
     * @return mixed
     */
    public function setTextData($data, $original)
    {
        return $data;
    }
}
