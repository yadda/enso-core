<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Exception;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;

/**
 * A field for showing lists of data as read only
 */
class ListField extends field implements FieldInterface
{
    protected $tag_name = 'enso-field-list';

    protected $data_getter;

    public function setDataGetter($callback)
    {
        $this->data_getter = $callback;

        return $this;
    }

    /**
     * Gets the correct value for this field from the passed data
     *
     * @param  object   $item           Data source
     * @param  string   $property_name  Override property name
     * @return mixed                    matched data
     */
    public function getFormData($item, $property_name = null)
    {
        if (is_null($this->data_getter)) {
            throw new Exception('ListField data_getter is not set.');
        }

        return call_user_func($this->data_getter, $item);
    }
}
