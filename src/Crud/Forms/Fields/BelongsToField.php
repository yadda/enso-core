<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\Fields\RelationshipField;

/**
 * An actual relationship field. Allows selecting other item[s] from a dropdown.
 */
class BelongsToField extends RelationshipField implements FieldInterface
{
    /**
     * Name of the foreign key for this relationship
     *
     * @var string
     */
    protected $relationship_foreign_key;

    /**
     * Gets the name of the column on the database that will be used to store
     * the key of the related object
     *
     * @param boolean $reset Whether to force new isntance
     *
     * @return string
     */
    public function getRelationshipForeignKey($reset = false): string
    {
        return $this->getRelationshipRelation($reset)->getForeignKeyName();
    }

    /**
     * Unlike other relationships, belongsTo only updates data on the model's
     * own table, so it can be called before performing a save.
     *
     * @param Model $item  Item to set data on
     * @param mixed $value data to set
     *
     * @return self
     */
    public function applyRequestData(Model &$item, $data): BelongsToField
    {
        $data = $this->getRequestData($data);

        $column_name = $this->getRelationshipForeignKey();
        $item->$column_name = $data;

        $this->setDirtiness($item->isDirty($column_name));

        return $this;
    }

    /**
     * As this field only saves data the the table of the actual model being
     * updated, no functionality is required to happen post-save
     *
     * @param Model $item  Item to set data on
     * @param mixed $value data to set
     *
     * @return self
     */
    public function applyRequestDataAfterSave(Model &$item, $data): BelongsToField
    {
        return $this;
    }

    /**
     * Gets the correct value for this field from the passed data
     *
     * @param object $item          data source
     * @param string $property_name override property name
     *
     * @return mixed matched data
     */
    public function getFormData($item, $property_name = null)
    {
        $this->doFieldIntegrityCheck();

        $property_name = $property_name ?? $this->getName();

        if (!$item->relationLoaded($property_name)) {
            $item->load($property_name);
        }

        if (is_null($relation = $item->$property_name)) {
            return $this->getDefaultValue();
        } else {
            return call_user_func($this->data_callback, $relation);
        }
    }
}
