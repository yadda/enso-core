<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Money\Money;
use MoneyFormatter;
use MoneyParser;
use Yadda\Enso\Crud\Forms\Fields\TextField;

class MoneyField extends TextField
{
    /**
     * Hook for modifying the request data, if required
     *
     * @param  mixed    $data           Original data
     * @return mixed                    Modified data
     */
    protected function modifyRequestData($data)
    {
        return $this->formatToPence($data);
    }

    /**
     * Hook for modifying the form data, if required
     *
     * @param  mixed    $data           Original data
     * @return mixed                    Modified data
     */
    protected function modifyFormData($data)
    {
        return $this->formatFromPence($data);
    }

    /**
     * Format a number in pence to a money string
     *
     * @param Integer $pence
     * @return String
     */
    public function formatFromPence($pence)
    {
        $currency = config('enso.settings.currency', 'GBP');
        $money = Money::$currency($pence);
        return MoneyFormatter::format($money);
    }

    /**
     * Convert a money string to an number of pence
     *
     * @param String $value
     * @return Integer
     */
    public function formatToPence($value)
    {
        $currency = config('enso.settings.currency', 'GBP');
        $money = MoneyParser::parse($value, $currency);
        return $money->getAmount();
    }
}
