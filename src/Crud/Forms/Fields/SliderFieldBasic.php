<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\SectionInterface;

class SliderFieldBasic extends Field implements FieldInterface
{
    protected $options = [];

    /**
     * Name of the custom vue component
     *
     * @var string
     */
    protected $tag_name = 'enso-field-slider-basic';

    /**
     * Laravel validation rules which can be converted to html validation rules
     * for the form input
     *
     * @var array
     */
    protected $applicable_validation_rules = [
        'required'
    ];

    /**
     * Sets the internal options array to a given array
     *
     * @param array     $options        Options to set
     */
    public function setOptions($options = [])
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Applies data to a given item. Can be overriden to provide funcionality
     * for non-simple data.
     *
     * @todo There is a bug in Field that prevents setting this value to zero.
     *  So i've overridden this method for now. Once it's fixed in Field we can
     *  remove this method from here.
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     */
    public function applyRequestData(Model &$item, $data)
    {
        $field_name = $this->getName();

        $value = $this->getRequestData($data);

        $item->fill([$field_name => $value]);
    }

    /**
     * Convert the field to an array
     *
     * @return Array
     */
    public function toArray()
    {
        $output = parent::toArray();
        $output['props']['options'] = $this->options;

        return $output;
    }
}
