<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use DB;
use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\Fields\RelationshipField;

/**
 * An actual relationship field. Allows selecting other item[s] from a dropdown.
 */
class HasOneField extends RelationshipField implements FieldInterface
{
    protected $relationship_relation;

    /**
     * Returns a blank instance of the Class that this relationship refers to
     *
     * @param  boolean      $reset              Whether to force new instance
     *
     * @return object
     */
    protected function getRelationshipRelation($reset = false)
    {
        if (is_null($this->relationship_relation) || $reset) {
            $model_instance = $this->getOwnerModelInstance();
            $property_name = $this->getName();
            $this->relationship_relation = $model_instance->$property_name();
        }

        return $this->relationship_relation;
    }

    /**
    * Gets the Class of the foreign participant of this relationship.
    *
    * @param  boolean      $reset              Whether to force new instance
    *
    * @return string
    */
    public function getRelationshipForeignClass($reset = false)
    {
        $relation_instance = $this->getRelationshipRelation($reset)->getRelated();
        return get_class($relation_instance);
    }

    /**
    * Gets the name of the column on the database that will be used to store
    * the key of the related object
    *
    * @param  boolean      $reset              Whether to force new isntance
    *
    * @return string
    */
    public function getRelationshipForeignKey($reset = false)
    {
        return $this->getRelationshipRelation($reset)->getForeignKeyName();
    }

    /**
     * This relationship requires that the id of the current item be set, and
     * as such must happen after a save for the create route
     *
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     *
     * @return self
     */
    public function applyRequestData(Model &$item, $data)
    {
    }

    /**
    * This relationship requires that the id of the current item be set, and
    * as such must happen after a save for the create route
     *
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     *
     * @return self
     */
    public function applyRequestDataAfterSave(Model &$item, $data)
    {
        $data = $this->getRequestData($data);

        $property_name = $this->getName();
        if (!$item->relationLoaded($property_name)) {
            $item->load($property_name);
        }

        $previous_value = $item->$property_name;

        // Summary: If there is no change, do nothing.
        if ((is_null($previous_value) && empty($data)) ||
            (!is_null($previous_value) && $previous_value->{$this->getSetting('track_by')} === $data)
        ) {
            return $this;
        }

        $raw = 'CASE ';

        if (!empty($data)) {
            $bindings = ['data' => $data];
            $raw .= 'WHEN `' . $this->getSetting('track_by') . '` = :data THEN ' . $item->getKey() . ' ';
        }
        $raw .= 'WHEN `' . $this->getRelationshipForeignKey() . '` = ' . $item->getKey() . ' THEN null ';
        $raw .= 'ELSE `' . $this->getRelationshipForeignKey() . '` END';

        $class = $this->getRelationshipForeignClass();
        $instance = new $class;
        $query = DB::table($instance->getTable());

        if (isset($bindings)) {
            $query->setBindings($bindings);
        }

        $query->update([$this->getRelationshipForeignKey() => DB::raw($raw)]);

        return $this;
    }

    /**
     * Gets the correct value for this field from the passed data
     *
     * @param  object       $item               Data source
     * @param  string       $property_name      Override property name
     *
     * @return mixed                            matched data
     */
    public function getFormData($item, $property_name = null)
    {
        $this->doFieldIntegrityCheck();

        $property_name = $property_name ?? $this->getName();

        if (!$item->relationLoaded($property_name)) {
            $item->load($property_name);
        }

        if (is_null($relation = $item->$property_name)) {
            return $this->getDefaultValue();
        } else {
            return call_user_func($this->data_callback, $relation);
        }
    }
}
