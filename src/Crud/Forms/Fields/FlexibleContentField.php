<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Yadda\Enso\Crud\Contracts\FlexibleFieldParser as FlexibleFieldParserContract;
use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Exceptions\FieldIntegrityException;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Forms\SectionInterface;
use Yadda\Enso\Crud\Traits\FieldHasRowSpecs;

class FlexibleContentField extends Field implements
    FieldInterface,
    HasCorrectTyping
{
    use FieldHasRowSpecs;

    /**
     * Vue component to use to render this field in a form
     *
     * @var string
     */
    protected $tag_name = 'enso-field-flexible-content';

    protected $applicable_validation_rules = [];

    /**
     * The default value to return if no other value is available
     *
     * @param mixed
     */
    protected $default = [];

    /**
     * SectionInterfaces to be treated like row specifications
     *
     * @var \Illuminate\Support\Collection
     */
    protected $row_specs;

    /**
     * Can this field be used inside a flexible content field
     *
     * @var boolean
     */
    protected static $flexible_field = true;

    protected $allow_row_deletion = true;

    protected $is_orderable = true;

    protected $use_drag_handle = true;

    protected $max_rows = null;

    protected $add_row_label = "Add row";

    public function __construct(string $name = 'main')
    {
        parent::__construct($name);

        $this->row_specs = new Collection;
    }

    /**
     * Gets the correct value for this field to populate the form
     *
     * @param object $item          Data source
     * @param string $property_name Override property name
     *
     * @return mixed matched data
     */
    public function getFormData($item, $property_name = null)
    {
        $this->doFieldIntegrityCheck();

        $helper = resolve(FlexibleFieldParserContract::class);

        if (is_null($property_name)) {
            $property_name = $this->getName();
        }

        $data = parent::getFormData($item, $property_name);

        if (empty($data)) {
            $data = $this->getDefaultValue();
        }

        if ($this->getTranslatable()) {
            $output = [];

            foreach ($this->getLanguages() as $code => $language) {
                if (isset($data[$code])) {
                    $output[$code] = $helper->expand($data[$code]);
                } else {
                    $output[$code] = [];
                }
            }

            return $output;
        } else {
            return $helper->expand($data);
        }
    }

    /**
     * Expand the stored JSON version of this field
     * for use in forms
     *
     * @param string $field JSON version from database
     *
     * @return void
     */
    public static function expandForJson($field)
    {
        if (!static::$flexible_field) {
            throw new Exception('Field type can not be used inside flexible content.');
        }

        $helper = resolve(FlexibleFieldParserContract::class);
        $field['content'] = $helper->expand($field['content']);
        return $field;
    }

    /**
     * Gets the appropriate piece of data for saving this field to the database.
     * Data comes from the data array, based on field_name.
     *
     * @todo this code is very much not DRY and should be refactored
     *
     * @param array  $data         Data to find value from
     * @param array  $section_name Section name override
     * @param string $field_name   Data name to fetch if not using current field's name
     *
     * @return mixed found value
     */
    public function getRequestData($data, $section_name = null, $field_name = null)
    {
        $helper = resolve(FlexibleFieldParserContract::class);

        $uncompressed_data = parent::getRequestData($data, $section_name, $field_name);

        if ($this->getTranslatable()) {
            // Pluck JUST the data for each row.
            $item_data = array_map(function ($lang) {
                return array_map(function ($uncompressed_datum) {
                    $content = [];

                    foreach ($uncompressed_datum['fields'] as $field_name => $field_data) {
                        $content[$field_name] = $field_data['content'];
                    }

                    return $content;
                }, $lang);
            }, $uncompressed_data);

            // Run any Field specific alterations on each row.
            foreach ($uncompressed_data as $code => $datum) {
                foreach ($datum as $index => $row) {
                    $spec_name = $row['type'];
                    $rowspec = $this->getRowSpec($spec_name);
                    $rowspec_fields = $rowspec->getFields();

                    // Wrap data in a 'fake' section name so that we can use the field's
                    // full `getRequestData` functionality.
                    $row_data = [$rowspec->getName() => $item_data[$code][$index]];

                    foreach ($rowspec_fields as $rowspec_field) {
                        $uncompressed_data[$code][$index]['fields'][$rowspec_field->getName()]['content']
                            = $rowspec_field->getRequestData($row_data, $rowspec->getName());
                    }
                }
            }
        } else {
            // Pluck JUST the data for each row.
            $item_data = array_map(function ($uncompressed_datum) {
                $content = [];

                foreach ($uncompressed_datum['fields'] as $field_name => $field_data) {
                    $content[$field_name] = $field_data['content'];
                }

                return $content;
            }, $uncompressed_data);

            // Run any Field specific alterations on each row.
            foreach ($uncompressed_data as $index => $row) {
                $spec_name = $row['type'];
                $rowspec = $this->getRowSpec($spec_name);
                $rowspec_fields = $rowspec->getFields();

                // Wrap data in a 'fake' section name so that we can use the field's
                // full `getRequestData` functionality.
                $row_data = [$rowspec->getName() => $item_data[$index]];
                foreach ($rowspec_fields as $rowspec_field) {
                    $uncompressed_data[$index]['fields'][$rowspec_field->getName()]['content']
                        = $rowspec_field->getRequestData($row_data, $rowspec->getName());
                }
            }
        }

        if ($this->getTranslatable()) {
            $compressed_data = [];

            foreach ($uncompressed_data as $code => $datum) {
                $compressed_data[$code] = $helper->compress($datum);
            }
        } else {
            // Run static, field-type compression on data returned (Eloquent objects
            // to ids, etc etc)
            $compressed_data = $helper->compress($uncompressed_data);
        }

        return $compressed_data;
    }

    /**
     * Gets the internal row_specs Collection
     *
     * @return \Illuminate\Support\Collection
     */
    public function getRowSpecs()
    {
        return $this->row_specs;
    }

    /**
     * Returns the interal row_specs, in the array format required for the associated
     * vue component
     *
     * @return array
     */
    public function getRowSpecsArray()
    {
        $row_spec_array = [];

        foreach ($this->getRowSpecs() as $row_spec) {
            $row_spec_array[$row_spec->getName()] = $row_spec->toArray();
        }

        return $row_spec_array;
    }

    /**
     * Boolean check to determine whether this field already has a rowspec with
     * a given name
     *
     * @param string $spec_name
     *
     * @return bool
     */
    public function hasRowSpec($spec_name)
    {
        return !!$this->getRowSpec($spec_name);
    }

    /**
     * Gets a field by it's name property from this section
     *
     * @param string $spec_name Name to check for
     *
     * @return \Yadda\Enso\Crud\Forms\SectionInterface|null
     */
    public function getRowSpec(string $spec_name)
    {
        return $this->row_specs->filter(function ($row_spec) use ($spec_name) {
            return $row_spec->getName() === $spec_name;
        })->first();
    }

    /**
     * Extracts and returns a row spec from the internal row_specs list.
     *
     * @param string $name
     *
     * @return \Yadda\Enso\Crud\Forms\SectionInterface|null
     */
    public function extractRowSpec($name)
    {
        $index = $this->getRowSpecIndex($name);

        if ($index !== false) {
            return $this->row_specs->splice($index, 1)->first();
        }

        return null;
    }

    /**
     * Removes a row_spec with a specified name, if present
     *
     * @param string $spec_name Name of row spec to remove
     *
     * @return self
     */
    public function removeRowSpec(string $spec_name)
    {
        $this->row_specs = $this->row_specs->reject(function ($item) use ($spec_name) {
            return $item->getName() === $spec_name;
        })->values();

        return $this;
    }

    /**
     * Appends a given row_spec to the internal RowSpecs collection
     *
     * @param \Yadda\Enso\Crud\Forms\SectionInterface $row_spec
     *
     * @return self
     */
    public function addRowSpec(SectionInterface $row_spec)
    {
        if ($this->hasRowSpec($row_spec->getName())) {
            throw new Exception(
                'Cannot add RowSpec with duplicate name: \''
                    . $row_spec->getName() . '\' to FlexibleContentField \''
                    . $this->getName() . '\''
            );
        }

        $this->row_specs->push($row_spec);

        return $this;
    }

    /**
     * Calls addRowSpec for an array of RowSpecs
     *
     * @param array $row_specs Instances of SectionInterface to add
     *
     * @return self
     */
    public function addRowSpecs(array $row_specs)
    {
        foreach ($row_specs as $key => $row_spec) {
            if (gettype($key) === 'string') {
                $this->addRowSpec(
                    FlexibleContentSection::make($key)
                        ->addFields($row_spec)
                );
            } else {
                $this->addRowSpec($row_spec);
            }
        }

        return $this;
    }

    /**
     * Adds a RowSpec after a field with the given name.
     *
     * @param string                                  $spec_name
     * @param \Yadda\Enso\Crud\Forms\SectionInterface $row_spec
     *
     * @return self
     */
    public function addRowSpecAfter(string $spec_name, SectionInterface $row_spec)
    {
        return $this->addRowSpecsAfter($spec_name, [$row_spec]);
    }

    /**
     * Adds an array of RowSpecs after a field with the given name.
     *
     * @param string $spec_name
     * @param array  $row_spec
     *
     * @return self
     */
    public function addRowSpecsAfter(string $spec_name, array $row_specs)
    {
        $index = $this->getRowSpecIndex($spec_name);

        if ($index === false) {
            throw new Exception('Cannot add RowSpec after one which does not exist.');
        }

        $this->row_specs->splice($index + 1, 0, $row_specs);

        return $this;
    }

    /**
     * Adds a RowSpec before a RowSpec with the given name.
     *
     * @param string                                  $spec_name
     * @param \Yadda\Enso\Crud\Forms\SectionInterface $row_spec
     *
     * @return self
     */
    public function addRowSpecBefore(string $spec_name, SectionInterface $row_spec)
    {
        return $this->addRowSpecsBefore($spec_name, [$row_spec]);
    }

    /**
     * Adds a RowSpec before a RowSpec with the given name.
     *
     * @param string $spec_name
     * @param array  $row_spec
     *
     * @return self
     */
    public function addRowSpecsBefore(string $spec_name, array $row_specs)
    {
        $index = $this->getRowSpecIndex($spec_name);

        if ($index === false) {
            throw new Exception('Cannot add RowSpec before one which does not exist.');
        }

        $this->row_specs->splice($index, 0, $row_specs);

        return $this;
    }

    /**
     * Adds a RowSpec at the beginning of the RowSpec collection.
     *
     * @param \Yadda\Enso\Crud\Forms\SectionInterface $row_spec
     *
     * @return self
     */
    public function prependRowSpec(SectionInterface $row_spec)
    {
        if ($this->hasRowSpec($row_spec->getName())) {
            throw new Exception(
                'Cannot add RowSpec with duplicate name: \''
                    . $row_spec->getName() . '\' to FlexibleContentField \''
                    . $this->getName() . '\''
            );
        }

        $this->row_specs->prepend($row_spec);

        return $this;
    }

    /**
     * Adds a RowSpec at the end of the RowSpec collection.
     *
     * @param \Yadda\Enso\Crud\Forms\SectionInterface $row_spec
     *
     * @return self
     */
    public function appendRowSpec(SectionInterface $row_spec)
    {
        if ($this->hasRowSpec($row_spec->getName())) {
            throw new Exception(
                'Cannot add RowSpec with duplicate name: \''
                    . $row_spec->getName() . '\' to FlexibleContentField \''
                    . $this->getName() . '\''
            );
        }

        $this->row_specs->push($row_spec);

        return $this;
    }

    /**
     * Moves a named RowSpec to the position just before another named RowSpec
     *
     * @param string $source_name
     * @param string $destination_name
     *
     * @return self
     */
    public function moveRowSpecAfter(string $source_name, string $destination_name)
    {
        $transfer = $this->extractRowSpec($source_name);

        if (is_null($transfer)) {
            return $this;
        }

        $destination_index = $this->getRowSpecIndex($destination_name);

        $this->row_specs->splice($destination_index + 1, 0, [$transfer]);

        return $this;
    }

    /**
     * Moves a named RowSpec to the position just before another named RowSpec
     *
     * @param string $source_name
     * @param string $destination_name
     *
     * @return self
     */
    public function moveRowSpecBefore(string $source_name, string $destination_name)
    {
        $transfer = $this->extractRowSpec($source_name);

        if (is_null($transfer)) {
            return $this;
        }

        $destination_index = $this->getRowSpecIndex($destination_name);

        $this->row_specs->splice($destination_index, 0, [$transfer]);

        return $this;
    }

    /**
     * Convert the field to an array
     *
     * @return array
     */
    public function toArray()
    {
        $array = parent::toArray();

        $array['props']['rowSpecs'] = $this->getRowSpecsArray();
        $array['props']['addRowLabel'] = $this->getAddRowLabel();
        $array['props']['allowRowDeletion'] = $this->isRowDeletionAllowed();
        $array['props']['orderable'] = $this->getOrderable();
        $array['props']['useDragHandle'] = $this->getUseDragHandle();
        $array['props']['maxRows'] = $this->getMaxRows();

        return $array;
    }

    /**
     * Helper function for the "add row" label
     *
     * @param string|null $new_label
     *
     * @return string|self
     */
    public function addRowLabel(string $new_label = null)
    {
        if (!is_null($new_label)) {
            return $this->setAddRowLabel($new_label);
        }

        return $this->getAddRowLabel();
    }

    /**
     * The current "add row" label
     *
     * @return string
     */
    public function getAddRowLabel(): string
    {
        return $this->add_row_label;
    }

    /**
     * Set the "add row" label
     *
     * @param string $new_label
     *
     * @return self
     */
    public function setAddRowLabel(string $new_label)
    {
        $this->add_row_label = $new_label;
        return $this;
    }

    /**
     * EXTEND THIS ONLY
     *
     * Extendable Field Integrity Check. All fields should extend this, call
     * parent versions and then add their own checks.
     *
     * @return void
     */
    protected function fieldIntegrityCheck()
    {
        parent::fieldIntegrityCheck();

        if (!count($this->getRowSpecs())) {
            throw new FieldIntegrityException(
                '[' . class_basename($this) . '] ' . $this->getName() .
                    ' Must implement at least one Row Spec.'
            );
        }

        foreach ($this->getRowSpecs() as $row_spec) {
            if (!count($row_spec->getFields())) {
                throw new FieldIntegrityException(
                    '[' . class_basename($this) . '] ' . $this->getName() .
                        '. Row Spec ' . $row_spec->getName() . ' must have at least 1 field.'
                );
            }
        }
    }

    /**
     * If false, rows will not be deletable.
     *
     * @param bool $allow
     *
     * @return self
     */
    public function allowRowDeletion($allow = true)
    {
        $this->allow_row_deletion = $allow;

        return $this;
    }

    /**
     * Find if rows are allowed to be deleted
     *
     * @return bool
     */
    public function isRowDeletionAllowed()
    {
        return $this->allow_row_deletion;
    }

    /**
     * Set whether the rows can be ordered by dragging
     *
     * @param bool $is_orderable
     *
     * @return self
     */
    public function setOrderable($is_orderable = true)
    {
        $this->is_orderable = $is_orderable;

        return $this;
    }

    /**
     * Find if rows can be ordered by dragging
     *
     * @return bool
     */
    public function getOrderable()
    {
        return $this->is_orderable;
    }

    /**
     * Set whether or not to use a drag handle (if not, make
     * the whole row draggable)
     *
     * @param bool $use_drag_handle
     *
     * @return self
     */
    public function setUseDragHandle($use_drag_handle)
    {
        $this->use_drag_handle = $use_drag_handle;

        return $this;
    }

    /**
     * Whether or not to use a drag handle
     *
     * @return bool
     */
    public function getUseDragHandle()
    {
        return $this->use_drag_handle;
    }

    /**
     * Set the maximum number of rows that can be added
     *
     * @param int $max
     *
     * @return self
     */
    public function setMaxRows($max)
    {
        $this->max_rows = $max;

        return $this;
    }

    /**
     * Get the maximum number of rows
     *
     * @return int|null
     */
    public function getMaxRows()
    {
        return $this->max_rows;
    }

    /**
     * Helper method for getting/setting maximum rows
     *
     * @param int $max
     *
     * @return int|self
     */
    public function maxRows($max = null)
    {
        if ($max) {
            return $this->setMaxRows($max);
        } else {
            return $this->getMaxRows();
        }
    }

    /**
     * Gets the current index of a rowspec, base on it's name.
     *
     * @param string $spec_name
     *
     * @return int
     */
    protected function getRowSpecIndex($spec_name)
    {
        return $this->row_specs->search(function ($row_spec) use ($spec_name) {
            return $row_spec->getName() === $spec_name;
        });
    }

    /***************************************************************************
     * DEPRECATED
     */

    /**
     * Gets a single row spec by name, if present. Otherwise, returns null
     *
     * @deprecated
     *
     * @param string $spec_name Name to check for
     *
     * @return \Yadda\Enso\Crud\Forms\SectionInterface|null Found Row spec, or null
     */
    public function getRowSpecByName(string $spec_name)
    {
        Log::warning('`getRowSpecByName` has been Deprecated. Use `getRowSpec` instead');

        return $this->getRowSpec($spec_name);
    }

    /**
     * Returns an array of RowSpecs based on a list of row names to check for
     *
     * @deprecated
     *
     * @param array $spec_names Row names to check for
     *
     * @return array Array of matched key specs
     */
    public function getRowSpecsByName(array $spec_names)
    {
        Log::warning('`getRowSpecsByName` has been Deprecated.');

        return (new Collection(array_map(function ($spec_name) {
            return $this->getRowSpec($spec_name);
        }, $spec_names)))->filter();
    }

    /**
     * Removes a row_spec with a specified name, if present
     *
     * @param string $spec_name Name of row spec to remove
     *
     * @return self
     */
    public function removeRowSpecByName(string $spec_name)
    {
        Log::warning('`removeRowSpecByName` has been Deprecated. Use `removeRowSpec` instead');

        return $this->removeRowSpec($spec_name);
    }

    /**
     * Removes a set of row specs, based on an array of names, if they are present
     *
     * @deprecated
     *
     * @param array $spec_names Array of names for specs to remove
     *
     * @return self
     */
    public function removeRowSpecsByName(array $spec_names)
    {
        Log::warning('`removeRowSpecsByName` has been Deprecated');

        foreach ($spec_names as $spec_name) {
            $this->removeRowSpec($spec_name);
        }

        return $this;
    }
}
