<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;

class DateTimeField extends Field implements
    FieldInterface,
    HasCorrectTyping
{
    protected $tag_name = 'enso-field-datetime';

    protected $applicable_validation_rules = ['required'];

    protected $column_type = 'timestamp';

    protected $default_timezone = 'UTC';

    protected $hide_seconds = true;

    /**
     * Can this field be used inside a flexible content field
     *
     * @var boolean
     */
    protected static $flexible_field = true;

    /**
     * Set the form in which this data will be stored
     *
     * Either date, datetime, time or timestamp.
     *
     * @param string $column_type
     * @return self
     */
    public function setColumnType(string $column_type)
    {
        $column_type = strtolower($column_type);

        if (!in_array($column_type, ['date', 'datetime', 'time', 'timestamp'])) {
            throw new \Exception('Invalid data type passed to DateTimeField::setDataType() - ' . $column_type);
        }

        $this->column_type = $column_type;

        return $this;
    }

    /**
     * Get the form in which this data will be stored
     *
     * Either date, datetime, time or timestamp
     *
     * @return string
     */
    public function getColumnType()
    {
        return $this->column_type;
    }

    /**
     * Helper method for getting or setting tyhe column type
     *
     * @param null|string $column_type
     * @return self|string
     */
    public function columnType($column_type = null)
    {
        if (is_null($column_type)) {
            return $this->getColumnType();
        } else {
            return $this->setColumnType($column_type);
        }
    }

    /**
     * Set the Timezone to use on new dates. Default is UTC.
     *
     * @param string $timezone
     * @return self
     */
    public function setDefaultTimezone(string $timezone)
    {
        if (!in_array($timezone, DateTimeZone::listIdentifiers())) {
            throw new \Exception('Invalid timezone passed to DateTimeField::setDefaultTimezone - ' . $timezone);
        }

        $this->default_timezone = $timezone;

        return $this;
    }

    /**
     * Get the default timezone
     *
     * @return string
     */
    public function getDefaultTimezone()
    {
        return $this->default_timezone;
    }

    /**
     * Helper method for getting/setting the deafult timezone
     *
     * @param null|string $timezone
     * @return self|string
     */
    public function defaultTimezone($timezone = null)
    {
        if (is_null($timezone)) {
            return $this->getDefaultTimezone();
        } else {
            return $this->setDefaultTimezone($timezone);
        }
    }

    /**
     * Set whether to hide the seconds input. If true, then seconds will
     * always be set to 0
     *
     * @param bool $hide_seconds
     * @return self
     */
    public function setHideSeconds($hide_seconds)
    {
        $this->hide_seconds = $hide_seconds;

        return $this;
    }

    /**
     * Get whether or not the seconds input should be hidden
     *
     * @return bool
     */
    public function getHideSeconds()
    {
        return $this->hide_seconds;
    }

    /**
     * Helper for showing/hiding the seconds column
     *
     * @param null|bool $hide_seconds
     * @return self|bool
     */
    public function hideSeconds($hide_seconds = null)
    {
        if (is_null($hide_seconds)) {
            return $this->getHideSeconds();
        } else {
            return $this->setHideSeconds($hide_seconds);
        }
    }

    /**
     * Get props to be passed to Vue component
     *
     * @return array
     */
    public function getProps()
    {
        $props = parent::getProps();

        $props['columnType'] = $this->getColumnType();
        $props['defaultTimezone'] = $this->getDefaultTimezone();
        $props['hideSeconds'] = $this->getHideSeconds();

        return $props;
    }

    /**
     * Gets the appropriate piece of data from the data array, based on
     * field_name
     *
     * @param  array    $data           data to find value from
     * @param  array    $section_name   Section name override
     * @param  string   $field_name     data name to fetch if not using current
     *                                  field's name
     * @return mixed                    found value
     */
    public function getRequestData($data, $section_name = null, $field_name = null)
    {
        $data = parent::getRequestData($data, $section_name, $field_name);

        if (!$data) {
            $value = null;
        } else {
            $value = Carbon::createFromFormat('Y-m-d G:i:s', substr($data['date'], 0, 19), $data['timezone']);

            if ($this->getColumnType() === 'time') {
                $value = $value->format('H:i:s');
            }
        }

        return $value;
    }

    /**
     * Gets the correct value for this field from the passed data
     *
     * @param  object   $item           Data source
     * @param  string   $property_name  Override property name
     * @return mixed                    matched data
     */
    public function getFormData($item, $property_name = null)
    {
        $value = parent::getFormData($item, $property_name);

        if (is_null($value)) {
            return $this->getDefaultValue();
        }

        if ($this->getColumnType() === 'time') {
            return Carbon::createFromFormat('H:i:s', $value);
        }

        return static::carbonToDateArray($value);
    }

    protected static function carbonToDateArray(Carbon $date)
    {
        return [
            'date' => $date->format('Y-m-d H:i:s'),
            'timezone' => 'UTC',
            'timezone_type' => 3,
        ];
    }

    /**
     * Expand the stored JSON version of this field
     * for use in forms
     *
     * @param string JSON version from database
     * @return Carbon
     */
    public static function expandForJson($field)
    {
        if (!is_null($field['content'])) {
            $field['content'] = Carbon::createFromFormat(
                'Y-m-d H:i:s',
                Arr::get($field, 'content.date'),
                Arr::get($field, 'content.timezone')
            );
        }

        return $field;
    }


    /**
     * Compress the data into a format suitable for
     * storing in the database as JSON
     *
     * @param  Array $field Data from the form
     * @return Array        Data for database
     */
    public static function compressForJson($field)
    {
        if (!static::$flexible_field) {
            throw new Exception('Field type can not be used inside flexible content.');
        }

        if (!is_null($field['content'])) {
            $field['content'] = static::carbonToDateArray($field['content']->timezone('UTC'));
        }

        return $field;
    }
}
