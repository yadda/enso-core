<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Exception;
use Hash;
use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;

use Yadda\Enso\Crud\Forms\SectionInterface;

class PasswordField extends Field implements FieldInterface
{
    protected $default = [null, null];
    protected $tag_name = 'enso-field-password';
    protected $applicable_validation_rules = ['required'];
    protected $confirmed = true;

    /**
     * Applies data to a given item. Can be overriden to provide funcionality
     * for non-simple data.
     *
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     */
    public function applyRequestData(Model &$item, $data)
    {
        $field_name = $this->getName();

        $value = $this->getRequestData($data);
        $new_password = $value[0];

        // Empty Passwords are not allowed. Given this, we can safely use an
        // empty password field to indicate that no update is required at this
        // time.
        if (empty($new_password)) {
            if (empty($item->$field_name)) {
                $item->fill([$field_name => 'unset']);
            }

            return;
        }

        $new_password = Hash::make($new_password);
        $item->fill([$field_name => $new_password]);
    }

    /**
     * Sets whether or not this password field should also supply a password
     * confirmation field
     *
     * @param boolean       $value          Whether it should be confirmed
     */
    public function setShouldConfirm($value)
    {
        $this->confirmed = $value;
        return $this;
    }

    /**
     * Gets the confirmed requirements of this password field.
     *
     * @return boolean
     */
    public function shouldConfirm()
    {
        return $this->confirmed;
    }

    /**
     * Gets the correct value for this field from the passed data. As this is a
     * password field, we don't ever want to populate it with existing data.
     *
     * @param  object   $item           Data source
     * @param  string   $property_name  Override property name
     * @return mixed                    matched data
     */
    public function getFormData($item, $property_name = null)
    {
        return $this->shouldConfirm() ? ['', ''] : [''];
    }

    /**
     * Convert the field to an array
     *
     * @return Array
     */
    public function toArray()
    {
        $array = parent::toArray();

        $array['confirmed'] = $this->shouldConfirm();

        return $array;
    }
}
