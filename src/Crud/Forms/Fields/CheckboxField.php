<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Illuminate\Database\Eloquent\Model;

use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\SectionInterface;

use Yadda\Enso\Crud\Contracts\UpdatesMultipleProperties;
use Yadda\Enso\Crud\Contracts\HasCorrectTyping;

class CheckboxField extends Field implements
    FieldInterface,
    UpdatesMultipleProperties,
    HasCorrectTyping
{
    /**
     * Options for this data set
     *
     * @var array
     */
    protected $options = [];

    /**
     * Name of the custom vue component
     *
     * @var string
     */
    protected $tag_name = 'enso-field-checkbox';

    /**
     * Laravel validation rules which can be converted to html validation rules
     * for the form input
     *
     * @var array
     */
    protected $applicable_validation_rules = [
        'required'
    ];

    /**
     * Sets the internal options array to a given array
     *
     * @param array     $options        Options to set
     */
    public function setOptions($options = [])
    {
        $this->options = $options;

        return $this;
    }

    public function addOptions($options)
    {
        $this->options = array_merge($this->options, $options);

        return $this;
    }

    public function removeOptions($options)
    {
        $options = array_combine($options, $options);
        $this->options = array_diff_key($this->options, $options);
    }

    /**
     * Gets the current list of options, or uses the name given to this
     * field as a single option
     *
     * @return array                    Options list
     */
    public function getOptions()
    {
        if (empty($this->options)) {
            return [$this->getName() => $this->getLabel()];
        } else {
            return $this->options;
        }
    }

    /**
     * Applies data to a given item. Can be overriden to provide funcionality
     * for non-simple data.
     *
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     *
     * @return self
     */
    public function applyRequestData(Model &$item, $data)
    {
        $field_name = $this->getName();
        $section_name = $this->getSectionName();

        foreach ($this->getOptions() as $option_name => $x) {
            if ($this->getTranslatable()) {
                $values = [];

                foreach ($this->getLanguages() as $code => $language) {
                    $values[$code] = (empty($data[$section_name][$field_name][$option_name][$code])
                                    ? $this->getDefaultSingleUnsetValue()
                                    : $this->getDefaultSingleSetValue($data));
                }

                $item->setTranslations($option_name, $values);
            } else {
                $new_value = (empty($data[$section_name][$field_name][$option_name])
                                ? $this->getDefaultSingleUnsetValue()
                                : $this->getDefaultSingleSetValue($data));

                $item->fill([$option_name => $new_value]);
            }
        }
    }

    /**
     * Gets the correct value for this field from the passed data
     *
     * @param  object   $item           Data source
     * @param  string   $property_name  Override property name
     * @return mixed                    matched data
     */
    public function getFormData($item, $property_name = null)
    {
        if (!is_object($item)) {
            return $this->getDefaultValue();
        }

        $results = [];
        foreach ($this->getOptions() as $option_name => $x) {
            if ($this->getTranslatable()) {
                if (!empty($item->getTranslations($option_name))) {
                    $results[$option_name] = $item->getTranslations($option_name);
                }
            } else {
                if (!!$item->getAttributeValue($option_name)) {
                    $results[$option_name] = $option_name;
                }
            }
        }
        if (empty($results)) {
            return $this->getDefaultValue();
        }

        return $results;
    }

    /**
     * Get the names of each of the potential values this can set
     *
     * @return array                    Names of values
     */
    public function getMultipleValueNames()
    {
        return array_keys($this->getOptions());
    }

    /**
     * Get the value that should be set when the option is specified in the
     * request data
     *
     * @param  array        $data       request data
     * @return mixed                    value to set
     */
    public function getDefaultSingleSetValue($data)
    {
        return true;
    }

    /**
     * Get the value that should be set when the option is not specified in the
     * request data
     *
     * @return mixed                    value to set
     */
    public function getDefaultSingleUnsetValue()
    {
        return false;
    }

    /**
     * Get the default value if this field is not translatable
     *
     * @return Object
     */
    protected function getDefault()
    {
        return (Object) [];
    }

    /**
     * Get the translatable default value for this field type
     *
     * @return Array
     */
    protected function getTranslatableDefault()
    {
        return [config('app.locale') => $this->getDefault()];
    }

    /**
     * Convert the field to an array
     *
     * @return Array
     */
    public function toArray()
    {
        $output = parent::toArray();

        $output['props']['options'] = $this->getOptions();

        return $output;
    }
}
