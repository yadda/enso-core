<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;

/**
 * Display static content in a CRUD form
 *
 * Can optionally use a vue component for laying out the content
 */
class StaticTextField extends Field implements FieldInterface, HasCorrectTyping
{
    /**
     * Name of the vue component to use to format $content
     *
     * If this is not set then $content will be displayed as plain text. If
     * $content is not a string, this MUST be set.
     *
     * @var string
     */
    protected $component;

    /**
     * The content to display
     *
     * @var mixed
     */
    protected $content;

    /**
     * Default value for this field
     *
     * @var string
     */
    protected $default = '';

    /**
     * Whether this field can be used inside Flexible Content
     *
     * @var boolean
     */
    protected static $flexible_field = true;

    /**
     * Readonly status of the field
     *
     * @var boolean
     */
    protected $readonly = true;

    /**
     * Vue component to use to display this field
     *
     * @var string
     */
    protected $tag_name = 'enso-field-static-text';

    /**
     * Set the content to display on the page
     *
     * If this is a string and component isn't set then the string will be
     * displayed on the page. If component is set then content will be used
     * as a prop and passed to the component to display as you wish.
     *
     * @param mixed $content content to display
     */
    public function setContent($content)
    {
        $this->props['content'] = $content;

        return $this;
    }

    /**
     * Get the content to display
     *
     * @return mixed
     */
    public function getContent()
    {
        return $this->props['content'];
    }

    /**
     * Set a vue component to use to render $content.
     *
     * @param string $component name of component
     *
     * @return self
     */
    public function setComponent($component)
    {
        $this->props['component'] = $component;

        return $this;
    }

    /**
     * Get the name the of the vue component to display $content
     *
     * @return void
     */
    public function getComponent()
    {
        return $this->props['component'];
    }
}
