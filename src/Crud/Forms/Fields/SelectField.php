<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Alert;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Exceptions\FieldIntegrityException;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;

class SelectField extends Field implements FieldInterface, HasCorrectTyping
{
    /**
     * Name of the custom vue component
     *
     * @var string
     */
    protected $tag_name = 'enso-field-select';

    /**
     * The default value to return if no other value is available
     *
     * @param mixed
     */
    protected $default = null;

    /**
     * An internal store for the 'final' options. This is here to prevent
     * re-calling the same query for option sets that derive from DB queries in
     * cases where they are required / inspected more than once.
     *
     * @var collection
     */
    protected $final_options;

    /**
     * Settings to pass to the vue component
     *
     * @param array
     */
    protected $settings;

    /**
     * Options for this data set
     *
     * @var array
     */
    protected $options = [];

    /**
     * Whether to expect Options to be provided as Option groups
     *
     * @var boolean
     */
    protected $expect_option_groups = false;

    /**
     * Callback to be run on options. This is designed to reduce the amount of
     * data transfered from server to client when using objects and eloquent
     * models with unnecessary data for the operation.
     *
     * @param callable
     */
    protected $data_callback;

    /**
     * A callback that can be used to expand the a value into a full item
     *
     * @param callable
     */
    protected $ajax_callback;

    /**
     * Laravel validation rules which can be converted to html validation rules
     * for the form input
     *
     * @var array
     */
    protected $applicable_validation_rules = [
        'required'
    ];

    /**
     * Can this field be used inside a flexible content field
     *
     * @var boolean
     */
    protected static $flexible_field = true;

    /**
     * Class of the Resource to use to convert a Crud item into an array
     *
     * @var string
     */
    protected $resource_class = \Yadda\Enso\Crud\Resources\ListResource::class;

    public function __construct($name)
    {
        parent::__construct($name);

        $this->settings = [
            'options'         => [],
            'multiple'        => false,
            'track_by'        => Config::get('enso.settings.select_track_by', 'id'),
            'label'           => Config::get('enso.settings.select_label_by', 'label'),
            'searchable'      => true,
            'clear_on_select' => true,
            'hide_selected'   => false,
            'placeholder'     => 'Select option',
            'allow_empty'     => true,
            'reset_after'     => false,
            'close_on_select' => true,
            // 'custom_label'    => ???
            'taggable'        => false,
            // 'tag_placeholder' => 'Press enter to create a tag',
            'max'             => null,
            'options_limit'   => 1000,
            'group_value'     => null,
            'group_label'     => null,
            'group_select'    => false,
            'block_keys'      => [],
            'internal_search' => true,
            'select_label'    => 'Press enter to select',
            'selected_label'  => 'Selected',
            'deselect_label'  => 'Press enter to remove',
            'show_labels'     => true,
            'limit'           => 99999,
            // 'limit_text'      => ???
            // 'loading'         => false,
            // 'disabled'        => false,
            'max_height'      => 300,
            'show_pointer'    => true,
            'ajax_url'        => null,
            'orderby'         => 'id',
            'order'           => 'ASC',
        ];

        $this->data_callback = function ($item) {
            return $this->resource_class::make($item)->resolve();
        };
    }

    /**
     * Checks for deprecated settings being used
     *
     * @param  string   $setting_name       Name of setting to check
     * @param  string   $ignore_warnings    Whether to ignore warnings
     */
    protected function checkDeprecatedSettings($setting_name, $ignore_warnings = false)
    {
        if (app()->environment('dev', 'local') && !$ignore_warnings) {
            if ($setting_name === 'options') {
                Alert::error('Deprecated use of setSetting(s) with the key `options` . Use setOptions instead.');
            }

            if ($setting_name === 'ajax_url') {
                Alert::error('Deprecated use of setSetting(s) with the key `ajax_url`. Use useAjax() instead.');
            }

            if ($setting_name === 'multiple') {
                Alert::error('Deprecated use of setSetting(s) with the key `multiple`. Use MultiSelectField instead.');
            }
        }
    }

    /**
     * The class of the Resource to use to convert a Crud item into an array
     *
     * @return string
     */
    public function getResourceClass(): string
    {
        return $this->resource_class;
    }

    /**
     * Set the class of the resource for data manipulation for this Field
     *
     * @param string $resource_class
     *
     * @return self
     */
    public function setResourceClass(string $resource_class)
    {
        $this->resource_class = $resource_class;

        return $this;
    }

    /**
     * Set an array of settings to pass to vue-multiselect.
     *
     * @param  array    $settings
     * @return self
     */
    public function setSettings($settings, $ignore_warnings = false)
    {
        foreach ($settings as $key => $value) {
            $this->checkDeprecatedSettings($key, $ignore_warnings);
        }

        $this->settings = array_merge($this->settings, $settings);
        return $this;
    }

    /**
     * Get array of settings for vue-multiselect
     *
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * Sets the value of a specific setting, by name.
     *
     * @param  string   $name       setting name
     * @param  mixed    $value      setting value
     *
     * @return self
     */
    public function setSetting($name, $value, $ignore_warnings = false)
    {
        $this->checkDeprecatedSettings($name, $ignore_warnings);
        $this->settings[$name] = $value;
        return $this;
    }

    /**
     * Gets a specific setting by name
     *
     * @param  string   $name       Setting to get
     *
     * @return mixed                Current setting value
     */
    public function getSetting($name)
    {
        return isset($this->settings[$name]) ? $this->settings[$name] : null;
    }

    /**
     * Set the fields disabled status
     *
     * @param boolean $is_disabled
     * @return self
     */
    public function setDisabled($is_disabled = true)
    {
        $this->settings['disabled'] = $is_disabled;

        return $this;
    }

    /**
     * Returns whether the SelectField should expect Options to be provided as
     * Option groups.
     *
     * @return boolean
     */
    protected function expectOptionGroups()
    {
        return $this->getSetting('group_value') || $this->getSetting('group_label');
    }

    /**
     * Helper call to determine whether this field should use ajax to get it's
     * options. It is based on the principle that if you specific an Ajax url,
     * then that is the intended method.
     *
     * @NOTE Specifying an ajax url WILL NEGATE any deliberately set 'options'.
     *
     * @return boolean
     */
    public function isAjax()
    {
        return $this->getSetting('ajax_url');
    }

    /**
     * Setter for Ajax specific functionality
     *
     * @param  string   $route      route to call for ajax items
     * @param  mixed    $method     Model class, or callback
     * @return self
     */
    public function useAjax($route, $method)
    {
        $this->settings['ajax_url'] = $route;

        // Accepts either classname or a callable.
        if (is_string($method)) {
            $this->ajax_callback = function ($value) use ($method) {
                $item = $method::query()->withoutGlobalScopes()->where($this->getSetting('track_by'), $value)->first();

                if (is_null($item)) {
                    return $this->getDefaultValue();
                }

                return call_user_func($this->data_callback, $item);
            };
            return $this;
        } elseif (is_callable($method)) {
            $this->ajax_callback = $method;
            return $this;
        }

        throw new FieldIntegrityException('Unknown parameter type ' . gettype($method) . ' passed to useAjax()');
    }

    /**
     * Overwrites the current options with a specified array. Should be provided
     * as an array or Collection of objects containing both the tract_by and
     * label properties, as set in the settings.
     *
     * e.g. [['id' => 1, 'name' => 'thing'], ... ], or as an associative array
     * of [Key => value pairs].
     *
     * @param mixed $options Options to set
     */
    public function setOptions($options)
    {
        if ($options instanceof Collection) {
            $options = $options->toArray();
        }

        if (empty($options)) {
            $this->options = $this->getDefaultValue();
            return $this;
        }

        // If it's and array and the first value is a string, it's been provided
        // as key => value pairs.
        if (is_array($options) && is_string(Arr::first($options))) {
            $options = $this->parseKeyValuePairs($options);
        }

        $this->options = $options;

        return $this;
    }

    /**
     * Converts Key => Value pair array to a set of data in the format used by
     * the vue-multiselect component
     *
     * @param  array  $options  [description]
     * @return                  [description]
     */
    protected function parseKeyValuePairs(array $options)
    {
        // If provided as key value pairs.
        $object_options = [];

        foreach ($options as $key => $value) {
            $object_options[] = [
                $this->getSetting('track_by') => $key,
                $this->getSetting('label') => $value
            ];
        }

        return $object_options;
    }

    /**
     * Returns the current option set.
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Makes a save of the final options, based on the current options and the
     * type that they are of.
     *
     * @param  boolean  $reset      Whether to force reset the list.
     *
     * @return self
     */
    public function calculateFinalOptions($reset = false)
    {
        if (is_null($this->final_options) || $reset) {
            $options = $this->getOptions();

            if ($options instanceof Builder) {
                $options = $options->select($this->getSetting('track_by'), $this->getSetting('label'))->get();
            }

            $this->final_options = $options;
        }

        return $this;
    }

    /**
     * Gets the actual final options for this field. If the item set in the
     * settings is an instance of Builder, then finish the query based on other
     * information present in the settings.
     *
     * @return array
     */
    public function getFinalOptions($as_array = true)
    {
        $this->calculateFinalOptions();

        if ($this->final_options instanceof Collection) {
            return $this->final_options->toArray();
        }

        return $this->final_options;
    }

    /**
     * Finds an item from the options array, based on it's track_by value
     *
     * @param  mixed   $identifier      id, slug, etc...
     *
     * @return array                    Matched Value, or default
     */
    public function getFromOptionsByTrackBy($identifier)
    {
        $track_by = $this->getSetting('track_by');


        // If identifier is passed as an object, find it's tracked value. This
        // is predominantly here to ease getting/setting this as a json entity
        if (is_object($identifier)) {
            $identifier = $identifier->$track_by;
        }

        if ($this->expectOptionGroups()) {
            $options = array_merge(...array_map(function ($option_group) {
                return $option_group[$this->getSetting('group_value')];
            }, $this->getFinalOptions()));
        } else {
            $options = $this->getFinalOptions();
        }

        foreach ($options as $option) {
            // Cast to object so that it works with both array and object
            // options. Intentionally loose comparitor so that it can work with
            // both id's and string slugs etc...
            if (((object) $option)->$track_by == $identifier) {
                // return as an element of an array so that we're always
                // returning selections in a standardised format.
                return $option;
            }
        }

        return $this->getDefaultValue();
    }

    /**
     * Unlike other relationships, belongsTo only updates data on the model's
     * own table, so it can be called before performing a save.
     *
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     *
     * @return self
     */
    public function applyRequestData(Model &$item, $data)
    {
        $data = $this->getRequestData($data);

        $column_name = $this->getName();
        $item->$column_name = $data;

        return $this;
    }

    /**
     * Gets the correct value for this field from the passed data
     *
     * @param  object       $item               Data source
     * @param  string       $property_name      Override property name
     *
     * @return mixed                            matched data
     */
    public function getFormData($item, $property_name = null)
    {
        $this->doFieldIntegrityCheck();

        $property_name = $property_name ?? $this->getName();

        try {
            $property_value = $item->$property_name;
        } catch (Exception $e) {
            $property_value = $this->getDefaultValue();
        }

        if ($this->isAjax()) {
            return call_user_func($this->ajax_callback, $property_value);
        } else {
            return $this->getFromOptionsByTrackBy($property_value);
        }
    }

    /**
     * Hook for modifying the request data, if required
     *
     * @param  mixed    $data           Original data
     * @return mixed                    Modified data
     */
    protected function modifyRequestData($data)
    {
        if (empty($data) || !is_array($data)) {
            return null;
        }

        /**
         * @todo This is a hack to make SelectFields in flexible content store
         * the full object rather than the trackBy value. Storing the trackBy
         * would be preferable but would take too much work right now. This
         * would require updating existing data and updating the vue
         * components to accept a single value and display the relevant object
         */
        if ($this->getSection() instanceof FlexibleContentSection) {
            return $data;
        }

        return $data[$this->getSetting('track_by')];
    }

    /**
     * Convert the field to an array
     *
     * @return Array
     */
    public function toArray()
    {
        $array = parent::toArray();

        $array['props']['settings'] = $this->settings;
        $array['props']['useAjax'] = $this->isAjax();
        $array['props']['settings']['options'] = $this->getFinalOptions();

        return $array;
    }

    /**
     * Gets a data type array to be used to store items to a CollectionSection
     *
     * @return array                Data type information
     */
    public function getDataType()
    {
        $data_type = [
            'type' => 'single',
            'content' => [
                'key' => $this->getSetting('track_by'),
            ],
        ];

        if ($this->getOptions() instanceof Builder) {
            $data_type['content']['class'] = get_class($this->getOptions()->getModel());
        }

        return $data_type;
    }

    /**
     * Generate a SelectField option for the given key & value pair
     *
     * @param string $key
     * @param string $value
     *
     * @return array
     */
    public static function makeOption($key, $value): array
    {
        return [
            Config::get('enso.settings.select_track_by') => $key,
            Config::get('enso.settings.select_label_by') => $value,
        ];
    }
}
