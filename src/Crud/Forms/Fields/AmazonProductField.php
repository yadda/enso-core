<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;

class AmazonProductField extends Field implements FieldInterface
{
    protected $default = '';
    protected $tag_name = 'enso-field-amazon-product';
    protected $applicable_validation_rules = ['required'];
}
