<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Exceptions\CrudException;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Handlers\Slugs;

class SlugField extends Field implements
    FieldInterface,
    HasCorrectTyping
{
    protected $default = '';

    protected $tag_name = 'enso-field-slug';

    protected $applicable_validation_rules = ['required'];

    protected $max_length = 255;

    /**
     * The field from which to automatically create the slug
     *
     * @var string
     */
    protected $source = 'name';

    /**
     * Route to insert slug into. "%SLUG%" should be used as a placeholder
     *
     * e.g. http://example.com/%SLUG%/foo/bar
     *
     * @var string
     */
    protected $route = '';

    /**
     * Set the name of the source field
     *
     * @param String $source
     * @return self
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get the name of the source field
     *
     * @return String
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Shorthand alias for getting/setting the name of the field that the slug
     * will be automatically filled by
     *
     * @param String $source Name of the source field
     * @return mixed
     */
    public function source($source = null)
    {
        if ($source === null) {
            return $this->getSource();
        } else {
            return $this->setSource($source);
        }
    }

    /**
     * Get or set the maximum field lenfth
     *
     * @param int $length
     * @return void
     */
    public function maxLength(int $length = null)
    {
        if ($length === null) {
            return $this->max_length;
        } else {
            $this->max_length = $length;

            return $this;
        }
    }

    /**
     * Set the route that this slug will be used in
     *
     * @param String $route
     * @return self
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get the route that this slug will be used in
     *
     * @return String
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Shorthand alias for getting/setting the route
     *
     * @param String $route
     * @return mixed
     */
    public function route($route = null)
    {
        if ($route === null) {
            return $this->getRoute();
        } else {
            return $this->setRoute($route);
        }
    }

    /**
     * A slightly hacky way to test whether we're on an create route
     *
     * @return boolean
     */
    public function isCreateRoute()
    {
        $action = request()->route()->getAction();

        return explode('@', $action['controller'])[1] === 'create';
    }

    /**
     * Gets the appropriate piece of data from the data array, based on
     * field_name
     *
     * @NOTE   Currently limited to source fields in the same section as the slug
     *         field.
     *
     * @param  Array    $data           data to find value from
     * @param  Array    $section_name   Section name override
     * @param  String   $field_name     data name to fetch if not using current
     *                                  field's name
     * @return mixed                    found value
     */
    public function getRequestData($data, $section_name = null, $field_name = null)
    {
        if (empty($field_name)) {
            $field_name = $this->getName();
        }

        if (empty($section_name)) {
            $section_name = $this->getSectionName();
        }

        $data_value = $data[$section_name][$field_name];

        if ($this->getTranslatable()) {
            foreach ($this->getLanguages() as $code => $lang) {
                if (empty($data_value[$code])) {
                    $data_value[$code] = $this->generateValueFromSource($data, $section_name, $code);
                }
            }
        } else {
            if (empty($data_value)) {
                $data_value = $this->generateValueFromSource($data, $section_name);
            }
        }

        return $data_value;
    }

    /**
     * Generates a new slug based on the value of the source field.
     *
     * @param  Array    $data               Data to find source in
     * @param  String   $section_name       Section name override
     *
     * @return String                       Newly generated slug
     */
    protected function generateValueFromSource($data, $section_name = null, $lang = null)
    {
        if (empty($section_name)) {
            $section_name = $this->getSectionName();
        }

        $source = $data[$section_name][$this->getSource()];

        // This slug source REALLY should be a required field, but just in case.
        if ($this->getTranslatable()) {
            if (!empty($source[$lang])) {
                $source_val = $source[$lang];
            } else {
                $values = array_values($source);

                if (count($values) === 0) {
                    throw new Exception('No value in slug source field.');
                }

                $source_val = $values[0];
            }
        } else {
            $source_val = $source;
        }

        if (empty($new_slug = $source_val)) {
            throw new Exception('Automatic slug cannot be generated from empty source field');
        }

        return $this->generateSlugFrom($new_slug);
    }

    /**
     * Extract Logic for generating a slug for this instance of SlugField.
     *
     * @param string $source_string
     *
     * @return string
     */
    public function generateSlugFrom(string $source_string): string
    {
        return Slugs::make()
            ->maxLength($this->max_length)
            ->generateFrom($source_string);
    }

    /**
     * Applies data to a given item. Can be overriden to provide funcionality
     * for non-simple data.
     *
     * @todo   this doesn't allow setting a value to 0 - it just sets it to default instead
     *
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     */
    public function applyRequestData(Model &$item, $data)
    {
        $field_name = $this->getName();

        $value = $this->ensureUniqueValue($item, $this->getRequestData($data));

        if ($this->getTranslatable()) {
            foreach ($this->getLanguages() as $code => $lang) {
                if (mb_strlen($value[$code]) > $this->maxLength()) {
                    throw new CrudException('The ' . $code . ' slug generated from ' . $this->getSource() . ' is too long. Please manually edit it.');
                }
            }
        } else {
            if (mb_strlen($value) > $this->maxLength()) {
                throw new CrudException('The slug generated from ' . $this->getSource() . ' is too long. Please manually edit it.');
            }
        }

        $item->fill([$field_name => $value]);
    }

    /**
     * Queries the database for instances of the item's model that start with
     * the currently selected slug, excluding that item.
     *
     * @param  Model    $item       Item to find table from
     * @param  String   $value      The slug string
     * @return String               The useable value
     */
    protected function ensureUniqueValue(Model &$item, $value)
    {
        $field_name = $this->getName();

        // If no change, return value as correct;
        if ($item->$field_name === $value) {
            return $value;
        }

        if ($this->getTranslatable()) {
            $query = $item::query()->withoutGlobalScopes()->where($item->getKeyName(), '!=', $item->getKey());

            $query->where(function ($query) {
                foreach ($this->getLanguages() as $code => $lang) {
                    if (isset($value[$code])) {
                        $query->where($this->getName . '->' . $code, $value[$code]);
                    }
                }

                return $query;
            });

            $results = $query->get();
        } else {
            return Slugs::make($item)
                ->maxLength($this->max_length)
                ->generateUniqueValue($value);
        }

        // If there are not existing matches, then value is fine to be used
        if ($results->count() === 0) {
            return $value;
        }

        // Iterate through results till a non-match is found
        return $this->findNextUnique($results->pluck($field_name)->toArray(), $value, null);
    }

    /**
     * Query the database table of the given item to find other instances that
     * start with the given slug. Then incrementally iterate over them to find
     * the first unique value
     *
     * @param  array    $result_slugs           All partially matches slugs
     * @param  string   $value                  Base slug string
     * @param  int      $current_iterator       Value to append to test
     * @return string                           First unique value
     */
    protected function findNextUnique(array $result_slugs, $value, $current_iterator)
    {
        if (is_null($current_iterator)) {
            return in_array($value, $result_slugs)
                ? $this->findNextUnique($result_slugs, $value, 1)
                : $value;
        } else {
            $new_value = $value . '-' . $current_iterator;
            return in_array($new_value, $result_slugs)
                ? $this->findNextUnique($result_slugs, $value, ++$current_iterator)
                : $new_value;
        }
    }

    /**
     * Convert this field to an array
     *
     * @return Array
     */
    public function toArray()
    {
        $array = parent::toArray();

        $array['props']['route'] = $this->route;
        $array['props']['source'] = $this->source;
        $array['props']['createRoute'] = $this->isCreateRoute();
        $array['props']['sectionName'] = $this->getSection()->getName();

        return $array;
    }
}
