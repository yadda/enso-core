<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Yadda\Enso\Crud\Exceptions\CrudException;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\RelationshipField as BaseRelationshipField;
use Yadda\Enso\Facades\EnsoCrud;
use Yadda\Enso\Media\Contracts\MediaFile as MediaFileContract;
use Yadda\Enso\Utilities\Helpers;

class FileUploadField extends BaseRelationshipField implements FieldInterface
{
    /**
     * File types that this field should be limited to access. Leave empty for
     * any file types.
     *
     * @var string[]
     */
    protected $accepted_file_types = [];

    /**
    * Laravel validation rules which can be converted to html validation rules
    * for the form input
    *
    * @var string[]
    */
    protected $applicable_validation_rules = [
        'required'
    ];

    /**
     * Default value (no files)
     *
     * @var array
     */
    protected $default = [];

    /**
     * Model that this upload field type instantiates
     *
     * @var string|null
     */
    protected $file_model;

    /**
     * Can this field be used inside a flexible content field
     *
     * @var boolean
     */
    protected static $flexible_field = true;

    /**
     * Maximum size of a single upload
     *
     * @var int
     */
    protected $max_file_size_mb = 2;

    /**
     * Maximum number of uploads to allow
     *
     * @var int
     */
    protected $max_file_uploads = 1;

    /**
     * The name of the column to order by for Relationships that are orderable.
     *
     * @var string|null
     */
    protected $relationship_order_by;

    /**
     * Eloquent relationship types that this field supports.
     *
     * @var string[]
     */
    protected $supported_relationship_types = [
        'BelongsTo',
        'BelongsToMany',
    ];

    /**
     * Name of the custom vue component
     *
     * @var string
     */
    protected $tag_name = 'enso-field-file-upload';

    /**
     * Directory to store files in
     *
     * @var string
     */
    protected $upload_path = 'general';

    /**
     * route to post uploads to
     *
     * @var string
     */
    protected $upload_route = 'admin.media.upload';

    /**
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);

        $this->file_model = get_class(resolve(MediaFileContract::class));

        $this->data_callback = function ($item) {
            return $item->mapForJson();
        };
    }

    /**
     * Wrapper function for the setter and getter for accepted_file_types
     *
     * @param array|null $file_types New file types to use
     *
     * @return self|array
     */
    public function acceptedFileTypes(array $file_types = null)
    {
        if (!is_null($file_types)) {
            return $this->setAcceptedFileTypes($file_types);
        }

        return $this->getAcceptedFileTypes();
    }

    /**
     * Adds a file type to the internal list (checking for uniquity as it does)
     *
     * @param string    $file_type      New File type
     */
    public function addAcceptedFileType(string $file_type)
    {
        if (!array_search($file_type, $this->accepted_file_types)) {
            $this->accepted_file_types[] = $file_type;
        }

        return $this;
    }

    /**
     * Adds an array of accepted file types to the internal Array
     *
     * @param array     $file_types     List of file types to add
     */
    public function addAcceptedFileTypes(array $file_types)
    {
        $this->accepted_file_types = array_unique(array_merge($this->accepted_file_types, $file_types));

        return $this;
    }

    /**
     * Ensures that the file data has a correctly sourced preview image.
     *
     * @param Collection $files
     *
     * @return Collection
     */
    protected static function addPreviews(Collection $files): Collection
    {
        $media_file = resolve(MediaFileContract::class);

        return $files->map(function ($file) use ($media_file) {
            $corrected_model = $media_file::getCorrectModelType($file);
            $corrected_model->setAttribute('preview', $corrected_model->getPreview());

            return $corrected_model;
        });
    }

    /**
     * Applies data to a given item. Can be overriden to provide funcionality
     * for non-simple data.
     *
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     *
     * @return self
     */
    public function applyRequestData(Model &$item, $data)
    {
        $relationship_name = $this->determineRelationshipName($item);
        $relationship = $item->{$relationship_name}();

        $values = $this->getRequestData($data);

        /**
         * BelongsTo is the only relationship that can be set during
         * applyRequestData. Others must be handled in applyRequestDataAfterSave
         * as they require the item to have an id, which will not be true when
         * this is field is on a create Form.
         */
        $relationship_class = class_basename($relationship);
        switch ($relationship_class) {
            case 'BelongsTo':
                $item->fill([
                    $relationship->getForeignKeyName() => Arr::get(Arr::first($values), (new $this->file_model)->getKeyName(), null)
                ]);
                break;
            default:
                if (!in_array($relationship_class, $this->supported_relationship_types)) {
                    throw new CrudException('Unsupported relationship type: ' . class_basename($relationship) . ' in ' . __CLASS__);
                }
        }
    }

    /**
     * Applies data to a given item. Can be overriden to provide funcionality
     * for non-simple data.
     *
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     *
     * @return self
     */
    public function applyRequestDataAfterSave(Model &$item, $data)
    {
        $relationship_name = $this->determineRelationshipName($item);
        $relationship = $item->{$relationship_name}();

        $relationship_class = class_basename($relationship);
        switch ($relationship_class) {
            case 'BelongsToMany':
                $values = $this->getRequestData($data);

                $relation_key_name = (new $this->file_model)->getKeyName();

                $file_ids = array_unique(array_filter(array_map(function ($item) use ($relation_key_name) {
                    return Arr::get($item, $relation_key_name, null);
                }, $values)));

                if ($this->isOrdered()) {
                    $file_ids = array_map(function ($count) {
                        return [$this->relationshipOrderBy() => $count];
                    }, array_flip($file_ids));
                }

                $item->{$relationship_name}()->sync($file_ids);
                break;
            default:
                if (!in_array($relationship_class, $this->supported_relationship_types)) {
                    throw new CrudException('Unsupported relationship type: ' . class_basename($relationship) . ' in ' . __CLASS__);
                }
        }

        /**
         * @todo - I don't believe this is correct as this never removes records
         *         associating files with items. It should determine relations to
         *         remove earlier in the code, but that isn't right either because
         *         that could detach files from this item that still supposed to
         *         be attached through another field.
         *
         *         Not sure there is a good way to do this at present. Maybe
         *         should redo the file list entirely at some point.
         */
        // if (EnsoCrud::maintainFileList(get_class($item))) {
        //     $item->files()->syncWithoutDetaching($file_ids);
        // }
    }

    /**
     * Compress the data into a format suitable for
     * storing in the database as JSON
     *
     * @param  Array $field Data from the form
     * @return Array        Data for database
     */
    public static function compressForJson($field)
    {
        $key_name = App::make(MediaFileContract::class)->getKeyName();

        $field['content'] = array_filter(array_map(function ($file) use ($key_name) {
            return Arr::get($file, $key_name, null);
        }, Arr::get($field, 'content', [])));

        return $field;
    }

    /**
     * Determining the relationship name this way keeps this field backwards
     * compatible. Previously, you would set the field name as the column name
     * to save into (for BelongsTo) and ALSO set the relationship name. This can
     * handle either.
     *
     * @param Model  $item
     *
     * @return string
     */
    protected function determineRelationshipName(Model $item): string
    {
        $field_name = $this->getName();

        return method_exists($item, $field_name)
            ? $field_name
            : $this->getRelationshipName();
    }

    /**
     * Expand the stored JSON version of this field for use in forms. Note that
     * this will persist the order of the files saved into the list.
     *
     * @param array $field
     *
     * @return array
     */
    public static function expandForJson($field_data)
    {
        $file_model = Helpers::getConcreteClass(MediaFileContract::class);

        $field_data['content'] = self::addPreviews(
            $file_model::whereIn('id', $field_data['content'])
                ->when(count($field_data['content']) > 1, function ($query) use ($field_data) {
                    $query->orderByRaw(DB::raw('FIELD(id, ' . implode(",", $field_data['content']) . ')'));
                })
                ->limit(count($field_data['content']))
                ->get()
        );

        return $field_data;
    }

    /**
     * Gets the current list of accepted file types
     *
     * @param boolean $as_string Return as a string if true
     * @param string  $separator implode separator when returning as a string
     *
     * @return mixed File types
     */
    public function getAcceptedFileTypes($as_string = false, $separator = ',')
    {
        if ($as_string) {
            return implode($separator, $this->accepted_file_types);
        }

        return $this->accepted_file_types;
    }

    /**
     * Returns the associated model.
     *
     * @return string
     */
    protected function getAssociatedModel()
    {
        return $this->file_model;
    }

    /**
     * Gets the current max upload files limit
     *
     * @return integer
     */
    public function getMaxFiles()
    {
        return $this->max_file_uploads;
    }

    /**
     * Gets the current maximum file size for any single upload by this field
     *
     * If null, a value from config  will be used: enso.media.max_file_size
     *
     * @return int
     */
    public function getMaxFileSize()
    {
        if (!is_null($this->max_file_size_mb)) {
            return $this->max_file_size_mb;
        }

        return Config::get('enso.media.max_file_size');
    }

    /**
     * Name of the relationship ordering column on this Field
     *
     * @return string|null
     */
    public function getRelationshipOrderBy(): ?string
    {
        return $this->relationship_order_by;
    }

    /**
     * Returns the default upload path for this Field
     *
     * @return string upload path beyond the base folder
     */
    public function getUploadPath(): string
    {
        return $this->upload_path;
    }

    /**
     * Whether the relationship that this Field saves into is ordered
     *
     * @return bool
     */
    public function isOrdered(): bool
    {
        return !empty($this->getRelationshipOrderBy());
    }

    /**
     * Determines whether this Field is for uploading a single file or not.
     *
     * Upload type is now determined by the Class of the relationship that this
     * field saves into. As such, the $max_files property is now only used to
     * determine the max file uploads for the vue component.
     *
     * This function is being left in place for backwards compatibility.
     *
     * @return boolean
     */
    public function isSingleFileUploadField(): bool
    {
        return (1 === (int) $this->getMaxFiles());
    }

    /**
     * Wrapper function for the setter and getter for max_files
     *
     * @param integer|null $filesize New file size, or null to get current
     *
     * @return self|integer|null
     */
    public function maxFiles($files = null)
    {
        if (!is_null($files)) {
            return $this->setMaxFiles($files);
        }

        return $this->getMaxFiles();
    }

    /**
     * Wrapper function for the setter and getter for max_file_size_mb
     *
     * @param  mixed    $filesize       New file size, or null to get current
     *
     * @return mixed                    Current File size, or self
     */
    public function maxFileSize($filesize = null)
    {
        if (is_null($filesize)) {
            return $this->getMaxFileSize();
        }

        return $this->setMaxFileSize($filesize);
    }

    /**
     * Wrapper function for the setter and getter for relationship order by
     *
     * @param string|null $column_name
     *
     * @return self|string|null
     */
    public function relationshipOrderBy(string $column_name = null)
    {
        if (!is_null($column_name)) {
            return $this->setRelationshipOrderBy($column_name);
        }

        return $this->getRelationshipOrderBy();
    }

    /**
     * Removes a single file type from the internal Array
     *
     * @param  string   $file_type      File type to remove
     *
     * @return self
     */
    public function removeAcceptedFileType(string $file_type): self
    {
        if (($key = array_search($file_type, $this->accepted_file_types)) !== false) {
            unset($this->accepted_file_types[$key]);
        }

        return $this;
    }

    /**
     * Removes an array of file types from the interal Array
     *
     * @param array $file_types File types to remove
     *
     * @return self
     */
    public function removeAcceptedFileTypes(array $file_types): self
    {
        $this->accepted_file_types = array_diff($this->accepted_file_types, $file_types);

        return $this;
    }

    /**
     * Sets the current accepted files list
     *
     * @param array $file_types New accepted Files list
     *
     * @return self
     */
    public function setAcceptedFileTypes(array $file_types): self
    {
        $this->accepted_file_types = $file_types;

        return $this;
    }

    /**
     * Sets the maximum number of files this field can upload. Null for
     * unlimited. Default 5
     *
     * @param integer|string|null $limit
     *
     * @return self
     */
    public function setMaxFiles($limit): self
    {
        if (!(is_numeric($limit) || is_null($limit))) {
            throw new Exception('MaxFiles must be null or a numeric value.');
        }

        $this->max_file_uploads = intval($limit);

        return $this;
    }

    /**
     * Sets the maxiumin file size (in mb) of a single upload via this field
     *
     * @param integer|string $filesize
     *
     * @return self
     */
    public function setMaxFileSize($filesize): self
    {
        if (!is_numeric($filesize)) {
            throw new Exception('Filesize must be a the number of mb for max size.');
        }

        $this->max_file_size_mb = intval($filesize);

        return $this;
    }

    /**
     * Sets the name of the relationship ordering column on this Field
     *
     * @param string $column_name
     *
     * @return self
     */
    public function setRelationshipOrderBy(string $column_name = null): self
    {
        $this->relationship_order_by = $column_name;

        return $this;
    }

    /**
     * Sets the upload path for this Field
     *
     * @param string
     *
     * @return self
     */
    public function setUploadPath(string $value): self
    {
        $this->upload_path = $value;

        return $this;
    }

    /**
     * Convert the field to an array, adding an upload route
     *
     * @return Array
     */
    public function toArray()
    {
        $file_model = $this->getAssociatedModel();

        $output = parent::toArray();
        $output['props']['maxFiles']        = $this->maxFiles();
        $output['props']['maxFileSize']     = $this->maxFileSize();
        $output['props']['uploadRoute']     = route($this->upload_route);
        $output['props']['uploadPath']      = $this->uploadPath();

        if (!empty($this->acceptedFileTypes())) {
            $output['props']['acceptedFileTypes'] = $this->getAcceptedFileTypes(true);
        }

        /**
         * Convert the id's passed by the form back into instances of the
         * associated model, for re-applying to the upload container when
         * validation fails
         */
        if (isset($output['old']) && count($output['old'])) {
            $old_files = $file_model::whereIn('id', $output['old'])->get();
            $output['props']['old'] = $old_files->toArray();
        }

        return $output;
    }

    /**
     * Wrapper function for the setter and getter for upload path
     *
     * @param string|null $upload_path
     *
     * @return self|string
     */
    public function uploadPath(string $upload_path = null)
    {
        if (!is_null($upload_path)) {
            return $this->setUploadPath($upload_path);
        }

        return $this->getUploadPath();
    }
}
