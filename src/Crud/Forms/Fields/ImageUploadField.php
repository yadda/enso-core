<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\Fields\FileUploadField;
use Yadda\Enso\Crud\Forms\SectionInterface;
use Yadda\Enso\Media\Contracts\ImageFile as ImageFileContract;

class ImageUploadField extends FileUploadField implements FieldInterface
{
    protected $accepted_file_types = [
        'image/*'
    ];

    /**
     * Name of the custom vue component
     *
     * @var String
     */
    protected $tag_name = 'enso-field-image-upload';

    /**
     * Model that this upload field type instantiated
     *
     * @var String
     */
    protected $file_model;

    /**
     * Whether or not this field type can be included in flexible content
     *
     * @var Boolean
     */
    protected static $flexible_field = true;

    /**
     * Laravel validation rules which can be converted to html validation rules
     * for the form input
     *
     * @var Array
     */
    protected $applicable_validation_rules = [
        'required'
    ];

    /**
     * Create a new instance
     *
     * @param String $name
     */
    public function __construct($name)
    {
        parent::__construct($name);

        $this->file_model = get_class(resolve(ImageFileContract::class));

        $this->data_callback = function ($item) {
            return [
                'id' => $item->id,
                'filename' => $item->original_filename,
                'filesize' => $item->filesize,
                'path' => $item->path,
                'preview' => $item->getPreview(),
            ];
        };
    }


    /**
     * Expand the stored JSON version of this field
     * for use in forms
     *
     * @param  Array $field Data from the database
     * @return Array        Data for form
     */
    public static function expandForJson($field)
    {
        $placeholder = resolve(ImageFileContract::class);
        $field['content'] = $placeholder::whereIn($placeholder->getKeyName(), $field['content'])->get();

        foreach ($field['content'] as $image) {
            $image['preview'] = $image->getPreview();
        }

        return $field;
    }

    /**
     * Compress the data into a format suitable for
     * storing in the database as JSON
     *
     * @param  Array $field Data from the form
     * @return Array        Data for database
     */
    public static function compressForJson($field)
    {
        $key_name = resolve(ImageFileContract::class)->getKeyName();

        $files = collect($field['content']);
        $file_ids = $files->pluck($key_name)->toArray();
        $field['content'] = $file_ids;
        return $field;
    }

    /**
     * Gets the correct value for this field from the passed data for a
     * Collection Section, i.e. for use when storing in json, e.g. for settings
     *
     * @param  object   $item           Data source
     * @param  string   $property_name  Override property name
     *
     * @return mixed                    matched data
     */
    public function getCollectionSectionFormData($item, $property_name = null)
    {
        $decoded = $this->getFormData($item, $property_name);

        if (!empty($item) && count($decoded)) {
            return array_map(function ($item_array) {
                if (!isset($item_array['id'])) {
                    return $item_array;
                }

                $image = $this->file_model::find($item_array['id']);
                if (!$image) {
                    return $item_array;
                }

                $item_array = $image->toArray();
                $item_array['preview'] = $image->getResizeUrl('uploader_preview', true);

                return $item_array;
            }, $decoded);
        }

        return $decoded;
    }
}
