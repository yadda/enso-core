<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Carbon\Carbon;

use Yadda\Enso\Crud\Forms\Fields\BelongsToManyField as BaseRelationshipField;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\Fields\SelectField;
use Yadda\Enso\Crud\Forms\SectionInterface;

/**
 * An actual relationship field. Allows selecting other item[s] from a dropdown.
 */
class OrderableRelationshipField extends BaseRelationshipField implements FieldInterface
{
    /**
     * Name of the custom vue component
     *
     * @var string
     */
    protected $tag_name = 'enso-field-orderable-relationship';

    /**
     * Hook for modifying the request data, if required
     *
     * @param  mixed    $data           Original data
     * @return mixed                    Modified data
     */
    protected function modifyRequestData($data)
    {
        if (empty($data) || !is_array($data)) {
            return $this->getDefaultValue();
        }

        $keys = [];
        $now = Carbon::now();
        $count = count($data);
        foreach ($data as $single) {
            $keys[$single[$this->getSetting('track_by')]] = [
                'order' => $count--,
                'created_at' => $now,
                'updated_at' => $now
            ];
        }

        return $keys;
    }
}
