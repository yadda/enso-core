<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Media\Contracts\ImageFile as ImageFileContract;

class GalleryField extends FileUploadFieldResumable implements FieldInterface
{
    /**
     * Maximum number of uploads to allow
     *
     * Once each file is uploaded it is removed from the dropzone element and
     * put into an array, so it kind of handles them one at a time. However a
     * user can drag multiple files onto the dropzone so it must be allow this.
     *
     * @var integer
     */
    protected $max_file_uploads = 1000;

    /**
     * Can this field type be used in a flexible content field
     *
     * @var boolean
     */
    protected static $flexible_field = false;

    /**
     * Name of the custom vue component
     *
     * @var string
     */
    protected $tag_name = 'enso-field-gallery';

    /**
     * Laravel validation rules which can be converted to html validation rules
     * for the form input
     *
     * @var array
     */
    protected $applicable_validation_rules = [
        'required'
    ];

    /**
     * Create a new GalleryField
     *
     * @param String $name
     */
    public function __construct($name)
    {
        parent::__construct($name);

        $this->file_model = get_class(resolve(ImageFileContract::class));

        $this->data_callback = function ($item) {
            return [
                'id' => $item->id,
                'filename' => $item->original_filename,
                'filesize' => $item->filesize,
                'path' => $item->path,
                'preview' => $item->getPreview(),
            ];
        };
    }

    /**
     * Expand the stored JSON version of this field
     * for use in forms
     *
     * @param  Array $field Data from the database
     * @return Array        Data for form
     */
    public static function expandForJson($field)
    {
        $ids = $field['content'];
        $placeholder = resolve(ImageFileContract::class);
        $field['content'] = $placeholder::whereIn($placeholder->getKeyName(), $ids)->get();

        foreach ($field['content'] as $image) {
            $image['preview'] = $image->getPreview();
        }

        // Re-order images to preserve the order as stored in the database
        $field['content'] = $field['content']->sortBy(function ($model) use ($ids) {
            return array_search($model->getKey(), $ids);
        })->values();

        return $field;
    }

    /**
     * Compress the data into a format suitable for
     * storing in the database as JSON
     *
     * @param  Array $field Data from the form
     * @return Array        Data for database
     */
    public static function compressForJson($field)
    {
        $key_name = resolve(ImageFileContract::class)->getKeyName();

        $files = collect($field['content']);
        $file_ids = $files->pluck($key_name)->toArray();
        $field['content'] = $file_ids;
        return $field;
    }

    /**
     * Applies data to a given item. Can be overriden to provide funcionality
     * for non-simple data.
     *
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     *
     * @return self
     */
    public function applyRequestData(Model &$item, $data)
    {
        $field_name = $this->getName();
        $value = $this->getRequestData($data);
        $item->fill([$field_name => $value]);
    }


    /**
     * Applies data to a given item. Can be overriden to provide funcionality
     * for non-simple data.
     *
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     *
     * @return self
     */
    public function applyRequestDataAfterSave(Model &$item, $data)
    {
    }

    /**
     * Gets the correct value for this field from the passed data
     *
     * @param  object   $item           Data source
     * @param  string   $property_name  Override property name
     * @return mixed                    matched data
     */
    public function getFormData($item, $property_name = null)
    {
        return Field::getFormData($item, $property_name);
    }
}
