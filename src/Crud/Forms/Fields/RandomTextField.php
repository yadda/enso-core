<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;

class RandomTextField extends Field implements
    FieldInterface,
    HasCorrectTyping
{
    protected $default = '';

    protected $tag_name = 'enso-field-random-text';

    protected $applicable_validation_rules = ['required'];

    protected static $flexible_field = true;

    protected $text_length = 12;

    /**
     * Gets data to be filtered by text filters from the passed data
     *
     * @param mixed $data
     *
     * @return mixed
     */
    public function getTextData($data)
    {
        return $data;
    }

    /**
     * Returns the filtered data in the correct format
     *
     * @param mixed $data
     * @param mixed $original
     *
     * @return mixed
     */
    public function setTextData($data, $original)
    {
        return $data;
    }

    /**
     * Set the length of the generated text
     *
     * @param int $length
     * @return self
     */
    public function setTextLength($length)
    {
        $this->text_length = $length;

        return $this;
    }

    /**
     * Get the length of the generated text
     *
     * @return int
     */
    public function getTextLength()
    {
        return $this->text_length;
    }

    /**
     * Get an array of props to apply to the vue component.
     *
     * These can be override by using setProp
     *
     * @return array
     */
    public function getProps()
    {
        $props = parent::getProps();

        $props['length'] = $this->getTextLength();

        return $props;
    }
}
