<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\SectionInterface;

class DateField extends Field implements
    FieldInterface,
    HasCorrectTyping
{
    protected $default = '';

    protected $tag_name = 'enso-field-date';

    protected $applicable_validation_rules = ['required'];

    protected static $flexible_field = true;

    /**
     * Applies data to a given item. Can be overriden to provide funcionality
     * for non-simple data.
     *
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     */
    public function applyRequestData(Model &$item, $data)
    {
        $field_name = $this->getName();

        $value = $this->getRequestData($data);

        if (!$value) {
            $value = null;
        } else {
            $value = Carbon::parse($value['date']);
        }

        $item->fill([$field_name => $value]);
    }

    /**
     * Gets the correct value for this field from the passed data
     *
     * @param  object   $item           Data source
     * @param  string   $property_name  Override property name
     * @return mixed                    matched data
     */
    public function getFormData($item, $property_name = null)
    {
        $value = parent::getFormData($item, $property_name);

        if (is_null($value)) {
            return $this->getDefaultValue();
        }

        return $value;
    }
}
