<?php

namespace Yadda\Enso\Crud\Forms;

use Yadda\Enso\Crud\Contracts\RelationshipSection as RelationshipSectionInferface;
use Yadda\Enso\Crud\Forms\Section;

abstract class RelationshipSection extends Section implements RelationshipSectionInferface
{
    /**
     * Whether this relationship section is allowed to have a null relation,
     * whether it must create at a new instance to be related on save.
     *
     * @var boolean
     */
    protected $allow_null_relation = true;

    protected $relation_callback;

    /**
     * Gets the currently set relation callback.
     *
     * @return callable|null
     */
    public function getRelationCallback()
    {
        return $this->relation_callback;
    }

    /**
     * Sets the Callback to call on the relation (with the data)
     * before saving the actual model. Returning false will prevent
     * saving changes.
     *
     * @param callable $callable
     *
     * @return self
     */
    public function setRelationCallback(callable $callable)
    {
        $this->relation_callback = $callable;

        return $this;
    }

    /**
     * Sets whether or not this Section should allow for the relation
     * empty
     *
     * @param boolean $value
     *
     * @return self
     */
    public function setAllowNullRelation($value)
    {
        $this->allow_null_relation = (bool)$value;

        return $this;
    }

    /**
     * Returns whether or not this Section has been set to allow for
     * empty relationships.
     *
     * @return boolean
     */
    public function getAllowNullRelation()
    {
        return $this->allow_null_relation;
    }

    /**
     * Helper function either set or get the allow_null_relation
     * state, depending on the argument passed (or not)
     *
     * @param null|bool $value
     *
     * @return bool|self
     */
    public function allowNullRelation($value = null)
    {
        if (is_null($value)) {
            return $this->getAllowNullRelation();
        }

        return $this->setAllowNullRelation($value);
    }

    /**
     * Checks to see whether there is any provided relation-data.
     *
     * @param array $data
     *
     * @return boolean
     */
    protected function hasRelationData($data)
    {
        if (!isset($data[$this->getName()])) {
            return false;
        }

        $relation_data = [];

        foreach ($this->getFields() as $field) {
            $relation_data[$field->getName()] = $field->getRequestData($data);
        }

        // This may need updating, but for now we can't use empty() as '0' could
        // be a legitimate value, whereas null and empty string are unlikely to be 'filled'
        // data.
        return !empty(array_filter($relation_data, function ($value) {
            return ($value !== null) && ($value !== '');
        }));
    }
}
