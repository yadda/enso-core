<?php

namespace Yadda\Enso\Crud\Forms;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Gate;

class FieldCollection extends Collection
{
    /**
     * Section that this collection is associated with
     *
     * @var SectionInterface
     */
    protected $section;

    /**
     * Setes the section that this collection is associated with
     *
     * @param SectionInterface $section
     *
     * @return FieldCollection
     */
    public function setSection(SectionInterface $section)
    {
        $this->section = $section;

        return $this;
    }

    public function getFormData($item)
    {
        return array_map(function ($field) use ($item) {
            return $field->getFormData($item);
        }, $this->items);
    }

    /**
     * Converts the fields stored in this collection into the correct data
     * structure for the Vue components.
     *
     * @return array
     */
    public function toArray(): array
    {
        return array_combine(
            $this->getFieldKeys(),
            $this->getFieldValues()
        );
    }

    /**
     * Determines whether the associated section is disabled.
     *
     * @return boolean
     */
    protected function sectionIsDisabled(): bool
    {
        if (!$this->section || is_null($this->section->getEditPermission())) {
            return false;
        }

        return Gate::denies('has-permission', $this->section->getEditPermission());
    }

    /**
     * Gets a list of keys for unrestricted fields
     *
     * @return array
     */
    protected function getFieldKeys()
    {
        return array_filter(array_map(function ($field) {
            if ($field->isRestricted()) {
                return null;
            }

            return $field->getName();
        }, $this->items));
    }

    /**
     * Gets a formatted data array for each unrestricted field.
     *
     * @return array
     */
    protected function getFieldValues(): array
    {
        return array_filter(
            array_map(function ($field) {
                if ($field->isRestricted()) {
                    return null;
                }

                if ($this->sectionIsDisabled()) {
                    $field->setDisabled(true);
                }

                return array_merge(
                    $field->toArray(),
                    [
                        'default' => $field->getDefaultValue(),
                        'field' => get_class($field),
                    ]
                );
            }, $this->items)
        );
    }
}
