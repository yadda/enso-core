<?php

namespace Yadda\Enso\Crud\Forms;

use Yadda\Enso\Crud\Contracts\FlexibleFieldParser as FlexibleFieldParserContract;

class FlexibleFieldParser implements FlexibleFieldParserContract
{
    /**
     * Expands the given data where needed, from a shortened DB friendly format
     * to a a full representation of the data (eg instantiate models based on a
     * pre-defined class, using the stored ids)
     *
     * @param  array    $data       Data to expand
     * @return array                Expanded data
     */
    public function expand($data)
    {
        if (is_null($data)) {
            $data = [];
        }

        foreach ($data as &$row) {
            foreach ($row['fields'] as $block_index => &$block) {
                if (empty($block['field'])) {
                    unset($row['fields'][$block_index]);
                    continue;
                }

                $block = call_user_func($block['field'] . '::expandForJson', $block);
            }
        }

        return $data;
    }

    /**
     * Compresses the given data where needed, from a full representation to a
     * shortened, DB friendly format (eg Referencing models by pre-defined class
     * and ID instead of full model data)
     *
     * @param  array    $data       Data to compress
     * @return array                Compressed data
     */
    public function compress($data)
    {
        foreach ($data as &$row) {
            foreach ($row['fields'] as $block_index => &$block) {
                if (empty($block['field'])) {
                    unset($row['fields'][$block_index]);
                    continue;
                }

                $block = call_user_func($block['field'] . '::compressForJson', $block);
            }
        }

        return $data;
    }
}
