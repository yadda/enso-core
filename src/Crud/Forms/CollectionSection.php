<?php

namespace Yadda\Enso\Crud\Forms;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use stdClass;
use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Contracts\UpdatesMultipleProperties;
use Yadda\Enso\Crud\Forms\FieldCollection;
use Yadda\Enso\Crud\Forms\Section;

class CollectionSection extends Section
{
    protected $collection_key = 'slug';

    protected $collection_value = 'decoded';

    /**
     * Sets the collection key of this Section
     *
     * @param string $key Name of column to key collection by
     */
    public function setCollectionKey($key)
    {
        $this->collection_key = $key;

        return $this;
    }

    /**
     * Gets the current collection key
     *
     * @return string Name of column to key collection by
     */
    public function getCollectionKey()
    {
        return $this->collection_key;
    }

    /**
     * Helper function for getting/setting the collection key
     *
     * @param string $key Name of column to key collection by
     *
     * @return mixed Current key or self, depending on arguments
     */
    public function collectionKey($key = null)
    {
        if (is_null($key)) {
            return $this->getCollectionKey();
        } else {
            return $this->setCollectionKey($key);
        }
    }

    /**
     * Sets the collection 'value' column name of this Section
     *
     * @param string $key Name of column to use for value
     */
    public function setCollectionValue($value_column)
    {
        $this->collection_value = $value_column;

        return $this;
    }

    /**
     * Gets the current collection 'value' column name
     *
     * @return string Name of column to use for value
     */
    public function getCollectionValue()
    {
        return $this->collection_value;
    }

    /**
     * Helper function for getting/setting the collection value column name
     *
     * @param string $key Name of column to use for value
     *
     * @return mixed Current value column name or self, depending on arguments
     */
    public function collectionValue($value_column = null)
    {
        if (is_null($value_column)) {
            return $this->getCollectionValue();
        } else {
            return $this->setCollectionValue($value_column);
        }
    }

    /**
     * Get interal field array
     *
     * @return \Yadda\Enso\Crud\Forms\FieldCollection Fields
     */
    public function getFields(): FieldCollection
    {
        $fields = parent::getFields();

        foreach ($fields as $field) {
            if (!($field instanceof HasCorrectTyping)) {
                throw new Exception(
                    'CollectionSection fields `' . class_basename($field) .
                        '` must implement HasCorrectTyping Interface'
                );
            }
        }

        return $fields;
    }

    /**
     * Applies data to the given item
     *
     * @param Model $repository Item to apply data to
     * @param array $data       All data
     */
    public function applyRequestData(&$repository, array $data)
    {
        foreach ($this->getFields() as $field) {
            if (!$field->shouldWriteData()) {
                continue;
            }

            $field_name = $field->getName();
            $field_data = $field->getRequestData($data);

            if ($field instanceof UpdatesMultipleProperties) {
                // Get all related setting names, then apply either a 'yes' or
                // 'no' value as given by the field itself
                $option_names = $field->getMultipleValueNames();
                foreach ($option_names as $option) {
                    if ($this->isValueTruthy($option, $field_data)) {
                        $new_value = $field->getDefaultSingleSetValue($data);
                    } else {
                        $new_value = $field->getDefaultSingleUnsetValue($data);
                    }

                    $repository->set($option, $new_value, $field->getDataType());
                }
            } else {
                $repository->set($field_name, $field_data, $field->getDataType());
            }
        }
    }


    /**
     * Check if a value in a given dataset is set and truthy
     *
     * @param string $option
     * @param array  $field_data
     *
     * @return boolean true if option exists and is truthy
     */
    public function isValueTruthy($option, $field_data)
    {
        if (is_null($field_data)) {
            return false;
        }

        if (array_key_exists($option, $field_data)) {
            return !!$field_data[$option];
        }

        return false;
    }

    /**
     * Gets the data associated with each of the fields and stores in a
     * formatted structure for passing to the vue components
     *
     * @todo   Find a better way to use / pass the collection to this section
     *
     * @param object $item data source
     *
     * @return mixed Found data values
     */
    public function getFormData($item)
    {
        $collection = $item->all()->keyBy($this->collectionKey());

        $form_data = [];
        foreach ($this->getFields() as $field) {
            $field_name = $field->getName();

            if ($field instanceof UpdatesMultipleProperties) {
                $form_data[$field_name] = $this->getMultiPropertyFormValues($collection, $field);
            } else {
                $item = $collection->get($field_name);

                if (is_null($item)) {
                    $model = $this->getModel();
                    $item = new $model;
                }

                $form_data[$field_name] = $field->getCollectionSectionFormData($item, $this->collectionValue());
            }
        }

        if (empty($form_data)) {
            return new stdClass;
        }

        return $form_data;
    }

    /**
     * Filters the collection to return only options relevant to the given
     * UpdatesMultipleProperties field
     *
     * @param \Illuminate\Support\Collection                       $collection Collection to filter
     * @param \Yadda\Enso\Crud\Contracts\UpdatesMultipleProperties $field      Field to base filtering on
     *
     * @return \Illuminate\Support\Collection Filtered results
     */
    protected function getFilteredOptions(Collection $collection, UpdatesMultipleProperties $field)
    {
        $key_field = $this->collectionKey();
        $option_names = $field->getMultipleValueNames();

        return $collection->filter(function ($item) use ($option_names, $key_field) {
            return in_array($item->$key_field, $option_names);
        });
    }

    /**
     * Gets correct data values for the form, base on field type
     *
     * @param \Illuminate\Support\Collection $collection All options
     * @param \Yadda\Enso\Crud\Forms\Field   $field      Field to use to get relevant options
     *
     * @return array Form data for the given field
     */
    protected function getMultiPropertyFormValues(Collection $collection, UpdatesMultipleProperties $field)
    {
        $matched_options = $this->getFilteredOptions($collection, $field);

        $key_field = $this->collectionKey();
        $value_field = $this->collectionValue();

        $values = [];
        foreach ($matched_options as $option) {
            if (!empty($option->$value_field)) {
                $values[$option->$key_field] = $option->$key_field;
            }
        }

        if (empty($values)) {
            $values = $field->getDefaultValue();
        }

        return $values;
    }
}
