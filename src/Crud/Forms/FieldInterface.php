<?php

namespace Yadda\Enso\Crud\Forms;

use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Forms\SectionInterface;

interface FieldInterface
{
    public static function make(string $name);

    public function setName(string $name);

    public function getName(): string;

    public function setLabel(string $label);

    public function getLabel();

    public function setPlaceholder(string $placeholder);

    public function getPlaceholder();

    public function setHelpText(string $help_text);

    public function getHelpText();

    public function setClasses(array $classes);

    public function getClasses();

    public function addClass(string $class);

    public function removeClass(string $class);

    public function setFieldsetClasses(array $classes);

    public function getFieldsetClasses();

    public function addFieldsetClass(string $class);

    public function removeFieldsetClass(string $class);

    public function setValidation($validation);

    public function getValidation();

    public function addTo(SectionInterface $section);

    public function applyRequestData(Model &$item, $value);

    public function applyRequestDataAfterSave(Model &$item, $value);

    public function setReadonly($is_readonly = true);

    public function setDisabled($is_disabled = true);
}
