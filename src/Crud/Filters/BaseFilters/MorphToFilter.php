<?php

namespace Yadda\Enso\Crud\Filters\BaseFilters;

use Illuminate\Database\Eloquent\Builder;
use Yadda\Enso\Crud\Exceptions\CrudException;
use Yadda\Enso\Crud\Filters\BaseFilters\TextFilter;

class MorphToFilter extends TextFilter
{
    /**
     * Map of morph types and columns that should be searched for those morphs.
     *
     * @var array
     */
    protected $morph_columns = [];

    /**
     * Query modifier
     *
     * @param Builder    $query
     * @param mixed|null $value
     *
     * @return void
     */
    public function callable(Builder $query, $value): void
    {
        $value = $this->parseValue($value);

        if (is_null($value) || $value === '') {
            return;
        }

        if (count($this->morph_columns) === 0) {
            throw new CrudException('MorphTo Filter incorrectly set up: No morph columns specified for searching');
        }

        $query->whereHasMorph(
            $this->relationship_name,
            array_keys($this->morph_columns),
            function ($query, $type) use ($value) {
                foreach ($this->morph_columns as $type_key => $columns) {
                    if ($type === $type_key) {
                        $this->columns = $columns;
                        $this->applyQueryModifications($query, $value);
                    }
                }
            }
        );
    }

    /**
     * Gets or sets the morph columns
     *
     * @param array|null $morph_columns
     *
     * @return mixed
     */
    public function morphColumns(?array $morph_columns)
    {
        if (is_null($morph_columns)) {
            return $this->getMorphColumns();
        }

        return $this->setMorphColumns($morph_columns);
    }
}
