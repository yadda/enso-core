<?php

namespace Yadda\Enso\Crud\Filters\BaseFilters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use InvalidArgumentException;
use Yadda\Enso\Crud\Exceptions\CrudException;
use Yadda\Enso\Crud\Filters\BaseFilters\BaseFilter;

class SelectFilter extends BaseFilter
{
    /**
     * Hard coded options to use for this Select field if not using an xhr
     * query
     *
     * @var array
     */
    protected $options;

    /**
     * Type of filter
     *
     * @var string
     */
    protected $type = 'select';

    /**
     * Gets the props property
     *
     * The ability to get Optinos directly has been prevented to mirror the
     * functionality provided by fields. Options should be set directly.
     *
     * @return array
     */
    public function getSettings(): array
    {
        return Arr::except($this->settings, 'options');
    }

    /**
     * Gets or sets the options
     *
     * @param array|null $options
     *
     * @return mixed
     */
    public function options(?array $options)
    {
        if (is_null($options)) {
            return $this->getOptions();
        }

        return $this->setOptions($options);
    }

    /**
     * Parses the value received by the filter
     *
     * Additionally filters out values which are not present in the options
     *
     * @param mixed $value
     *
     * @return mixed
     */
    public function parseValue($value)
    {
        return Arr::get($value, 'id', null);
    }

    /**
     * Sets the settings on this filter
     *
     * @param array $settings
     * @param bool  $merge
     *
     * @return self
     */
    public function setSettings(array $settings, bool $merge = false)
    {
        if (array_key_exists('options', $settings)) {
            throw new InvalidArgumentException(
                'Updating options via settings is deprecated. Use options() '
                . 'instead'
            );
        }

        if ($merge) {
            $this->settings = array_merge(
                $this->settings,
                $settings
            );
        } else {
            $this->settings = $settings;
        }

        return $this;
    }

    /**
     * Outputs this instance into a data array for the Crud.
     *
     * @return array
     */
    public function toArray(): array
    {
        if (!count($this->options)) {
            throw new CrudException('Select Filters must have at least one option specified');
        }

        if (!$this->hasAccess()) {
            return [];
        }

        return array_merge(
            parent::toArray(),
            [
                'props' => array_merge(
                    $this->props,
                    [
                        'settings' => array_merge(
                            $this->settings,
                            [
                                'options' => $this->mappedOptions(),
                            ],
                        ),
                    ],
                ),
            ],
        );
    }

    /**
     * Apply columns to the given query as a self-container where statement
     *
     * @param Builder $query
     * @param mixed   $value
     *
     * @return void
     */
    protected function applyQueryModifications(Builder $query, $value): void
    {
        $query->where(function ($query) use ($value) {
            foreach ($this->columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $value . '%');
            }
        });
    }

    /**
     * Maps options into array elements with an id and name to work with default
     * select filter settings.
     *
     * @return array
     */
    protected function mappedOptions(): array
    {
        if (is_array(Arr::first($this->options))) {
            return $this->options;
        }

        return array_map(function ($key, $value) {
            return [
                Config::get('enso.settings.select_track_by') => $key,
                Config::get('enso.settings.select_label_by') => $value,
            ];
        }, array_keys($this->options), $this->options);
    }
}
