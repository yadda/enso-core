<?php

namespace Yadda\Enso\Crud\Filters;

use Yadda\Enso\Crud\Filters\BaseFilters\TextFilter;

class PageFilter extends TextFilter
{
    /**
     * Columns to search in
     *
     * @var array
     */
    protected $columns = [
        'title',
    ];

    /**
     * Label to apply to the filter
     *
     * @var string
     */
    protected $label = 'Search';

    /**
     * Props to apply to the filter
     *
     * @var array
     */
    protected $props = [
        'placeholder' => 'Search...',
        'help-text' => 'Search by title',
    ];
}
