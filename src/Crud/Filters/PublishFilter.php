<?php

namespace Yadda\Enso\Crud\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Filters\BaseFilters\SelectFilter;
use Yadda\Enso\Facades\EnsoCrud;

class PublishFilter extends SelectFilter
{
    /**
     * Columns to search in
     *
     * @var array
     */
    protected $columns = [
        'id'
    ];

    /**
     * Label to apply to the filter
     *
     * @var string
     */
    protected $label = 'Is Published';

    /**
     * Settings to apply to the filter
     *
     * @var array
     */
    protected $settings = [
        'allow_empty' => false,
        'show_labels' => false,
    ];

    public function __construct(string $crud_name)
    {
        $track_by = Config::get('enso.settings.select_track_by');
        $label_by = Config::get('enso.settings.select_label_by');

        $this->default([
            $track_by => 'all',
            $label_by => 'All',
        ]);

        $this->setSettings([
            'track_by' => $track_by,
            'label' => $label_by,
        ]);

        $model_class = EnsoCrud::modelClass($crud_name);

        $this->options(array_merge(
            [
                [$track_by => 'all', $label_by => 'All'],
                [$track_by => 'published', $label_by => 'Published'],
            ],
            (new $model_class)->getPublishAtColumn()
                ? [[$track_by => 'future', $label_by => 'Future']]
                : [],
            [
                [$track_by => 'unpublished', $label_by => 'Unpublished'],
            ]
        ));
    }

    /**
     * Apply columns to the given query as a self-container where statement
     *
     * @param Builder $query
     * @param mixed   $value
     *
     * @return void
     */
    protected function applyQueryModifications(Builder $query, $value): void
    {
        switch ($value) {
            case 'future':
                $query->willPublishLater();
                break;
            case 'unpublished':
                $query->notPublished();
                break;
            case 'published':
                $query->published();
                break;
            case 'all':
            default:
                // Do nothing
        }

        // $query->where(function ($query) use ($value) {
        //     foreach ($this->columns as $column) {
        //         $query->orWhere($column, 'LIKE', '%' . $value . '%');
        //     }
        // });
    }
}
