<?php

namespace Yadda\Enso\Crud\Filters;

use Yadda\Enso\Crud\Filters\BaseFilters\TextFilter;

class UserFilter extends TextFilter
{
    /**
     * Columns to search in
     *
     * @var array
     */
    protected $columns = [
        'display_name',
        'email',
        'real_name',
        'username',
    ];

    /**
     * Label to apply to the filter
     *
     * @var string
     */
    protected $label = 'Search';

    /**
     * Props to apply to the filter
     *
     * @var array
     */
    protected $props = [
        'placeholder' => 'Search...',
        'help-text' => 'User\'s name, display name, username or email',
    ];
}
