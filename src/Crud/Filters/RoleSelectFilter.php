<?php

namespace Yadda\Enso\Crud\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;
use Yadda\Enso\Crud\Filters\BaseFilters\SelectFilter;
use Yadda\Enso\Users\Contracts\Role;

class RoleSelectFilter extends SelectFilter
{
    /**
     * Columns to search in
     *
     * @var array
     */
    protected $columns = [
        'id'
    ];

    /**
     * Initial value to apply to the filter
     *
     * @var mixed
     */
    protected $default = ['id' => 'all', 'name' => 'All Users'];

    /**
     * Label to apply to the filter
     *
     * @var string
     */
    protected $label = 'User Roles';

    /**
     * Props to apply to the filter
     *
     * @var array
     */
    protected $props = [
        'placeholder' => 'Search...',
        'help-text' => 'Users with the selected Role',
    ];

    /**
     * Name of a relationship to query
     *
     * Populate to search by relationship. Leave blank to search base query
     *
     * @var string
     */
    protected $relationship_name = 'roles';

    /**
     * Settings to apply to the filter
     *
     * @var array
     */
    protected $settings = [
        'label' => 'name',
        'track_by' => 'id',
        'allow_empty' => false,
        'show_labels' => false,
    ];

    public function __construct()
    {
        // Have to add arrays as array_merge reindexes numeric keys
        $this->options(
            [
                'all' => 'All Users',
                'any' => 'Any Roles',
            ]
            + App::make(Role::class)::orderBy('name', 'ASC')->pluck('name', 'id')->toArray()
            + [
                'none' => 'None (Regular user)',
            ]
        );
    }

    /**
     * Apply value as a relationship modifier
     *
     * @param Builder $query
     * @param mixed   $value
     *
     * @return void
     */
    protected function applyAsRelationship(Builder $query, $value): void
    {
        switch ($value) {
            case 'all':
                break;
            case 'any':
                $query->has($this->relationship_name);
                break;
            case 'none':
                $query->has($this->relationship_name, '=', 0);
                break;
            default:
                $query->whereHas($this->relationship_name, function ($query) use ($value) {
                    $this->applyQueryModifications($query, $value);
                });
        }
    }

    /**
     * Apply columns to the given query as a self-contained where statement
     *
     * @param Builder $query
     * @param mixed   $value
     *
     * @return void
     */
    protected function applyQueryModifications(Builder $query, $value): void
    {
        if (in_array($value, ['all', 'any'])) {
            // Do not modify query to show all
        } elseif ($value === 'none' && !$this->relationship_name) {
            $query->where(function ($query) use ($value) {
                foreach ($this->columns as $column) {
                    $query->where(function ($query) use ($column) {
                        $query->where($column, '')
                            ->orWhereNull($column);
                    });
                }
            });
        } else {
            $query->where(function ($query) use ($value) {
                foreach ($this->columns as $column) {
                    $query->orWhere($column, $value);
                }
            });
        }
    }
}
