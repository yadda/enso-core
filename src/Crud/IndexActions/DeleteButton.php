<?php

namespace Yadda\Enso\Crud\IndexActions;

use Yadda\Enso\Crud\IndexActions\BaseActions\BaseActionButton;

class DeleteButton extends BaseActionButton
{
    /**
     * Content for this action's button
     *
     * @var string
     */
    protected $button_content = "fa fa-trash-o";

    /**
     * Confirmation modal settings
     *
     * @var array
     */
    protected $confirm = [
        'title' => 'Sure?',
        'text' => 'Are you sure you want to delete this item?',
        'type' => 'warning',
    ];

    /**
     * The HTTP method to use
     *
     * @var string
     */
    protected $method = 'DELETE';

    /**
     * Order of this action
     *
     * @var string
     */
    protected $order = 9999;

    /**
     * Route this button should direct to. The value in this property should be
     * the segments of the URL that come after it's base admin url. Add '%ID%'
     * in your route to denote the ID of the model that the index table line
     * represents.
     *
     * @var string
     */
    protected $route = '/%ID%';

    /**
     * Title property of the action
     *
     * @var string
     */
    protected $title = 'Delete';

    /**
     * Class of the wrapping element for this action
     *
     * @var string
     */
    protected $wrapper_class = 'button is-danger';
}
