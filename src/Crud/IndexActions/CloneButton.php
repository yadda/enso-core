<?php

namespace Yadda\Enso\Crud\IndexActions;

use Yadda\Enso\Crud\IndexActions\BaseActions\BaseLinkButton;

class CloneButton extends BaseLinkButton
{
    /**
     * Content for this action's button
     *
     * @var string
     */
    protected $button_content = "fa fa-clone";

    /**
     * Title property of the action
     *
     * @var string
     */
    protected $title = 'Clone';

    /**
     * Route this button should direct to. The value in this property should be
     * the segments of the URL that come after it's base admin url. Add '%ID%'
     * in your route to denote the ID of the model that the index table line
     * represents.
     *
     * @var string
     */
    protected $route = '/%ID%/clone';

    /**
     * Class of the wrapping element for this action
     *
     * @var string
     */
    protected $wrapper_class = 'button is-primary';
}
