<?php

namespace Yadda\Enso\Crud\IndexActions\BaseActions;

use Yadda\Enso\Crud\IndexActions\BaseActions\BaseLinkButton;

abstract class BaseActionButton extends BaseLinkButton
{
    /**
     * Vue component used to render this action
     *
     * @var string
     */
    protected $component = 'action-button';

    /**
     * Confirmation modal settings
     *
     * @var array
     */
    protected $confirm = [
        'title' => 'Sure?',
        'text' => 'Are you sure?',
        'type' => 'warning',
    ];

    /**
     * The HTTP method to use
     *
     * @var string
     */
    protected $method = 'POST';

    /**
     * Gets or sets the confirm array content.
     *
     * @param array|null $confirm_array
     *
     * @return array|self
     */
    public function confirm(array $confirm_array = null)
    {
        if (is_null($confirm_array)) {
            return $this->getConfirm();
        }

        $this->setConfirm($confirm_array);

        return $this;
    }

    /**
     * Gets the confirm property
     *
     * @return array
     */
    public function getConfirm(): array
    {
        return $this->confirm;
    }

    /**
     * Gets the method property
     *
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Gets or sets the method.
     *
     * @param string|null $method
     *
     * @return string|self
     */
    public function method(string $method = null)
    {
        if (is_null($method)) {
            return $this->getMethod();
        }

        $this->setMethod($method);

        return $this;
    }

    /**
     * Sets the confirm on this
     *
     * @param array $confirm
     *
     * @return self
     */
    public function setConfirm(array $confirm): self
    {
        $this->confirm = $confirm;

        return $this;
    }

    /**
     * Sets the method on this
     *
     * @param string $method
     *
     * @return self
     */
    public function setMethod(string $method): self
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Converts this instance into a data array for the Crud.
     *
     * @return array
     */
    public function toArray(): array
    {
        return array_merge(
            parent::toArray(),
            [
                'confirm' => $this->confirm,
                'method' => $this->method,
            ]
        );
    }
}
