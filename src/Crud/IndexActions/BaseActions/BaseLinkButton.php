<?php

namespace Yadda\Enso\Crud\IndexActions\BaseActions;

use Yadda\Enso\Crud\Contracts\IsIndexAction;

abstract class BaseLinkButton implements IsIndexAction
{
    /**
     * Content for this action's button
     *
     * @var string
     */
    protected $button_content = 'fa-circle-o';

    /**
     * Type of this action's button
     *
     * @var string
     */
    protected $button_type = 'fa';

    /**
     * Vue component used to render this action
     *
     * @var string
     */
    protected $component = 'link-button';

    /**
     * The callable that returns a boolean to determine whether this Buttons
     * appears based on an item.
     *
     * @var callable|null
     */
    protected $condition;

    /**
     * Order of this action
     *
     * @var string
     */
    protected $order = 0;

    /**
     * Route this button should direct to. The value in this property should be
     * the segments of the URL that come after it's base admin url. Add '%ID%'
     * in your route to denote the ID of the model that the index table line
     * represents.
     *
     * eg. '/%ID%/clone' a page with the id of 12  would take you to
     *     http://site.com/admin/pages/12/clone
     *
     * @var string
     */
    protected $route = '%ID%/edit';

    /**
     * Title property of the action
     *
     * @var string
     */
    protected $title = 'Button';

    /**
     * Class of the wrapping element for this action
     *
     * @var string
     */
    protected $wrapper_class = '';

    /**
     * Gets or sets the button content
     *
     * @param string|null $button_content
     *
     * @return string|self
     */
    public function buttonContent(string $button_content = null)
    {
        if (is_null($button_content)) {
            return $this->getButtonContent();
        }

        return $this->setButtonContent($button_content);
    }

    /**
     * Gets or sets the button type
     *
     * @param string|null $button_type
     *
     * @return string|self
     */
    public function buttonType(string $button_type = null)
    {
        if (is_null($button_type)) {
            return $this->getButtonType();
        }

        return $this->setButtonType($button_type);
    }

    /**
     * Gets or sets the component
     *
     * @param string|null $component
     *
     * @return string|self
     */
    public function component(string $component = null)
    {
        if (is_null($component)) {
            return $this->getComponent();
        }

        return $this->setComponent($component);
    }

    /**
     * Gets or sets the condition
     *
     * @param callable|null $condition
     *
     * @return callable|null|self
     */
    public function condition(callable $condition = null)
    {
        if (is_null($condition)) {
            return $this->getCondition();
        }

        return $this->setCondition($condition);
    }

    /**
     * Gets the button content property
     *
     * @return string
     */
    public function getButtonContent(): string
    {
        return $this->button_content;
    }

    /**
     * Gets the button type property
     *
     * @return string
     */
    public function getButtonType(): string
    {
        return $this->button_type;
    }

    /**
     * Gets the component property
     *
     * @return string
     */
    public function getComponent(): string
    {
        return $this->component;
    }

    /**
     * Gets the condition property
     *
     * @return callable|null
     */
    public function getCondition(): ?callable
    {
        return $this->condition;
    }

    /**
     * Gets the order property
     *
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * Gets the route property
     *
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * Gets the title property
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Gets the wrapper class property
     *
     * @return string
     */
    public function getWrapperClass(): string
    {
        return $this->wrapper_class;
    }

    /**
     * Return newly instantiated action
     *
     * @return IsIndexAction
     */
    public static function make(): IsIndexAction
    {
        return new static;
    }

    /**
     * Combine the exiting condition with a new condition.
     *
     * @param callable $new_condition
     * @param boolean  $combine_mode - true = &&, false = ||
     *
     * @return void
     */
    public function mergeCondition(callable $new_condition, bool $combine_mode = true)
    {
        $prior_condition = $this->condition();

        if ($prior_condition) {
            $this->condition(function ($item) use ($prior_condition, $new_condition, $combine_mode) {
                return $combine_mode
                    ? (call_user_func($prior_condition, $item) && call_user_func($new_condition, $item))
                    : (call_user_func($prior_condition, $item) || call_user_func($new_condition, $item));
            });
        } else {
            $this->condition($new_condition);
        }

        return $this;
    }

    /**
     * Gets or sets the order
     *
     * @param int|null $order
     *
     * @return int|self
     */
    public function order(string $order = null)
    {
        if (is_null($order)) {
            return $this->getOrder();
        }

        return $this->setOrder($order);
    }

    /**
     * Gets or sets the route
     *
     * @param string|null $route
     *
     * @return string|self
     */
    public function route(string $route = null)
    {
        if (is_null($route)) {
            return $this->getRoute();
        }

        return $this->setRoute($route);
    }

    /**
     * Sets the button content on this
     *
     * @param string $button_content
     *
     * @return self
     */
    public function setButtonContent(string $button_content): self
    {
        $this->button_content = $button_content;

        return $this;
    }

    /**
     * Sets the button type on this
     *
     * @param string $button_type
     *
     * @return self
     */
    public function setButtonType(string $button_type): self
    {
        $this->button_type = $button_type;

        return $this;
    }

    /**
     * Sets the component on this
     *
     * @param string $component
     *
     * @return self
     */
    public function setComponent(string $component): self
    {
        $this->component = $component;

        return $this;
    }

    /**
     * Sets the condition on this
     *
     * @param callable $condition
     *
     * @return self
     */
    public function setCondition(callable $condition = null): self
    {
        $this->condition = $condition;

        return $this;
    }

    /**
     * Sets the order on this
     *
     * @param string $order
     *
     * @return self
     */
    public function setOrder(string $order): self
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Sets the route on this
     *
     * @param string $route
     *
     * @return self
     */
    public function setRoute(string $route): self
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Sets the title on this
     *
     * @param string $title
     *
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Sets the wrapper class on this
     *
     * @param string $wrapper_class
     *
     * @return self
     */
    public function setWrapperClass(string $wrapper_class): self
    {
        $this->wrapper_class = $wrapper_class;

        return $this;
    }

    /**
     * Gets or sets the title
     *
     * @param string|null $title
     *
     * @return string|self
     */
    public function title(string $title = null)
    {
        if (is_null($title)) {
            return $this->getTitle();
        }

        return $this->setTitle($title);
    }

    /**
     * Converts this instance into a data array for the Crud.
     *
     * @return array
     */
    public function toArray(): array
    {
        $data = [
            'button' => [
                'type' => $this->button_type,
                'content' => $this->button_content,
            ],
            'component' => $this->component,
            'order' => $this->order,
            'route' => $this->route,
            'title' => $this->title,
            'wrapperClass' => $this->wrapper_class,
        ];

        if (!empty($this->condition)) {
            $data['only_when'] = $this->condition;
        }

        return $data;
    }

    /**
     * Gets or sets the wrapper class
     *
     * @param string|null $wrapper_class
     *
     * @return string|self
     */
    public function wrapperClass(string $wrapper_class = null)
    {
        if (is_null($wrapper_class)) {
            return $this->getWrapperClass();
        }

        return $this->setWrapperClass($wrapper_class);
    }
}
