<?php

namespace Yadda\Enso\Crud\Exceptions;

use Exception;

class InvalidCrudClassException extends Exception
{
    //
}
