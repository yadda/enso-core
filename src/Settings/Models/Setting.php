<?php

namespace Yadda\Enso\Settings\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Yadda\Enso\Crud\Contracts\IsCrudModel as ContractsIsCrudModel;
use Yadda\Enso\Crud\Traits\IsCrudModel;

use Yadda\Enso\Media\Contracts\ImageFile as ImageFileContract;
use Yadda\Enso\Settings\Contracts\Model as SettingInterface;

class Setting extends Model implements SettingInterface, ContractsIsCrudModel
{
    use IsCrudModel;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'value' => 'array'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'value'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'enso_settings';

    /**
     * Properly wrap Setting data for saving to the database
     *
     * @param mixed  $value
     * @param string $data_type
     *
     * @return array
     */
    public function encodeData($value, $data_type)
    {
        if (!isset($data_type['type'])) {
            throw new Exception('Setting encode data must specifiy a type.');
        }

        $encoded = [
            'type' => $data_type['type'],
            'content' => array_merge($data_type['content'], ['value' => $value]),
        ];

        return $encoded;
    }

    /**
     * Decode stored Settings, hydrating models where required
     *
     * @return mixed
     */
    public function getDecodedAttribute()
    {
        if (!isset($this->value['type'])) {
            return null;
        }

        switch ($this->value['type']) {
            case 'ordered-collection':
                $class = $this->value['content']['class'];
                $key = $this->value['content']['key'];
                $values = array_keys($this->value['content']['value']);
                $decoded = $this->reorderCollection(call_user_func($class . '::whereIn', $key, $values)->get());
                break;
            case 'collection':
                $class = $this->value['content']['class'];
                $key = $this->value['content']['key'];
                $values = $this->value['content']['value'];

                $decoded = call_user_func($class . '::whereIn', $key, $values)->get();
                break;
            case 'multiple':
                $key = $this->value['content']['key'];
                $decoded = $this->value['content']['value'];
                break;
            case 'single':
                $key = $this->value['content']['key'];
                $values = $this->value['content']['value'];
                if (isset($this->value['content']['class'])) {
                    $class = $this->value['content']['class'];
                    $decoded = call_user_func($class . '::find', $values);
                } else {
                    $decoded = $values;
                }
                break;
            case 'image':
                $image_ids = [];
                foreach ($this->value['content']['value'] as $image_array) {
                    if (isset($image_array['id'])) {
                        $image_ids[] = $image_array['id'];
                    }
                }

                $image_model = resolve(ImageFileContract::class);

                $decoded = $image_model::whereIn('id', $image_ids)->get();
                break;
            case 'scalar':
            default:
                $decoded = $this->value['content']['value'];
                break;
        }

        return $decoded;
    }

    /**
     * Order an orderable collection setting
     *
     * @param mixed $collection
     *
     * @return mixed
     */
    protected function reorderCollection($collection)
    {
        $values = $this->value['content']['value'];

        $with_order = $collection->map(function ($item) use ($values) {
            $item->pivot_order = $values[$item->id]['order'];
            return $item;
        });

        // Values is used to reset the keys so that when the collection is toJson'ed,
        // it won't be considered an associative array and convert to an object.
        return $with_order->sortByDesc('pivot_order')->values();
    }
}
