<?php

namespace Yadda\Enso\Settings\Crud;

use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Forms\CollectionSection;
use Yadda\Enso\Crud\Forms\Fields\CheckboxField;
use Yadda\Enso\Crud\Forms\Fields\EmailField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\SectionInterface;
use Yadda\Enso\Crud\Tables\Text;
use Yadda\Enso\Meta\Crud\MetaCollectionSection;
use Yadda\Enso\Settings\Contracts\Crud as CrudContract;
use Yadda\Enso\Settings\Contracts\Model as ModelContract;

class Setting extends Config implements CrudContract
{
    public function configure()
    {
        $this->setModel(get_class(resolve(ModelContract::class)))
            ->setRoute('admin.settings')
            ->setViewsDir('setting')
            ->setNameSingular('Setting')
            ->setNamePlural('Settings')
            ->setSearchColumns(['name'])
            ->setTableColumns([
                (new Text('id')),
                (new Text('name')),
            ])
            ->setRules([
                'analytics.google-analytics-code' => [
                    'regex:/^UA-\d{4,10}-\d{1,4}$/',
                    'nullable',
                    'sometimes',
                ],
                'analytics.google-tag-manager-code' => [
                    'regex:/^GTM-\w{4,10}$/',
                    'nullable',
                    'sometimes',
                ],
            ]);
    }

    public function create(Form $form)
    {
        $form->addSections([
            $this->createMainSection(),
            $this->createAnalyticsSection(),
            MetaCollectionSection::make('meta'),
        ]);

        foreach (config('enso.settings.extra') as $extra) {
            $form = $extra::updateForm($form);
        }

        return $form;
    }

    /**
     * Get the base route for this controller.
     *
     * @param string $action Action to append to url, e.g. edit
     * @return string
     */
    public function getRoute($action = null)
    {
        if ($action === 'edit') {
            return 'admin.settings.index';
        }

        return parent::getRoute($action);
    }

    /**
     * Generates a 'Main' section for the Settings Crud
     *
     * @return SectionInterface
     */
    protected function createMainSection(): SectionInterface
    {
        return CollectionSection::make('main')->addFields(
            [
                TextField::make('site-name')
                    ->addFieldsetClass('is-half'),
                EmailField::make('administrator-email')
                    ->addFieldsetClass('is-half'),
                CheckboxField::make('holding_page_enabled')
                    ->setLabel('Holding Page')
                    ->setOptions([
                        'holding_page_enabled' => 'Show Holding Page',
                    ]),
            ],
        );
    }

    /**
     * Generates an analytics section for the Settings Crud
     *
     * @return SectionInterface
     */
    protected function createAnalyticsSection(): SectionInterface
    {
        return CollectionSection::make('analytics')
            ->addFields([
                TextField::make('google-analytics-code')
                    ->setPlaceholder('E.g. UA-12345678-9')
                    ->setPattern('^UA-\d{4,10}-\d{1,4}$'),
                TextField::make('google-tag-manager-code')
                    ->setPlaceholder('E.g. GTM-12345678')
                    ->setPattern('^GTM-\w{4,10}'),
            ]);
    }
}
