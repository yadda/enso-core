<?php

namespace Yadda\Enso\Settings\Controllers;

use Enso;
use Eventy;
use Illuminate\Http\Request;
use Yadda\Enso\Crud\Controller;
use Yadda\Enso\Settings\Contracts\Controller as ControllerContract;
use Yadda\Enso\Settings\Contracts\Crud as CrudContract;
use Yadda\Enso\Utilities\Helpers;

/**
 * Back-end controller for Settings
 */
class SettingController extends Controller implements ControllerContract
{
    /**
     * Attempt to load a CrudConfig based on this controller name
     *
     * @return CrudConfig
     */
    protected function loadConfig()
    {
        $this->setConfig(resolve(CrudContract::class));

        Enso::setJSData('crud', $this->getConfig()->getJSConfig());
    }

    /**
     * Show a list of CRUD items.
     *
     * @param \Illuminate\Http\Request $request HTTP request
     *
     * @return String
     */
    public function index(Request $request)
    {
        $this->getConfig()->getEditForm()->setModelInstance(app('ensosettings'));

        Enso::setJSData('crud.form', $this->getConfig()->getJsEditForm());
        Enso::setJSData('crud.item', $this->getConfig()->getEditForm()->getFormData());

        Eventy::action('crud.edit', $this->getConfig(), $request);

        return view(
            $this->getConfig()->getCrudView('edit'),
            [
                'item'      => app('ensosettings'),
                'crud'      => $this->getConfig(),
                'form_view' => $this->getConfig()->getCrudView('forms.form'),
                'action'    => route('admin.settings.update')
            ]
        );
    }

    /**
     * Update settings
     *
     * @param \Illuminate\Http\Request $requestt HTTP request
     * @param integer $id Not used
     *
     * @return void
     */
    public function update(Request $request, $id = null)
    {
        Eventy::action('crud.update.before-validation', $this->getConfig(), $request);

        $this->validate(
            $request,
            Eventy::filter('crud.update.rules', $this->getConfig()->getRules(), $this->getConfig(), $request),
            Eventy::filter('crud.update.messages', $this->getConfig()->getMessages(), $this->getConfig(), $request)
        );

        Eventy::action('crud.update.before', $this->getConfig(), $request);

        $data = Eventy::filter('crud.update.data', $request->all(), $this->getConfig(), $request);

        $item = $this->getConfig()
            ->getEditForm()
            ->setModelInstance(app('ensosettings'))
            ->applyRequestData($data);

        Eventy::action('crud.update.after', $this->getConfig(), $request);

        if ($request->ajax()) {
            return [
                'status' => 'success',
                'data' => null,
                'buttons' => $this->getUpdateButtons($item),
            ];
        } else {
            return $this->getConfig()
                ->getRedirect('update')
                ->with('success', $this->getConfig()->getNameSingular() . ' was updated successfully.');
        }
    }

    /**
     * Returns an array of button definitions to return to with an ajax response
     * to a store request to populate the actions list on success notification.
     *
     * @param Model $item
     *
     * @return array
     */
    protected function getStoreButtons($item)
    {
        return [
            [
                'label' => 'Ok',
                'url' => route($this->getConfig()->getRoute('index')),
            ],
        ];
    }
}
