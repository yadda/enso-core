<?php

namespace Yadda\Enso\Settings\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Enso Settings Facade
 */
class EnsoSettings extends Facade
{
    /**
     * Get the facade accessor
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'ensosettings';
    }
}
