<?php

use Yadda\Enso\Settings\Contracts\Controller as SettingController;
use Yadda\Enso\Utilities\Helpers;

Route::group(['middleware' => ['web', 'enso']], function () {
    Route::get('admin/settings')
        ->uses(Helpers::getConcreteClass(SettingController::class) . '@index')
        ->name('admin.settings.index');

    Route::patch('admin/settings/update')
        ->uses(Helpers::getConcreteClass(SettingController::class) . '@update')
        ->name('admin.settings.update');
});
