<?php

namespace Yadda\Enso\Settings\Contracts;

use Yadda\Enso\Crud\Forms\Form;

interface ExtraSettings
{
    /**
     * Take a form and process it by adding, removing
     * or updating sections or fields.
     *
     * @param Form $form
     *
     * @return Form
     */
    public static function updateForm(Form $form): Form;
}
