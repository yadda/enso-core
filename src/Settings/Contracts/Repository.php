<?php

namespace Yadda\Enso\Settings\Contracts;

interface Repository
{
    /**
     * Gets all of the settings
     *
     * @return \Illuminate\Support\Collection Settings collection
     */
    public function all();

    /**
     * Checks whether a given setting exists, by slug
     *
     * @param string $name Slug of setting
     *
     * @return boolean Whether it exists
     */
    public function has($name);

    /**
     * Gets the value of a given setting
     *
     * @param string $name Slug of setting
     *
     * @return mixed Value of setting
     */
    public function get($name);

    /**
     * Sets a given setting, either updating an existing setting or
     * creating a new one
     *
     * @param string $name      Slug of setting to set
     * @param mixed  $value     Setting value to set
     * @param array  $data_type Array describing the type of data to store
     *
     * @return void
     */
    public function set($name, $value, $data_type);
}
