<?php

namespace Yadda\Enso\Localisation;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

/**
 * A fake model to use for applying data to for JSON data sections
 */
class JsonTranslatable extends Model
{
    use HasTranslations;

    protected $translatable = [];
    protected $fillable = [];
    protected $casts = [];

    public function __construct(array $attributes = [], array $translatables = [], array $fillables = [])
    {
        $this->translatable = $translatables;
        $this->fillable = $fillables;

        foreach ($translatables as $translatable) {
            $this->casts[$translatable] = 'array';

            // This is a hack to get WYSIWYG fields to work. We could do with a
            // more generic way of handling this. Maybe something like the
            // UpdatesMultipleColumns contract...
            $this->translatable[] = $translatable . '_json';
        }

        parent::__construct($attributes);
    }
}
