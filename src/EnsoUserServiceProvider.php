<?php

namespace Yadda\Enso;

use EnsoMenu;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Yadda\Enso\Users\Contracts\Role as RoleContract;
use Yadda\Enso\Users\Contracts\RoleCrud as RoleCrudContract;
use Yadda\Enso\Users\Contracts\User as UserContract;
use Yadda\Enso\Users\Contracts\UserCrud as UserCrudContract;
use Yadda\Enso\Users\Crud\Role as RoleCrudConcrete;
use Yadda\Enso\Users\Crud\User as UserCrudConcrete;
use Yadda\Enso\Users\Models\Role as RoleConcrete;
use Yadda\Enso\Users\Models\User as UserConcrete;

/**
 * Service Provider for the Enso user and roles system
 */
class EnsoUserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $installable_dir = __DIR__ . '/../installable/';

        $this->mergeConfigFrom($installable_dir . 'config/crud/role.php', 'enso.crud.role');
        $this->mergeConfigFrom($installable_dir . 'config/crud/user.php', 'enso.crud.user');

        $this->loadRoutesFrom(__DIR__ . '/Users/routes/web.php');

        $this->publishes([
            __DIR__ . '/../installable/src/Crud/User.php' => app_path('Crud/User.php'),
            __DIR__ . '/../installable/src/Crud/Role.php' => app_path('Crud/Role.php'),
        ], 'enso-crudconfig');

        $this->publishes([
            __DIR__ . '/../installable/src/Models/User.php' => app_path('Models/User.php'),
            __DIR__ . '/../installable/src/Models/Role.php' => app_path('Models/Role.php'),
        ], 'enso-models');

        $this->publishes([
            __DIR__ . '/../installable/src/Controllers/Admin/UserController.php'
            => app_path('Http/Controllers/Admin/UserController.php'),
            __DIR__ . '/../installable/src/Controllers/Admin/RoleController.php'
            => app_path('Http/Controllers/Admin/RoleController.php'),
        ], 'enso-config');

        EnsoMenu::addItems([
            Config::get('enso.crud.user.menuitem'),
            Config::get('enso.crud.role.menuitem'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind(UserContract::class, UserConcrete::class);
        app()->bind(RoleContract::class, RoleConcrete::class);
        app()->bind(UserCrudContract::class, UserCrudConcrete::class);
        app()->bind(RoleCrudContract::class, RoleCrudConcrete::class);
    }
}
