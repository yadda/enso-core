<?php

namespace Yadda\Enso\Facades;

use Illuminate\Support\Facades\Facade;

class EnsoMeta extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ensometa';
    }
}
