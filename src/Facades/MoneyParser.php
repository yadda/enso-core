<?php

namespace Yadda\Enso\Facades;

use Illuminate\Support\Facades\Facade;

class MoneyParser extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'moneyparser';
    }
}
