<?php

namespace Yadda\Enso\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Facade for Enso Crud
 */
class EnsoCrud extends Facade
{
    /**
     * Get the facade accessor
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'ensocrud';
    }
}
