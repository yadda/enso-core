<?php

namespace Yadda\Enso\Facades;

use Illuminate\Support\Facades\Facade;

class EnsoFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'enso';
    }
}
