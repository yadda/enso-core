<?php

namespace Yadda\Enso\Facades;

use Illuminate\Support\Facades\Facade;

class EnsoMenu extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ensomenu';
    }
}
