<?php

namespace Yadda\Enso\Facades;

use Illuminate\Support\Facades\Facade;

class EnsoCategories extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ensocategories';
    }
}
