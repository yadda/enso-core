<?php

namespace Yadda\Enso\Facades;

use Illuminate\Support\Facades\Facade;

class MoneyFormatter extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'moneyformatter';
    }
}
