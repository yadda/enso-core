<?php

use Illuminate\Support\Facades\Route;
use Yadda\Enso\Facades\EnsoCrud;

Route::group(['middleware' => ['web', 'enso']], function () {
    EnsoCrud::crudRoutes('admin/site-menus', 'sitemenu', 'admin.site-menus');
});
