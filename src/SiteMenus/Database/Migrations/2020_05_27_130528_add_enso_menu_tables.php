<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEnsoMenuTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enso_site_menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('title')->nullable();
            $table->string('slug')->unique();
            $table->integer('max_visible_items')->default(5);
            $table->integer('max_items')->default(10);
            $table->timestamps();
        });

        Schema::create('enso_site_menu_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('site_menu_id')->index();
            $table->bigInteger('order')->nullable()->index();
            $table->string('url')->nullable();
            $table->string('label')->nullable();
            $table->string('target')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enso_site_menu_items');
        Schema::dropIfExists('enso_site_menus');
    }
}
