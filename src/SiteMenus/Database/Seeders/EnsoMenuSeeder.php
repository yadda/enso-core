<?php

namespace Yadda\Enso\SiteMenus\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Yadda\Enso\SiteMenus\Contracts\Menu;

class EnsoMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu_model = App::make(Menu::class);

        $menus = $menu_model::all()->keyBy('slug');

        if (!$menus->has('main-menu')) {
            $menu_model::create([
                'name' => 'Main Menu',
                'title' => '',
                'slug' => 'main-menu',
                'max_visible_items' => 3,
                'max_items' => 10,
            ]);
        }

        if (!$menus->has('footer-menu')) {
            $menu_model::create([
                'name' => 'Footer Menu',
                'title' => 'Menu',
                'slug' => 'footer-menu',
                'max_visible_items' => 3,
                'max_items' => 10,
            ]);
        }
    }
}
