<?php

namespace Yadda\Enso\SiteMenus\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

interface MenuItem
{
    /**
     * Menu that this item is in
     *
     * @return BelongsTo
     */
    public function menu(): BelongsTo;
}
