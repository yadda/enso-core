<?php

namespace Yadda\Enso\SiteMenus\Contracts;

use Illuminate\Database\Eloquent\Relations\HasMany;

interface Menu
{
    /**
     * Items in this menu
     *
     * @return HasMany
     */
    public function items(): HasMany;
}
