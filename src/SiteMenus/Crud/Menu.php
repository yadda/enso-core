<?php

namespace Yadda\Enso\SiteMenus\Crud;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Validation\Rule;
use TorMorten\Eventy\Facades\Eventy;
use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Forms\Fields\HasManyRepeaterField;
use Yadda\Enso\Crud\Forms\Fields\SelectField;
use Yadda\Enso\Crud\Forms\Fields\SlugField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Crud\Tables\Text;
use Yadda\Enso\SiteMenus\Contracts\Menu as MenuContract;
use Yadda\Enso\SiteMenus\Contracts\MenuCrud;
use Yadda\Enso\Utilities\Helpers;

class Menu extends Config implements MenuCrud
{
    /**
     * Regex pattern to determins if a given url is a valid url
     */
    const URL_REGEX = '^((https?:)?\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?';

    /**
     * Defines configuration for this CRUD item
     *
     * @return void
     */
    public function configure()
    {
        $model_class = Helpers::getConcreteClass(MenuContract::class);

        $this->model($model_class)
            ->route('admin.site-menus')
            ->views('site-menus')
            ->name('Site Menu')
            ->paginate(25)
            ->order('name', 'ASC')
            ->searchColumns(['name'])
            ->columns([
                (new Text('name')),
                (new Text('slug')),
            ])
            ->rules([
                'main.name' => 'required|string|unique:enso_site_menus,name',
                'main.slug' => ['nullable', 'string', Rule::unique('enso_site_menus', 'slug')],
                'main.max_visible_items' => ['required', 'numeric', 'lte:main.max_items'],
                'main.max_items' => ['required', 'numeric'],
                'main.items.*.fields.label.content' => ['required', 'string'],
            ])->messages([
                'main.items.*.fields.label.content.required' => 'Menu items must have a label',
                'main.items.*.fields.label.content.string' => 'Menu items labels must be strings',
            ]);

        Eventy::addAction('crud.edit', function (Config $config, Request $request) {
            $item = $config->getEditForm()->getModelInstance();

            $config->getEditForm()
                ->getSection('main')
                ->getField('items')
                ->setMaxRows($item->max_items);
        }, 20, 2);

        Eventy::addAction('crud.store.after', [$this, 'dropMenuCache'], 20, 2);
        Eventy::addAction('crud.update.after', [$this, 'dropMenuCache'], 20, 2);
    }

    /**
     * Default form configuration.
     *
     * @return \Yadda\Enso\Crud\Forms\Form
     */
    public function create(Form $form)
    {
        $update_permission = (Auth::check() && Auth::user()->hasPermission('site-menu-update'));

        $form->addSections([
            Section::make('main')->addFields([
                TextField::make('name')
                    ->setHelpText('For admins to identify this menu. Will not be shown publically.')
                    ->addFieldsetClass('is-6'),
                TextField::make('title')
                    ->setHelpText('Public-facing name of this menu')
                    ->addFieldsetClass('is-6'),
                TextField::make('max_visible_items')
                    ->setDefaultValue(0)
                    ->addFieldsetClass('is-6')
                    ->setHelpText('The number of items that will show in the menu. Adding more than this will condense them into a burger menu')
                    ->setDisabled(!$update_permission),
                TextField::make('max_items')
                    ->setDefaultValue(10)
                    ->addFieldsetClass('is-6')
                    ->setDisabled(!$update_permission),
                SlugField::make('slug')
                    ->setRoute('%SLUG%')
                    ->setSource('name'),
                HasManyRepeaterField::make('items')
                    ->setInverseRelationshipName('menu')
                    ->setRequestApplicationCallback([$this, 'itemsRequestApplication'])
                    ->setRelationApplicationCallback([$this, 'itemsRelationApplication'])
                    ->addRowSpecs([
                        $this->createMenuItemRowspec(),
                    ]),
            ]),
        ]);

        return $form;
    }

    /**
     * Drops cached values relating to the given item
     *
     * @param Config  $config
     * @param Request $request
     *
     * @return void
     */
    public function dropMenuCache(Config $config, Request $request): void
    {
        $item = $config->getEditForm()->getModelInstance();

        Cache::forget('enso-menus-' . $item->slug);
        Cache::forget('enso-menu-list');
    }

    /**
     * Applies request data to an individual model instance. Does not save the
     * model instance after updating to preserve any dirtiness states arising
     * from this update.
     *
     * @param Model $parent
     * @param Model $item
     * @param array $data
     * @param int   $index
     *
     * @return Model
     */
    public function itemsRequestApplication(Model $parent, Model $item, array $data, int $index): Model
    {
        try {
            $item->fill(array_merge(
                $this->parseItemData($data),
                [
                    'site_menu_id' => $parent->getKey(),
                    'order' => $index,
                ]
            ));
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new CrudException('Unable to save MenuItems');
        }

        return $item;
    }

    /**
     * Applies new and removed Relationship logic
     *
     * @param Model      $item
     * @param Collection $new_current_relations
     * @param Collection $removed_relations
     *
     * @return Model
     */
    public function itemsRelationApplication(Model $item, Collection $new_current_relations, Collection $removed_relations): Model
    {
        try {
            $new_current_relations->each->save();
            $removed_relations->each->delete();
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new CrudException('Unable to save MenuItems');
        }

        return $item;
    }

    /**
     * Selects a default target based on the url
     *
     * @param string $url
     *
     * @return string
     */
    protected function chooseDefaultTarget(string $url): string
    {
        if (preg_match('#' . static::URL_REGEX . '#', $url)) {
            return '_blank';
        } else {
            return '_self';
        }
    }

    /**
     * Creates a MenuItem rowspec
     *
     * @return FlexibleContentSection
     */
    protected function createMenuItemRowspec(): FlexibleContentSection
    {
        return FlexibleContentSection::make('items')
            ->setLabel('Menu Item')
            ->addFields([
                TextField::make('id')
                    ->addFieldSetClass('is-hidden')
                    ->setDisabled(),
                TextField::make('label')
                    ->addFieldSetClass('is-3'),
                TextField::make('url')
                    ->addFieldSetClass('is-6'),
                SelectField::make('target')
                    ->addFieldSetClass('is-3')
                    ->setLabel('Open in')
                    ->setOptions([
                        '_blank' => 'New Tab',
                        '_self' => 'Current Tab',
                    ]),
            ]);
    }

    /**
     * Parses raw item data into a useable format
     *
     * @param array $data
     *
     * @return array
     */
    protected function parseItemData(array $data): array
    {
        $url =  $this->parseUrl(Arr::get($data, 'url', ''));

        if (!$target = Arr::get($data, 'target.id')) {
            $target = $this->chooseDefaultTarget($data['url']);
        }

        return [
            'label' => Arr::get($data, 'label'),
            'url' => $url,
            'target' => $target,
        ];
    }

    /**
     * Parse user input URLs to strip out local domains, and ensure that relative
     * paths are prefixed with a forward slash to make them relative to the site,
     * not the page they are on.
     *
     * @param string $url
     *
     * @return string
     */
    protected function parseUrl(string $url): string
    {
        preg_match('#(?:https?:\/\/)(.*)#', url('/'), $matches);

        $local_domain = Arr::get($matches, '1', '');

        if (preg_match('#' . static::URL_REGEX . '#', $url)) {
            preg_match('#^(?:(?:https?:)?\/\/)?' . $local_domain . '(.*)#', $url, $matches);

            if (isset($matches[1])) {
                return '/' . trim($matches[1], '/');
            } else {
                return $url;
            }
        } elseif (preg_match('#^[a-zA-Z]+\:#', $url)) {
            return trim($url, ' /');
        } else {
            return '/' . trim($url, '/');
        }
    }
}
