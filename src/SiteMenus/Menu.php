<?php

namespace Yadda\Enso\SiteMenus;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\App;
use Yadda\Enso\Crud\Contracts\IsCrudModel as ContractsIsCrudModel;
use Yadda\Enso\Crud\Traits\IsCrudModel;
use Yadda\Enso\SiteMenus\Contracts\Menu as MenuContract;
use Yadda\Enso\SiteMenus\Contracts\MenuItem;

class Menu extends Model implements MenuContract, ContractsIsCrudModel
{
    use IsCrudModel;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'enso_site_menus';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'title',
        'max_items',
        'max_visible_items',
        'slug',
    ];

    /**
     * Items in this menu
     *
     * @return HasMany
     */
    public function items(): HasMany
    {
        return $this->hasMany(App::make(MenuItem::class), 'site_menu_id')->orderBy('order', 'ASC');
    }
}
